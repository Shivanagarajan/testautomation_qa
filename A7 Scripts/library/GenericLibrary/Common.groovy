package library.GenericLibrary

import java.text.*
import java.util.Date.*

import library.*

class Common 
{
	static Calendar exeStartTime
	static Calendar exeEndTime
	/**
	 * Get the random user id
	 * @return random user id with 'aria' as prefix
	 */
	static def getRandomUserID()
	{
		def useridEtn;
		def userId;
		useridEtn = Math.round(System.currentTimeMillis( )/1000)
		userId = "aria" + useridEtn.toString( );
		Thread.sleep(2000); // sleep for a milli second : to avoid returning same id 
		return userId;
	}
	
	/**
	 * Get the current time in date format
	 * @return current time in date format
 	 */
	static String getCurrentTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss")
		//NVP
		String formattedDate = sdf.format(date);
		//String formattedDate = sdf.format(date.getDateTimeString());
		//NVP
		return formattedDate
	}
	
	static Calendar setStartTime() {
		
		exeStartTime = Calendar.getInstance()
		
	}
	
	static Calendar setEndTime() {
		
		exeEndTime = Calendar.getInstance()
	}
	
	static long totalExecutionTime() {
		
		return getDiffInSeconds(exeStartTime, exeEndTime)
	}
	
	static long getDiffInSeconds(Calendar cal1, Calendar cal2) {
		
		long diff = cal2.getTimeInMillis() - cal1.getTimeInMillis();
		// Calculate difference in hours
		long diffHours = (diff / 1000)
		
		return diffHours;
	}
	
	static String getCurrentTimeInFormat() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY MMMM dd EEE HH:mm:ss")
		//NVP
		String formattedDate = sdf.format(date);
		//String formattedDate = sdf.format(date.getDateTimeString());
		//NVP
		return formattedDate
	}
	
	static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
		//NVP
		String formattedDate = sdf.format(date);
		//String formattedDate = sdf.format(date.getDateTimeString());
		//NVP
		return formattedDate
	}
	
	static boolean isInteger(String str) {
		try
		  {
			double d = Integer.parseInt(str);
		  }
		  catch(NumberFormatException nfe)
		  {
			return false;
		  }
		  return true;
	}
	
	static int findLargest(List numbers){
		
		double largest = numbers.get(0).toString().toDouble()
		for(int i = 0; i < numbers.size(); i++){
			if(numbers.get(i).toString().toDouble() > largest){
				largest = numbers.get(i).toString().toDouble()
			}
		}
		return largest.toInteger()
	}
	
//	static def findLargest(List numbers){
//		
//		def largest = numbers.get(0)
//		for(int i = 0; i < numbers.size(); i++){
//			if(numbers.get(i) > largest){
//				largest = numbers.get(i)
//			}
//		}
//		return largest
//	}
	
	static def findSmallest(List numbers){
		
		def largest = numbers.get(0)
		for(int i = 0; i < numbers.size(); i++){
			if(numbers.get(i) < largest){
				largest = numbers.get(i)
			}
		}
		return largest
	}
	
	static boolean stringReplacementInFile(String fileName, List replacementStr) {
		boolean isSuccessful = false
		try
		{
			File file = new File(fileName);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "", oldtext = "";
			while((line = reader.readLine()) != null)
			{
				oldtext += line + "\r\n";
			}
			reader.close();
			// replace a word in a file
			//String newtext = oldtext.replaceAll("drink", "Love");
			
			//To replace a line in a file
			String newtext 
			
			for(int i=0 ; i<replacementStr.size() ; i++) 
			{
				String[] replace = replacementStr.get(i).split("&");
				//To replace a line in a file
				newtext = oldtext.replaceAll(replace[0], replace[1]);
				oldtext = newtext
			}
			

			FileWriter writer = new FileWriter(fileName);
			writer.write(newtext);
			writer.close();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		return isSuccessful;
	}
}