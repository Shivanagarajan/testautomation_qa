package library.UI

import java.io.File;
import java.lang.reflect.Method
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import java.text.DecimalFormat

import library.ConnectDB;
import library.InputMethods;
import library.LogResult;
import library.UsageVerificationMethods
import library.ReadData







import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.Proxy.ProxyType
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.*
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.Keys

import library.Constants.Constant
import library.Constants.ExpValueConstants
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathAdminToolsApi;
import library.Constants.Constant.RESULT_TYPE;

import org.openqa.selenium.support.PageFactory;

import groovy.util.XmlParser

import org.openqa.selenium.Keys



class StatementTemplates {
	public static WebDriver driver = null;
	public boolean appflag = false;
	Proxy proxy = new Proxy();
	public DesiredCapabilities dc = null;
	String winHandleMain
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	//VerificationMethods vm=new VerificationMethods(db)
	UsageVerificationMethods u=new UsageVerificationMethods(db)
	InputMethods im=new InputMethods();
	
	FirefoxProfile profile=new FirefoxProfile()
	//String uName = "astates"
	//String password = "Arialog@123"
	String uName = "kmani"
	String password = "aspire123"
	String titleStatementPage="Aria A+ Billing Platform - View/Print Invoice/Statement"
	String titleAccountsPage="Aria 6: Accounts"
	By txtUsername=By.id("username")
	By txtPassword=By.id("password")
	By btnLogin =By.xpath("//*[@id='f']/input[6]")

	By ddClient = By.xpath ("//*[@id='clientSelector']")



	By tabAdhocSrch = By.xpath("//li[@class='subnav']//a//span[text()='Ad-Hoc Search']")
	By tabAccounts = By.xpath ("//div[@role='tablist']//h3//a//div[text()='Accounts']")
	By tabAcctOnClick = By.xpath("//h3[@id='ui-accordion-1-header-2']")
	By tabAcctSrch = By.xpath("//ul[@id='ui-accordion-1-panel-2']/li[1]/a/div")
	By tabSrchOnClk = By.xpath("//ul[@id='ui-accordion-1-panel-2']/li[1]")
	By acctOverview=By.xpath("id('tax_group_header')")

	By linkLogout = By.xpath ( "//li[@class='log_out']/a[text()='Log Out']")
	By contentHeader = By.xpath ( "//div//h2")
	By txtSrch = By.xpath("id('quicksearch')")
	By btnSearch = By.xpath("//input[@value='Search']")
	By linkAcctNo = By.xpath("//table[@id='adhoc_search_results']/tbody/tr[2]/td[1]/a")

	By linkRecentInvoices=By.xpath("//a[@class='doAccountsPanel' and text()='Recent Invoices']")
	By tabInvoices=By.xpath("//a[@class='accountBottomTab' and text()='Invoices']")
	By tabPendingInvoices = By.xpath("//a[@class='accountBottomTab' and text()='Pending Invoices']")
	By linkInvoiceno=By.xpath("//table[contains(@id,'DataTables_Table_')]//a")
	By linkviewPrintStatement =By.xpath("//*[@id='page-tools']//span[text()='View & Print']")
	By btnPrint=By.xpath("//input[@type='button']")

	By linkPendingInvoiceno = By.xpath("//table[contains(@id,'DataTables_Table_')]//td[3]/a")
	By linkApprove = By.linkText("Approve/View");
	By linkApprovelink = By.xpath("//ul[@id='page-tools']/li[1]/a/span");
	By linkApproveSubmit = By.xpath("//*[@id='accountsSectionBottomContainer']/div/div/form/p/input")
	By linkInvoiceTab = By.linkText("Invoices");
	By divPendingInvoiceError = By.xpath("//div[@ class = 'error-box']/p[contains(text(),'Invoice number')]")

	By tbAdress=By.xpath("//*[@id='content']/table[1]/tbody/tr/td[1]")
	By st_AccountNumber=By.xpath("//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[1]/td")
	By st_InvoiceNumber=By.xpath("//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[4]/td")
	By st_StatementDate=By.xpath("//div[@id='content']/table[1]/tbody/tr/td[3]/table/tbody/tr[2]/td")

	//204
	By tabItemList_214=By.xpath("//table[@id='customer-detail']//tr")
	By tabPriceList_214=By.xpath("//table[@id='customer-detail']//td[string-length()>1][2]")
	By tabCreditActivityItemList=By.xpath("//div[@id='content']/table//tr//td[contains(text(),'(applied')]")
	//By tabCreditActivityItemList=By.xpath("//div[@id='content']/table//tr//td[contains(text(),'(applied')]")

	//By tabStatementTemplate=By.xpath("//li[@class='ui-state-default ui-corner-top']//a[@class='accountBottomTab' and text()='Statement Template']")
	By tabStatementTemplate=By.linkText("Statement Template")

	//Credit note
	By creditNote=By.xpath("//div[@id='content-wrapper']/table[7]/tbody/tr[2]/td[contains(text(),'Credit Note')]")
	By lnkCreditNote=By.xpath("//div[@id='content-wrapper']/table[7]/tbody/tr[2]/td[3]/a")
	By buttonResendMessage=By.xpath("//div[@id='accountsSectionBottomContainer']/div/div/p[2]/input")
	By barresize=By.xpath("//div/div[@id='nav_resize']/a")

	By linkRecentStatements=By.xpath("//a[@class='doAccountsPanel' and text()='Recent Statements']")
	By linkViewAllInvoices=By.xpath("//a [text()='View All Invoices ']")
	By tabStatements=By.xpath("//a[@class='accountBottomTab' and text()='Statements']")
	By previewNxtStmt=By.xpath("//a[@class='doAccountsPanel' and span[text()='Preview Next Statement']]")

	By leftnavigationinvoice=By.xpath("//div[text()='Statements & Invoices']")
	By tabInvoice=By.xpath("//*[@id='bottomPaneTabBar']/ul/li/a[text()='Invoices']")
	
	public StatementTemplates() {
		logi.logInfo("Inside statement template constructor and driver value : " +driver);
		if(driver == null)
		{
			//profile.setPreference("network.proxy.type", ProxyType.AUTODETECT.ordinal());
			proxy.setNoProxy("*.ariasystems.net")
			profile.setAcceptUntrustedCertificates(true)
			profile.setProxyPreferences(proxy)
			dc = DesiredCapabilities.firefox();
			dc.setCapability(FirefoxDriver.PROFILE, profile);
			dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			dc.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
			logi.logInfo("webdriver instance is null");
			try {
				logi.logInfo("Inside StatementTemplates Const")
				driver = new FirefoxDriver(dc)
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(60,TimeUnit.SECONDS);
				Set<String> windows = driver.getWindowHandles()
				logi.logInfo("Active windows count in StatementTemplates constructor "+windows.size())
			}

			catch(Exception e) {
				logi.logInfo("Method Exception: "+e.printStackTrace());
			}
		} else
		{
			logi.logInfo("Already a browser instance is opened and is active");
		}
	}


	public boolean launchApplication() {
		boolean success = false;

		try {
			if(!appflag)
			{
				logi.logInfo("Going to launch Application");
				driver.get("https://secure.unstable.qa.ariasystems.net/ui/app.php")
				driver.manage().window().maximize()
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS)
				success = true;
				appflag=true
			}
			else if(driver.findElement(linkLogout).isDisplayed() ) {
				logi.logInfo("Application launched successfully");
				success = true;
			}
			
		}
		catch(Exception e) {
			logi.logInfo("launchApplication Method Exception: "+e.printStackTrace());
		}

		return success;
	}

	public boolean login() {
		boolean succ = false;
		logi.logInfo "reloading page..."
		logi.logInfo("appflag value "+appflag)
		try {
			if(launchApplication()) {
				if(driver.getCurrentUrl().contains("login-aria.php")==false)
				{
					logi.logInfo("Already logged in ");
					succ = true;
				}
				else
				if(driver.findElement(btnLogin).isDisplayed())
				{
					logi.logInfo("Going to login")
					driver.findElement(txtUsername).sendKeys(uName)
					driver.findElement(txtPassword).sendKeys(password)
					driver.findElement(btnLogin).click();
					waitForTheElement(tabAccounts)
					if(driver.findElement(tabAccounts).isDisplayed())
					{
						logi.logInfo("Logged into application successfully");
						succ = true;
					}
				}
			}
			else
			{
				logi.logInfo("login else part ");
			}
		}catch(Exception e) {
			logi.logInfo("Method Exception: "+e.printStackTrace());
			if(driver.findElement(btnLogin).isDisplayed())
			{
				logi.logInfo("Going to login")
				driver.findElement(txtUsername).sendKeys(uName)
				driver.findElement(txtPassword).sendKeys(password)
				driver.findElement(btnLogin).click();
				waitForTheElement(tabAccounts)
				if(driver.findElement(tabAccounts).isDisplayed())
				{
					logi.logInfo("Logged into application successfully");
					succ = true;
				}
			}
		}


		return succ
	}
	public boolean selectClient( ) {
		logi.logInfo("Calling selectClient");
		try {
			boolean flg = false;
			String selectedClient;
			ConnectDB db = new ConnectDB()
			String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			logi.logInfo("Going to selectClient" +clientNo);
			
			String baseUrl = "https://secure.unstable.qa.ariasystems.net/ui/app.php"
					String[] selectClient1 = baseUrl.split("/ui")
					String selectClientURL = selectClient1[0]+"/ui/app.php/Client/change/"+clientNo
					
					String xpath="//select[@id='clientSelector']/option[@value='"+clientNo+"']"
					String selected_xpath="//select[@id='clientSelector']/option[@selected='selected']"
					logi.logInfo("Framed xpath "+xpath)
					//waitForTheElement(By.xpath(selected_xpath))
					selectedClient = driver.findElement(By.xpath(selected_xpath)).getAttribute("value").toString()
					logi.logInfo("selectedClient "+selectedClient)
					if(clientNo.equals(selectedClient)) {
						logi.logInfo("Client already selected");
						flg = true;
					}
					else {
						//driver.findElement(By.xpath(xpath)).click();
						driver.get(selectClientURL);
						driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
						flg=true
					}
			
			return flg;
		} catch (Exception e) {
			e.printStackTrace()
			captureShot("Errro in selectClient"+Constant.TESTCASEID)
			
		}
	}



	public boolean invokeUIMethod(String methodname,String param)
	
	{
		logi.logInfo "methodname "+methodname.toString()
		boolean r
		try
		{
			Class[] parametersTypes = new Class[1]
			parametersTypes[0] = String.class;
			Object[] parameters = new Object[1]
			parameters[0]=param
			Method m= this.getClass().getDeclaredMethod(methodname,parametersTypes)
			r=m.invoke(this,parameters)
			logi.logInfo "Result from method "+r.toString()
		}catch(e)
		{

			logi.logInfo  e
		}
		return r
	}
	public boolean invokeUIMethod(String methodname)
	{
		logi.logInfo "Calling invokeUIMethod for method" +methodname		
		
		boolean r=this."$methodname"()
		logi.logInfo "Result from method "+r.toString()
		return r
	}

	public void quitDriver()
	{
		try
		{
			//driver.close()
			driver.quit()
		}
		catch(Exception e)
		{
			logi.logInfo("quitDriver Method Exception: "+e.printStackTrace());
		}

	}
	public String getValueFromUI(String xpath){

		String splitted_xpath = null;
		splitted_xpath = xpath.split('#')[1]
		String value
		logi.logInfo("Splitted xpath is : "+splitted_xpath)

		try {
			value = driver.findElement(By.xpath(splitted_xpath)).getText()

			logi.logInfo("Got Value is  : "+value)

		} catch (Exception e) {
			e.printStackTrace()
			value ="NO VALUE"

		}

		return value
	}

	public boolean accountSearch(String s)
	{
		boolean success = false;
		try {
			waitForTheElement(tabAccounts)
			//Store the current window handle
			winHandleMain = driver.getWindowHandle();
			logi.logInfo( "TESTCASEID ON SEARCH "+Constant.TESTCASEID)
			if(driver.findElement(tabAccounts).isDisplayed())
			{
				String isAcctTabSelected = driver.findElement(tabAcctOnClick).getAttribute("aria-selected")
				logi.logInfo("isAcctTabSelected "+isAcctTabSelected)
				if(isAcctTabSelected.equalsIgnoreCase("false"))
				{
					logi.logInfo(" Going to select accounts tab")
					driver.findElement(tabAccounts).click();
					logi.logInfo("Selected accounts tab")
				}
				else
				{
					logi.logInfo("Accounts tab already selected")
				}
				if(driver.findElement(tabAcctOnClick).getAttribute("aria-selected").equalsIgnoreCase("true"))
				{
					logi.logInfo("Accounts tab expanded")
					logi.logInfo "acct search "+driver.findElement(tabAcctSrch).isDisplayed()
					//waitForTheElement(tabAcctSrch)
					String isSrchExpanded = driver.findElement(tabSrchOnClk).getAttribute("class")
					logi.logInfo "isSrchExpanded "+isSrchExpanded
					if(isSrchExpanded.trim().equals("") )
					{
						logi.logInfo("Going to click search option")
						driver.findElement(tabAcctSrch).click();
						logi.logInfo("Clicked search option")
					}
					/*if(driver.findElement(txtSrch).isDisplayed())
					 {
					 logi.logInfo("search text box is aviailable")
					 boolean flg_search=search()
					 if(flg_search)
					 success=true
					 }*/
					else if(driver.findElement(tabSrchOnClk).getAttribute("class").equalsIgnoreCase("active_subnav"))
					{
						logi.logInfo "Already search option enabled"
					}
					waitForTheElement(tabAdhocSrch)
					driver.findElement(tabAdhocSrch).click();
					Thread.sleep(6000)
					String acct_no= im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
					boolean flg_search=search(acct_no)
					if(flg_search)
						success=true
				}
				else
				{
					logi.logInfo("acct search else part")
				}
			}
			return success
		} catch (Exception e) {
			e.printStackTrace()
			captureShot("Errro in accountSearch_"+Constant.TESTCASEID)
		}
	}
	
	public boolean search(String acct_no)
	{
		logi.logInfo( "calling search for acct "+acct_no)

		boolean flg_search=false
		try {
			waitForTheElement(contentHeader)
			String header  = driver.findElement(contentHeader).getText()
			if(header.equals("Ad-Hoc Search"))
			{
				logi.logInfo("Adhoc search page opened in the right panel")
				logi.logInfo("search text box displayed :" +driver.findElement(txtSrch).isDisplayed().toString())
				if(driver.findElement(txtSrch).isDisplayed())
				{
					Thread.sleep(2000)

					//driver.findElement(txtSrch).sendKeys("10699746");
					driver.findElement(txtSrch).sendKeys(acct_no);
					driver.findElement(txtSrch).sendKeys(Keys.RETURN);
					
				}
				else
				{
					logi.logInfo("Search else part")
				}
			}
		} catch (Exception  e) {
			logi.logInfo ("SearchMethodEXception"+e.printStackTrace())
			flg_search = false;
		}
	}
	public boolean isAcctTabEnabled(String tabName)
	{
		boolean flg = false
		String acctTabs = "//*[@id='bottomPaneTabBar']//li[contains(@class,'ui-tabs-selected')]//a[text()='#tab#']"
		logi.logInfo("Tab to be enabled: "+tabName)
		acctTabs = acctTabs.replaceAll("#tab#", tabName)
		logi.logInfo(" After replacement: "+acctTabs)
		if(driver.findElement(By.xpath(acctTabs)).isDisplayed())
		{
			flg = true;
		}

		return flg;
	}


	public boolean gotoInvoiceFromRecentInvoices()
	{
		boolean flg = false
		logi.logInfo("Calling gotoRecentInvoice")
		try
		{
			driver.findElement(linkRecentInvoices).click()
			logi.logInfo(" Clicked Recent invoces tab")
			waitForTheElement(tabInvoices)
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			//String invoiceNo ="14841587"
			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
				driver.findElement(linkInvoiceno).click();
			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = true;

		}


		return flg;



	}

	public boolean viewCurrentStatementFromInvoiceTab(String acct_no)
	{
		boolean flg = false
		logi.logInfo("Calling viewCurrentStatementFromInvoiceTab")
		try
		{


			String winHandleBefore = driver.getWindowHandle();
			String origTitle=driver.title
			logi.logInfo("Window title" +origTitle)
			waitForTheElement(linkviewPrintStatement)
			driver.findElement(linkviewPrintStatement).click()
			Set<String> windows = driver.getWindowHandles()
			logi.logInfo("Active windows count "+windows.size())

			//"Aria A+ Billing Platform - View/Print Invoice/Statement"
			for (String w : windows) {
				if (!driver.switchTo().window(w).getTitle().equals(origTitle))
				{
					driver.switchTo().window(w);
					driver.manage().window().maximize();
					logi.logInfo("Window title now" +driver.title)
					WebDriverWait wait = new WebDriverWait(driver,30);
					
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("msgToolFrame"));
					//ExpectedConditions.frameToBeAvailableAndSwitchToIt("msgToolFrame")
					logi.logInfo("Frame value" +driver.findElement(btnPrint).getAttribute("value"))

				}
			}
			//waitForTheElement(btnPrint)
			if(driver.findElement(btnPrint).isDisplayed())
			{
				logi.logInfo("Window title now now" +driver.title)
				driver.switchTo().defaultContent()
				logi.logInfo("Window title now now now" +driver.title)
				//WebDriverWait wait1 = new WebDriverWait(driver,30);
				//wait1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("msgBodyFrame"));
				logi.logInfo(driver.findElements(By.tagName("frame")).size().toString())
				//ExpectedConditions.frameToBeAvailableAndSwitchToIt("msgBodyFrame")
				driver.switchTo().frame(1);

				//String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
				//String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
				//driver.get("https://secure.unstable.qa.ariasystems.net/ui/mod_plsql.php/aria/csrtools/dashboard_invoice.show_inv_msg_wait?inInvoiceNo="+invoiceNo)
				Thread.sleep(2000)
				//WebElement divInsideFrame=driver.findElement(By.xpath("//div[@id='content']"))
				//waitForTheElement(divInsideFrame)
				logi.logInfo("Switched to Frame2")
				
				//JavascriptExecutor js = (JavascriptExecutor) driver;
				//js.executeScript("window.frames['msgBodyFrame'].document.getElementById('content').scrollIntoView());")

				flg = true;
			}
		}catch (Exception e)
		{
			logi.logInfo("FrameException : "+e.getMessage());
			flg = false;
		}

		return flg;

	}

	public  waitForTheElement(By by){

		try {

			for(int i=0;i<90;i++)
			{
				logi.logInfo("Waiting for element "+by)
				Thread.sleep(2000)
				if(driver.findElement(by).isDisplayed())
					break
			}
		} catch (Exception e) {
			logi.logInfo("Element not found "+by)
		}


	}
	public void captureShot(String fileName)
	{
		String screenshotPath = Constant.logFilePath
		logi.logInfo("screenshot has to be saved at :"+screenshotPath)
		File ss = new File(screenshotPath+"//"+fileName+".png")
		try
		{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			//js.executeScript("window.scrollTo( document.body.scrollWidth,0)");
			File capture = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(capture,new File(ss.absolutePath));

		}
		catch(Exception ex)
		{
			logi.logInfo("ScreenshotException : "+ex.getMessage());
			ex.printStackTrace();
		}

	}

	public LinkedHashMap uv_getBillingAdressLineItem_ST(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_ST")
		String addr_line_items="//*[@id='content']/table[1]/tbody/tr/td[1]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		logi.logInfo"Plaintext"
		logi.logInfo txt
		String txt1=txt.split("(?i)Bill to:")[1]
		logi.logInfo"Splittedtext"
		logi.logInfo txt1
		txt1=txt1.replace("</strong>", "")
		txt1=txt1.replace("<br><br>","")
		txt1=txt1.replace("\n", "").replace("\r", "")
		txt1=txt1.replace("</p>","")
		txt1= txt1.trim()
		txt1.replaceAll("","")
		logi.logInfo"****************"
		logi.logInfo txt1
		LinkedHashMap<String,String> addrDetails_exp=new LinkedHashMap<String,String>()
		List <String> addrLines=txt1.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		logi.logInfo "addrLines list  size"+addrLines.size().toString()
		List <String> finaladdrLines=[]
		addrLines.each  {line ->
			logi.logInfo "line--"+line
			if(!line.trim().equals(""))
			{
				logi.logInfo("adding to new loop")
				finaladdrLines.add(line.trim().toString())
				//logi.logInfo "finaladdrLines list in if111 "+finaladdrLines.toString()

			}
			else
			{
				logi.logInfo "elseline--"+line
			}
		}
		//logi.logInfo "finaladdrLines list "+finaladdrLines.toString()
		int max=finaladdrLines.size()
		//logi.logInfo "finaladdrLines list  size"+max.toString()
		int counter=6
		String val
		/*for (int a=0;a<=addrLines.size()-1;a++)
		 {
		 logi.logInfo a+" =>  "+addrLines.get(a)
		 }*/
		int b=max-1
		while(b>=0)
		{
			counter=counter-1
			String key="ADDR_LINE"+counter.toString()
			//logi.logInfo "key "+key
			if(!finaladdrLines.get(b).trim().equals(""))
			{
				val=finaladdrLines.get(b).trim()
			}
			//logi.logInfo "val "+val
			addrDetails_exp.put(key,val)
			if(b==0)
				break
			else
				b--
		}
		logi.logInfo addrDetails_exp.toString()
		return addrDetails_exp.sort()
	}

	public LinkedHashMap uv_stmt_getBillingAdressLineItem_DB(String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getBillingAdressLineItem_DB")

		String query="select b.company_name as ADDR_LINE1 ,b.address1 as ADDR_LINE2,b.address2 as ADDR_LINE3, case when ( b.city is not null and b.state is not null and b.zip is not null) then (b.city||','||' '||b.state|| chr(38)||'nbsp;' ||chr(38)||'nbsp;'||b.zip) "+
				"else '' end as ADDR_LINE4,c.country_english as ADDR_LINE5 from ariacore.current_billing_info b join ariacore.all_client_country c on c.client_no=b.client_no and b.country=c.country where b.acct_no="+acct_no
		LinkedHashMap<String,String> addrDetails=new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		//logi.logInfo "columns count"+columns
		String val
		while (resultSet.next()){

			for(int i=1; i<=columns; i++){
				String tt= resultSet.getString(i)
				if((tt!=null) && (!tt.trim().equals("")) )
				{
					addrDetails.put(md.getColumnName(i).toString(),tt.trim().toString());
				}
				else
				{
					logi.logInfo("Unabel to add this item "+tt)
				}

			}

		}
		logi.logInfo addrDetails.toString()

		return addrDetails
	}
	public LinkedHashMap uv_stmt_getBillingAdressLineItem_DB_214 (String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getBillingAdressLineItem_DB_214")

		String query="select (b.first_name || ' '|| b.last_name) as ADDR_LINE0 ,(b.company_name) as ADDR_LINE1 ,b.address1 as ADDR_LINE2,b.address2 as ADDR_LINE3, case when ( b.city is not null and b.state is not null and b.zip is not null) then (b.city||','||' '||b.state|| chr(38)||'nbsp;' ||chr(38)||'nbsp;'||b.zip) "+
				"else '' end as ADDR_LINE4,c.country_english as ADDR_LINE5 from ariacore.current_billing_info b join ariacore.all_client_country c on c.client_no=b.client_no and b.country=c.country where b.acct_no="+acct_no
		LinkedHashMap<String,String> addrDetails=new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		//logi.logInfo "columns count"+columns
		String val
		while (resultSet.next()){

			for(int i=1; i<=columns; i++){
				String tt= resultSet.getString(i)
				if((tt!=null) && (!tt.trim().equals("")) )
				{
					addrDetails.put(md.getColumnName(i).toString(),tt.trim().toString());
				}
				else
				{
					logi.logInfo("Unabel to add this item "+tt)
				}

			}

		}
		logi.logInfo addrDetails.toString()

		return addrDetails
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_ST(String tcid){

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_ST")

		List keyValues = [
			"PLAN_NAME",
			"STMT_DATE",
			"UNITS",
			"RATE",
			"AMOUNT"
		];
		int counter=0
		int kcounter=0
		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()

		List<WebElement> items = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[@class='lineitem']]"))

		items.each  { item ->
			logi.logInfo("Preparing Item Hash")
			counter=counter+1
			LinkedHashMap internalHM = new LinkedHashMap<String,String>()
			List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
			logi.logInfo("tds count : "+tds.size().toString())
			tds.each {titem ->
				logi.logInfo("td value "+titem.getText())
				internalHM.put(keyValues.get(kcounter), titem.getText())
				kcounter=kcounter+1

			}
			kcounter=0
			internalHM=internalHM.sort()
			finalMap.put("Line Item"+counter,internalHM)

		}
		return finalMap
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB(tcid,accountNo)
	}
	public LinkedHashMap uv_getChildInvoiceLineItemsFromStatements_DB(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB(tcid,accountNo)
	}

	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatements_DB(String tcid,String accountNo){

		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatements_DB")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		LinkedHashMap hm = new LinkedHashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		double tempUsageRate = 0.0
		int recRateScale = 0
		int usgRateScale = 0
		int tempUsageRate1=0
		String orderlabelQuery="select alt_label  from ariacore.order_items  where item_no= (select item_no from ariacore.all_invoice_details where invoice_no= %s and seq_num= %s) and order_no=(select order_no from ariacore.all_invoice_details where invoice_no = %s and seq_num  = %s)and client_no=%s"
		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select count(seq_Num) as COUNT from ariacore.gl_detail where invoice_no = %s"
		String usageRateQry = "select NVL(trim(to_char(usage_rate,'99,990.99')),0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL THEN TRIM(TO_CHAR(a.comments))   WHEN a.is_order_based=1     then '%s' ELSE TRIM(TO_CHAR(a.plan_name)) || ' ' || a.comments  END ) as Plan_name, TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Stmt_Date, (CASE  WHEN a.usage_rate =0 and a.service_no =0 THEN '0.00' WHEN a.usage_rate is null THEN '0.00' WHEN a.recurring=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end) WHEN a.usage_based=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end)  WHEN a.is_setup=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) when a.is_tax = 1 then '0.00' END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, nvl(aid.usage_rate,0) as usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee,ser.is_order_based,ser.is_tax,ser.is_surcharge, aid.orig_coupon_cd,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String generalQry = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL THEN TRIM(TO_CHAR(a.comments))  WHEN a.is_order_based=1 and a.orig_credit_id is null THEN '%s'  WHEN a.is_order_based=1 and a.orig_credit_id is not null  THEN 'Credit - '||a.client_service_id when (a.orig_credit_id IS NOT NULL and a.service_no !=0  and   a.plan_name is not null )  THEN 'Credit - ' ||  TRIM(TO_CHAR(a.plan_name))|| ', '|| a.client_service_id  WHEN (a.orig_credit_id IS NOT NULL    AND a.service_no       !=0 and   a.plan_name is  null )     THEN 'Credit - '      || a.client_service_id  ELSE TRIM(TO_CHAR(a.plan_name)) || ' ' || a.comments  END ) as Plan_name, TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Stmt_Date, (CASE  WHEN a.usage_rate =0 and a.service_no =0 THEN '0.00' WHEN a.usage_rate is null THEN '0.00' WHEN a.recurring=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end) WHEN a.usage_based=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end)  WHEN a.is_setup=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) when a.is_tax = 1 then '0.00' END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, nvl(aid.usage_rate,0) as usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee,ser.is_order_based,ser.is_tax,ser.is_surcharge,ser.client_service_id, aid.orig_coupon_cd, aid.orig_credit_id,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)


		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo))
		logi.logInfo("Seq num is :: "+seqCount)

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			ResultSet rs = db.executePlaQuery(String.format(usageRateQry,invoiceNo,row));
			while(rs.next()) {
				tempUsageRate = rs.getDouble(1)

			}

			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				if(tempUsageRate.toString().contains("."))
				{
					String txt=tempUsageRate.toString().split("\\.")[0]
					tempUsageRate1=txt.length()
					logi.logInfo("tempUsageRate1 is ::: "+tempUsageRate1)
					recRateScale =Integer.parseInt(recuringRateScale)+tempUsageRate1+1
					logi.logInfo("recRateScale is ::: "+recRateScale)
					usgRateScale = tempUsageRate1+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)

				}else
				{
					recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				}

			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}
			String olable= db.executeQueryP2(String.format(orderlabelQuery,invoiceNo,row,invoiceNo,row,clientNo))
			hm = db.executeQueryP2(String.format(generalQry,olable,recRateScale,recRateScale,usgRateScale,usgRateScale,invoiceNo,row,clientNo))
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			hm=hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;

		}
		temp++
		// voidedTransactions hash

		int noOfvoidedPayments = 0
		LinkedHashMap voidedpHash = new LinkedHashMap<String,String>()
		String loopCountQry = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-6,-3,-2,-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop = "SELECT case when a.transtype=-1 then  a.description || ' '|| a.srcno   || ' (applied ' || TO_CHAR(create_date,'DD-MM-YYYY') || ')' else a.description || ' (applied ' || TO_CHAR(create_date,'DD-MM-YYYY')  || ')' end as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date,ATRANS.TRANSACTION_TYPE as transtype,ATRANS.source_no as srcno,row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-3,-6,-2,-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfvoidedPayments = db.executeQueryP2(String.format(loopCountQry,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in voided transactions : "+noOfvoidedPayments)
		for(int pay = 1;pay <=noOfvoidedPayments; pay++){

			voidedpHash = db.executeQueryP2(String.format(addnlLoop,accountNo,accountNo,pay))
			voidedpHash.put("UNITS", " ")
			voidedpHash.put("RATE", " ")
			voidedpHash.put("STMT_DATE", " ")
			logi.logInfo("The voided related hash is : "+voidedpHash.toString())

			//pHash.put("Line Item"+temp,"")
			if(voidFlag=='TRUE')
			{
				logi.logInfo("voidflag is true")
				voidedpHash=voidedpHash.sort()
				mDB.put("Line Item"+temp,voidedpHash)
				temp++
			}
		}

		//balance forward
		int noOfBalTrans = 0
		
		int noOfBalto = db.executeQueryP2("Select count(*) from ariacore.acct_transaction where acct_no="+accountNo+ " and transaction_type = 4 AND statement_no=(select max(statement_no) from ariacore.acct_statement where acct_no="+accountNo+ " )" ).toString().toInteger()
		int noOfBalfrom = db.executeQueryP2("Select count(*) from ariacore.acct_transaction where acct_no="+accountNo+ " and transaction_type = 5 and  statement_no=(select max(statement_no) from ariacore.acct_statement where acct_no="+accountNo+")" ).toString().toInteger()
		logi.logInfo("Total Balance TO Transfers: "+noOfBalto)
		logi.logInfo("Total Balance From Transfers: "+noOfBalfrom)
		String bttoDesc = db.executeQueryP2("SELECT distinct * FROM  (SELECT 'Balance transfer from acct '|| BT.from_acct_no  || ' (User ID ' || ac.userid || ')' AS PLAN_NAME  FROM ARIACORE.balance_transfer BT  JOIN ariacore.acct ac  ON BT.from_acct_no  = ac.acct_no  WHERE from_Acct_no IN    (SELECT from_acct_no    FROM ARIACORE.balance_transfer WHERE to_Acct_no = "+accountNo+") ORDER BY create_date DESC  ) ")
		String btfromDesc =  db.executeQueryP2("SELECT distinct  * FROM  (SELECT 'Balance transfer to acct '|| BT.to_acct_no  || ' (User ID ' || ac.userid || ')' AS PLAN_NAME  FROM ARIACORE.balance_transfer BT  JOIN ariacore.acct ac  ON BT.to_acct_no  = ac.acct_no  WHERE to_Acct_no IN (SELECT to_acct_no FROM ARIACORE.balance_transfer WHERE from_Acct_no = "+accountNo+") ORDER BY create_date DESC  )")
		String FrmamtQuery=" select AMOUNT from ( SELECT case when transaction_type = 5 then '-' || TRIM(TO_CHAR(amount,'99,990.99'))  else TRIM(TO_CHAR(amount,'99,990.99')) end AS AMOUNT,row_number() over ( ORDER BY source_no  ) as sno  FROM ariacore.acct_transaction   WHERE acct_no  = %s   AND transaction_type = %s  ) where sno=%s "
		
		for(int bt=1;bt<=noOfBalfrom;bt++)
		{
			logi.logInfo("Total Balance FROM LOOP: "+bt)
			LinkedHashMap balFromHash = new LinkedHashMap<String,String>()
			balFromHash.put("UNITS", " ")
			balFromHash.put("RATE", "0.00")
			balFromHash.put("STMT_DATE", "-")
			balFromHash.put("PLAN_NAME",btfromDesc)
			String amt = db.executeQueryP2(String.format(FrmamtQuery,accountNo,'5',bt))
			balFromHash.put("AMOUNT",amt)
			logi.logInfo("The payment related hash is : "+balFromHash.toString())
			balFromHash.each {k,v ->
				String v1= v.toString().trim()
				balFromHash.put(k,v1)
			}
			balFromHash=balFromHash.sort()
			mDB.put("Line Item"+temp,balFromHash)
			temp++
		}
		
		for(int bt=1;bt<=noOfBalto;bt++)
		{
			logi.logInfo("Total Balance TO LOOP: "+bt)
			LinkedHashMap balHash = new LinkedHashMap<String,String>()
			balHash.put("UNITS", " ")
			balHash.put("RATE", "0.00")
			balHash.put("STMT_DATE", "-")
			balHash.put("PLAN_NAME",bttoDesc)
			String amt = db.executeQueryP2(String.format(FrmamtQuery,accountNo,'4',bt))
			balHash.put("AMOUNT",amt)
			logi.logInfo("The payment related hash is : "+balHash.toString())
			balHash.each {k,v ->
				String v1= v.toString().trim()
				balHash.put(k,v1)
			}
			balHash=balHash.sort()
			mDB.put("Line Item"+temp,balHash)
			temp++
		}

		
		


		// Transactions hash


		int noOfPayments = 0
		LinkedHashMap pHash = new LinkedHashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,6,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,( case when ATRANS.TRANSACTION_TYPE=13 then TRIM(TO_CHAR((ATRANS.amount),'99,990.99')) else '-'  || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')) end) AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,6,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			pHash.put("UNITS", " ")
			pHash.put("RATE", " ")
			pHash.put("STMT_DATE", " ")
			logi.logInfo("The payment related hash is : "+pHash.toString())
			pHash=pHash.sort()
			mDB.put("Line Item"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())
		String deltaFlag=db.executeQueryP2("select param_val from ariacore.aria_client_params where client_no="+clientNo+"  and param_name ='DISPLAY_DELTA_VALUES_FOR_PRORATION'")
		if(deltaFlag=="TRUE")
			mDB.each{k,v->
				v.put("RATE", "")

			}
		logi.logInfo("Final hash after delta flag : "+mDB.toString())
		return mDB;
	}


	public boolean close()
	{
		logi.logInfo("Calling close")
		boolean flg=false
		logi.logInfo("title =>"+ driver.title)


		try {

			if( driver.title.equals(titleStatementPage)) {
				logi.logInfo("in template window")
				driver.close();
				driver.switchTo().window(winHandleMain)
				Thread.sleep(1000)
				logi.logInfo("title after switch  =>"+ driver.title)
			}
			if(driver.title.contains("Accounts"))
			{
				logi.logInfo("Back to org window")
				flg=true
			}
			else
			{
				logi.logInfo("in template window with other title")
				driver.close();				
				driver.quit();
				
				driver.switchTo().window(winHandleMain)
				
				Thread.sleep(1000)
				logi.logInfo("title after switch  =>"+ driver.title)
			}

		} catch (Exception e) {
			logi.logInfo e.printStackTrace()
		}
		return flg
	}
	public String uv_Remit_ClientName_106(String tcid)
	{
		logi.logInfo("Calling uv_Remit_ClientName_106")
		String addr_line_items="//*[@id='content']/table[1]/tbody/tr/td[1]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		txt=txt.replace("<strong>Bill To:</strong>", "")
		txt=txt.replace("<strong>Remit Payment To:</strong>", "")
		txt=txt.replace("\n", "").replace("\r", "")
		txt= txt.trim()
		txt.replaceAll("","")
		logi.logInfo"****************"
		logi.logInfo txt
		List <String> addrLines=txt.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		String remitClient = addrLines.get(1).trim()
		logi.logInfo("remitClient:" +remitClient)

		return remitClient
	}

	public String uv_ClientCsrPhone_106(String tcid)
	{
		logi.logInfo("Calling uv_ClientCsrPhone_106")
		String phone="//*[@id='content']/p[3]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(phone)).getAttribute("innerHTML")
		List <String> addrLines=txt.split("<br>")
		logi.logInfo "Phone list: "+addrLines.toString()
		String phoneNo = addrLines.get(1).trim()
		logi.logInfo("Phoneno:" +phoneNo)

		return phoneNo
	}

	public String getAttributeValueFromUI(String xpath){

		String splitted_xpath = null;
		String splitted_attri = xpath.split('#')[0]
		splitted_xpath = xpath.split('#')[1]
		String value
		logi.logInfo("Splitted xpath is : "+splitted_xpath)
		logi.logInfo("Ssplitted_attri is : "+splitted_attri)
		String attrName = splitted_attri.split("attr-")[1]
		logi.logInfo("Attribute Name : "+attrName)
		try {
			value = driver.findElement(By.xpath(splitted_xpath)).getAttribute(attrName)
			logi.logInfo("Got Value is  : "+value)

		} catch (Exception e) {
			e.printStackTrace()
			value ="NO VALUE"

		}

		return value
	}

	public String uv_STMT_PAYMENTS_RECEIVED_208_TEMP(String acct_no)
	{
		logi.logInfo("Calling uv_STMT_PAYMENTS_RECEIVED_208_TEMP")
		ConnectDB db = new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String stmt_no = db.executeQueryP2("Select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String pay_amt = db.executeQueryP2("SELECT TRIM(TO_CHAR(NVL(SUM(pymt_total),0),'99,990.99')) from ariacore.acct_stmt_sum where statement_no="+stmt_no)
		return pay_amt
	}

	public LinkedHashMap uv_getBillingAdressLineItem_ST_208(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_ST_208")
		String addr_line_items="//*[@id='content']/table[1]/tbody/tr/td[1]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		txt=txt.replace("<strong>Bill To:</strong>", "")
		txt=txt.replace("\n", "").replace("\r", "")
		txt= txt.trim()
		txt.replaceAll("","")
		logi.logInfo"****************"
		logi.logInfo txt
		LinkedHashMap<String,String> addrDetails_exp=new LinkedHashMap<String,String>()
		List <String> addrLines=txt.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		int max=addrLines.size()
		logi.logInfo "addrLines list  size"+max.toString()
		int counter=0
		String val
		/*for (int a=0;a<=addrLines.size()-1;a++)
		 {
		 logi.logInfo a+" =>  "+addrLines.get(a)
		 }*/
		for(int i=0;i<max;i++)
		{
			logi.logInfo("iteration: "+i+" value: "+addrLines.get(i).toString())
		}
		for (int b=6;b<=max-1;b++)
		{
			counter=counter+1
			String key="ADDR_LINE"+counter.toString()
			//logi.logInfo "key "+key
			if(addrLines.get(b).trim().equals(""))
				val="NO VALUE"
			else
				val=addrLines.get(b).toString().trim()
			//logi.logInfo "val "+val
			addrDetails_exp.put(key,val)

		}
		logi.logInfo addrDetails_exp.toString()
		return addrDetails_exp
	}

	public String uv_STMT_TOTAL_BALANCE_DUE_208(String acct_no)
	{
		logi.logInfo("Calling uv_STMT_TOTAL_BALANCE_DUE_208")
		DecimalFormat d = new DecimalFormat("##,###.00");
		ConnectDB db = new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String origAmt = db.executeQueryP2("SELECT CASE WHEN bal_fwd IS NULL THEN TRIM(TO_CHAR(NVL((debit-credit),0),'99990.99')) ELSE TRIM(TO_CHAR(NVL(((bal_fwd+debit)-credit),0),'99990.99'))  End FROM ariacore.gl WHERE acct_no ="+acct_no+" and invoice_no = (select max(invoice_no) from ariacore.gl where acct_no="+acct_no+") ")
		logi.logInfo("origAmt: "+origAmt)
		String bal_trans_amt = db.executeQueryP2("SELECT TRIM(TO_CHAR((NVL(SUM(amount),0)),'99990.99')) from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type=4 and statement_no=(Select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no+" )")
		logi.logInfo("bal_trans_amt: "+bal_trans_amt)
		String dueAmt = d.format(origAmt.toDouble()+bal_trans_amt.toDouble()).toString()

		return dueAmt
	}

	public String uv_ClientCsrPhone_208(String tcid)
	{
		logi.logInfo("Calling uv_ClientCsrPhone_208")
		String phone="//*[@id='content']/p[2]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(phone)).getAttribute("innerHTML")
		List <String> addrLines=txt.split("<br>")
		logi.logInfo "Phone list: "+addrLines.toString()
		String phoneNo = addrLines.get(1).trim()
		logi.logInfo("Phoneno:" +phoneNo)

		return phoneNo
	}

	public String uv_ClientUssUrl_208(String tcid)
	{
		logi.logInfo("Calling uv_ClientUssUrl_208")
		String url="//*[@id='content']/p[3]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt=driver.findElement(By.xpath(url)).getText()

		logi.logInfo "url text: "+txt
		String weburl = txt.split("visit")[1].trim()
		logi.logInfo("weburl:" +weburl)

		return weburl
	}

	public LinkedHashMap uv_stmt_getInvoiceLineItemsFromStatements_ST_208(String tcid)
	{
		logi.logInfo("Calling uv_stmt_getInvoiceLineItemsFromStatements_ST_208")
		LinkedHashMap<String,String> invoiceDetails=new LinkedHashMap<String,String>()
		int counter=0
		int kcounter=0
		List<String> keys=[
			'PLAN_NAME',
			'SERVICE_NAME',
			'UNITS',
			'RATE',
			'AMOUNT'
		]
		List<WebElement> items = driver.findElements(By.xpath("//div[@id='content']//table[2]//th[contains(text(),'Balance Transfer Detail')]/../preceding-sibling::tr[@class='lineitem']"));
		logi.logInfo("Items count : "+items.size().toString())
		items.each  { item ->
			logi.logInfo("Preparing Item Hash")
			counter=counter+1
			LinkedHashMap hm = new LinkedHashMap<String,String>()
			List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
			logi.logInfo("tds count : "+tds.size().toString())
			tds.each {titem ->
				logi.logInfo("td value "+titem.getText())
				hm.put(keys.get(kcounter), titem.getText())
				kcounter=kcounter+1

			}
			kcounter=0
			hm=hm.sort()
			invoiceDetails.put("Line Item"+counter,hm)

		}
		logi.logInfo("invoiceDetails "+invoiceDetails.toString())
		return invoiceDetails.sort()

	}

	public LinkedHashMap uv_stmt_getInvoiceLineItemsFromStatements_DB_208(String accountNo){

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatementsDB")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		LinkedHashMap hm = new LinkedHashMap<String,String>()
		List<String> gl_seq_num = []
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0

		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "Select count(*) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no=ser.service_no where invoice_no= %s AND ser.is_setup=0 AND ser.is_surcharge=0 AND (ser.custom_to_client_no=3285843 OR ser.custom_to_client_no IS NULL)"
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT comments, (CASE WHEN (START_DATE IS NULL OR END_DATE IS NULL) THEN '-' ELSE to_number(TO_CHAR(start_date,'dd')) || '-' || to_number(TO_CHAR(start_date,'mm')) || '-' ||to_number(TO_CHAR(start_date,'yyyy')) || ' - ' ||(TO_CHAR(end_date,'dd')) || '-' || to_number(TO_CHAR(end_date,'mm')) || '-' ||to_number(TO_CHAR(end_date,'yyyy')) END) AS Result, usage_units, usage_rate, debit FROM ariacore.all_invoice_details WHERE invoice_no =%s and seq_num=%s "
		String generalQry = "SELECT TRIM(TO_CHAR(a.plan_name)) || ' ' || (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Plan_name, TRIM(TO_CHAR(a.comments)) as Service_name , TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN a.recurring=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Units FROM (SELECT ser.recurring, aid.usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup,ser.is_order_based, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND ser.is_surcharge = 0 AND ser.is_setup=0 AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		//"SELECT TRIM(TO_CHAR(a.plan_name)) as Plan_name, TRIM(TO_CHAR(a.comments)) as Service_name , TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN a.recurring=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate||'.00') WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Units FROM (SELECT ser.recurring, aid.usage_rate, aid.comments, aid.usage_units, aid.debit, ser.usage_based, aid.plan_name, ser.is_setup,ser.is_order_based, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.all_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo))
		logi.logInfo("Seq num is :: "+seqCount)

		ResultSet rs= db.executePlaQuery("Select glt.seq_num from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no=ser.service_no where invoice_no="+invoiceNo+" AND ser.is_setup=0 AND ser.is_surcharge=0 AND (ser.custom_to_client_no=3285843 OR ser.custom_to_client_no IS NULL)")

		while(rs.next())
		{
			String num = rs.getString(1)
			gl_seq_num.add(num)
		}

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))

			hm = db.executeQueryP2(String.format(generalQry,recRateScale,usgRateScale,invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))
			if(hm.get("UNITS").equals(" "))
			{
				logi.logInfo("Units has space")
				hm.put("UNITS","")
			}
			if(hm.containsKey("PLAN_NAME"))
			{
				String pname = hm.get("PLAN_NAME").toString().trim()
				hm.put("PLAN_NAME",pname)
			}
			//hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())
			hm=hm.sort()
			mDB.put("Line Item"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB.sort();
	}

	public LinkedHashMap uv_stmt_getBalTransferLineItemsFromStatements_ST_208(String accountNo){

		logi.logInfo("Calling uv_getBalTransferLineItemsFromStatements_ST_208")

		List keyValues = [
			"BAL_TRANSFER_DESC",
			"BAL_TRANSFER_AMOUNT"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		String invoiceNo = ""
		String seqNumQry = "select count(*) as COUNT from ariacore.acct_transaction where transaction_type=4 and acct_no= %s"


		String seqNum = db.executeQueryP2(String.format(seqNumQry,accountNo))
		logi.logInfo("Bal Transfer count is :: "+seqNum)

		for(int i=1 ; i<=Integer.parseInt(seqNum) ; i++){

			int rowVal = 0
			logi.logInfo ("The value of i is "+i+" and rowval is "+rowVal)
			HashMap internalHM = new HashMap<String,String>()
			for(int k=1 ; k<=5 ; k+=4){



				String expath = "//table[2]//tr[child::th[@class='inline' and contains(text(),'Balance Transfer Detail')]]/following-sibling::tr[@class='lineitem'][%s]/td[%s]"
				//"//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				String uiValue = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				internalHM.put(keyValues.get(rowVal),uiValue)
				internalHM.sort()
				rowVal = rowVal+1
			}

			finalMap.put("Line Item"+i , internalHM)
			logi.logInfo("Final hash at the time of sequence no "+i+" is : "+finalMap.toString())
		}
		return finalMap
	}

	public LinkedHashMap uv_stmt_getBalTransferLineItemsFromStatements_DB_208(String accountNo){

		logi.logInfo("Calling uv_stmt_getBalTransferLineItemsFromStatements_DB_208")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')


		ConnectDB db = new ConnectDB()

		String seqNumQry = "select count(*) as COUNT from ariacore.acct_transaction where transaction_type=4 and acct_no= %s and client_no=%s"


		String seqNum = db.executeQueryP2(String.format(seqNumQry,accountNo,clientNo))
		logi.logInfo("Bal Transfer count is :: "+seqNum)

		for(int row=1 ; row<=Integer.parseInt(seqNum) ; row++){

			HashMap hm = new HashMap<String,String>()
			String sourceno=db.executeQueryP2("Select source_no from (Select source_no,row_number() over (order by source_no asc ) as seqNo from ariacore.acct_transaction where acct_no="+accountNo+" and transaction_type=4) where seqNo="+row)
			String descQuery ="select 'Invoice xfr (acct '||from_acct_no||')' from ariacore.balance_transfer  where to_acct_no=%s and transfer_no=%s "
			String desc=db.executeQueryP2(String.format(descQuery,accountNo,sourceno))
			hm.put("BAL_TRANSFER_DESC", desc)

			String amt= db.executeQueryP2("Select TRIM(TO_CHAR(NVL(SUM(amount),0),'99,990.99')) from (Select amount,row_number() over (order by source_no asc ) as seqNo from ariacore.acct_transaction where acct_no="+accountNo+" and transaction_type=4) where seqNo="+row)
			hm.put("BAL_TRANSFER_AMOUNT", amt)

			//hm.sort()

			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}
	public LinkedHashMap uv_getInvoiceDetails_ST_214(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceDetails_ST_204")
		LinkedHashMap<String,String> invoiceDetails=new LinkedHashMap<String,String>()
		int counter=0
		List<WebElement> items = driver.findElements(tabItemList_214);
		logi.logInfo("items size "+items.size().toString())
		for(int a=1;a<=items.size()-1;a++){
			counter=counter+1
			HashMap hm = new HashMap<String,String>()
			String desc=items.get(a).findElement(By.xpath(".//following-sibling::td[1]")).getText()
			hm.put("PLAN_NAME", desc)
			String amt= items.get(a).findElement(By.xpath(".//following-sibling::td[3]")).getText()
			hm.put("PRICE", amt)
			invoiceDetails.put("Line Item"+counter,hm)
		}
		logi.logInfo("invoiceDetails "+invoiceDetails.toString())
		return invoiceDetails.sort()

	}



	public LinkedHashMap uv_getInvoiceDetails_DB_214(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceDetails_DB_204")
		LinkedHashMap invoiceDetailsDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		int counter=0
		//String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
		String symbol=db.executeQueryP2("select case when currency_symbol = 'EUR' then '�' else currency_htm_symbol end from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
		
		//recurringloop
		int rows=db.executeQueryP2("select count(*) from ariacore.all_invoice_details where invoice_no="+invoiceNo+" and service_no in (select service_no from ariacore.all_service where orig_coupon_cd is null and orig_credit_id is null and service_type in ('RC'))").toInteger()
		String query1="Select plan_name as PLAN_NAME ,service_no,( '%s' || trim(TO_CHAR(debit,'99,999.99'))  ) as PRICE  from (select plan_name,a.service_no,debit,row_number() over(ORDER BY a.service_no asc ) as seqno from ariacore.all_invoice_details a where a.orig_coupon_cd is null and a.orig_credit_id is null and a.service_no in (select service_no from ariacore.all_service where service_type in ('RC')) and a.invoice_no=%s )  where seqno=%s order by service_no asc"
		for(int row=1 ; row<=rows ; row++){
			hm = new HashMap<String,String>()
			counter=counter+1
			hm = db.executeQueryP2(String.format(query1,symbol,invoiceNo,row))
			logi.logInfo("Individual hash after recurring is :: "+hm.toString())
			hm.remove("SERVICE_NO")
			invoiceDetailsDB.put("Line Item"+counter,hm)

		}
		//Orderloop
		int orders=db.executeQueryP2("select count(*) from ariacore.all_invoice_details where invoice_no="+invoiceNo+" and service_no in (select service_no from ariacore.all_service where orig_coupon_cd is null and service_type in ('OR'))").toInteger()
		String orderquery="Select replace((case when plan_name is null then ' ' else plan_name  end),'') as PLAN_NAME ,service_no,( '%s' || trim(TO_CHAR(debit,'99,999.99'))  ) as PRICE  from (select plan_name,a.service_no,debit,row_number() over(ORDER BY a.service_no asc ) as seqno from ariacore.all_invoice_details a where a.orig_coupon_cd is null and a.service_no in (select service_no from ariacore.all_service where service_type in ('OR')) and a.invoice_no=%s )  where seqno=%s order by service_no asc"
		for(int row=1 ; row<=orders ; row++){
			hm = new HashMap<String,String>()			
			counter=counter+1
			hm = db.executeQueryP2(String.format(orderquery,symbol,invoiceNo,row))
			hm.each {k,v ->
				String v1=v.toString().trim()
				logi.logInfo("updating hash ")
				hm.put(k, v1)
				logi.logInfo("updated hash  :: "+hm.toString())
			}
			logi.logInfo("Individual hash after order is :: "+hm.toString())
			hm.remove("SERVICE_NO")
			invoiceDetailsDB.put("Line Item"+counter,hm)

		}
		//Dunning charges
		int dcharges=db.executeQueryP2("select count(*) from ( select amount ,row_number() over (order by charge_id desc )from ariacore.dunning_charge where acct_no="+acct_no+" )").toInteger()		
		String dquery="select '%s' || PRICE from ( select trim(TO_CHAR(amount,'99,999.99'))   as PRICE ,row_number() over (order by charge_id desc ) as sno from ariacore.dunning_charge where acct_no=%s ) where sno=%s"
		for(int r=1 ; r<=dcharges ; r++){
			counter=counter+1
			hm = new HashMap<String,String>()
			hm.put("PRICE", db.executeQueryP2(String.format(dquery,symbol,acct_no,r))	)		
			hm.put("PLAN_NAME","")
			logi.logInfo("Individual hash after dunning is :: "+hm.toString())
			invoiceDetailsDB.put("Line Item"+counter,hm)

		}
		
		//UsageLoop
		int usages=db.executeQueryP2("select count(*) from ariacore.all_invoice_details where invoice_no="+invoiceNo+" and service_no in (select service_no from ariacore.all_service where orig_coupon_cd is null and service_type in ('US'))").toInteger()
		String usagequery="Select plan_name as PLAN_NAME ,service_no,( '%s' || trim(TO_CHAR(debit,'99,999.99'))  ) as PRICE  from (select plan_name,a.service_no,debit,row_number() over(ORDER BY a.service_no asc ) as seqno from ariacore.all_invoice_details a where a.orig_coupon_cd is null and a.service_no in (select service_no from ariacore.all_service where service_type in ('US')) and a.invoice_no=%s )  where seqno=%s order by service_no asc"
		for(int row=1 ; row<=usages ; row++){
			hm = new HashMap<String,String>()
			counter=counter+1
			hm = db.executeQueryP2(String.format(usagequery,symbol,invoiceNo,row))
			logi.logInfo("Individual hash after usage is :: "+hm.toString())
			hm.remove("SERVICE_NO")
			invoiceDetailsDB.put("Line Item"+counter,hm)

		}
		
		

		return invoiceDetailsDB.sort()
	}
	public boolean gotoInvoiceFromInvoicesTab()
	{
		boolean flg = false
		logi.logInfo("Calling gotoInvoiceFromInvoicesTab")
		try
		{
			//driver.findElement(linkRecentInvoices).click()
			//logi.logInfo(" Clicked Recent invoces tab")
			waitForTheElement(tabInvoices)
			if(driver.findElement(tabInvoices).isDisplayed())
			{
				driver.findElement(tabInvoices).click()
			}
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			//String invoiceNo ="14841587"
			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
				driver.findElement(linkInvoiceno).click();
			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = true;

		}


		return flg;

	}

	public String isElementFound(String elementPath)
	{
		logi.logInfo("Inside isElementFound")
		String new_xpath = null;
		new_xpath = elementPath.split('#')[1]
		String found = "FALSE"
		logi.logInfo("new  xpath is : "+new_xpath)

		try {
			if(driver.findElement(By.xpath(new_xpath)).isDisplayed())
			{
				found = "TRUE"
			}


		} catch (Exception e)
		{
			found = "FALSE"
			e.printStackTrace()


		}

		return found
	}
	public LinkedHashMap uv_getCreditActivtyLoopItems_ST_214(String tcid)

	{

		logi.logInfo("Calling uv_getCreditActivtyLoopItems_ST_214")
		LinkedHashMap<String,String> creditActivtyLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		int counter=0
		try {
			List<WebElement> items = driver.findElements(tabCreditActivityItemList);
			items.each  { item ->
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				String desc=item.getText()
				hm.put("DESCRIPTION", desc.trim())
				String amt= item.findElement(By.xpath(".//following-sibling::td")).getText()
				hm.put("AMOUNT", amt.trim())
				creditActivtyLoopItems.put("Line Item"+counter,hm)
			}

		} catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItems

	}

	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_214(String tcid)

	{

		logi.logInfo("Calling uv_getCreditActivtyLoopItems_DB_214")
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select case when currency_symbol = 'EUR' then '�' else currency_htm_symbol end from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			// Voided refunds- Electronic
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}

			// Voided refunds - External
			String extvrefundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int extvrefundsCount=db.executeQueryP2(String.format(extvrefundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String extvquery5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=extvrefundsCount ; a++){

				hm = db.executeQueryP2(String.format(extvquery5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}

			//Write Offs
			String wopaymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (6) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int wopaymnetscount1=db.executeQueryP2(String.format(wopaymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String woquery4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (6) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=wopaymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(woquery4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			//cash credit
			String ccextpaymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int ccextpaymnetscount1=db.executeQueryP2(String.format(ccextpaymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String ccquery4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=ccextpaymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(ccquery4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			//payments - external
			String extpaymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int extpaymnetscount1=db.executeQueryP2(String.format(extpaymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String extquery4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=extpaymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(extquery4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			// Refunds - Electronic
			String elrefundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int elrefundsCount=db.executeQueryP2(String.format(elrefundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String elquery5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=elrefundsCount ; a++){

				hm = db.executeQueryP2(String.format(elquery5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				//if(voidFlag=='TRUE')
				//{
				counter=counter+1
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				//}
			}

			// Refunds - External
			String exrefundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int exrefundsCount=db.executeQueryP2(String.format(exrefundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String exquery5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=exrefundsCount ; a++){

				hm = db.executeQueryP2(String.format(exquery5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				//if(voidFlag=='TRUE')
				//{
				counter=counter+1
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				//}
			}



			// Voided invoice
			String paymnetsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount=db.executeQueryP2(String.format(paymnetsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query2="SELECT a.description || ' '||%s|| ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount ; a++){

				hm = db.executeQueryP2(String.format(query2,voidinvoice,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided invoice :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)
				}
			}

			//ServiceCredits
			String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
			String serviceCreditsCountQuery="select count(*) from ariacore.all_credits where acct_no=%s and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s AND ((orig_coupon_cd is null and orig_credit_id is not null) OR service_no=0))"
			int rows=db.executeQueryP2(String.format(serviceCreditsCountQuery,acct_no,invoiceNo)).toInteger()
			String query1="select description,amount from ( select  ( (case when  credit_type='S' then 'Service Credit (applied ' end ) || '%s'  || ')' ) as DESCRIPTION ,( '%s' ||'-'||TRIM(TO_CHAR((amount),'99,990.99')) ) as AMOUNT, row_number() over (order by credit_id asc) as seq from ariacore.all_credits where acct_no=%s  and credit_id in  ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s  AND ((orig_coupon_cd is null and orig_credit_id is not null) OR service_no=0)) ) where seq=%s"
			for(int row=1 ; row<=rows ; row++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query1,glbillDate,symbol,acct_no,invoiceNo,row))
				logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			logi.logInfo( "counter value : "+counter.toString())

			//Coupons
			String couponsCountquery="select count(*) from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and orig_credit_id is null"
			int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
			String query3="select DESCRIPTION,AMOUNT from ( select gld.comments|| ' (applied '  || to_char(gl.due_date,'DD-MM-YYYY')  || ')' as DESCRIPTION,(  '%s'||TRIM(TO_CHAR((gld.debit),'99,990.99'))) as AMOUNT,row_number() over(order by gld.seq_num) as seq  from ariacore.gl_Detail  gld join ariacore.gl on gl.invoice_no=gld.invoice_no where gld.invoice_no=%s and gld.orig_coupon_cd is not null) where seq=%s"
			//String query3="select DESCRIPTION,AMOUNT from ( select gldac.comments|| ' (applied '  || to_char(gl.due_date,'DD-MM-YYYY')  || ')' as DESCRIPTION,(  '%s'||' '||TRIM(TO_CHAR((gldac.debit),'99,990.99'))) as AMOUNT,row_number() over (order by gldac.invoice_no asc) AS seq  from ariacore.gl_detail_after_credit gldac join ariacore.gl on gl.invoice_no=gldac.invoice_no where gldac.invoice_no=%s and gldac.seq_num in (select seq_num from gl_detail where invoice_no =%s and orig_coupon_cd is not null and orig_credit_id is null)) where seq=%s"
			for(int a=1 ; a<=couponsCount ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query3,symbol,invoiceNo,a))
				logi.logInfo("Individual row of hash after adding coupons :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			//payments - electronic
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		logi.logInfo ("creditActivtyLoopItemsDB ==> "+creditActivtyLoopItemsDB.toString())
		return creditActivtyLoopItemsDB

	}

	public HashMap uv_getBillingDetailsUI_222(String tcid){
		logi.logInfo("Calling method uv_getBillingDetailsUI_222")
		LinkedHashMap billingDetails = new LinkedHashMap<String,String>()

		String expath = "//div[@id='content']/table[1]/tbody/tr/td[1]/table/tbody/tr[%s]/td"
		int i=1

		for(int k=4 ; k<=7 ; k++){

			String formattedXpath = String.format(expath,k)
			logi.logInfo("formattedXpath : "+formattedXpath)
			String uiValue = "NO VALUE"

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
					logi.logInfo("uiValue : "+uiValue)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}
			billingDetails.put("BILLTO_LINEITEM_"+i,uiValue.trim())
			i++
		}
		billingDetails.sort()

		logi.logInfo("Billing details fetched from API is : "+billingDetails.toString())
		return billingDetails
	}
	public HashMap uv_getBillingDetailsDB_222(String tcid){

		logi.logInfo("Calling method uv_getBillingDetailsDB_222")
		LinkedHashMap billingDetails = new LinkedHashMap<String,String>()

		ConnectDB db =new ConnectDB()
		String acct_no = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		logi.logInfo("Acct no is : "+acct_no)

		String billingQry = "select trim(COMPANY_NAME) AS BILLTO_LINEITEM_1,ADDRESS1 AS BILLTO_LINEITEM_2 ,CITY ||' '|| STATE || ' ' || ZIP AS BILLTO_LINEITEM_3,COUNTRY AS BILLTO_LINEITEM_4 FROM ARIACORE.BILLING_INFO WHERE ACCT_NO = %s"
		logi.logInfo("Fotmatted query is : "+billingQry)

		billingDetails = db.executeQueryP2(String.format(billingQry, acct_no))
		//		billingDetails.each { k, v ->
		//			String val = v.toString().trim()
		//			billingDetails.put(k,val)
		//		}
		//billingDetails.sort()

		logi.logInfo("Billing details fetched from db is : "+billingDetails.toString())
		return billingDetails
	}

	public LinkedHashMap uv_verifyBeginLoopRecurItemsDB_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopRecurItemsDB_222")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNo = ""
		String recuringRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		String generalQry =  "select TRIM(TO_CHAR(a.plan_name)) as DESCRIPTION, RPAD(a.usage_rate ||'.', %s, 0) as UNIT_PRICE, a.base_plan_units as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.recurring, aid.base_plan_units, aid.seq_num from ariacore.services ser join ariacore.all_invoice_details aid on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.recurring=1 and aid.ORIG_COUPON_CD is null )a where seq_num = %s"
		String countQry =  "select count(*) from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.recurring, aid.base_plan_units, aid.seq_num from ariacore.services ser join ariacore.all_invoice_details aid on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.recurring=1 and aid.ORIG_COUPON_CD is null )a"
		String recuringQry = "select param_val from ariacore.all_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		int totalCount = db.executeQueryP2(String.format(countQry,invoiceNo,clientNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)


			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,invoiceNo,clientNo,row))

			hm = db.executeQueryP2(String.format(generalQry,recRateScale,invoiceNo,clientNo,row))

			hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Recurring Details"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}

	public LinkedHashMap uv_verifyBeginLoopRecurItemsUI_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopRecurItemsUI_222")

		LinkedHashMap invoiceDetails  = new LinkedHashMap<String ,HashMap<String,String>>()

		List keyValues = [
			"DESCRIPTION",
			"UNIT_PRICE",
			"LICENSES",
			"NET_PRICE"
		];

		String description = ""
		String lisText = ""
		String lisText1 = ""
		String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"

		int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[contains(td,' ')]")).size()
		logi.logInfo("Total no of rows available is : "+totalCount)
		int iterate = 1
		for(int i=0; i<totalCount; i++){
			HashMap lineItemDetails = new HashMap<String,String>()
			String temp = "//div[@id='content']/table[2]/tbody/tr[%s]/td[3]"
			String temp1 = "//div[@id='content']/table[2]/tbody/tr[%s]/td[2]"
			int k = i+2
			logi.logInfo("Recuring :: "+k)

			lisText = driver.findElement(By.xpath(String.format(temp,k))).getText().toString()
			logi.logInfo("liscence : "+lisText)

			lisText1 = driver.findElement(By.xpath(String.format(temp1,k))).getText().toString()
			logi.logInfo("liscence1 : "+lisText1)

			if(lisText.length() >=1 && lisText1.length() >=2){
				logi.logInfo("inside recuring loop")
				for(int row =1;row<=4;row++){

					try {
						logi.logInfo("inside try : ")
						if(driver.findElement(By.xpath(String.format(expath,k,row))).isDisplayed()){
							description = driver.findElement(By.xpath(String.format(expath,k,row))).getText()
							logi.logInfo("Value is  : "+description)
						}
					} catch (Exception e) {
						logi.logInfo ("Element not found Exception")
						e.printStackTrace()
					}
					lineItemDetails.put(keyValues.get(row-1),description)
				}
				logi.logInfo("lineItemDetails :: "+lineItemDetails.toString())
				lineItemDetails.sort()
				invoiceDetails.put("Recurring Details"+iterate,lineItemDetails)
				iterate++
			} else{
				logi.logInfo("Line item "+iterate+" is not a recuring service")
			}
			//lineItemDetails.clear()

		}
		logi.logInfo("invoiceDetails :: "+invoiceDetails.toString())
		return invoiceDetails
	}
	public LinkedHashMap uv_verifyBeginLoopUsgItemsDB_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopUsgItemsDB_222")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNo = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String usageRateQry = "select a.usage_rate from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null)a where a.custom_seq_num=%s"
		String generalQry =  "select TRIM(TO_CHAR(a.plan_name)) as DESCRIPTION, (case when regexp_like(trim(a.usage_rate), '[.]') then RPAD(a.usage_rate, %s, 0)   else  RPAD(a.usage_rate  ||'.', %s, 0)  end)  AS UNIT_PRICE,  replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.usage_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.usage_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		String usageScaleQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		String totalCountQry = "select count(*) from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null and orig_coupon_cd is null)a"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		//int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		int totalCount = db.executeQueryP2(String.format(totalCountQry,invoiceNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			usageRateScale = db.executeQueryP2(String.format(usageScaleQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row)).toString().toDouble().toInteger()
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			int tempUsageRate1 = tempUsageRate.toString().length()
			logi.logInfo("tempUsageRate1 is :: "+tempUsageRate1)


			if(tempUsageRate1 != null){
				logi.logInfo("Inside not null statement")
				usgRateScale = tempUsageRate1 + Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))

			hm = db.executeQueryP2(String.format(generalQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}

			hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Usage Details"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}

	public LinkedHashMap uv_verifyBeginLoopUsgItemsUI_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopRecurItemsUI_222")

		LinkedHashMap invoiceDetails  = new LinkedHashMap<String ,HashMap<String,String>>()

		List keyValues = [
			"DESCRIPTION",
			"UNIT_PRICE",
			"LICENSES",
			"NET_PRICE"
		];

		String description = ""
		String lisText = ""
		String expath = "//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]][%s]/td[%s]"

		int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		logi.logInfo("Total no of rows available is : "+totalCount)
		int iterate = 1
		for(int i=0; i<totalCount; i++){
			HashMap lineItemDetails = new HashMap<String,String>()
			String temp = "//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]][%s]/td[1]"
			lisText = driver.findElement(By.xpath(String.format(temp,i+1))).getText().toString()
			logi.logInfo("Plan Name :: "+lisText)
			if(lisText.length() >=1){
				logi.logInfo("inside recuring loop")
				for(int row =1;row<=4;row++){

					try {
						logi.logInfo("inside try : ")
						if(driver.findElement(By.xpath(String.format(expath,i+1,row))).isDisplayed()){
							description = driver.findElement(By.xpath(String.format(expath,i+1,row))).getText()
							logi.logInfo("Usage Value is  : "+description)
						}
					} catch (Exception e) {
						logi.logInfo ("Element not found Exception")
						e.printStackTrace()
					}
					lineItemDetails.put(keyValues.get(row-1),description)
				}
				logi.logInfo("lineItemDetails :: "+lineItemDetails.toString())
				lineItemDetails.sort()
				invoiceDetails.put("Usage Details"+iterate,lineItemDetails)
				iterate++
			} else{
			logi.logInfo("Line item "+iterate+" is not a usage service")
			}
			//lineItemDetails.clear()
			
		}
		logi.logInfo("invoiceDetails :: "+invoiceDetails.toString())
		return invoiceDetails
	}
	public LinkedHashMap uv_getCreditActivtyLoopItems_ST_222(String tcid)

	{

		logi.logInfo("Calling uv_getCreditActivtyLoopItems_ST_222")
		LinkedHashMap<String,String> CALoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		int counter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr//td[contains(text(),'(applied')]"));
			items.each  { item ->
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				String desc=item.getText()
				hm.put("DESCRIPTION", desc)
				String amt= item.findElement(By.xpath(".//following-sibling::td")).getText()
				hm.put("AMOUNT", amt)
				CALoopItems.put("Line Item"+counter,hm)
			}

		} catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return CALoopItems

	}

	public LinkedHashMap uv_verifyBeginLoopOrderItemsDB_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopOrderItemsDB_222")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNo = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String usageRateQry = "select a.usage_rate from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null)a where a.custom_seq_num=%s"
		String generalQry =  "select replace((case when a.plan_name is null then ' ' else to_char(a.plan_name) end),'') as DESCRIPTION, TRIM(TO_CHAR(a.usage_rate,'99,990.99')) as UNIT_PRICE, replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.is_order_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.is_order_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		//String generalQry =  "select replace((case when a.plan_name is null then ' ' else to_char(a.plan_name) end),'') as DESCRIPTION, (case when regexp_like(trim(a.usage_rate), '.') then RPAD(a.usage_rate, %s, 0)   else  RPAD(a.usage_rate  ||'.', %s, 0)  end)  AS UNIT_PRICE, replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.is_order_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.is_order_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		String totalCountQry = "select count(*) from ariacore.all_invoice_details WHERE invoice_no=%s and ORIG_CLIENT_SKU is not null and order_no is not null"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		//int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		int totalCount = db.executeQueryP2(String.format(totalCountQry,invoiceNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			logi.logInfo("Formatted query"+String.format(generalQry,invoiceNo,clientNo,row))

			hm = db.executeQueryP2(String.format(generalQry,invoiceNo,clientNo,row))
			hm.each {k,v ->
				logi.logInfo("Before trim ::::: "+v.toString())
				String v1 = v.toString().trim()
				logi.logInfo("After trim ::::: "+v.toString())
				hm.put(k,v1)
			}

			hm.sort()
			logi.logInfo("Individual row of hash for order items is :: "+hm.toString())

			mDB.put("Order Details"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}

	public LinkedHashMap uv_verifyBeginLoopOrderItemsUI_222(String tcid){

		logi.logInfo("Calling uv_verifyBeginLoopOrderItemsUI_222")

		LinkedHashMap invoiceDetails  = new LinkedHashMap<String ,HashMap<String,String>>()

		List keyValues = [
			"DESCRIPTION",
			"UNIT_PRICE",
			"LICENSES",
			"NET_PRICE"
		];

		String description = ""
		String planText = ""
		String lisText = ""
		String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"

		int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[contains(td,' ')]")).size()
		logi.logInfo("Total no of rows available is : "+totalCount)
		int iterate = 1
		for(int i=0; i<totalCount; i++){
			HashMap lineItemDetails = new HashMap<String,String>()
			String temp = "//div[@id='content']/table[2]/tbody/tr[%s]/td[3]"
			String temp1 = "//div[@id='content']/table[2]/tbody/tr[%s]/td[1]"
			int k = i+2
			logi.logInfo("Order ## :: "+k)

			lisText = driver.findElement(By.xpath(String.format(temp,k))).getText().toString()
			logi.logInfo("liscence Text : "+lisText)

			planText = driver.findElement(By.xpath(String.format(temp1,k))).getText().toString()
			logi.logInfo("Plan Text : "+planText)
			if(lisText.length() <=1 && planText.length()<=1){
				logi.logInfo("inside order loop")
				for(int row =1;row<=4;row++){

					try {
						logi.logInfo("inside try : ")
						if(driver.findElement(By.xpath(String.format(expath,k,row))).isDisplayed()){
							description = driver.findElement(By.xpath(String.format(expath,k,row))).getText()
							logi.logInfo("Value is  : "+description)
						}
					} catch (Exception e) {
						logi.logInfo ("Element not found Exception")
						e.printStackTrace()
					}
					lineItemDetails.put(keyValues.get(row-1),description)
				}
				logi.logInfo("lineItemDetails :: "+lineItemDetails.toString())
				lineItemDetails.sort()
				invoiceDetails.put("Order Details"+iterate,lineItemDetails)
				iterate++
			} else{
				logi.logInfo("Line item "+iterate+" is not a order based service")
			}
			//lineItemDetails.clear()

		}
		logi.logInfo("invoiceDetails :: "+invoiceDetails.toString())
		return invoiceDetails
	}
	
	public LinkedHashMap uv_verifyBeginLoopTaxSummaryDetailsUI_222(String tcid){
		logi.logInfo("Calling uv_verifyBeginLoopTaxSummaryDetailsUI_222")
		List keyValues = [
			"DESCRIPTION",
			"UNIT_PRICE",
			"LICENSES",
			"NET_PRICE"
			];
		LinkedHashMap taxItemsUI = new LinkedHashMap<String,HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		
		int totalCount = 0
		String expath = "//div[@id='content']/table[2]/tbody/tr[td[contains(text(),'  ')]]"
		
		totalCount = driver.findElements(By.xpath(expath)).size()
		logi.logInfo("Total rows available is :: "+totalCount)
		
		String description = ""
		int iterate = 1
		String expath1 = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"
		for(int i=0; i<totalCount; i++){
			HashMap lineItemDetails = new HashMap<String,String>()
			String temp = "//div[@id='content']/table[2]/tbody/tr[%s]/td[1]"
			int k = i+2
			logi.logInfo("Tax :: "+k)
			
			String lisText = driver.findElement(By.xpath(String.format(temp,k))).getText().toString()
			logi.logInfo("Tax : length : "+lisText.length())
			
			if(lisText.contains("%") && lisText.contains("Tax")){
				logi.logInfo("inside tax loop ::: "+i)
				for(int row =0;row<4;row++){
				
					try {
						logi.logInfo("inside try : ")
						if(driver.findElement(By.xpath(String.format(expath1,k,(row-1)))).isDisplayed()){
							description = driver.findElement(By.xpath(String.format(expath1,k,(row-1)))).getText()
							logi.logInfo("Value is  : "+description)
						}
					} catch (Exception e) {
						logi.logInfo ("Element not found Exception")
						e.printStackTrace()
					}
					hm.put(keyValues.get(row),description)
				}
				logi.logInfo("Tax ItemDetails :: "+hm.toString())
				//hm.sort()
				if(hm.get("DESCRIPTION").toString().contains("%") && hm.get("DESCRIPTION").toString().contains("Tax") && hm.get("NET_PRICE").toString().length()>=3){
					logi.logInfo("Putting into hash :: "+i)

					hm.each {k1,v ->
						String v1 = v.toString().trim()
						hm.put(k1,v1)
					}

					taxItemsUI.put("Tax_LineItem"+iterate,hm)
					logi.logInfo("Tax Details :: "+taxItemsUI.toString())
					iterate++
				}
			} else{
			logi.logInfo("Line item "+i+" is not a Tax service")
			}
		}
		return taxItemsUI
	}
	public LinkedHashMap uv_verifyBeginLoopTaxSummaryDetailsDB_222(String tcid){
		logi.logInfo("Calling uv_verifyBeginLoopTaxSummaryDetailsDB_222")
		LinkedHashMap taxItemsDB = new LinkedHashMap<String,HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		
		ConnectDB db = new ConnectDB()
		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo

		String lineQry = "select count(distinct(seq_num)) as totalCount from ariacore.gl_tax_detail where invoice_no = %s"
		String taxItemsQry = "select s.comments AS Description ,sum(gtl.debit ) AS Net_Price, ' ' AS Unit_price, ' '  AS Licenses from ariacore.all_invoice_tax_details gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"
		
		String totalLineItems = ""
		
		String invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		
		totalLineItems = db.executeQueryP2(String.format(lineQry,invoiceNo)).toString()
		logi.logInfo("Total Line items are : "+totalLineItems)
		
		for(int lines = 1;lines<=Integer.parseInt(totalLineItems);lines++){

			hm = db.executeQueryP2(String.format(taxItemsQry,invoiceNo,clientNo))
			logi.logInfo("Values returned from DB are : "+hm.toString())

			hm.each {k,v ->
				String v1 = v.toString().trim()
				hm.put(k,v1)
			}
			
			taxItemsDB.put("Tax_LineItem"+lines,hm)
			logi.logInfo("Individual Hash are : "+taxItemsDB)
		}
		
		return taxItemsDB
	}

	public boolean childaccountSearch()
	{
		boolean success = false;

		waitForTheElement(tabAccounts)
		//Store the current window handle
		winHandleMain = driver.getWindowHandle();
		if(driver.findElement(tabAccounts).isDisplayed())
		{

			String isAcctTabSelected = driver.findElement(tabAcctOnClick).getAttribute("aria-selected")
			logi.logInfo("isAcctTabSelected "+isAcctTabSelected)
			if(isAcctTabSelected.equalsIgnoreCase("false"))
			{
				logi.logInfo(" Going to select accounts tab")
				driver.findElement(tabAccounts).click();
				logi.logInfo("Selected accounts tab")

			}
			else
			{
				logi.logInfo("Accounts tab already selected")
			}

			if(driver.findElement(tabAcctOnClick).getAttribute("aria-selected").equalsIgnoreCase("true"))
			{
				logi.logInfo("Accounts tab expanded")
				logi.logInfo "acct search "+driver.findElement(tabAcctSrch).isDisplayed()
				//waitForTheElement(tabAcctSrch)

				String isSrchExpanded = driver.findElement(tabSrchOnClk).getAttribute("class")
				logi.logInfo "isSrchExpanded "+isSrchExpanded
				if(isSrchExpanded.trim().equals("") )
				{
					logi.logInfo("Going to click search option")
					driver.findElement(tabAcctSrch).click();
					logi.logInfo("Clicked search option")
				}
				/*if(driver.findElement(txtSrch).isDisplayed())
				 {
				 logi.logInfo("search text box is aviailable")
				 boolean flg_search=search()
				 if(flg_search)
				 success=true
				 }*/
				else if(driver.findElement(tabSrchOnClk).getAttribute("class").equalsIgnoreCase("active_subnav"))
				{
					logi.logInfo "Already search option enabled"

				}
				waitForTheElement(tabAdhocSrch)
				driver.findElement(tabAdhocSrch).click();
				String acct_no= u.getValueFromResponse("create_acct_complete.a","//acct_no")
				boolean flg_search=search(acct_no)
				if(flg_search)
					success=true

			}
			else
			{
				logi.logInfo("acct search else part")
			}
		}
		return success
	}

	public boolean gotoChildInvoiceFromRecentInvoices()
	{
		boolean flg = false
		logi.logInfo("Calling gotoChildInvoiceFromRecentInvoices")
		try
		{
			driver.findElement(linkRecentInvoices).click()
			logi.logInfo(" Clicked Recent invoces tab")
			waitForTheElement(tabInvoices)
			String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			//String invoiceNo ="14841587"
			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
				driver.findElement(linkInvoiceno).click();
			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = true;

		}


		return flg;



	}

	public boolean viewChildStatementFromInvoiceTab()
	{
		logi.logInfo("Inside viewChildStatementFromInvoiceTab")
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return viewCurrentStatementFromInvoiceTab(acct_no)
	}

	/*public boolean viewStatementFromInvoiceTab(String str)
	{
		logi.logInfo("Inside viewStatementFromInvoiceTab")
		logi.logInfo("str value "+str)
		if(str.contains("#"))
		str=str.replace("#",",")
		logi.logInfo("str value "+str)
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
		logi.logInfo "acct_no ::: "+acct_no.toString()
		return viewCurrentStatementFromInvoiceTab(acct_no)
	}*/
	public boolean viewStatementFromInvoiceTab()
	{
		logi.logInfo("Inside viewStatementFromInvoiceTab_m")
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		logi.logInfo "acct_no ::: "+acct_no.toString()
		return viewCurrentStatementFromInvoiceTab(acct_no)
	}
	public boolean viewStatementFromInvoiceTab_m()
	{
		logi.logInfo("Inside viewStatementFromInvoiceTab")
		String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
		return viewCurrentStatementFromInvoiceTab(acct_no)
	}
	public LinkedHashMap uv_getBillingAdressLineItem_DB(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_DB")
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		return uv_stmt_getBillingAdressLineItem_DB(acct_no)
	}

	public LinkedHashMap uv_getChildBillingAdressLineItem_DB(String tcid)
	{
		logi.logInfo("Calling uv_getChildBillingAdressLineItem_DB")
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")

		return uv_stmt_getBillingAdressLineItem_DB(acct_no)
	}

	public String uv_STMT_PAYMENTS_RECEIVED_208(String tcid)
	{
		logi.logInfo("Calling uv_STMT_PAYMENTS_RECEIVED_208")
		String acct_no= u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_STMT_PAYMENTS_RECEIVED_208_TEMP(acct_no)
	}

	public String uv_STMT_CHILD_PAYMENTS_RECEIVED_208(String tcid)
	{
		logi.logInfo("Calling uv_STMT_CHILD_PAYMENTS_RECEIVED_208")
		String acct_no= u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_STMT_PAYMENTS_RECEIVED_208_TEMP(acct_no)
	}
	public String uv_TOTAL_BALANCE_DUE_208(String tcid)
	{
		logi.logInfo("Calling uv_TOTAL_BALANCE_DUE_208")
		String acct_no= u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_STMT_TOTAL_BALANCE_DUE_208(acct_no)
	}
	public String uv_CHILD_TOTAL_BALANCE_DUE_208(String tcid)
	{
		logi.logInfo("Calling uv_CHILD_TOTAL_BALANCE_DUE_208")
		String acct_no= u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_STMT_TOTAL_BALANCE_DUE_208(acct_no)
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_ST_208(String tcid)
	{

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_ST_208")
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_ST_208(accountNo)
	}
	public LinkedHashMap uv_getChildInvoiceLineItemsFromStatements_ST_208(String tcid)
	{

		logi.logInfo("Calling uv_getChildInvoiceLineItemsFromStatements_ST_208")
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_ST_208(accountNo)
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB_208(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_DB_208")
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_DB_208(accountNo)

	}
	public LinkedHashMap uv_getChildInvoiceLineItemsFromStatements_DB_208(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_DB_208")
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_DB_208(accountNo)

	}
	public LinkedHashMap uv_getBalTransferLineItemsFromStatements_ST_208(String tcid)
	{
		logi.logInfo("Calling uv_getBalTransferLineItemsFromStatements_ST_208")
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getBalTransferLineItemsFromStatements_ST_208(accountNo)

	}

	public LinkedHashMap uv_getChildBalTransferLineItemsFromStatements_ST_208(String tcid)
	{
		logi.logInfo("Calling uv_getChildBalTransferLineItemsFromStatements_ST_208")
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getBalTransferLineItemsFromStatements_ST_208(accountNo)

	}
	public LinkedHashMap uv_getBalTransferLineItemsFromStatements_DB_208(String tcid){
		logi.logInfo("Calling uv_getBalTransferLineItemsFromStatements_DB_208")
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getBalTransferLineItemsFromStatements_DB_208(accountNo)
	}
	public LinkedHashMap uv_getChildBalTransferLineItemsFromStatements_DB_208(String tcid){
		logi.logInfo("Calling uv_getChildBalTransferLineItemsFromStatements_DB_208")
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getBalTransferLineItemsFromStatements_DB_208(accountNo)
	}

	public String uv_verifyLineDescriptionLengthUI_222(String tcid){

		logi.logInfo("Calling uv_verifyLineDescriptionLengthUI_222")

		boolean lengthFlag = false
		String description = ""
		String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"

		int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[contains(td,' ')]")).size()
		logi.logInfo("Total no of rows available is : "+totalCount)
		int iterate = 1
		for(int i=0; i<totalCount; i++){
			int k = i+2
			logi.logInfo("Recuring :: "+k)

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(String.format(expath,k,1))).isDisplayed()){
					description = driver.findElement(By.xpath(String.format(expath,k,1))).getText()
					logi.logInfo("Value is  : "+description)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}
			if(description.length()<=50){
				lengthFlag = true
			}

		}
		return lengthFlag.toString().toUpperCase()
	}

	public String uv_VerifyLineItemDescriptionTruncate_ST(String tcid){

		logi.logInfo("Calling uv_verifyLineDescriptionLengthUI_222")

		boolean lengthFlag = true
		String description = ""
		String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"

		int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[@class='lineitem']]")).size()

		logi.logInfo("Total no of rows available is : "+totalCount)
		int iterate = 1
		for(int i=0; i<totalCount; i++){
			int k = i+2
			logi.logInfo("Recuring :: "+k)

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(String.format(expath,k,1))).isDisplayed()){
					description = driver.findElement(By.xpath(String.format(expath,k,1))).getText()
					logi.logInfo("Value is  : "+description)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}
			if(description.length()>50){
				lengthFlag = false
			}

		}
		return lengthFlag.toString().toUpperCase()
	}

	public def getAcctStatementTemplate(String clientNo, String accountNo){

		ConnectDB db = new ConnectDB()
		String tempDetail = ""
		String query = ""

		tempDetail = db.executeQueryP2(query)
		logi.logInfo("The template details are : "+tempDetail)
		return "106 - Adobe Statement Template II"
		//return tempDetail
	}

	// TEMPLATE 211

	public String uv_GET_DATE_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_STMT_GET_DATE_211(accountNo)

	}
	public String uv_CHILD_GET_DATE_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_STMT_GET_DATE_211(accountNo)

	}
	public String uv_STMT_GET_DATE_211(String acctNo)
	{
		logi.logInfo("Calling method uv_STMT_GET_DATE_211")
		ConnectDB db = new ConnectDB()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		// String format = db.executeQueryP2("Select param_val from ariacore.all_client_params where client_no="+clientNo+" and param_name ='INTL_DATE_FORMAT_MASK'").toString()
		String format = "dd-MM-yyyy"
		String invNo = u.getInvoiceNoVT(clientNo,acctNo).toString();
		String billDate  = db.executeQueryP2("Select TO_CHAR(bill_date,'"+format+"' ) from ariacore.all_invoices where invoice_no="+invNo+" and client_no="+clientNo).toString()

		return billDate
	}
	public LinkedHashMap uv_getBillToLineItems_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getBillToLineItems_DB_211(accountNo)
	}
	public LinkedHashMap uv_getChildBillToLineItems_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getBillToLineItems_DB_211(accountNo)
	}
	public LinkedHashMap uv_stmt_getBillToLineItems_DB_211(String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getBillToLineItems_DB_211")

		//String query= "Select company_name as BillCompanyName,first_name as BillFirstName,last_name as BillLastName,address1 as BillAddress1,address2 as BillAddress2,address3 as BillAddress3,city as BillCity,state as BillState,zip as BillZip,country as BillCountry from ariacore.current_billing_info where acct_no="+acct_no
		String query= "select (b.company_name) as ADDR_LINE1 ,b.first_name ||' '|| b.last_name as ADDR_LINE2 ,b.address1 as ADDR_LINE3,b.address2 as ADDR_LINE4, case when ( b.city is not null and b.state is not null and b.zip is not null) then (b.city||' '||b.state|| chr(38)||'nbsp;' ||chr(38)||'nbsp;'||b.zip) "+
				"else '' end as ADDR_LINE5,b.country as ADDR_LINE6 from ariacore.current_billing_info b join ariacore.all_client_country c on c.client_no=b.client_no and b.country=c.country where b.acct_no="+acct_no
		LinkedHashMap<String,String> addrDetails=new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		//logi.logInfo "columns count"+columns
		String val
		while (resultSet.next()){

			for(int i=1; i<=columns; i++){
				String tt= resultSet.getString(i)
				if((tt!=null) && (!tt.trim().equals("")) )
				{
					addrDetails.put(md.getColumnName(i).toString(),tt.trim().toString());
				}
				else
				{
					addrDetails.put(md.getColumnName(i).toString(),"NO VALUE");
				}

			}

		}
		logi.logInfo "Bill To address details DB are "+addrDetails.toString()

		return addrDetails
	}
	public LinkedHashMap uv_getBillToLineItems_ST_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getBillToLineItems_ST_211(accountNo)
	}
	public LinkedHashMap uv_getChildBillToLineItems_ST_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getBillToLineItems_ST_211(accountNo)
	}
	public LinkedHashMap uv_stmt_getBillToLineItems_ST_211(String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getBillToLineItems_ST_211")
		LinkedHashMap<String,String> addrDetails_act=new LinkedHashMap<String,String>()
		List<WebElement> items = driver.findElements(By.xpath("//table[1]//td[text()='BILL TO:']/following-sibling::td[1]//td[ (string-length()>1) and not (contains(text(),'Tax ID: ') )]"))
		int counter=0
		items.each  { item ->
			logi.logInfo("Preparing Item Hash")
			counter=counter+1
			addrDetails_act.put("ADDR_LINE"+counter, item.getText())
		}

		return addrDetails_act
	}

	public LinkedHashMap uv_getShipToLineItems_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getShipToLineItems_DB_211(accountNo)
	}
	public LinkedHashMap uv_getChildShipToLineItems_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getShipToLineItems_DB_211(accountNo)
	}
	public LinkedHashMap uv_stmt_getShipToLineItems_DB_211(String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getShipToLineItems_DB_211")
		String query= "SELECT company_name AS ACCT_ADDR_LINE1, first_name || ' ' || last_name        AS ACCT_ADDR_LINE2, address1          AS ACCT_ADDR_LINE3, address2          AS ACCT_ADDR_LINE4, city || ' ' ||  state_prov || ' ' || postal_code          AS ACCT_ADDR_LINE5,   country           AS ACCT_ADDR_LINE6 FROM ariacore.acct WHERE acct_no="+acct_no
		LinkedHashMap<String,String> addrDetails=new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB()
		
		addrDetails = db.executeQueryP2(query);
				
		logi.logInfo "Ship To address details DB are "+addrDetails.toString()

		return addrDetails
	}

	public LinkedHashMap uv_getShipToLineItems_ST_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getShipToLineItems_ST_211(accountNo)
	}
	public LinkedHashMap uv_getChildShipToLineItems_ST_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getShipToLineItems_ST_211(accountNo)
	}
	public LinkedHashMap uv_stmt_getShipToLineItems_ST_211(String acct_no)
	   {
			  logi.logInfo("Calling uv_stmt_getShipToLineItems_ST_211")
			  LinkedHashMap<String,String> addrDetails_act=new LinkedHashMap<String,String>()
			  List<WebElement> items = driver.findElements(By.xpath("//table[1]//td[text()='SHIP TO:']/following-sibling::td[1]//td[ (string-length()>1) and not (contains(text(),'Tax ID: ') )]"))
			  int counter=0
			  items.each  { item ->
					 logi.logInfo("Preparing Item Hash")
					 counter=counter+1
					 addrDetails_act.put("ACCT_ADDR_LINE"+counter, item.getText())
			  }
			  
			  return addrDetails_act
	   }

	public boolean gotoInvoiceFromPendingInvoices()
	{
		boolean flg = false
		logi.logInfo("Calling gotoInvoiceFromPendingInvoices")
		try
		{
			driver.findElement(linkRecentInvoices).click()
			logi.logInfo(" Clicked Recent invoces tab")
			if(driver.findElement(tabPendingInvoices).isDisplayed())
			{
				logi.logInfo ("Pending invoice tab is displayed")
				logi.logInfo("text is :"+ driver.findElement(tabPendingInvoices).getText())
				waitForTheElement(tabPendingInvoices)
				//driver.findElement(By.linkText("Pending Invoices")).click();
				driver.findElement(tabPendingInvoices).click();
			}

			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getPendingInvoiceNoVT(clientNo, acct_no).toString();
			//String invoiceNo ="14841587"
			if(driver.findElement(linkPendingInvoiceno).getText()==invoiceNo)
				driver.findElement(linkApprove).click();
			logi.logInfo ("clicked approve link in pending invoice")
			waitForTheElement(tabPendingInvoices)
			driver.findElement(linkApprovelink).click();
			logi.logInfo ("clicked approve link in from page tool pending invoice")
			waitForTheElement(tabPendingInvoices)
			driver.findElement(linkApproveSubmit).click();
			logi.logInfo ("clicked approve link in pending invoice")
			waitForTheElement(divPendingInvoiceError)

			driver.findElement(linkInvoiceTab).click();
			waitForTheElement(linkInvoiceTab)
			//driver.findElement(linkRecentInvoices).click()
			logi.logInfo(" Clicked Recent invoces tab")
			waitForTheElement(tabInvoices)
			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
				driver.findElement(linkInvoiceno).click();

			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = false;

		}


		return flg;



	}

	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_ST_211(String tcid){

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_ST_211")

		List keyValues = [
			"ITEM_NO",
			"ITEM_DESCRIPTION",
			"QTY",
			"UNIT_PRICE",
			"TOTAL"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap internalHM = new HashMap<String,String>()
		String invoiceNo = ""

		int seqNum = driver.findElements(By.xpath("//table[2]//tr")).size()
		logi.logInfo("Seq num count is :: "+seqNum)

		for(int i=2 ; i<=seqNum ; i++){

			internalHM = new HashMap<String,String>()
			for(int k=1 ; k<=5 ; k++){
				int rowVal = i+2
				logi.logInfo ("The value of i is "+i+" and incremented is "+rowVal)

				String expath = "//table[2]//tr[%s]//td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				String uiValue = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				internalHM.put(keyValues.get(k-1),uiValue)
				internalHM.sort()

			}

			finalMap.put("Line Item"+(i-1) , internalHM)
			logi.logInfo("Final hash at the time of sequence no "+(i-1)+" is : "+finalMap)
		}

		return finalMap
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB_211(tcid,accountNo)
	}
	public LinkedHashMap uv_getChildInvoiceLineItemsFromStatements_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB_211(tcid,accountNo)
	}

	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatements_DB_211(String tcid,String accountNo)
	{

		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatements_DB_211")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		List<String> gl_seq_num = []
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		String generalQry =  "SELECT a.seq_num as item_no,TRIM(TO_CHAR(a.comments)) as item_description, (CASE  WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Qty, ( CASE WHEN a.recurring=1 THEN RPAD(a.usage_rate  ||'.',%d, 0) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.',%d, 0) WHEN a.service_no=0 THEN NVL( TO_CHAR( a.usage_rate), ' ' )  WHEN a.is_order_based=1 THEN TRIM(TO_CHAR(a.usage_rate,'99,990.99')) END) AS unit_price, TRIM(TO_CHAR(a.debit,'99,990.99')) as total FROM (SELECT ser.recurring, aid.seq_num,aid.usage_rate,aid.comments,aid.usage_units, aid.debit,ser.usage_based,ser.is_order_based,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid  ON ser.service_no = aid.service_no   WHERE aid.invoice_no = %s  AND aid.seq_num = %s AND (ser.is_surcharge        = 0 OR ser.is_surcharge IS NULL)  AND ser.is_setup   =0  AND ser.is_tax = 0  AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a "
		String recuringQry = "select param_val from ariacore.all_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		ResultSet rs= db.executePlaQuery("Select aid.seq_num from ariacore.all_invoice_details aid JOIN ariacore.services ser ON aid.service_no=ser.service_no where aid.invoice_no="+invoiceNo+" AND ser.is_setup=0 AND ser.is_tax = 0 AND (ser.is_surcharge=0 OR ser.is_surcharge IS NULL) AND (ser.custom_to_client_no="+clientNo+ " OR ser.custom_to_client_no IS NULL) order by  ser.recurring desc,ser.is_order_based desc,ser.usage_based desc")

		while(rs.next())
		{
			String num = rs.getString(1)
			gl_seq_num.add(num)
		}

		for(int row=1 ; row<=gl_seq_num.size() ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))

			hm = db.executeQueryP2(String.format(generalQry,recRateScale,usgRateScale,invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))

			String symbol = db.executeQueryP2("Select currency_htm_symbol from ariacore.currency  where currency_cd=(select currency_cd from ariacore.acct where acct_no="+accountNo+")")

			if(hm.containsKey("UNIT_PRICE"))
			{
				if(hm.get("UNIT_PRICE").toString().equals(" "))
				{
					logi.logInfo("unit price is space")
					hm.put("UNIT_PRICE","")
				}
				else
				{
					String pname = symbol.toString()+" "+hm.get("UNIT_PRICE").toString()
					hm.put("UNIT_PRICE",pname)
				}

			}
			if(hm.get("QTY").toString().equals(" "))
			{
				logi.logInfo("qty is space")
				hm.put("QTY","")
			}
			if(hm.containsKey("TOTAL"))
			{
				String pname = symbol.toString()+hm.get("TOTAL").toString()
				hm.put("TOTAL",pname)
			}
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())
		return mDB;
	}

	public LinkedHashMap uv_getTotalSummaryItemsFromStatements_ST_211(String tcid)
	{

		logi.logInfo("Calling uv_getTotalSummaryItemsFromStatements_ST_211")

		List keyValues = [
			"CURRENCY_CODE",
			"SUB_TOTAL",
			"TAX",
			"TOTAL",
			"AMOUNT_DUE"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap internalHM = new HashMap<String,String>()
		String invoiceNo = ""

		int seqNum = driver.findElements(By.xpath("//*[@id='content']/table[4]/tbody/tr/td[2]/table/tbody/tr")).size()
		logi.logInfo("Seq num count is :: "+seqNum)

		for(int i=1 ; i<=seqNum ; i++){

			String expath = "//*[@id='content']/table[4]/tbody/tr/td[2]/table/tbody/tr[%s]//td[2]"
			String formattedXpath = String.format(expath,i)
			logi.logInfo("formattedXpath : "+formattedXpath)
			String uiValue = "NO VALUE"

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
					logi.logInfo("uiValue : "+uiValue)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}

			finalMap.put(keyValues.get(i-1),uiValue)
			finalMap.sort()
		}

		return finalMap
	}
	public String uv_getTotalDue_201(String tcid)
	{
		logi.logInfo("Calling uv_getTotalDue_201")
		String TotalDue_201="//table[2]//td[2]"
		String fullText = driver.findElement(By.xpath(TotalDue_201)).getText()
		logi.logInfo("fullText "+fullText)
		String value=fullText.split("Total Amount Due: ")[1]
		return value.trim()
	}

	public String uv_getPaymentDue_202(String tcid)
	{
		logi.logInfo("Calling uv_getPaymentDue_202")
		String TotalDue_201="//table[2]//td[2]"
		String fullText = driver.findElement(By.xpath(TotalDue_201)).getText()
		logi.logInfo("fullText "+fullText)
		String value=fullText.split("Payment Due by ")[1]
		value= value.split("Total Amount Due:")[0]
		return value.trim()
	}
	public String uv_getMaskedFormPayment_exp_201(String tcid)
	{
		String ccnumber=u.getValueFromRequest("create_acct_complete","//cc_number")
		return uv_getStmtMaskedFormPayment_exp_201(ccnumber)
	}
	public String uv_getMaskedFormPaymentForChild_exp_201(String tcid)
	{
		String ccnumber=u.getValueFromRequest("create_acct_complete.a","//cc_number")
		return uv_getStmtMaskedFormPayment_exp_201(ccnumber)
	}


	public String uv_getMaskedFormPaymentForAch_exp_201(String tcid)
	{
		String bank_acct_no=u.getValueFromRequest("create_acct_complete","//bank_acct_no")
		return uv_getStmtMaskedFormPaymentForAch_exp_201(bank_acct_no)
	}

	public String uv_getChildMaskedFormPayment_exp_201(String tcid)
	{
		String ccnumber=u.getValueFromRequest("create_acct_complete.a","//cc_number")
		return uv_getStmtMaskedFormPayment_exp_201(ccnumber)
	}

	public String uv_getStmtMaskedFormPayment_exp_201(String ccnumber)
	{
		logi.logInfo("Calling uv_getStmtMaskedFormPayment_exp_201")
		logi.logInfo("ccnumber "+ccnumber)
		String val
		if(ccnumber=='NoVal')
			val='{None}'
		else
		{
			String mask= ccnumber.substring(12,16)
			String t="(Visa) ************"
			val= t+mask
		}
		return val
	}
	public String uv_getStmtMaskedFormPaymentForAch_exp_201(String bank_acct_no)
	{
		logi.logInfo("Calling uv_getStmtMaskedFormPaymentForAch_exp_201")
		logi.logInfo("bank_acct_no "+bank_acct_no)
		String val
		if(bank_acct_no=='NoVal')
			val='{None}'
		else
		{
			String mask= bank_acct_no.substring(4,7)
			String t="Checking Acount: (BANKNORTH, NA) ************"
			val= t+mask
		}
		return val
	}
	public String uv_getMaskedFormPayment_ui_201(String tcid)
	{
		logi.logInfo("Calling uv_getMaskedFormPayment_ui_201")
		String xpath="//div[@id='content']"
		String txt1=driver.findElement(By.xpath(xpath)).getText()
		txt1=txt1.split("Your current form of payment is ")[1]
		return txt1
	}
	public LinkedHashMap uv_getBillingAdressLineItem_ST_201(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_ST_201")
		String addr_line_items="//*[@id='content']/table[3]/tbody/tr/td[1]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt1=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		logi.logInfo"Plaintext"
		logi.logInfo txt1
		txt1=txt1.replace("\n", "").replace("\r", "")
		txt1=txt1.replace("</p>","")
		txt1= txt1.trim()
		txt1.replaceAll("","")
		logi.logInfo"****************"
		logi.logInfo txt1
		LinkedHashMap<String,String> addrDetails_exp=new LinkedHashMap<String,String>()
		List <String> addrLines=txt1.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		logi.logInfo "addrLines list  size"+addrLines.size().toString()
		List <String> finaladdrLines=[]
		addrLines.each  {line ->
			logi.logInfo "line--"+line
			if(!line.trim().equals(""))
			{
				logi.logInfo("adding to new loop")
				finaladdrLines.add(line.trim().toString())
				//logi.logInfo "finaladdrLines list in if111 "+finaladdrLines.toString()

			}
			else
			{
				logi.logInfo "elseline--"+line
			}
		}

		int max=finaladdrLines.size()
		int counter=6
		String val

		int b=max-1
		while(b>=0)
		{
			counter=counter-1
			String key="ADDR_LINE"+counter.toString()
			//logi.logInfo "key "+key
			if(!finaladdrLines.get(b).trim().equals(""))
			{
				val=finaladdrLines.get(b).trim()
			}
			//logi.logInfo "val "+val
			addrDetails_exp.put(key,val)
			if(b==0)
				break
			else
				b--
		}
		logi.logInfo addrDetails_exp.toString()
		return addrDetails_exp.sort()
	}

	public LinkedHashMap uv_getWTaxlAllItemsLoop_ST_201(String tcid)

	{

		logi.logInfo("Calling uv_getAllWTaxItemsItems_ST_201")
		String wTaxLoopTable="//table[4]//td[@style='padding:5px']/.."
		LinkedHashMap<String,String> wTaxLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'FROM_DATE',
			'TO_DATE',
			'QUANTITY',
			'DESCRIPTION',
			'PRICE',
			'TAX',
			'PERCENT_OF_PERIOD',
			'TOTAL_PRICE'
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(wTaxLoopTable));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				wTaxLoopItems.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return wTaxLoopItems

	}
	public LinkedHashMap uv_getWTaxlAllItemsLoop_DB_201(String tcid)

	{

		logi.logInfo("Calling uv_getWTaxlAllItemsLoop_DB_201")
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String ItemsCountQuery1= "select count(*) from ariacore.all_invoice_details where invoice_no=%s "
			int itemscount1=db.executeQueryP2(String.format(ItemsCountQuery1,invoiceNo)).toString().toInteger()
			//String query4="select replace((case when to_char(trunc(start_date)) is null then ' ' else to_char(trunc(start_date))  end),'')  as FROM_DATE, replace((case when to_char(trunc(end_date)) is null then ' ' else to_char(trunc(end_date))  end),'')  as TO_DATE , replace((case when to_char(base_plan_units) is null then ' ' else to_char(base_plan_units)  end),'')   as QUANTITY, (plan_name||' '||comments) as DESCRIPTION,'%s' || ' ' ||trim(to_char(NVL(usage_rate,0),'99,990.99')) as PRICE, '%s' || ' ' || '%s' as TAX,replace((case when to_char(NVL(proration_factor*100,0),'99,990.99') is null then ' ' else (to_char(NVL(proration_factor*100,0) ,'99,990.99') ) end),'')  as PERCENT_OF_PERIOD,usage_rate+%s as TOTAL_PRICE from ariacore.all_invoice_details where invoice_no=%s and seq_num=%s"

			String recuringRateScale = ""
			String usageRateScale = ""
			int tempUsageRate = 0
			int recRateScale = 0
			int usgRateScale = 0

			String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
			String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
			String usageRateQry = "select NVL(usage_rate,0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"

			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			for(int a=1 ; a<=itemscount1 ; a++){
				counter=counter+1

				recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
				logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

				tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,a)).toString().toDouble().toInteger()
				logi.logInfo("tempUsageRate is :: "+tempUsageRate)

				usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
				logi.logInfo("usageRateScale num is :: "+usageRateScale)

				if(tempUsageRate != null){
					logi.logInfo("Inside not null statement")
					recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				} else{
					logi.logInfo("Inside not null else statement")
					recRateScale = Integer.parseInt(recuringRateScale)
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = Integer.parseInt(usageRateScale)
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				}
				String taxQuery="select  trim(TO_CHAR(NVL(sum(debit),0),'99,990.99')) from ariacore.gl_tax_detail where invoice_no=%s and is_excluded=0 and taxed_seq_num=%s"
				String taxValue=db.executeQueryP2(String.format(taxQuery,invoiceNo,a)).toString()
				String orderlabelQuery="select alt_label  from ariacore.order_items  where item_no= (select item_no from ariacore.all_invoice_details where invoice_no= %s and seq_num= %s) and order_no=(select order_no from ariacore.all_invoice_details where invoice_no = %s and seq_num  = %s)and client_no=%s"
				String olable= db.executeQueryP2(String.format(orderlabelQuery,invoiceNo,a,invoiceNo,a,clientNo))
				String query4="SELECT REPLACE((   CASE    WHEN TO_CHAR(TRUNC(a.start_date)) IS NULL    THEN ' '    ELSE TO_CHAR(TRUNC(a.start_date),'dd-mm-yyyy')  END),'') AS FROM_DATE,  REPLACE((  CASE  WHEN TO_CHAR(TRUNC(a.end_date)) IS NULL    THEN ' '    ELSE TO_CHAR(TRUNC(a.end_date),'dd-mm-yyyy')  END),'') AS TO_DATE ,  REPLACE((  CASE    WHEN TO_CHAR(a.base_plan_units) IS NULL    THEN ' '    ELSE TO_CHAR(a.base_plan_units)  END),'') AS QUANTITY,   ( CASE when a.orig_coupon_cd is not null then a.comments  when a.is_order_based=1    Then '%s'  else (a.plan_name ||' ' ||a.comments)   END   ) AS DESCRIPTION, '%s'|| ' '|| (CASE    WHEN a.usage_rate =0    AND a.service_no  =0    THEN '0.00'    WHEN a.recurring=1    THEN (      CASE        WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %s, 0) ELSE RPAD(a.usage_rate ||'.', %s, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate||'.00')WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99'))  WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00')  WHEN a.is_tax = 1 THEN '0.00' END) AS PRICE,   '%s'  || ' '  ||  case when a.taxdebit is not null then TRIM(TO_CHAR (a.taxdebit,'99,990.99')) else '0.00' end  AS TAX,REPLACE(( CASE  WHEN TO_CHAR(NVL(a.proration_factor*100,0),'99,990.99') IS NULL  THEN ' '  ELSE (TO_CHAR(NVL(a.proration_factor*100,0) ,'99,990.99') )  END),'') || ' '||chr(37)  AS PERCENT_OF_PERIOD, '%s ' || case when a.taxdebit is not null then trim(TO_CHAR((a.debit+a.taxdebit),'99,990.99')) else trim(TO_CHAR((a.debit),'99,990.99'))  end AS TOTAL_PRICE  FROM  (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.proration_factor, aid.orig_coupon_cd, aid.orig_credit_id ,aid.base_plan_units, aid.start_date, aid.end_date, ser.usage_based,  aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no ,Case when ( orig_coupon_cd is not null or orig_credit_id is not null ) then 0.00 else gtl.debit end as taxdebit FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no  left join ariacore.gl_tax_Detail gtl on  aid.invoice_no=gtl.invoice_no   and aid.seq_num=gtl.taxed_seq_num   WHERE aid.invoice_no  = %s  AND aid.seq_num = %s  and ser.is_tax=0 AND (ser.custom_to_client_no=%s  OR ser.custom_to_client_no IS NULL))a"
				hm = db.executeQueryP2(String.format(query4,olable,symbol,recRateScale,recRateScale,usgRateScale,symbol,symbol,invoiceNo,a,clientNo))
				//hm = db.executeQueryP2(String.format(query4,symbol,symbol,taxValue,taxValue,invoiceNo,a))
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				logi.logInfo("Individual row of hash after adding all items :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
		}

		catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB

	}
	public String getTextUIWithIndex(String xpath)
	{
		String splitted_xpath = null;
		String index = xpath.split('#')[0].split("text-")[1]
		splitted_xpath = xpath.split('#')[1]
		String completeValue
		String finalValue
		logi.logInfo("Splitted xpath is : "+splitted_xpath)
		logi.logInfo("Splitted index : "+index)
		String newvalues=[]
		try {
			completeValue = driver.findElement(By.xpath(splitted_xpath)).getText()
			logi.logInfo("Got Value is  : "+completeValue)
			completeValue=completeValue.replace("\n", "##")
			logi.logInfo("Got Value after replace  : "+completeValue)
			String [] values=completeValue.split("##")
			logi.logInfo("values ARE  : "+values)
			logi.logInfo("values size  : "+values.size().toString())
			if(values.size()==3)
			{
				newvalues[0]=''
				newvalues.addAll(values)
				finalValue=newvalues[index.toInteger()-1]
			}else
				finalValue=values[index.toInteger()-1]

		} catch (Exception e) {
			logi.logInfo( e.printStackTrace())
			finalValue ="NO VALUE"

		}

		return finalValue
	}
	public LinkedHashMap uv_getOrderItemsLoop_ST_201(String tcid)

	{

		logi.logInfo("Calling uv_getOrderItemsLoop_ST_201")
		String wTaxLoopTable="//div[@id='content']/table[6]/tbody/tr//td[not(contains(@style,'font-weight:bold'))]/.."
		LinkedHashMap<String,String> orderLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'LINE_NO',
			'ITEM_NO',
			'SKU',
			'LABEL',
			'DESCRIPTION',
			'UNITS',
			'RATE',
			'AMOUNT'
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(wTaxLoopTable));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				hm=hm.sort()
				orderLoopItems.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return orderLoopItems.sort()

	}
	public LinkedHashMap uv_getOrderItemsLoop_DB_201(String tcid)
	{
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtOrderItemsLoop_DB_201(acct_no)
	}
	public LinkedHashMap uv_getChildOrderItemsLoop_DB_201(String tcid)
	{
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getStmtOrderItemsLoop_DB_201(acct_no)
	}
	public LinkedHashMap uv_getStmtOrderItemsLoop_DB_201(String acct_no)

	{

		logi.logInfo("Calling uv_getStmtOrderItemsLoop_DB_201")
		LinkedHashMap orderItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()

			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String ItemsCountQuery1= "select count(*) from  ARIACORE.ORDER_ITEMS ots join ariacore.orders o on o.order_no=ots.order_no and o.client_no=ots.client_no join ARIACORE.inventory_items its on its.item_no=ots.item_no and its.client_no=ots.client_no where o.invoice_no=%s"
			int itemscount1=db.executeQueryP2(String.format(ItemsCountQuery1,invoiceNo)).toString().toInteger()

			for(int a=1 ; a<=itemscount1 ; a++){
				counter=counter+1
				String query4="select ots.line_no as LINE_NO,ots.item_no as ITEM_NO,its.client_sku as SKU,ots.alt_label as LABEL ,its.long_desc as DESCRIPTION,ots.units as UNITS,trim(to_char(ots.unit_amount,'99,990.99') )as RATE,trim(to_char(ots.line_amount,'99,990.99') ) as AMOUNT from  ARIACORE.ORDER_ITEMS ots join ariacore.orders o on o.order_no=ots.order_no and o.client_no=ots.client_no join ARIACORE.inventory_items its on its.item_no=ots.item_no and its.client_no=ots.client_no where o.invoice_no=%s and  ots.line_no=%s"
				hm = db.executeQueryP2(String.format(query4,invoiceNo,a))
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				hm=hm.sort()
				logi.logInfo("Individual row of hash after adding all items and sorted:: "+hm.toString())
				orderItemsDB.put("Line Item"+counter,hm)

			}
		}

		catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return orderItemsDB.sort()

	}

	public String uv_getOrderComments_DB_201(String tcid)
	{
		logi.logInfo("Calling uv_getOrderItemsLoop_DB_201")
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String val=db.executeQueryP2("SELECT REPLACE((   CASE    WHEN comments IS NULL    THEN ' '    ELSE comments  END),'') AS FROM_DATE FROM ARIACORE.ORDERS WHERE ACCT_NO="+acct_no)
		return val.trim()
	}

	public LinkedHashMap uv_getCreditActivtyLoopItems_ST_201(String tcid)

	{
		logi.logInfo("Calling uv_getCreditActivtyLoopItems_ST_201")
		String wTaxLoopTable="//div[@id='content']/table[11]/tbody/tr//td[not(contains(@style,'font-weight:bold'))]/.."
		LinkedHashMap<String,String> creditActivtyLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'CREDITDESCRIPTION',
			'CREDITREASONTEXT',
			'CREDITREASONCODE',
			'CREDITCOMMENT',
			'CREDITUNAPPLIEDAMT',
			'CREDITSHORTDESCRIPTION',
			'CREDITDATE',
			'CREDITSTARTDATE',
			'CREDITENDDATE',
			'CREDITCOUPONCODE',
			'CREDITAMOUNT'
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(wTaxLoopTable));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				LinkedHashMap hm = new LinkedHashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				hm=hm.sort()
				creditActivtyLoopItems.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}


		return creditActivtyLoopItems

	}

	public LinkedHashMap uv_getSTmtCreditActivtiyLoopItems_DB_201(String acct_no)
	{
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		try {
			logi.logInfo("Calling uv_getSTmtCreditActivtiyLoopItems_DB_201")

			LinkedHashMap hm = new LinkedHashMap<String,String>()

			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
			// Voided invoice
			String paymnetsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount=db.executeQueryP2(String.format(paymnetsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query2="SELECT a.description || ' '||%s|| ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||chr(38)||'nbsp;' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount ; a++){

				hm = db.executeQueryP2(String.format(query2,voidinvoice,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided invoice :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					hm=hm.sort()
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)
				}
			}
			// Voided refunds
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||chr(38)||'nbsp;' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					hm=hm.sort()
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}

			//Coupons
			String couponsCountquery="select count(*) from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and orig_credit_id is null"
			int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
			String couponquery="select comments|| ' (applied ' || to_char(start_Date,'dd-mm-yyyy')||')' as CREDITDESCRIPTION ,'Coupon Application' as CREDITREASONTEXT ,'9999' as CREDITREASONCODE ,comments as CREDITCOMMENT,'0.00'as CREDITUNAPPLIEDAMT  ,to_char(start_date,'dd-mm-yyyy') as CREDITDATE , to_char(start_date,'dd-mm-yyyy') as CREDITSTARTDATE ,to_char(end_date,'dd-mm-yyyy') as CREDITENDDATE,orig_coupon_cd as CREDITCOUPONCODE ,trim(to_char(debit,'99,990.99')) as CREDITAMOUNT ,comments as CREDITSHORTDESCRIPTION from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and seq_num=%s"
			for(int a=1 ; a<=couponsCount ; a++){
				counter=counter+1
				String recseq="SELECT seq_num FROM   (SELECT seq_num, row_number() over (order by seq_num ) AS sno   FROM ariacore.all_invoice_details WHERE invoice_no=%s  AND orig_coupon_cd is not null  )a WHERE sno=%s"
				String lno=db.executeQueryP2(String.format(recseq,invoiceNo,a))
				hm = db.executeQueryP2(String.format(couponquery,invoiceNo,lno))
				logi.logInfo("Individual row of hash after adding coupons :: "+hm.toString())
				hm=hm.sort()
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			//ServiceCredits
			String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
			String serviceCreditsCountQuery="select count(*) from ariacore.all_credits where acct_no=%s and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s )"
			int rows=db.executeQueryP2(String.format(serviceCreditsCountQuery,acct_no,invoiceNo)).toInteger()
			String query1="select CreditReasonText,CREDITDESCRIPTION,CREDITDATE,CREDITSTARTDATE,CREDITENDDATE,CreditReasonCode,CreditComment,CreditUnappliedAmt,CreditShortDescription ,CreditCouponCode ,CreditAmount from (select case when credit_type='S' then 'Service Credit' end as CreditShortDescription ,reason_cd as CreditReasonCode, '%s' as CREDITDATE,replace((case when TO_CHAR(TRUNC( proration_related_start_date) ,'dd-mm-yyyy') is null then ' ' else TO_CHAR(TRUNC( proration_related_start_date) ,'dd-mm-yyyy')  end),'')  as CREDITSTARTDATE, replace((case when TO_CHAR(TRUNC( proration_related_end_date) ,'dd-mm-yyyy') is null then ' ' else TO_CHAR(TRUNC( proration_related_end_date) ,'dd-mm-yyyy')  end),'') as CREDITENDDATE,reason_text as CreditReasonText,'Service Credit (applied ' || '%s' || ')' as CREDITDESCRIPTION, comments as CreditComment,trim(to_char(left_to_apply,'99,990.99')) as CreditUnappliedAmt ,replace((case when orig_coupon_cd is null then ' ' else orig_coupon_cd  end),'') as CreditCouponCode, '-'||trim(to_char(amount,'99,990.99')) as CreditAmount,row_number() over(order by credit_id desc) as seq from ariacore.all_credits where acct_no=%s  and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s  ) )a where seq=%s"
			for(int row=1 ; row<=rows ; row++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query1,glbillDate,glbillDate,acct_no,invoiceNo,row))
				logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				hm=hm.sort()
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)


			}
			logi.logInfo( "counter value : "+counter.toString())

			//payments
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||chr(38)||'nbsp;' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				hm=hm.sort()
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB
	}
	public LinkedHashMap uv_getRecLoopItems_ST_201(String tcid)

	{
		logi.logInfo("Calling uv_getRecLoopItems_ST_201")
		String recLoopTable="//div[@id='content']/table[9]/tbody/tr//td[not(contains(@style,'font-weight:bold'))]/.."
		LinkedHashMap<String,String> recLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'PLAN_NAME',
			'START_DATE',
			'END_DATE',
			'UNITS',
			'RATE',
			'CREDIT'
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(recLoopTable));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				LinkedHashMap hm = new LinkedHashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				hm=hm.sort()
				recLoopItems.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}


		return recLoopItems

	}
	public LinkedHashMap uv_getRecLoopItems_DB_201(String tcid){

		String accountNo=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtRecLoopItems_DB_201(accountNo)
	}

	public LinkedHashMap uv_getChildRecLoopItems_DB_201(String tcid){

		String accountNo=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getStmtRecLoopItems_DB_201(accountNo)
	}

	public LinkedHashMap uv_getStmtRecLoopItems_DB_201(String accountNo)
	{

		logi.logInfo("Calling uv_getStmtRecLoopItems_DB_201")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		LinkedHashMap hm = new LinkedHashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		int recRateScale = 0
		int tempUsageRate=0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select count(*) from ariacore.all_invoice_details where invoice_no=%s and service_no in (select service_no from ariacore.services where recurring=1) and orig_coupon_cd is null and orig_credit_id is  null and  surcharge_rate_seq_no is null"
		//String generalQry = "SELECT comments, (CASE WHEN (START_DATE IS NULL OR END_DATE IS NULL) THEN '-' ELSE  to_number(TO_CHAR(start_date,'dd'))  || '-'  || to_number(TO_CHAR(start_date,'mm'))  || '-'  ||to_number(TO_CHAR(start_date,'yyyy')) || ' - ' ||(TO_CHAR(end_date,'dd'))  || '-'  || to_number(TO_CHAR(end_date,'mm'))  || '-'  ||to_number(TO_CHAR(end_date,'yyyy')) END) AS Result, usage_units, usage_rate, debit  FROM ariacore.all_invoice_details WHERE invoice_no =%s and seq_num=%s "
		String generalQry = " select * from (SELECT replace((case when plan_name is null then ' ' else plan_name  end),'')  as plan_name,to_char(trunc(start_date)  ,'dd-mm-yyyy')as START_DATE,to_char(trunc(end_Date),'dd-mm-yyyy') as END_DATE,base_plan_units as UNITS,'%s '|| (case when regexp_like(trim(usage_rate), '[.]')    THEN RPAD(usage_rate, %s, 0)     ELSE RPAD(usage_rate||'.', %s, 0)   end) as RATE , seq_num  as sno from ariacore.all_invoice_details where invoice_no=%s and service_no in (select service_no from ariacore.services where recurring=1) and orig_coupon_cd is  null and orig_credit_id is  null  and surcharge_rate_seq_no is null )where sno=%s"

		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"

		String usageRateQry = "select NVL(usage_rate,0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		ConnectDB db = new ConnectDB()
		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+accountNo+")")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo))
		logi.logInfo("Seq num count is :: "+seqCount)

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){
			String recseq="SELECT seq_num FROM   (SELECT seq_num, row_number() over (order by seq_num ) AS sno   FROM ariacore.all_invoice_details WHERE invoice_no=%s   AND service_no IN     (SELECT service_no FROM ariacore.services WHERE recurring=1     )   )a WHERE sno=%s"
			String lno=db.executeQueryP2(String.format(recseq,invoiceNo,row))
			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)
			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,lno)).toString().toDouble().toInteger()
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)


			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)

			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)

			}

			hm = db.executeQueryP2(String.format(generalQry,symbol,recRateScale,recRateScale,invoiceNo,lno))
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			hm=hm.sort()
			hm.remove("SNO")
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}
	public LinkedHashMap uv_getTotalSummaryItemsFromStatements_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtTotalSummaryItemsFromStatements_DB_211(tcid,accountNo)
	}
	public LinkedHashMap uv_getChildTotalSummaryItemsFromStatements_DB_211(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getStmtTotalSummaryItemsFromStatements_DB_211(tcid,accountNo)
	}

	public LinkedHashMap uv_getStmtTotalSummaryItemsFromStatements_DB_211(String tcid,String accountNo)
	{

		logi.logInfo("Calling uv_getStmtTotalSummaryItemsFromStatements_DB_211")


		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		ConnectDB db = new ConnectDB()
		String childAcctNo = "null"
		String currecny_cd=""
		String GLBaseDebit = "0.00"
		String GLTaxDebit = "0.00"
		String GLOnlyDebit = "0.00"
		String GLBalDue = "0.00"

		currecny_cd = db.executeQueryP2("Select currency_cd from ariacore.currency where currency_cd=(Select currency_cd from ariacore.acct where acct_no="+accountNo+")")
		String senAcctNo = db.executeQueryP2("Select senior_acct_no from ariacore.acct where acct_no="+accountNo).toString()
		if(senAcctNo.equals("null"))
		{
			logi.logInfo("This is parent acct")
			int child_cnt = db.executeQueryP2("Select count(acct_no) from ariacore.acct where senior_acct_no= "+accountNo).toInteger()
			if(child_cnt > 0)
			{
				childAcctNo = db.executeQueryP2("Select acct_no from ariacore.acct where senior_acct_no= "+accountNo).toString()
			}
			else
				childAcctNo = "null"
		}

		else if(!senAcctNo.equals("null"))
		{
			logi.logInfo("This is child acct")
		}

		if(childAcctNo.equals("null"))
		{
			String invoiceNo = u.getInvoiceNoVT(clientNo, accountNo).toString();
			GLBaseDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' ) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+invoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=0")
			logi.logInfo("GLBaseDebit :"+GLBaseDebit)
			GLTaxDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' ) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+invoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=1")
			logi.logInfo("GLTaxDebit :"+GLTaxDebit)
		}
		else
		{
			String pinvoiceNo = u.getInvoiceNoVT(clientNo, accountNo).toString();
			String cinvoiceNo = u.getInvoiceNoVT(clientNo, childAcctNo).toString();
			String PGLBaseDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' ) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+pinvoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=0")
			String CGLBaseDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' ) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+cinvoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=0")
			GLBaseDebit = (PGLBaseDebit.toDouble()+CGLBaseDebit.toDouble()).toString()
			logi.logInfo("GLBaseDebit-parent:child :"+GLBaseDebit)

			String PGLTaxDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' )from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+pinvoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=1")
			String CGLTaxDebit = db.executeQueryP2("Select TO_CHAR(NVL(SUM(glt.debit),0),'99,990.99' ) from ariacore.gl_detail glt JOIN ariacore.services ser ON glt.service_no= ser.service_no where glt.invoice_no="+cinvoiceNo+" and glt.client_no=ser.custom_to_client_no and glt.client_no="+clientNo+" and ser.is_tax=1")
			GLTaxDebit = (PGLTaxDebit.toDouble() + CGLTaxDebit.toDouble()).toString()
			logi.logInfo("GLTaxDebit-parent:child :"+GLTaxDebit)

		}
		String invoiceNo = u.getInvoiceNoVT(clientNo, accountNo).toString();
		GLOnlyDebit =db.executeQueryP2("Select TO_CHAR(NVL(SUM(debit),0),'99,990.99' ) from ariacore.gl_detail where invoice_no="+invoiceNo+" and client_no="+clientNo).toString()
		logi.logInfo("GLOnlyDebit :"+GLOnlyDebit)
		GLBalDue = GLOnlyDebit
		logi.logInfo("GLBalDue :"+GLBalDue)
		String curSymbol = db.executeQueryP2("Select currency_htm_symbol from ariacore.currency where currency_cd=(Select currency_cd from ariacore.acct where acct_no="+accountNo+")")
		logi.logInfo("curSymbol :"+curSymbol)

		hm.put("CURRENCY_CODE", currecny_cd.toUpperCase())
		hm.put("SUB_TOTAL", curSymbol+(GLBaseDebit).trim())
		hm.put("TAX", curSymbol+(GLTaxDebit).trim())
		hm.put("TOTAL", curSymbol+(GLOnlyDebit).trim())
		hm.put("AMOUNT_DUE", curSymbol+(GLBalDue).trim())

		logi.logInfo("Summary hash expected :"+hm.toString())

		return hm.sort()
	}
	public LinkedHashMap uv_getBillingAdressLineItem_DB_214(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_DB")
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		return uv_stmt_getBillingAdressLineItem_DB_214(acct_no)
	}

	public LinkedHashMap uv_getChildBillingAdressLineItem_DB_214(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_DB")
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")

		return uv_stmt_getBillingAdressLineItem_DB_214(acct_no)
	}

	public LinkedHashMap uv_getChildCreditActivtyLoopItems_DB_201(String tcid)
	{

		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_getSTmtCreditActivtiyLoopItems_DB_201 (acct_no)
	}

	public def uv_isUsageBilledInFirstInvoice_106(String tcid){
		logi.logInfo ("Calling uv_isUsageBilledInFirstInvoice_106")
		boolean successFlag = false
		List resultList = []

		ConnectDB db = new ConnectDB()
		String invoiceNo = ""
		List usageDetails = []

		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String invoiceQry = "select min(invoice_no) from ariacore.gl where acct_no ="+accountNo
		String query = "select PLAN_NAME ||' ' ||comments from ariacore.all_invoice_details where invoice_no = %s and usage_type is not null and orig_coupon_cd is null"

		invoiceNo = db.executeQueryP2(invoiceQry)
		logi.logInfo("Invoice number for statement is : "+invoiceNo)

		usageDetails = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(query, invoiceNo)))
		logi.logInfo("Usage details list : "+usageDetails.toString())

		if(!usageDetails.isEmpty()){
			logi.logInfo ("Inside list is not empty loop")
			int seqNum = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[@class='lineitem']]")).size()
			logi.logInfo("Seq num count is :: "+seqNum)

			for(int i=1 ; i<=seqNum ; i++){
				int rowVal = i+2
				logi.logInfo ("The value of i is "+i+" and incremented is "+rowVal)

				String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[1]"
				String formattedXpath = String.format(expath,rowVal)
				logi.logInfo("formattedXpath : "+formattedXpath)
				String uiValue = "NO VALUE"

				try {
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}
				resultList.add(uiValue)
			}
			logi.logInfo("List is :: "+resultList.toString())
			if(resultList.containsAll(usageDetails)){
				successFlag = true
			}
		}
		return successFlag.toString().toUpperCase()
	}

	public LinkedHashMap uv_getInvoiceLineItemsFromStatementsUI_Adobe(String tcid){

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements")

		List keyValues = [
			"DESCRIPTION",
			"UNIT_PRICE",
			"LICENSES",
			"NET_PRICE"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap internalHM = new HashMap<String,String>()
		String invoiceNo = ""
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo


		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		int seqNum = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[contains(td,' ')]")).size()
		logi.logInfo("Seq num count is :: "+seqNum)

		for(int i=1 ; i<=seqNum ; i++){

			internalHM = new HashMap<String,String>()
			for(int k=1 ; k<=4 ; k++){
				int rowVal = i+1
				logi.logInfo ("The value of i is "+i+" and incremented is "+rowVal)

				String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"
				String formattedXpath = String.format(expath,rowVal,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				String uiValue = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				internalHM.put(keyValues.get(k-1),uiValue)
				internalHM.sort()

			}

			finalMap.put("Line Item"+i , internalHM)
			logi.logInfo("Final hash at the time of sequence no "+i+" is : "+finalMap)
		}
		return finalMap
	}

	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatementsDB_Adobe(String tcid){

		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatementsDB_Adobe")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select count(seq_Num) as COUNT from ariacore.gl_detail where invoice_no = %s and usage_type is null and length(service_no)>4 and service_no not in (select service_no from ariacore.all_service where is_surcharge=1 and client_no=%s) and orig_coupon_cd is null and orig_credit_id is null"
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		String generalQry = "SELECT case when a.orig_coupon_cd is not null then a.comments || ' (applied '|| '%s' || ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Licenses, (CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0  and  (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		String seqNumQry2 = "SELECT aid.seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND ser.is_tax = 0  AND ser.is_min_fee = 0  AND aid.usage_type IS NULL AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) order by seq_num"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,clientNo))
		logi.logInfo("Seq num is :: "+seqCount)

		List seqNum = []
		seqNum = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(seqNumQry2, invoiceNo, clientNo)))

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,seqNum.get(row-1)))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,glbillDate,recRateScale,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,seqNum.get(row-1),clientNo,clientNo))

			hm = new HashMap<String,String>()
			hm = db.executeQueryP2(String.format(generalQry,glbillDate,recRateScale,recRateScale,usgRateScale,invoiceNo,seqNum.get(row-1),clientNo,clientNo))
			if(hm.get("Licenses").equals(" "))
			{
				logi.logInfo("Units has space")
				hm.put("Licenses","")
			}
			if(hm.containsKey("Description"))
			{
				String pname = hm.get("Description").toString().trim()
				hm.put("Description",pname)
			}
			hm=hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;
		}
		temp++
		logi.logInfo("mDB mDB mDB mDB :: "+mDB.toString())

		//Service Credits
		String countquery="select count(sno) from (SELECT debit,row_number() over (order by debit) as sno from ariacore.gl_detail where invoice_no = %s and usage_type is null and length(service_no)>4 and service_no not in (select service_no from ariacore.all_service where is_surcharge=1 and client_no=%s) and (orig_coupon_cd is not null or orig_credit_id is not null) )"
		int creditrows= db.executeQueryP2(String.format(countquery,invoiceNo,clientNo)).toInteger()
		logi.logInfo("creditrows " +creditrows)
		String generalcreditquery="SELECT case when a.orig_coupon_cd is not null then a.comments || ' (applied '|| '%s' || ')'  when a.orig_credit_id  is not null  THEN 'Service Credit' || ' (applied ' || '%s'|| ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description,(CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,aid.orig_credit_id,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val ,row_number() over (order by aid.debit) as sno FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND  (aid.orig_coupon_cd is not null or aid.orig_credit_id is not null)  and ser.is_tax = 0  and (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a where sno=%s"
		HashMap hms = new HashMap<String,String>()
		for(int row=1 ; row<=creditrows ; row++){
			hms = db.executeQueryP2(String.format(generalcreditquery,glbillDate,glbillDate,invoiceNo,clientNo,clientNo,row))

			hms.each {k,v ->
				String v1= v.toString().trim()
				hms.put(k,v1)
			}
			hms.put("LICENSES",' ')
			hms.put("UNIT_PRICE",' ')
			hms=hms.sort()
			logi.logInfo("Individual row of hash is :: "+hms.toString())

			mDB.put("Line Item"+temp,hms)
			temp++



		}
		// Usage items
		HashMap hmu = new HashMap<String,String>()
		int usgRateScale1 = 0

		usageRateQry = "select a.usage_rate from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null)a where a.custom_seq_num=%s"
		String generalUsageQry =  "select TRIM(TO_CHAR(a.plan_name)) as DESCRIPTION, (case when regexp_like(trim(a.usage_rate), '[.]') then RPAD(a.usage_rate, %d, 0)   else  RPAD(a.usage_rate  ||'.', %d, 0)  end)  AS UNIT_PRICE,  replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.usage_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.is_min_fee = 0 and ser.usage_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		String totalCountQry = "select count(*) from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null and orig_coupon_cd is null)a"

		//int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		int totalCount = db.executeQueryP2(String.format(totalCountQry,invoiceNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row)).toString().toDouble().toInteger()
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			int tempUsageRateI = tempUsageRate.toString().length()
			logi.logInfo("tempUsageRate1 is :: "+tempUsageRateI)

			if(tempUsageRateI != null){
				logi.logInfo("Inside not null statement")
				usgRateScale = tempUsageRateI + Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))

			hmu = db.executeQueryP2(String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))
			hmu.each {k,v ->
				String v1= v.toString().trim()
				hmu.put(k,v1)
			}

			hmu=hmu.sort()
			logi.logInfo("Individual row of hash is :: "+hmu.toString())

			mDB.put("Line Item"+temp,hmu)
			temp++

		}

		// Payments

		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Description, a.Net_Price FROM (SELECT TRANS.DESCRIPTION ,('- '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS Net_Price , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			pHash.put("LICENSES", " ")
			pHash.put("UNIT_PRICE", " ")
			logi.logInfo("The payment related hash is : "+pHash.toString())

			//			pHash.each {k,v ->
			//				String v1= v.toString().trim()
			//				pHash.put(k,v1)
			//			}

			mDB.put("Line Item"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		// Tax Items
		String percent = "0.00"
		HashMap hmtax = new HashMap<String,String>()
		String lineQry = "select distinct(seq_num) from ariacore.gl_tax_detail where invoice_no = %s order by seq_num"
		String taxRateQry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = %s and seq_num = %s"
		//String taxItemsQry = "select s.comments AS Description ,sum(gtl.debit ) AS Net_Price, ' ' AS Unit_price, ' '  AS Licenses from ariacore.all_invoice_tax_details gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"
		String taxItemsQry = "select s.comments || ' %s' AS Description ,TRIM(TO_CHAR((sum(gtl.debit )),'99,990.99')) AS Net_Price from ariacore.gl_tax_detail gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"

		def totalLineItems = []

		totalLineItems = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(lineQry,invoiceNo)))
		logi.logInfo("Total Line items are : "+totalLineItems)
		for(int lines = 0;lines<totalLineItems.size();lines++){
			logi.logInfo("Inside for loop ")

			percent = db.executeQueryP2(String.format(taxRateQry,invoiceNo,totalLineItems.get(lines)))
			logi.logInfo("The Tax percentage is : "+percent.toString())
			percent = "0"+percent+"%"
			logi.logInfo("The Tax percentage after concat : "+percent.toString())

			hmtax = db.executeQueryP2(String.format(taxItemsQry,percent,invoiceNo,clientNo))
			hmtax.put("LICENSES", " ")
			hmtax.put("UNIT_PRICE", " ")
			logi.logInfo("The Tax related hash is : "+pHash.toString())

			mDB.put("Line Item"+temp,hmtax)
			temp++
			logi.logInfo("Individual Hash tax are : "+hmtax.toString())
		}

		logi.logInfo("Final hash is : "+mDB.toString())
		return mDB;
	}
	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII(String tcid){
		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		//String seqNumQry = "select count(seq_Num) as COUNT from ariacore.gl_detail where invoice_no = %s and usage_type is null and length(service_no)>4 and service_no not in (select service_no from ariacore.all_service where is_surcharge=1 and client_no=%s) and orig_coupon_cd is null and orig_credit_id is null"
		String seqNumQry = "select count(SEQ_NUM) as totalCount FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no           = aid.service_no  WHERE aid.invoice_no        = %s  AND ser.is_tax              = 0   AND aid.orig_credit_id     IS NULL   AND ser.is_setup            = 0   AND aid.usage_type         IS NULL   AND (ser.custom_to_client_no=%s   OR ser.custom_to_client_no IS NULL) "
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT case when a.orig_coupon_cd is not null then a.comments || ' (applied '|| '%s' || ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Licenses, (CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0  and  (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String generalQry = "SELECT case when a.is_order_based = 1 then ' ' else TRIM(TO_CHAR(a.plan_name)) end AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  when a.is_order_based = 1 then ' ' ELSE TO_CHAR(a.usage_units) END ) AS Licenses, TRIM(TO_CHAR(a.debit,'99,990.99')) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0 and aid.orig_credit_id is null  and ser.is_setup = 0 AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		String seqNumQry2 = "SELECT aid.seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND ser.is_tax = 0  AND ser.is_min_fee = 0 and ser.is_setup =0 and aid.orig_credit_id is null AND aid.usage_type IS NULL AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) order by seq_num"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,clientNo))
		logi.logInfo("Seq num is :: "+seqCount)

		List seqNum = []
		seqNum = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(seqNumQry2, invoiceNo, clientNo)))

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,seqNum.get(row-1)))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,seqNum.get(row-1),clientNo))
			hm = new HashMap<String,String>()
			hm = db.executeQueryP2(String.format(generalQry,recRateScale,recRateScale,usgRateScale,invoiceNo,seqNum.get(row-1),clientNo))

			if(hm.get("Licenses").equals(" "))
			{
				logi.logInfo("Units has space")
				hm.put("Licenses","")
			}
			if(hm.containsKey("Description"))
			{
				String pname = hm.get("Description").toString().trim()
				hm.put("Description",pname)
			}
			hm.sort()
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;
		}
		temp++
		logi.logInfo("mDB mDB mDB mDB :: "+mDB.toString())

		/*//Service Credits
		String countquery="select count(*) from (select row_number() over(ORDER BY orig_credit_id asc ) as sno  from ariacore.all_invoice_details where invoice_no=%s and orig_credit_id is not null and orig_credit_id in (select credit_id from  ariacore.credits where acct_no=%s) )"
		int creditrows= db.executeQueryP2(String.format(countquery,invoiceNo,accountNo)).toInteger()
		logi.logInfo("creditrows " +creditrows)
		String generalcreditquery="SELECT case when a.orig_coupon_cd is not null and  a.orig_credit_id is NULL then a.comments || ' (applied '|| '%s' || ')'  when a.orig_credit_id  is not null  THEN 'Service Credit' || ' (applied ' || '%s'|| ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description,(CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,aid.orig_credit_id,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val ,row_number() over (order by aid.debit) as sno FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND  (aid.orig_coupon_cd is not null or aid.orig_credit_id is not null)  and ser.is_tax = 0  and (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a where sno=%s"
		HashMap hms = new HashMap<String,String>()
		for(int row=1 ; row<=creditrows ; row++){
			hms = db.executeQueryP2(String.format(generalcreditquery,glbillDate,glbillDate,invoiceNo,clientNo,clientNo,row))

			hms.each {k,v ->
				String v1= v.toString().trim()
				hms.put(k,v1)
			}
			hms.put("LICENSES",' ')
			hms.put("UNIT_PRICE",' ')
			hms.sort()
			logi.logInfo("Individual row of hash is :: "+hms.toString())

			mDB.put("Line Item"+temp,hms)
			temp++



		}*/
		// Usage items
		HashMap hmu = new HashMap<String,String>()
		int usgRateScale1 = 0

		usageRateQry = "select a.usage_rate from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null)a where a.custom_seq_num=%s"
		String generalUsageQry =  "select TRIM(TO_CHAR(a.plan_name)) as DESCRIPTION, (case when regexp_like(trim(a.usage_rate), '[.]') then RPAD(a.usage_rate, %d, 0)   else  RPAD(a.usage_rate  ||'.', %d, 0)  end)  AS UNIT_PRICE,  replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.usage_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.is_min_fee = 0 and ser.usage_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		String totalCountQry = "select count(*) from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null and orig_coupon_cd is null)a"

		//int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		int totalCount = db.executeQueryP2(String.format(totalCountQry,invoiceNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row)).toString().toDouble().toInteger()
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			int tempUsageRateI = tempUsageRate.toString().length()
			logi.logInfo("tempUsageRate1 is :: "+tempUsageRateI)

			if(tempUsageRateI != null){
				logi.logInfo("Inside not null statement")
				usgRateScale = tempUsageRateI + Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))

			hmu = db.executeQueryP2(String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))
			hmu.each {k,v ->
				String v1= v.toString().trim()
				hmu.put(k,v1)
			}

			hmu.sort()
			logi.logInfo("Individual row of hash is :: "+hmu.toString())

			mDB.put("Line Item"+temp,hmu)
			temp++

		}

		//Service Credits
		String countquery="select count(*) from (select row_number() over(ORDER BY orig_credit_id asc ) as sno  from ariacore.all_invoice_details where invoice_no=%s and (orig_coupon_cd IS NOT NULL  or service_no = 0) )"
		int creditrows= db.executeQueryP2(String.format(countquery,invoiceNo,accountNo)).toInteger()
		logi.logInfo("creditrows " +creditrows)
		String generalcreditquery="SELECT case when a.orig_coupon_cd is not null and  a.orig_credit_id is NULL then a.comments || ' (applied '|| '%s' || ')'  when a.orig_credit_id  is not null  THEN 'Service Credit' || ' (applied ' || '%s'|| ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description,(CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,aid.orig_credit_id,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val ,row_number() over (order by aid.debit) as sno FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND  (aid.orig_coupon_cd is not null or aid.orig_credit_id is not null)  and ser.is_tax = 0  and (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a where sno=%s"
		HashMap hms = new HashMap<String,String>()
		for(int row=1 ; row<=creditrows ; row++){
			hms = db.executeQueryP2(String.format(generalcreditquery,glbillDate,glbillDate,invoiceNo,clientNo,clientNo,row))

			hms.each {k,v ->
				String v1= v.toString().trim()
				hms.put(k,v1)
			}
			hms.put("LICENSES",' ')
			hms.put("UNIT_PRICE",' ')
			hms.sort()
			logi.logInfo("Individual row of hash is :: "+hms.toString())

			mDB.put("Line Item"+temp,hms)
			temp++
		}
		
		// Payments

		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Description, a.Net_Price FROM (SELECT TRANS.DESCRIPTION ,('- '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS Net_Price , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			pHash.put("LICENSES", " ")
			pHash.put("UNIT_PRICE", " ")
			logi.logInfo("The payment related hash is : "+pHash.toString())

			//                                            pHash.each {k,v ->
			//                                                            String v1= v.toString().trim()
			//                                                            pHash.put(k,v1)
			//                                            }

			mDB.put("Line Item"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		// Tax Items
		String percent = "0.00"
		HashMap hmtax = new HashMap<String,String>()
		String lineQry = "select distinct(seq_num) from ariacore.gl_tax_detail where invoice_no = %s order by seq_num"
		String taxRateQry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = %s and seq_num = %s"
		//String taxItemsQry = "select s.comments AS Description ,sum(gtl.debit ) AS Net_Price, ' ' AS Unit_price, ' '  AS Licenses from ariacore.all_invoice_tax_details gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"
		String taxItemsQry = "select s.comments || ' %s' AS Description ,TRIM(TO_CHAR((sum(gtl.debit )),'99,990.99')) AS Net_Price from ariacore.gl_tax_detail gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"

		def totalLineItems = []

		totalLineItems = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(lineQry,invoiceNo)))
		logi.logInfo("Total Line items are : "+totalLineItems)
		for(int lines = 0;lines<totalLineItems.size();lines++){
			logi.logInfo("Inside for loop ")

			percent = db.executeQueryP2(String.format(taxRateQry,invoiceNo,totalLineItems.get(lines)))
			logi.logInfo("The Tax percentage is : "+percent.toString())
			percent = "0"+percent+"%"
			logi.logInfo("The Tax percentage after concat : "+percent.toString())

			hmtax = db.executeQueryP2(String.format(taxItemsQry,percent,invoiceNo,clientNo))
			hmtax.put("LICENSES", " ")
			hmtax.put("UNIT_PRICE", " ")
			logi.logInfo("The Tax related hash is : "+pHash.toString())

			mDB.put("Line Item"+temp,hmtax)
			temp++
			logi.logInfo("Individual Hash tax are : "+hmtax.toString())
		}

		logi.logInfo("Final hash is : "+mDB.toString())
		return mDB;
	}
	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII_2223(String tcid){
		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII_2223")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		int temp = 0
		String invoiceNo = ""
		List seqNum = []
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser ON ser.service_no           = aid.service_no  WHERE aid.invoice_no        = %s  AND ser.is_tax              = 0   AND aid.orig_credit_id     IS NULL   AND ser.is_setup            = 0   and recurring = 0 and is_surcharge = 0 AND aid.usage_type         IS NULL   AND (ser.custom_to_client_no=%s   OR ser.custom_to_client_no IS NULL) "
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		String generalQry = "SELECT case when a.is_order_based = 1 then ' ' else TRIM(TO_CHAR(a.plan_name)) end AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  when a.is_order_based = 1 then ' ' ELSE TO_CHAR(a.usage_units) END ) AS Licenses, TRIM(TO_CHAR(a.debit,'99,990.99')) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0 and aid.orig_credit_id is null  and ser.is_setup = 0 and ser.recurring = 0 AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		String seqNumQry2 = "SELECT aid.seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND ser.is_tax = 0  AND ser.is_min_fee = 0 and ser.is_setup =0 and aid.orig_credit_id is null AND aid.usage_type IS NULL AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) order by seq_num"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)

		
		seqNum = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(seqNumQry,invoiceNo,clientNo)))

		for(int row=1 ; row<=seqNum.size(); row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,seqNum.get(row-1)))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,recRateScale,Integer.parseInt(usageRateScale),Integer.parseInt(usageRateScale),invoiceNo,seqNum.get(row-1),clientNo))
			hm = new HashMap<String,String>()
			hm = db.executeQueryP2(String.format(generalQry,recRateScale,recRateScale,Integer.parseInt(usageRateScale),Integer.parseInt(usageRateScale),invoiceNo,seqNum.get(row-1),clientNo))

			if(hm.get("Licenses").equals(" "))
			{
				logi.logInfo("Units has space")
				hm.put("Licenses","")
			}
			if(hm.containsKey("Description"))
			{
				String pname = hm.get("Description").toString().trim()
				hm.put("Description",pname)
			}
			
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;
		}
		temp++
		logi.logInfo("mDB mDB mDB mDB :: "+mDB.toString())


		

		logi.logInfo("Final hash is : "+mDB.toString())
		return mDB;
	}
	
	public String uv_getTotalStmntCredits(String tcid)
	{
		logi.logInfo("Calling md_getTotalStmntCredits")
		DecimalFormat d = new DecimalFormat("0.00");
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String fval=" - "+d.format(val).toString()
		return fval.trim()
	}

	/////// Parasuram methods
	
	public String uv_getTotalStmntCreditsWithWriteOff(String tcid)
	{
		logi.logInfo("Calling md_getTotalStmntCredits")
		DecimalFormat d = new DecimalFormat("0.00");
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13, 6,-6)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String fval=" - "+d.format(val).toString()
		return fval.trim()
	}
	
	public LinkedHashMap uv_getMembershipDetailsXML_DB(String tcid){
		logi.logInfo("Calling method uv_getMembershipDetailsXML_DB")
		LinkedHashMap lHM = new LinkedHashMap<String,String>()
		
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		logi.logInfo("Account Number is "+accountNo)
		
		ConnectDB db = new ConnectDB()
		// Queries
		String generalQry = "SELECT act.mi, binfo.last_name, binfo.address1, binfo.city, binfo.state, binfo.zip, binfo.country, to_char(act.last_bill_thru_date,'dd-MM-yyyy') as member_expiry_dt, act.acct_no as aria_account_no, act.first_name FROM ariacore.billing_info binfo JOIN ariacore.acct act ON binfo.acct_no  = act.acct_no WHERE act.acct_no =%s"
			
		// Data fetching
		lHM = db.executeQueryP2(String.format(generalQry, accountNo))
		logi.logInfo("Membership details hash is : "+lHM.toString())
		
		//statement_no
		String formatStmt = uv_getFormattedStmtID(tcid)
		logi.logInfo("formatStmt is "+formatStmt)
		
		lHM.put("STATEMENT_NO",formatStmt)
		
		////cc_last_4_digits
		String ccnumber=u.getValueFromRequest("create_acct_complete","//cc_number")
		logi.logInfo("ccnumber "+ccnumber)
		String maskedStmt
		if(ccnumber=='NoVal')
			maskedStmt=" "
		else
		{
			String mask= ccnumber.substring(12,16)
			String t="************"
			maskedStmt= t+mask
		}
		logi.logInfo("maskedStmt is "+maskedStmt)
		
		lHM.put("CC_LAST_4_DIGITS",maskedStmt)
		
		lHM.put("MEMBERSHIP_NO"," ")
		lHM.put("MEMBER_SINCE"," ")
		lHM.put("FULL_NAME"," " )
		
		return lHM.sort()
	}

	public LinkedHashMap uv_getMembershipDetailsXML_UI(String tcid){
		logi.logInfo("Calling method uv_getMembershipDetailsXML_UI")
		List keyValues = [
			"STATEMENT_NO",
			"MEMBERSHIP_NO",
			"MEMBER_SINCE",
			"MEMBER_EXPIRY_DT",
			"FULL_NAME",
			"ARIA_ACCOUNT_NO",
			"CC_LAST_4_DIGITS",
			"FIRST_NAME",
			"MI",
			"LAST_NAME",
			"ADDRESS1",
			"CITY",
			"STATE",
			"ZIP",
			"COUNTRY"
			];
		
		LinkedHashMap lHM = new LinkedHashMap<String,String>()
		String uiValue = ""
		
		//WebElement element1 = driver.findElement(By.xpath("//bill_data/primary_member_details"));
		String expath = "//bill_data/primary_member_details/"
		
		for(int i=0; i< keyValues.size(); i++){
			uiValue = " "
			logi.logInfo("Loop is "+i)
			
			String formattedXpath = expath+keyValues[i]
			logi.logInfo("Formatted xpath is :: "+formattedXpath)
			
			try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}
				
				lHM.put(keyValues[i],uiValue)
		}
				
		logi.logInfo("Membership details hash is : "+lHM.toString())
				
		return lHM.sort()
	}
	public LinkedHashMap uv_getTransactionDetailsXML_DB(String tcid){

		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatements_DB")

		LinkedHashMap mXMLDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hmXML = new HashMap<String,String>()
		
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		logi.logInfo("Account Number is "+accountNo)

		int temp = 0
		String invoiceNo = ""
		List seqCount = []
		
		String seqNumQry = "SELECT aid.seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser ON ser.service_no          = aid.service_no join ariacore.all_invoices ai on ai.invoice_no = aid.invoice_no WHERE aid.invoice_no       = %s AND ser.custom_to_client_no=%s and ser.is_surcharge=0 AND aid.ORIG_coupon_cd    IS NULL order by seq_num"
		String invoiceNoQry = "SELECT MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String generalQry = "SELECT a.invoice_no as ID,   'invoice' as type, to_char(a.bill_date,'dd-MM-yyyy') as bill_date , a.seq_num as line_no, a.plan_name as plan_desc,a.debit||'.00' as amt FROM (SELECT  aid.seq_num, ai.bill_date, aid.invoice_no, aid.plan_name, aid.debit, ser.is_order_based, row_number() over (order by seq_num asc) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser ON ser.service_no          = aid.service_no join ariacore.all_invoices ai on ai.invoice_no = aid.invoice_no WHERE aid.invoice_no       = %s AND ser.custom_to_client_no=%s and ser.is_surcharge=0 AND aid.ORIG_coupon_cd    IS NULL )a WHERE a.custom_seq_num = %s"

		ConnectDB db = new ConnectDB()
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)


		seqCount = db.executeQueryReturnValuesAsArray(String.format(seqNumQry,invoiceNo,clientNo))
		logi.logInfo("Seq num are :: "+seqCount)

		for(int row=1 ; row<=seqCount.size() ; row++){

			hmXML = db.executeQueryP2(String.format(generalQry,invoiceNo,clientNo,seqCount[row-1]))
			hmXML.each {k,v ->
				String v1= v.toString().trim()
				hmXML.put(k,v1)
			}
			hmXML.sort()
			logi.logInfo("Individual row of hash is :: "+hmXML.toString())

			mXMLDB.put("Transaction_"+row,hmXML)
			temp = row;

		}
		temp++
		
		// Transactions hash


		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,6,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A "
		//String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,( case when ATRANS.TRANSACTION_TYPE=13 then TRIM(TO_CHAR((ATRANS.amount),'99,990.99')) else '-'  || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')) end) AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,6,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
		String addnlLoop1 = "SELECT a.event_no as ID, a.desc1 as type, TO_CHAR(create_date,'DD-MM-YYYY') as bill_date, '1' as line_no, a.description || ' (applied ' || TO_CHAR(create_date,'DD-MM-YYYY') || ')' AS PLAN_DESC, a.amount||'.00' as amt FROM (SELECT TRANS.DESCRIPTION , ( CASE WHEN ATRANS.TRANSACTION_TYPE=13 THEN TRIM(TO_CHAR((ATRANS.amount),'99,990.99'))  ELSE '-' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')) END)  AS AMOUNT , ATRANS.STATEMENT_NO  AS STNO, TRUNC(ATRANS.CREATE_DATE) AS create_date, ATRANS.event_no, TRANS.description as desc1, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS  JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE  = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no        = %s  AND TRANS.transaction_type IN (2,3,6,13,10)  AND atrans.statement_no     = (SELECT NVL(MAX(statement_no),'0') FROM ariacore.acct_statement  WHERE acct_no = %s ) )A WHERE A.seq = %s"
		

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			logi.logInfo("The payment related hash is : "+pHash.toString())
			mXMLDB.put("Transaction_"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mXMLDB.toString())

		return mXMLDB;
	}

	public LinkedHashMap uv_getTransactionDetailsXML_UI(String tcid){
		logi.logInfo("Calling method uv_getMembershipDetailsXML_UI")
		
		List keyValues = [
			"ID",
			"TYPE",
			"DATE",
			"LINE_NO",
			"DESC",
			"AMT"
			];
		
		List keyValues1 = [
			"ID",
			"TYPE",
			"BILL_DATE",
			"LINE_NO",
			"PLAN_DESC",
			"AMT"
			];
		
		LinkedHashMap transHM = new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap innerHash = new LinkedHashMap<String,String>()
		String uiValue = ""
		
		int xpathCount = driver.findElements(By.xpath("//bill_data/billing_info/transactions/transaction")).size()
		
		String expath = "//bill_data/billing_info/transactions/transaction[%s]/%s"
		for(int trans=0; trans<xpathCount; trans++){
			int cntr = trans+1
			innerHash = new LinkedHashMap<String,String>()
			
			for(int i=0; i< keyValues.size(); i++){

				logi.logInfo("Loop is "+i+" and counter is "+cntr )

				String formattedXpath = String.format(expath,trans+1,keyValues[i])
				logi.logInfo("Formatted xpath is :: "+formattedXpath)

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				innerHash.put(keyValues1[i],uiValue)
				logi.logInfo("Inner hash details for "+i+" is "+innerHash.toString())
			}
			transHM.put("Transaction_"+cntr,innerHash)

			logi.logInfo("Transaction details as of line item "+trans+" is "+transHM.toString())

		}
				
		return transHM.sort()
	}
	def uv_getFormattedStmtID(String tcid){

		logi.logInfo("Calling method uv_getFormattedStmtID")
		LinkedHashMap lHM = new LinkedHashMap<String,String>()

		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		logi.logInfo("Account Number is "+accountNo)

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		ConnectDB db = new ConnectDB()
		String seqNum = ""
		int paramVal = 0
		String formattedStatementId = ""

		String generalQry = "SELECT ACT.COUNTRY||'-'||LPAD(ACT.MAX_SEQ_NO,%s,0) FROM ARIACORE.ACCT ACT WHERE ACT.ACCT_NO = %s"
		String valQry = "SELECT PARAM_VAL  FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME = 'ACCT_LVL_STMT_ID_LEN' AND CLIENT_NO = "+clientNo
		String seqNumQry = "select MAX_SEQ_NO from ariacore.acct where  acct_no ="+accountNo

		paramVal = db.executeQueryP2(valQry).toInteger()
		logi.logInfo("Param val is :: "+paramVal)

		seqNum = db.executeQueryP2(seqNumQry)
		logi.logInfo("SeqNum val is :: "+seqNum)
		logi.logInfo("Length of SeqNum val is :: "+seqNum.length())

		//           int x = (paramVal - seqNum.length())
		//           logi.logInfo("Value of x is : "+x)

		formattedStatementId = db.executeQueryP2(String.format(generalQry,paramVal,accountNo)).toString()
		logi.logInfo("Formatted formattedStatementId val is :: "+formattedStatementId)

		return formattedStatementId
	}
	

	def uv_verifyTaxExemptionAppliedTax(String tcid){
		boolean retFlag = false

		int taxExemptLevel = u.getValueFromRequest("create_acct_complete","//tax_exemption_level").toInteger()
		logi.logInfo("Tax Exemption level from request is :: "+taxExemptLevel)

		if(taxExemptLevel == 0 ){

		} else if(taxExemptLevel == 1 ){

		} else if(taxExemptLevel == 2 ){

		} else if(taxExemptLevel == 3 ){

		}else{
			logi.logInfo("Tax Exemption level is not valid ")
			return retFlag
		}

		return retFlag.toString().toUpperCase()
	}

	public LinkedHashMap uv_verifybeginLoopAllWTaxItemsUI_222555(String tcid){

		logi.logInfo("Calling method uv_verifybeginLoopAllWTaxItemsUI_222555")

		List keyValues = [
			"DESCRIPTION",
			"TAXTYPE",
			"TAXRATE",
			"TAXINDICATOR",
			"TAXTOTAL",
			"GROSSTOTAL"
		];

		LinkedHashMap transHM = new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap innerHash = new LinkedHashMap<String,String>()
		String uiValue = ""

		int xpathCount = driver.findElements(By.xpath("//div[@id='content']/table[3]/tbody/tr[td]")).size()

		String expath = "//div[@id='content']/table[3]/tbody/tr[%s]/td[%s]"
		for(int trans=1; trans<=xpathCount; trans++){
			int cntr = trans+1
			innerHash = new LinkedHashMap<String,String>()

			for(int i=0; i< keyValues.size(); i++){

				logi.logInfo("Loop is "+i+" and counter is "+cntr )

				String formattedXpath = String.format(expath,cntr,i+1)
				logi.logInfo("Formatted xpath is :: "+formattedXpath)

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText().trim()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				innerHash.put(keyValues[i],uiValue)
				logi.logInfo("Inner hash details for "+i+" is "+innerHash.toString())
			}
			transHM.put("Tax_Line_Item_"+trans,innerHash)

			logi.logInfo("Tax details as of line item "+cntr+" is "+transHM.toString())

		}
		transHM = transHM.sort()
		return transHM
	}

public LinkedHashMap uv_verifybeginLoopAllWTaxItemsDB_222555(String tcid){
		
		logi.logInfo("Calling method uv_verifybeginLoopAllWTaxItemsDB_222555")
		
		LinkedHashMap totalTaxHash = new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap innerTaxHash = new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB();
		
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		
		String invoiceNo = ""
		int seqNum = 0
		
		// Query
		String invQuery = "Select max(invoice_no) from ariacore.gl where acct_no = %s"
		String seqQuery = "select count(*) as totCount from ariacore.gl_detail where invoice_no = %s"
		String mainQuery = "SELECT (case when a.plan_name is null then ' ' else TRIM(TO_CHAR(a.plan_name)) end) AS Description,  case when (a.seq_num = a.taxed_seq_num) then 'State Taxes' else ' ' end as TaxType, NVL(trim(to_char(a.tax_rate*100,'99,990.99')),'0.00') AS TaxRate, (case when to_char(a.tax_method) is null then ' ' else to_char(a.tax_method) end) AS TaxIndicator, NVL(trim(to_char(a. glt_debit,'99,990.99')),'0.00') AS TaxTotal, trim(to_char((nvl(a.gl_debit,0.00)+nvl(a.glt_debit,0.00)),'99,990.99')) as GrossTotal FROM (SELECT  aid.plan_name, aid.debit AS gl_debit, ser.is_tax, aid.service_no, glt.tax_type, glt.tax_rate, glt.tax_method, glt.debit AS glt_debit,aid.seq_num, glt.taxed_seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no  = aid.service_no AND aid.invoice_no = %s LEFT JOIN ariacore.gl_tax_detail glt ON aid.invoice_no  = glt.invoice_no AND aid.seq_num  =glt.taxed_seq_num WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL)  order by aid.seq_num )a"
		
		invoiceNo = db.executeQueryP2(String.format(invQuery,accountNo))
		logi.logInfo("Invoice no is "+invoiceNo)
		
		seqNum = db.executeQueryP2(String.format(seqQuery,invoiceNo)).toInteger()
		logi.logInfo("seqNum no is "+seqNum)
		
		for(int lines = 0; lines<seqNum; lines++){
			logi.logInfo("Loop is "+lines)
			int cntr = lines+1
			
			String formattedQry = String.format(mainQuery,invoiceNo,invoiceNo,cntr,clientNo)
			logi.logInfo("formattedQry :: "+formattedQry)
			
			innerTaxHash = db.executeQueryP2(formattedQry)
			
			innerTaxHash.each {k,v ->
				String v1= v.toString().trim()
				innerTaxHash.put(k,v1)
			}
			
			logi.logInfo("innerTaxHash :: "+innerTaxHash)
			
			totalTaxHash.put("Tax_Line_Item_"+cntr,innerTaxHash)
		}
		logi.logInfo("Final hash is : "+totalTaxHash);
		totalTaxHash = totalTaxHash.sort()
		return totalTaxHash
	}

public LinkedHashMap uv_verifybeginTaxSummaryLoopUI_222555(String tcid){

	logi.logInfo("Calling method uv_verifybeginTaxSummaryLoopUI_222555")
	
	List keyValues = [
		"TSUMMARYDESCRIPTION",
		"TSUMMARYBASEAMOUNT",
		"TSUMMARYRATE",
		"TAXAMOUNT"
		];
	
	LinkedHashMap transSummaryHM = new LinkedHashMap<String,LinkedHashMap<String,String>>()
	LinkedHashMap innerTaxHash = new LinkedHashMap<String,String>()
	String uiValue = ""
	
	int xpathCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td]")).size()
	logi.logInfo("xpathCount is "+xpathCount )
	
	String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"
	for(int trans=1; trans<=xpathCount; trans++){
		int cntr = trans+1
		innerTaxHash = new LinkedHashMap<String,String>()
		
		for(int i=0; i< keyValues.size(); i++){

			logi.logInfo("Loop is "+i+" and counter is "+cntr )

			String formattedXpath = String.format(expath,cntr,i+1)
			logi.logInfo("Formatted summary xpath is :: "+formattedXpath)

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					uiValue = driver.findElement(By.xpath(formattedXpath)).getText().trim()
					logi.logInfo("uiValue : "+uiValue)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}

			innerTaxHash.put(keyValues[i],uiValue)
			
		}
		logi.logInfo("Inner hash details after remove is  "+innerTaxHash.toString())
		innerTaxHash=innerTaxHash.sort()
		transSummaryHM.put("Tax_Summary_Item_"+trans,innerTaxHash)

		logi.logInfo("Tax details as of line item "+trans+" is "+transSummaryHM)

	}
	transSummaryHM = transSummaryHM.sort()
	return transSummaryHM
}

public LinkedHashMap uv_GetTaxSummaryItemsFromDB_222555(String tcid)

{
	   logi.logInfo("Entered into uv_GetTaxSummaryItemsFromDB_212")
	   LinkedHashMap taxSummaryDB = new LinkedHashMap<String , LinkedHashMap<String,String>>()
	   LinkedHashMap tSummaryInternal = new LinkedHashMap<String,String>()
	   String acct_no = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
	   int counter=0
	   String invoice_No=""
	   String seniorAcctNo = ""
	   String taxsum1qry = ""
	   ConnectDB db = new ConnectDB()

	   try {
			  String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
			  invoice_No = db.executeQueryP2(invoiceQry)
			  
			  String acctTypeQry = "Select senior_acct_no from ariacore.acct where acct_no = "+acct_no
			  seniorAcctNo =db.executeQueryP2(acctTypeQry)

			  String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			  String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			  int seqnum=db.executeQueryP2("SELECT COUNT(*)  FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND ORIG_CREDIT_ID is null AND SERVICE_NO IN(401,402,403,404,405)").toInteger()

			  if(seniorAcctNo == null){
				  logi.logInfo("Inside parent loop ")
				  taxsum1qry="SELECT * FROM (SELECT COMMENTS AS TSummaryDescription FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" and orig_credit_id is null AND SERVICE_NO IN(401,402,403,404,405)GROUP BY COMMENTS),(SELECT trim(to_char(SUM(DEBIT),'99,990.99')) TSummaryBaseAmount FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+"  and orig_coupon_cd is null and orig_credit_id is null and  service_no not in(400,401,402,403)),(SELECT TRIM(TO_CHAR((a.RATE * 100),'99,999.99')) TSummaryRate FROM (SELECT DISTINCT TAX_RATE RATE FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+")a),(SELECT  trim(to_char(SUM(DEBIT),'99,990.99')) TaxAmount FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" and orig_credit_id is null AND SERVICE_NO IN(401,402,403,404,405)GROUP BY COMMENTS)"
			  } else{
			  logi.logInfo("Inside child loop ")
				  taxsum1qry="SELECT * FROM (SELECT COMMENTS AS TSummaryDescription, trim(to_char(SUM(DEBIT),'99,990.99')) TaxAmount FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" and orig_credit_id is null AND SERVICE_NO IN(401,402,403,404,405)GROUP BY COMMENTS),(SELECT trim(to_char(SUM(DEBIT),'99,990.99')) TSummaryBaseAmount FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+"   and orig_coupon_cd is null and orig_credit_id is null and service_no not in(400,401,402,403)),(SELECT TRIM(TO_CHAR((a.RATE * 100),'99,999.99')) TSummaryRate FROM (SELECT DISTINCT TAX_RATE RATE FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+")a)"
			  }
			  for(int rcount=1;rcount <= seqnum;rcount++)
			  {
					logi.logInfo "rcount value is"+rcount
					
					tSummaryInternal=db.executeQueryP2(String.format(taxsum1qry,symbol,symbol))
					
					logi.logInfo "Summary internal Hashmap "+tSummaryInternal
					taxSummaryDB.put("Tax_Summary_Item_"+rcount,tSummaryInternal)
					
					logi.logInfo("Final SummaryDB hashmap is "+taxSummaryDB)
					counter=rcount
			  }
			  counter++
	   }

	   catch (Exception  e) {
			  logi.logInfo "MethodException"
			  logi.logInfo e.getMessage()
	   }
	   taxSummaryDB = taxSummaryDB.sort()
	   return taxSummaryDB
}

	public LinkedHashMap uv_getInvoiceLineItems_10101010_UI(String tcid){

		logi.logInfo("Calling uv_getInvoiceLineItems_10101010_UI")

		List keyValues = [
			"PLAN_NAME",
			"STMT_DATE",
			"UNITS",
			"RATE",
			"PRICE_TIER",
			"NET",
			"TAX",
			"TOTAL"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap internalHM = new HashMap<String,String>()
		String invoiceNo = ""
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo


		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		int seqNum = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[@class='lineitem'] and not(child::td[contains(text(),'Invoice Total')])]")).size()
		logi.logInfo("Seq num count is :: "+seqNum)

		for(int i=1 ; i<=seqNum ; i++){

			internalHM = new HashMap<String,String>()
			for(int k=1 ; k<=8 ; k++){
				int rowVal = i+2
				logi.logInfo ("The value of i is "+i+" and incremented is "+rowVal)

				String expath = "//div[@id='content']/table[2]/tbody/tr[%s]/td[%s]"
				String formattedXpath = String.format(expath,rowVal,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				String uiValue = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				internalHM.put(keyValues.get(k-1),uiValue)
				internalHM.sort()

			}

			finalMap.put("Line Item"+i , internalHM)
			logi.logInfo("Final hash at the time of sequence no "+i+" is : "+finalMap)
		}
		return finalMap
	}
	public LinkedHashMap uv_getInvoiceLineItems_10101010_DB(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return getStmtInvoiceLineItemsFromStatements_10101010_DB(tcid,accountNo)
	}
	public LinkedHashMap uv_getChildInvoiceLineItems_10101010_DB(String tcid)
	{
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return getStmtInvoiceLineItemsFromStatements_10101010_DB(tcid,accountNo)
	}

	public LinkedHashMap getStmtInvoiceLineItemsFromStatements_10101010_DB(String tcid,String accountNo){

		logi.logInfo("Calling getStmtInvoiceLineItemsFromStatements_10101010_DB" +accountNo)

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		double tempUsageRate = 0.0
		int recRateScale = 0
		int usgRateScale = 0
		int tempUsageRate1=0
		String orderlabelQuery="select alt_label  from ariacore.order_items  where order_no= ( select order_no  from ariacore.orders  where acct_no=%s)"
		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select count(seq_Num) as COUNT from ariacore.gl_detail where invoice_no = %s"
		String usageRateQry = "select NVL(trim(to_char(usage_rate,'99,990.99')),0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT comments, (CASE WHEN (START_DATE IS NULL OR END_DATE IS NULL) THEN '-' ELSE  to_number(TO_CHAR(start_date,'dd'))  || '-'  || to_number(TO_CHAR(start_date,'mm'))  || '-'  ||to_number(TO_CHAR(start_date,'yyyy')) || ' - ' ||(TO_CHAR(end_date,'dd'))  || '-'  || to_number(TO_CHAR(end_date,'mm'))  || '-'  ||to_number(TO_CHAR(end_date,'yyyy')) END) AS Result, usage_units, usage_rate, debit  FROM ariacore.all_invoice_details WHERE invoice_no =%s and seq_num=%s "
		String generalQry = "SELECT b.Plan_name, b.Stmt_Date, b.Rate, b.Units, (case when  b.Price_Tiers is null then ' ' else trim(( SELECT case when (to_unit is null and from_unit is null)  then '0.00' when to_unit is null then (from_unit||'+') else (from_unit || ' - ' || to_unit) end  FROM ariacore.acct_rates_history_details WHERE acct_no   =%s AND service_no  = b.service_no AND rate_seq_no =   b.Price_Tiers)) end) as Price_Tier, TRIM(TO_CHAR(b.Net,'99,990.99'))       AS Net, TRIM(TO_CHAR(b.Tax,'99,990.99'))       AS Tax, TRIM(TO_CHAR(b.tax+b.net,'99,990.99')) AS Total FROM (SELECT ( CASE WHEN a.orig_coupon_cd IS NOT NULL THEN TRIM(TO_CHAR(a.comments)) WHEN a.is_order_based=1 THEN '%s' ELSE TRIM(TO_CHAR(a.plan_name))   || ' ' || a.comments END ) AS Plan_name, (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE      IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - ' || (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Stmt_Date, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.usage_rate IS NULL THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(TO_CHAR(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(TO_CHAR(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(TO_CHAR(a.usage_rate,'99,990.99')) ||'.', %s, 0) END) WHEN a.usage_based=1 THEN ( CASE WHEN  regexp_like(trim(TO_CHAR(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(TO_CHAR(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(TO_CHAR(a.usage_rate,'99,990.99')) ||'.', %s, 0) END) WHEN a.is_setup=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_tax = 1 THEN '0.00' END) AS Rate, (CASE WHEN a.usage_units IS NULL THEN NVL(TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END )   AS Units, a.debit AS Net, ( CASE WHEN a.seq_num = a.taxed_seq_num THEN a.tdebit ELSE 0.00 END) AS Tax, a.tiers as Price_Tiers,  a.service_no FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date,       aid.end_date,       ser.usage_based,       aid.plan_name,       ser.is_setup,       ser.is_min_fee,       ser.is_order_based, ser.is_tax,  ser.is_surcharge,  aid.orig_coupon_cd,  aid.service_no,  glt.debit AS tdebit, glt.TAXED_SEQ_NUM, aid.RATE_SEQ_NO as tiers,  aid.seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no left JOIN ariacore.gl_tax_detail glt     ON aid.invoice_no = glt.invoice_no AND aid.seq_num = glt.taxed_seq_num WHERE aid.invoice_no = %s AND aid.seq_num  = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a )b"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String olable= db.executeQueryP2(String.format(orderlabelQuery,accountNo))

		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo))
		logi.logInfo("Seq num is :: "+seqCount)

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			ResultSet rs = db.executePlaQuery(String.format(usageRateQry,invoiceNo,row));
			while(rs.next()) {
				tempUsageRate = rs.getDouble(1)

			}

			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				if(tempUsageRate.toString().contains("."))
				{
					String txt=tempUsageRate.toString().split("\\.")[0]
					tempUsageRate1=txt.length()
					logi.logInfo("tempUsageRate1 is ::: "+tempUsageRate1)
					recRateScale =Integer.parseInt(recuringRateScale)+tempUsageRate1+1
					logi.logInfo("recRateScale is ::: "+recRateScale)
					usgRateScale = tempUsageRate1+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)

				}else
				{
					recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				}

			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			hm = db.executeQueryP2(String.format(generalQry,accountNo,olable,recRateScale,recRateScale,usgRateScale,usgRateScale,invoiceNo,row,clientNo))
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;

		}
		temp++
		// voidedTransactions hash

		int noOfvoidedPayments = 0
		HashMap voidedpHash = new HashMap<String,String>()
		
		String loopCountQry = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-3,-2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,('- '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-3,-2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfvoidedPayments = db.executeQueryP2(String.format(loopCountQry,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in voided transactions : "+noOfvoidedPayments)
		for(int pay = 1;pay <=noOfvoidedPayments; pay++){

			voidedpHash = db.executeQueryP2(String.format(addnlLoop,accountNo,accountNo,pay))
			voidedpHash.put("UNITS", " ")
			voidedpHash.put("RATE", " ")
			voidedpHash.put("STMT_DATE", " ")
			logi.logInfo("The voided related hash is : "+voidedpHash.toString())

			//pHash.put("Line Item"+temp,"")
			if(voidFlag=='TRUE')
			{
				logi.logInfo("voidflag is true")
				mDB.put("Line Item"+temp,voidedpHash)
				temp++
			}
		}


		// Balance Transfer Hash
		int noOfBalTrans = 0
		HashMap balHash = new HashMap<String,String>()
		noOfBalTrans = db.executeQueryP2("Select count(*) from ariacore.acct_transaction where acct_no="+accountNo+ " and statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no ="+accountNo+"  ) and transaction_type IN (4,5)" ).toString().toInteger()
		logi.logInfo("Total Balance Transfers: "+noOfBalTrans)
		String btItemsLoop = "SELECT Plan_name, AMOUNT from (Select tt.description as Plan_name,TRIM(TO_CHAR(atr.amount,'99,990.99')) AS AMOUNT ,row_number() over (order by event_no asc) AS seq_no from ariacore.transaction_types tt JOIN ariacore.acct_transaction atr ON tt.transaction_type=atr.transaction_type where atr.acct_no="+accountNo+" and atr.transaction_type IN  (4,5) and atr.statement_no=(Select max(statement_no) from ariacore.acct_statement where acct_no="+accountNo+")) where  seq_no=%s"
		for(int bt=1;bt<=noOfBalTrans;bt++)
		{
			balHash = db.executeQueryP2(String.format(btItemsLoop,bt))
			balHash.put("UNITS", "")
			balHash.put("RATE", "0.00")
			balHash.put("STMT_DATE", "-")
			logi.logInfo("The payment related hash is : "+balHash.toString())
			mDB.put("Line Item"+temp,balHash)
			
			temp++
		}
		// Transactions hash


		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			pHash.put("UNITS", " ")
			pHash.put("RATE", " ")
			pHash.put("STMT_DATE", " ")
			logi.logInfo("The payment related hash is : "+pHash.toString())
			
			mDB.put("Line Item"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}

	/////////////////////// Other methods

	public String uv_getTotalStmntCreditsChildAccount(String tcid)
	{
		logi.logInfo("Calling md_getTotalStmntCredits")
		DecimalFormat d = new DecimalFormat("0.00");
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String fval=" - "+d.format(val).toString()
		return fval.trim()
	}


	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_201(String tcid)
	{
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getSTmtCreditActivtiyLoopItems_DB_201(acct_no)
	}
	public LinkedHashMap uv_getCreditActivtyLoopItems_222(String tcid)

	{
		logi.logInfo("Calling uv_getCreditActivtyLoopItems_222")
		LinkedHashMap<String,String> creditActivtyLoopItemsAlone=new LinkedHashMap<String , HashMap<String,String>>();
		List<String> keys=[
			'CREDITACTIVITYDATE',
			'SHORTCREDITACTIVITYDESCRIPTION',
			'CREDITACTIVITYAMOUNT',
			'CREDITACTIVITYUNAPPLIEDAMOUNT',
			'CREDITACTIVITYDESCRIPTION',
			'CREDITACTIVITYREASONTEXT',
			'CREDITACTIVITYREASONCODE',
			'CREDITACTIVITYCOMMENT',
			'CREDITACTIVITYSTARTDATE',
			'CREDITACTIVITYENDDATE',
			'CREDITACTIVITYCOUPONCODE',
			'GLRECURRINGDISCOUNTCREDITS'
		]
		String crediTable="//div[@id='content']/table[3]/tbody/tr[td]"
		int counter=0
		int kcounter=0
		try {
			List<WebElement> credititems = driver.findElements(By.xpath(crediTable));
			logi.logInfo("Credit Items count : "+credititems.size().toString())
			credititems.each  { item ->
				logi.logInfo("Preparing CreditItem Hash")
				counter=counter+1
				LinkedHashMap hm = new LinkedHashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				hm=hm.sort()
				creditActivtyLoopItemsAlone.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}


		return creditActivtyLoopItemsAlone

	}

	public LinkedHashMap uv_getSTmtCreditActivtiyLoopItems_DB_222(String acct_no)
	{
		LinkedHashMap creditActivtyLoopItemsAloneDB = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		try {
			logi.logInfo("Entered into uv_getSTmtCreditActivtiyLoopItems_DB_222")
			HashMap hm = new HashMap<String,String>()
			ConnectDB db = new ConnectDB()
			int counter=0
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")

			//payments
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' ' || '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsAloneDB.put("Line Item"+counter,hm)

			}
			// Voided invoice
			String paymnetsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount=db.executeQueryP2(String.format(paymnetsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query2="SELECT a.description || ' '||%s|| ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' ' || '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount ; a++){

				hm = db.executeQueryP2(String.format(query2,voidinvoice,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided invoice :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsAloneDB.put("Line Item"+counter,hm)
				}
			}
			// Voided refunds
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13,-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' '|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12,13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsAloneDB.put("Line Item"+counter,hm)

				}
			}


			//ServiceCredits
			String serviceCreditsCountQuery="select count(*) from ariacore.all_credits where acct_no=%s and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s )"
			int rows=db.executeQueryP2(String.format(serviceCreditsCountQuery,acct_no,invoiceNo)).toInteger()
			String query1="select CreditReasonText as CREDITACTIVITYREASONTEXT,CREDITDESCRIPTION as CREDITACTIVITYDESCRIPTION,CreditReasonCode as CREDITACTIVITYREASONCODE,CreditComment AS CREDITACTIVITYCOMMENT,('%s ' || CreditUnappliedAmt) as CREDITACTIVITYUNAPPLIEDAMOUNT,CreditShortDescription as SHORTCREDITACTIVITYDESCRIPTION ,CreditCouponCode as CREDITACTIVITYCOUPONCODE ,('%s '|| CreditAmount) AS CREDITACTIVITYAMOUNT from (select case when credit_type='S' then 'Service Credit' end as CreditShortDescription ,reason_cd as CreditReasonCode,reason_text as CreditReasonText,'Service Credit (applied ' || to_char(trunc(create_date),'dd-mm-yyyy') || ')' as CREDITDESCRIPTION, comments as CreditComment,trim(to_char(left_to_apply,'99,990.99')) as CreditUnappliedAmt ,replace((case when orig_coupon_cd is null then ' ' else orig_coupon_cd  end),'') as CreditCouponCode, '-'||trim(to_char(amount,'99,990.99')) as CreditAmount,row_number() over(order by credit_id desc) as seq from ariacore.all_credits where acct_no=%s  and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s  ) )a where seq=%s"
			for(int row=1 ; row<=rows ; row++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query1,symbol,symbol,acct_no,invoiceNo,row))
				hm.put("CREDITACTIVITYDATE", "NO DATA ")
				hm.put("CREDITACTIVITYSTARTDATE", "NO DATA ")
				hm.put("CREDITACTIVITYENDDATE", "NO DATA ")
				logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				creditActivtyLoopItemsAloneDB.put("Line Item"+counter,hm)


			}
			logi.logInfo( "counter value : "+counter.toString())

			//Coupons
			String couponsCountquery="select count(*) from ariacore.all_invoice_details where invoice_no=15260079 and orig_coupon_cd is not null"
			int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
			String couponquery="select comments|| ' (applied ' || to_char(start_Date,'dd-mm-yyyy')||')' as CREDITACTIVITYDESCRIPTION ,'Coupon Application' as CREDITACTIVITYREASONTEXT ,'9999' as CREDITACTIVITYREASONCODE ,comments as CREDITACTIVITYCOMMENT,'0.00'as CREDITACTIVITYUNAPPLIEDAMT  ,to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYDATE , to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYSTARTDATE ,to_char(end_date,'dd-mm-yyyy') as CREDITACTIVITYENDDATE,orig_coupon_cd as CREDITACTIVITYCOUPONCODE ,trim(to_char(debit,'99,990.99')) as CREDITACTIVITYAMOUNT ,comments as CREDITACTIVITYSHORTDESCRIPTION from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and seq_num=%s"
			for(int a=1 ; a<=couponsCount ; a++){
				counter=counter+1
				String recseq="SELECT seq_num FROM   (SELECT seq_num, row_number() over (order by seq_num ) AS sno   FROM ariacore.all_invoice_details WHERE invoice_no=%s  AND orig_coupon_cd is not null  )a WHERE sno=%s"
				String lno=db.executeQueryP2(String.format(recseq,invoiceNo,a))
				hm = db.executeQueryP2(String.format(couponquery,invoiceNo,lno))
				logi.logInfo("Individual row of hash after adding coupons :: "+hm.toString())
				creditActivtyLoopItemsAloneDB.put("Line Item"+counter,hm)

			}
		}


		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsAloneDB
	}

	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_222(String tcid)
	{

		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getSTmtCreditActivtiyLoopItems_DB_222 (acct_no)
	}

	public String uv_GLRecurDiscountCredits_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_RecurringDiscountCredits_222(acct_no)
	}

	public String uv_GLRecurDiscountCreditsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_RecurringDiscountCredits_222(acct_no)
	}

	public String uv_RecurringDiscountCredits_222(String acct_no)
	{

		logi.logInfo " Entered into uv_RecurringDiscountCredits_222 method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List recServiceNo = []
		String recServiceNo1=""
		String recamt = ""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_recurring=1"
		recServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Rec service number "+recServiceNo

		recServiceNo1=ConvertArrayListtoCommaDelimited(recServiceNo)
		logi.logInfo "List of comma separated service number "+recServiceNo1

		if(recServiceNo.size()> 0)
		{
			String recamtqry="SELECT TRIM(TO_CHAR(a.totalrecurring - (SELECT sum(amt) FROM ariacore.gl_detail_after_credit WHERE invoice_no="+invoice_No+" AND service_no IN("+recServiceNo1+")),'99,990.99'))   AS GLrecuaftercredits FROM (SELECT SUM(debit) totalrecurring FROM ariacore.all_invoice_details WHERE invoice_no="+invoice_No+" AND service_no IN("+recServiceNo1+"))a"
			recamt = db.executeQueryP2(recamtqry)
		}
		else
			recamt="0.00"

		return recamt
	}

	public String uv_GL_Credit_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLFullCredit_222(acct_no)
	}

	public String uv_GL_CreditChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLFullCredit_222(acct_no)
	}

	public String uv_GLFullCredit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GLFullCredit_222 method "
		DecimalFormat d = new DecimalFormat("#.00");
		ConnectDB db = new ConnectDB()

		String invoice_No=""
		String glcredit=""
		String payment=""
		double fullcredit=0.00
		String glfullcredit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String glcreditqry="SELECT TRIM(TO_CHAR(A.COUPONAMT +(SELECT NVL(SUM(TOTAL_APPLIED),0) FROM ARIACORE.ALL_CREDITS WHERE ACCT_NO="+acct_no+"  AND REASON_CD IN(1,2,3,4,5,97)),'99,990.99'))FROM (SELECT ABS(NVL(SUM(DEBIT),0)) COUPONAMT FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO   ="+invoice_No+"  AND  (ORIG_COUPON_CD IS NOT NULL  OR ORIG_CREDIT_COMMENTS LIKE '%Service credit of%'))A"
		glcredit = db.executeQueryP2(glcreditqry)

		String paymentqry="Select NVL(SUM(Amount),0) from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type in(3,2) and event_no in(select max(event_no) from ariacore.acct_transaction where acct_no="+acct_no+")"
		payment= db.executeQueryP2(paymentqry)

		fullcredit=(Double.parseDouble(glcredit))+(Double.parseDouble(payment))
		logi.logInfo " fullcredit " + fullcredit

		glfullcredit = d.format(fullcredit).toString()
		logi.logInfo " Returing fullcredit  " + glfullcredit

		return glfullcredit

	}

	public String uv_GLTotal_Ser_Credits_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLTotalSerCredits(acct_no)
	}

	public String uv_GLTotal_Ser_Credits_Child_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLTotalSerCredits(acct_no)
	}


	public String uv_GLTotalSerCredits(String acct_no)
	{
		logi.logInfo " Entered into uv_GLTotalSerCredits method "
		ConnectDB db = new ConnectDB()
		String invoice_No=""
		String gltotsercdt=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		/*Backup code for total service credits*/

		//String gltotsercdtqry="SELECT TRIM(TO_CHAR(SUM(AMOUNT),'99,999.99')) FROM ARIACORE.CREDITS WHERE ACCT_NO="+acct_no+" and (COMMENTS LIKE '%"+invoice_No+"%' OR REASON_CD IN(1,2,3,4,5,97))"
		//gltotsercdt = db.executeQueryP2(gltotsercdtqry)

		String gltotsercdtqry="SELECT TRIM(TO_CHAR(NVL(ABS(SUM(DEBIT)),0),'99,990.99')) FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND DEBIT < 0 AND ORIG_COUPON_CD IS NULL"
		gltotsercdt = db.executeQueryP2(gltotsercdtqry)

		return gltotsercdt
	}

	public String uv_GLUsageDiscountCredits_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLUsgDiscountCredit(acct_no)
	}

	public String uv_GLUsageDiscountCreditsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLUsgDiscountCredit(acct_no)
	}

	public String uv_GLUsgDiscountCredit(String acct_no)
	{
		logi.logInfo " Entered into uv_GLUsgDiscountCredit method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List usgServiceNo = []
		String usgServiceNo1=""
		String usgamt = ""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_usage=1"
		usgServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Usage service number "+usgServiceNo

		usgServiceNo1=ConvertArrayListtoCommaDelimited(usgServiceNo)
		logi.logInfo "List of comma separated service number "+usgServiceNo1

		if(usgServiceNo.size()> 0)
		{
			logi.logInfo " Inside IF"
			String usgamtqry="SELECT TRIM(TO_CHAR(a.totalusage - (SELECT sum(amt) FROM ariacore.gl_detail_after_credit WHERE invoice_no="+invoice_No+" AND service_no IN("+usgServiceNo1+")),'99,990.99'))   AS GLusageaftercredits FROM (SELECT SUM(debit) totalusage FROM ariacore.all_invoice_details WHERE invoice_no="+invoice_No+" AND service_no IN("+usgServiceNo1+"))a"
			usgamt = db.executeQueryP2(usgamtqry)
		}
		else
			usgamt="0.00"

		return usgamt
	}



	public String uv_GLTotalRecAfterCredit(String acct_no)
	{
		logi.logInfo " Entered into uv_GLTotalRecAfterCredit method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List recServiceNo = []
		String recamt = ""
		String recServiceNo1=""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_recurring=1"
		recServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Rec service number "+recServiceNo

		recServiceNo1=ConvertArrayListtoCommaDelimited(recServiceNo)
		logi.logInfo "List of comma separated service number "+recServiceNo1

		if(recServiceNo.size()>0)
		{
			String recamtqry="SELECT TRIM(TO_CHAR(NVL(SUM(amt),0),'99,990.99')) glrecuraftercredits FROM ariacore.gl_detail_after_credit  WHERE invoice_no="+invoice_No+"  AND service_no  IN("+recServiceNo1+") AND DEBIT > 0"
			recamt = db.executeQueryP2(recamtqry)
		}
		else
			recamt="0.00"

		return recamt
	}

	public String uv_GET_CHILD_ACCT_NO_FROM_USERID(String testcaseId)
	{
		logi.logInfo ("Calling md_GET_CHILD_ACCT_NO_FROM_USERID")
		String userid = u.getValueFromRequest("create_acct_complete.a","//userid")
		ConnectDB db = new ConnectDB()
		String acct_no=db.executeQueryP2("select acct_no from ariacore.acct where userid='"+userid+"'")
		return acct_no
	}


	public String uv_GLTotalUsageAfterCredits_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLTotalUsageAfterCredit(acct_no)
	}

	public String uv_GLTotalUsageAfterCreditsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLTotalUsageAfterCredit(acct_no)
	}


	public String uv_GLTotalUsageAfterCredit(String acct_no)
	{
		logi.logInfo " Entered into uv_GLTotalUsageAfterCredit method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List usgServiceNo = []
		String usgServiceNo1=""
		String usgcamt = ""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_usage=1"
		usgServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Usage service number "+usgServiceNo

		usgServiceNo1=ConvertArrayListtoCommaDelimited(usgServiceNo)
		logi.logInfo "List of comma separated service number "+usgServiceNo1

		if(usgServiceNo.size()> 0)
		{
			String usgamtqry="SELECT TRIM(TO_CHAR(SUM(amt),'99,990.99')) glrecuraftercredits FROM ariacore.gl_detail_after_credit  WHERE invoice_no="+invoice_No+"  AND service_no  IN("+usgServiceNo1+")"
			usgcamt = db.executeQueryP2(usgamtqry)
		}
		else
			usgcamt="0.00"

		return usgcamt
	}



	public LinkedHashMap uv_getBillingAdressLineItem_ST_Experian(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_ST_Experian")
		String addr_line_items="//table[2]/tbody/tr/td[1]"
		String txt1=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		logi.logInfo"Plaintext"
		logi.logInfo txt1
		LinkedHashMap<String,String> addrDetails_exp=new LinkedHashMap<String,String>()
		List <String> addrLines=txt1.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		logi.logInfo "addrLines list  size"+addrLines.size().toString()
		List <String> finaladdrLines=[]
		addrLines.each  {line ->
			logi.logInfo "line--"+line
			if(!line.trim().equals(""))
			{
				logi.logInfo("adding to new loop")
				finaladdrLines.add(line.trim().toString())

			}
			else
			{
				logi.logInfo "elseline--"+line
			}
		}
		int max=finaladdrLines.size()
		int counter=6
		String val
		int b=max-1
		while(b>=0)
		{
			counter=counter-1
			String key="ADDR_LINE"+counter.toString()
			if(!finaladdrLines.get(b).trim().equals(""))
			{
				val=finaladdrLines.get(b).trim()
			}
			addrDetails_exp.put(key,val)
			if(b==0)
				break
			else
				b--
		}
		logi.logInfo addrDetails_exp.toString()
		return addrDetails_exp.sort()
	}

	public LinkedHashMap uv_getBillingAdressLineItem_DB_Experian_232(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_DB_Experian")
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		return uv_stmt_getBillingAdressLineItem_DB_Experian(acct_no)
	}

	public LinkedHashMap uv_stmt_getBillingAdressLineItem_DB_Experian(String acct_no)
	{
		logi.logInfo("Calling uv_stmt_getBillingAdressLineItem_DB_Experian")

		String query="select (b.company_name) as ADDR_LINE0,(b.first_name || ' '|| b.last_name) as ADDR_LINE1 ,b.address1 as ADDR_LINE2,b.address2 as ADDR_LINE3, case when ( b.city is not null and b.state is not null and b.zip is not null) then (b.zip ||' ' || b.city ||' '||b.state) "+
				"else '' end as ADDR_LINE4,c.country_english as ADDR_LINE5 from ariacore.current_billing_info b join ariacore.all_client_country c on c.client_no=b.client_no and b.country=c.country where b.acct_no="+acct_no
		LinkedHashMap<String,String> addrDetails=new LinkedHashMap<String,String>()
		ConnectDB db = new ConnectDB()
		ResultSet resultSet = db.executePlaQuery(query);
		ResultSetMetaData md = resultSet.getMetaData();
		int columns = md.getColumnCount();
		String val
		while (resultSet.next()){

			for(int i=1; i<=columns; i++){
				String tt= resultSet.getString(i)
				if((tt!=null) && (!tt.trim().equals("")) )
				{
					addrDetails.put(md.getColumnName(i).toString(),tt.trim().toString());
				}
				else
				{
					logi.logInfo("Unabel to add this item "+tt)
				}

			}

		}
		logi.logInfo addrDetails.toString()

		return addrDetails
	}
	public String ConvertArrayListtoCommaDelimited(List aList)
	{
		logi.logInfo " Entered into uv_ConvertArrayListtoCommaDelimited method "
		String commaDelimited =""
		
		commaDelimited=aList.toString().replace("[", "").replace("]", "")
		logi.logInfo "Comma separated arrary list "+commaDelimited
		return commaDelimited
	}

	public String uv_GlOnlyDebitParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlOnlyDebit_222(acct_no)
	}

	public String uv_GlOnlyDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlOnlyDebit_222(acct_no)
	}
	public String uv_GlOnlyDebit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlOnlyDebit_222 method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glOnlyDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String glonlydbtq="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND TRANSACTION_TYPE NOT IN(4) AND SOURCE_NO="+invoice_No
		glOnlyDebit=db.executeQueryP2(glonlydbtq)
		return glOnlyDebit
	}


	public String uv_GlDebitParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlDebit_222(acct_no)
	}

	public String uv_GlDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlDebit_222(acct_no)
	}
	public String uv_GlDebit_222(String acct_no)
	{
		DecimalFormat d = new DecimalFormat("##,###.00");
		logi.logInfo " Entered into uv_GlOnlyDebit_222 method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String transQuery="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND transaction_type in (4)"
		String gldbtq="select TRIM(TO_CHAR(NVL(sum(debit),0),'99,990.99')) from ariacore.all_invoices where invoice_no="+invoice_No
		glDebit=db.executeQueryP2(gldbtq)
		String transamt=db.executeQueryP2(transQuery)
		return  d.format( glDebit.toDouble()+transamt.toDouble()).toString()

	}

	public String uv_GLTotalRecurChargesParent_222(String tcid)
	{
		logi.logInfo " Entered into uv_GLTotalRecurChargesParent_222 method "
		ReadData rdObj =new ReadData()
		DecimalFormat d = new DecimalFormat("##.00")
		String services =""
		String resp_level_cd=""
		String parentAcctno=""
		String childAcctno=""
		String parentRecAmt=""
		String childRecAmt=""
		String totRecAmt=""
		String localTcId = tcid.split("-")[1]

		services= rdObj.getTestDataMap("StatementsUI",localTcId).get("Service_Order")
		logi.logInfo "Service order  " +services

		resp_level_cd=rdObj.getTestDataMap("create_acct_complete.a",localTcId).get("resp_level_cd")
		logi.logInfo "Resp_level_cd from input  " +resp_level_cd

		if(services.contains("create_acct_complete.a") && (resp_level_cd.equals("2") || resp_level_cd.equals("3") ||resp_level_cd.equals("4")))
		{
			parentAcctno=u.getValueFromResponse("create_acct_complete","//acct_no")
			parentRecAmt=uv_GLTotalRecCharges(parentAcctno)
			logi.logInfo "Recurring amt for parent  " +parentRecAmt

			childAcctno=u.getValueFromResponse("create_acct_complete.a","//acct_no")
			childRecAmt=uv_GLTotalRecCharges(childAcctno)
			logi.logInfo "Recurring amt for child  " +childRecAmt

			totRecAmt=d.format(parentRecAmt.toDouble()+childRecAmt.toDouble()).toString()
			logi.logInfo "TotalRecurring amount  " +totRecAmt
			return totRecAmt
		}
		else
			String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLTotalRecCharges(acct_no)
	}

	public String uv_GLTotalRecurChargesChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLTotalRecCharges(acct_no)
	}


	public String uv_GLTotalRecCharges(String acct_no)
	{
		logi.logInfo " Entered into uv_GLTotalRecCharges method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List recServiceNo = []
		String recServiceNo1 =""
		String totalrecamt = ""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_recurring=1"
		recServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Rec service number "+recServiceNo

		recServiceNo1=ConvertArrayListtoCommaDelimited(recServiceNo)
		logi.logInfo "List of comma separated service number "+recServiceNo1

		if(recServiceNo.size()>0)
		{
			String recamtqry="SELECT TRIM(TO_CHAR(SUM(DEBIT),'99,990.99')) TotalRecCharge FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+"  AND service_no  IN("+recServiceNo1+") AND DEBIT > 0"
			totalrecamt = db.executeQueryP2(recamtqry)
		}
		else
			totalrecamt="0.00"

		return totalrecamt
	}

	public String uv_GLTotalUsgChargesParent_222(String tcid)
	{
		logi.logInfo " Entered into uv_GLTotalUsgChargesParent_222 method "
		ReadData rdObj =new ReadData()
		DecimalFormat d = new DecimalFormat("#0.00")
		String serviceOrder =""
		String resp_level_cd=""
		String parentAcctno=""
		String childAcctno=""
		String parentUsgAmt=""
		String childUsgAmt=""
		String totUsgAmt=""
		String localTcId = tcid.split("-")[1]

		serviceOrder= rdObj.getTestDataMap("StatementsUI",localTcId).get("Service_Order")
		logi.logInfo "Service order  " +serviceOrder

		resp_level_cd=rdObj.getTestDataMap("create_acct_complete.a",localTcId).get("resp_level_cd")
		logi.logInfo "Resp_level_cd from input  " +resp_level_cd

		if(serviceOrder.contains("create_acct_complete.a") && (resp_level_cd.equals("2") || resp_level_cd.equals("3") ||resp_level_cd.equals("4")))
		{
			parentAcctno=u.getValueFromResponse("create_acct_complete","//acct_no")
			parentUsgAmt=uv_GLTotalUsgCharges(parentAcctno)
			logi.logInfo "Usage amt for parent  " +parentUsgAmt

			childAcctno=u.getValueFromResponse("create_acct_complete.a","//acct_no")
			childUsgAmt=uv_GLTotalUsgCharges(childAcctno)
			logi.logInfo "Usage amt for child  " +childUsgAmt

			totUsgAmt=d.format(parentUsgAmt.toDouble()+childUsgAmt.toDouble()).toString()
			logi.logInfo "TotalUsage amount  " +totUsgAmt
			return totUsgAmt
		}
		else
			String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GLTotalUsgCharges(acct_no)
	}

	public String uv_GLTotalUsgChargesChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLTotalUsgCharges(acct_no)
	}


	public String uv_GLTotalUsgCharges(String acct_no)
	{
		logi.logInfo " Entered into uv_GLTotalUsgCharges method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		List usgServiceNo = []
		String usgServiceNo1 =""
		String totalusgamt = ""

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serviceNoqry="SELECT service_no FROM ariacore.all_service WHERE service_no IN(SELECT service_no FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+" AND client_no="+clientNo+")AND client_no="+clientNo+" AND is_usage=1"
		usgServiceNo = Arrays.asList(db.executeQueryReturnValuesAsArray(serviceNoqry))
		logi.logInfo "Usage service number "+usgServiceNo

		usgServiceNo1=ConvertArrayListtoCommaDelimited(usgServiceNo)
		logi.logInfo "List of comma separated service number "+usgServiceNo1

		if(usgServiceNo.size()>0)
		{
			String usgamtqry="SELECT TRIM(TO_CHAR(SUM(DEBIT),'99,990.99')) TotalUsgCharge FROM ariacore.all_invoice_details  WHERE invoice_no="+invoice_No+"  AND service_no  IN("+usgServiceNo1+") AND DEBIT > 0"
			totalusgamt = db.executeQueryP2(usgamtqry)
		}
		else
			totalusgamt="0.00"

		return totalusgamt
	}

	public LinkedHashMap uv_GetTaxSummaryItemsFromStmts_212(String tcid)
	{

		logi.logInfo("Entered into uv_GetTaxSummaryItemsFromStmts_212")

		List headers = [
			"VAT SUMMARY",
			"AMOUNT",
			"RATE",
			"VAT AMOUNT"
		];
		int rowCount,columnCount
		int counter=1
		String uiitems=""
		LinkedHashMap tSumMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap tSumSubMap = new HashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[3]/tbody/tr")).size()
		columnCount = driver.findElements(By.xpath("//div[@id='content']/table[3]/tbody/tr[1]/td")).size()
		logi.logInfo("row  count is :: "+rowCount)
		logi.logInfo("column count is :: "+columnCount)

		for(int i=2 ; i<=rowCount ; i++){

			for(int k=1 ; k<=columnCount ; k++){

				String expath = "//*[@id='content']/table[3]/tbody/tr[%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				uiitems = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiitems = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiitems)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				tSumSubMap.put(headers.get(k-1),uiitems)
				tSumSubMap.sort()
				logi.logInfo " Internal HashMap "+tSumSubMap

			}

			tSumMap.put("Line Item"+counter, tSumSubMap)
			logi.logInfo("Final hash at the time of sequence no "+counter+" is : "+tSumMap)
			counter++
		}
		return tSumMap
	}

	public LinkedHashMap uv_GetTaxSummaryItemsFromDB_Parent_212(String tcid)
	{
		logi.logInfo("Entered into uv_GetTaxSummaryItemsFromDB_Parent_212")
		LinkedHashMap parentMap= new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap childMap= new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap resultMap= new LinkedHashMap<String,HashMap<String,String>>()
		LinkedHashMap innerHash = new LinkedHashMap<String,String>()
		ReadData rdObj =new ReadData()
		DecimalFormat d = new DecimalFormat("#0.00")
		String serviceOrder =""
		String resp_level_cd=""
		String parentAcctno=""
		String childAcctno=""
		String totalcalamt=""
		String localTcId = tcid.split("-")[1]

		serviceOrder= rdObj.getTestDataMap("StatementsUI",localTcId).get("Service_Order")
		logi.logInfo "Service order  " +serviceOrder

		resp_level_cd=rdObj.getTestDataMap("create_acct_complete.a",localTcId).get("resp_level_cd")
		logi.logInfo "Resp_level_cd from input  " +resp_level_cd

		if(serviceOrder.contains("create_acct_complete.a") && (resp_level_cd.equals("2") || resp_level_cd.equals("3") ||resp_level_cd.equals("4")))
		{
			parentAcctno=u.getValueFromResponse("create_acct_complete","//acct_no")
			parentMap =uv_GetTaxSummaryItemsFromDB_212(parentAcctno)
			logi.logInfo "Parent hashmap result is" +parentMap

			childAcctno=u.getValueFromResponse("create_acct_complete.a","//acct_no")
			childMap =uv_GetTaxSummaryItemsFromDB_212(childAcctno)
			logi.logInfo "child hashmap result is" +childMap


			if((parentMap.keySet()).equals(childMap.keySet()))
			{
				for(int i=1;i<=parentMap.size();i++)
				{
					parentMap.each() { k, v ->

						v.each() { key1,value1 ->

							logi.logInfo "Key1 is " +key1+" and Value1 is "+value1

							if(key1.equals("VAT AMOUNT") || key1.equals("AMOUNT"))
							{
								logi.logInfo "Inside Key check key1 equals"+value1.toDouble()
								double calcamt=(value1.toDouble()) + (childMap.get(k).get(key1).toString().toDouble())

								logi.logInfo "Calculated amount after adding both parent and child hash"+calcamt
								innerHash.put(key1,'$ '+calcamt)
								logi.logInfo "innerhash after VAT AMOUNT & AMOUNT calculation" +innerHash
							}
							else
								innerHash.put(key1,value1)
						}

						resultMap.put(k,innerHash)
						logi.logInfo "Final resultMap " +resultMap
					}
				}
				return resultMap.sort()
			}

		}
		/*parentMap.each() { k, v ->
		 v.each() { key1,value1 ->
		 childMap.each() { k1,v1 ->
		 if(v1.containsKey(key))
		 {
		 logi.logInfo "Key for each iteration" + key
		 logi.logInfo "value for each iteration" + value
		 if(key.equals("VAT AMOUNT") || key.equals("AMOUNT"))
		 {
		 totalcalamt=d.format((parentMap.get(key).toDouble()+parentMap.get(key).toDouble())).toString()
		 logi.logInfo "Total calculated amount" +totalcalamt
		 resultMap.put(key,totalcalamt)
		 logi.logInfo "Result Map after the calculation" +resultMap
		 }
		 else
		 {
		 resultMap.put(key,value)
		 logi.logInfo "Result Map in else" +resultMap
		 }
		 }
		 }
		 }
		 return  resultMap
		 */

		else
		{
			String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
			return uv_GetTaxSummaryItemsFromDB_212(acct_no)
		}
	}
	public LinkedHashMap uv_GetTaxSummaryItemsFromDB_Child_212(String tcid)
	{
		logi.logInfo("Entered into uv_GetTaxSummaryItemsFromDB_Child_212")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GetTaxSummaryItemsFromDB_212(acct_no)
	}

public LinkedHashMap uv_GetTaxSummaryItemsFromDB_212(String acct_no)
	
	{
		logi.logInfo("Entered into uv_GetTaxSummaryItemsFromDB_212")
		LinkedHashMap taxSummaryDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap tSummaryInternal = new HashMap<String,String>()
		int counter=0
		String invoice_No=""
		ConnectDB db = new ConnectDB()

		try {
			String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
			invoice_No = db.executeQueryP2(invoiceQry)

			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			int seqnum=db.executeQueryP2("SELECT COUNT(*)  FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND SERVICE_NO IN(401,402,403,404,405)").toInteger()

			for(int rcount=1;rcount <= seqnum;rcount++)
			{
				logi.logInfo "rcount value is"+rcount
				String taxsum1qry="SELECT * FROM (SELECT COMMENTS AS \"VAT SUMMARY\",('%s' || SUM(DEBIT)) AMOUNT FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND SERVICE_NO IN(401,402,403,404,405)GROUP BY COMMENTS),(SELECT SUM(DEBIT) AMOUNT FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND SEQ_NUM IN(SElECT TAXED_SEQ_NUM FROM ARIACORE.ALL_INVOICE_TAX_DETAILS WHERE INVOICE_NO="+invoice_No+") AND CLIENT_NO="+clientNo+"),(SELECT TAX_ID,('%s' || SUM(DEBIT)) AS \"VAT AMOUNT\" FROM ARIACORE.ALL_INVOICE_TAX_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" GROUP BY TAX_ID),(SELECT TRIM(TO_CHAR((a.RATE * 100),'99,999.99')) RATE FROM (SELECT DISTINCT TAX_RATE RATE FROM ARIACORE.ALL_INVOICE_TAX_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND SEQ_NUM  IN(SELECT SEQ_NUM FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND CLIENT_NO="+clientNo+" AND SERVICE_NO IN(401,402,403,404,405)))a)"
				tSummaryInternal=db.executeQueryP2(String.format(taxsum1qry,symbol,symbol))
				
				tSummaryInternal.remove("TAX_ID")
				tSummaryInternal.sort()
				logi.logInfo "Summary internal Hashmap "+tSummaryInternal
				taxSummaryDB.put("Line Item"+rcount,tSummaryInternal)
				logi.logInfo("Final SummaryDB hashmap is "+taxSummaryDB)
				counter=rcount
			}
			counter++
		}

		catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return taxSummaryDB.sort()

	}

	public boolean changeTemplate()
	{
		logi.logInfo("Calling changeTemplate");
		boolean success=false
		try {
			waitForTheElement(tabStatementTemplate)
			logi.logInfo "ST tab displayed: "+ driver.findElement(tabStatementTemplate).isDisplayed()

			driver.findElement(tabStatementTemplate).click()
			logi.logInfo " Navigated to tabStatementTemplate "
			waitForTheElement(By.xpath ("//input[@value='Edit Fields']"))
			driver.findElement(By.xpath ("//input[@value='Edit Fields']")).click();
			String tempno="201"
			logi.logInfo("Going to select template" +tempno);
			String xpath="//select[@id='inTemplateNo']/option[@value='"+tempno+"']"
			logi.logInfo("Framed xpath "+xpath)
			driver.findElement(By.xpath(xpath)).click()
			driver.findElement(By.xpath ("//input[@value='Save Changes']")).click()
			success=true
		} catch (Exception e) {
			e.printStackTrace()
		}
		return success
	}


	public String uv_nextbillDate_214(String tcid)
	{
		String formattedXpath="//p[contains(text(),'monthly bill date is')]"
		String uiValue
		if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
			uiValue= driver.findElement(By.xpath(formattedXpath)).getText().split("monthly bill date is")[1]
			uiValue=uiValue.replaceAll("\\.", "")
			return uiValue.trim()
		}
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_ST_Experian(String tcid){

		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_Experian")

		List keyValues = [
			"ITEM_NAME",
			"AMOUNT"
		];

		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap internalHM = new HashMap<String,String>()
		String invoiceNo = ""
		String uiValue = "NO VALUE"
		String temp = "NO VALUE"
		String expath
		String formattedXpath
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")

		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)

		int seqNum = driver.findElements(By.xpath("//table[3]/tbody/tr")).size()
		int orgsize = seqNum-4

		logi.logInfo("Seq num count is :: "+seqNum)
		logi.logInfo("line item count is :: "+orgsize)

		for(int i=1 ; i<=orgsize ; i++)
		{
			internalHM = new HashMap<String,String>()
			for(int k=1 ; k<=2 ; k++)
			{
				int rowVal = i+2
				logi.logInfo ("The value of i is "+i+" and incremented is "+rowVal)
				int tdcount = driver.findElements(By.xpath("//table[3]/tbody/tr["+rowVal+"]/td")).size()
				logi.logInfo ("total TD count "+tdcount)
				if (tdcount>2)
				{
					logi.logInfo ("inside if ")
					expath = "//table[3]/tbody/tr[%s]/td[%s]"
					formattedXpath = String.format(expath,rowVal,(k+1))
					logi.logInfo("formattedXpath : "+formattedXpath)

				}
				else
				{
					logi.logInfo ("inside else ")
					expath = "//table[3]/tbody/tr[%s]/td[%s]"
					formattedXpath = String.format(expath,rowVal,k)
					logi.logInfo("formattedXpath : "+formattedXpath)
				}
				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						uiValue = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+uiValue)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				internalHM.put(keyValues.get(k-1),uiValue)
				internalHM.sort()

			}

			finalMap.put("Line Item"+i , internalHM)
			logi.logInfo("Final hash at the time of sequence no "+i+" is : "+finalMap)
		}
		return finalMap
	}


	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB_Experian(String tcid)
	{

		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB_Experian(tcid,accountNo)
	}

	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatements_DB_Experian(String tcid,String accountNo){

		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatements_DB_Experian")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		int temp = 0
		int c = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		double tempUsageRate = 0.0
		int recRateScale = 0
		int usgRateScale = 0
		int tempUsageRate1=0
		String orderlabelQuery="select alt_label  from ariacore.order_items  where order_no= ( select order_no  from ariacore.orders  where acct_no=%s)"
		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "select count(seq_num) as COUNT from ariacore.gl_detail where invoice_no = %s and service_no not in(400,401,402,403,404,405) AND surcharge_rate_seq_no IS NULL and service_no not in (Select glt.service_no from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no=%s and ser.service_type IN ('AC') and glt.client_no=ser.client_no and glt.client_no=%s) order by seq_num"
		String usageRateQry = "select NVL(trim(to_char(usage_rate,'99,990.99')),0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		String generalQry = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL THEN TRIM(TO_CHAR(a.comments))   WHEN a.is_order_based=1     then '%s' ELSE TRIM(TO_CHAR(a.plan_name)) || ' ' || a.comments  END ) as Plan_name, TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Stmt_Date, (CASE  WHEN a.usage_rate =0 and a.service_no =0 THEN '0.00' WHEN a.usage_rate is null THEN '0.00' WHEN a.recurring=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end) WHEN a.usage_based=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end)  WHEN a.is_setup=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) when a.is_tax = 1 then '0.00' END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, nvl(aid.usage_rate,0) as usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee,ser.is_order_based,ser.is_tax,ser.is_surcharge, aid.orig_coupon_cd,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String generalQry1 = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL  THEN TRIM(TO_CHAR(REGEXP_SUBSTR(a.comments,'[^,]+')))  WHEN a.is_order_based=1  THEN '%s' ELSE REGEXP_SUBSTR(a.comments,'[^,]+') END ) AS ITEM_NAME, TRIM(TO_CHAR(a.debit,'99,990.99')) AS Amount FROM  (SELECT ser.recurring,  NVL(aid.usage_rate,0) AS usage_rate,    aid.comments,    aid.debit,    aid.plan_name,    ser.is_setup,    ser.is_min_fee,    ser.is_order_based,    ser.is_tax,    ser.is_surcharge,    aid.orig_coupon_cd,    aid.service_no  FROM ariacore.services ser  JOIN ariacore.all_invoice_details aid  ON ser.service_no   = aid.service_no  WHERE aid.invoice_no    = %s  AND aid.seq_num  = %s AND (ser.custom_to_client_no=%s  OR ser.custom_to_client_no IS NULL) )a"

		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String olable= db.executeQueryP2(String.format(orderlabelQuery,accountNo))
		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,invoiceNo,clientNo))
		logi.logInfo("Seq num is :: "+seqCount)

		List seq=[]
		String Seqquery ="select seq_num from ariacore.gl_detail where invoice_no = "+invoiceNo+" and service_no not in(400,401,402,403,404,405) AND surcharge_rate_seq_no IS NULL and service_no not in (Select glt.service_no from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no= "+invoiceNo+" and ser.service_type IN ('AC') and glt.client_no=ser.client_no and glt.client_no="+clientNo+") order by seq_num"
		ResultSet rs1=db.executePlaQuery(Seqquery)
		logi.logInfo "Executed result set...."
		while(rs1.next())
		{
			logi.logInfo" Inside result set of plan....."
			String Seq_no=rs1.getString(1)
			seq.add(Seq_no)
		}
		logi.logInfo "seq list : "+seq.toString()


		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)


			ResultSet rs = db.executePlaQuery(String.format(usageRateQry,invoiceNo,row+1));
			while(rs.next()) {
				tempUsageRate = rs.getDouble(1)
			}

			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				if(tempUsageRate.toString().contains("."))
				{
					String txt=tempUsageRate.toString().split("\\.")[0]
					tempUsageRate1=txt.length()
					logi.logInfo("tempUsageRate1 is ::: "+tempUsageRate1)
					recRateScale =Integer.parseInt(recuringRateScale)+tempUsageRate1+1
					logi.logInfo("recRateScale is ::: "+recRateScale)
					usgRateScale = tempUsageRate1+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)

				}else
				{
					recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				}

			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}
			logi.logInfo "seq number : "+seq[c].toString()
			hm = db.executeQueryP2(String.format(generalQry1,olable,invoiceNo,seq[c],clientNo))
			c=c+1
			hm.each {k,v ->
				String v1= v.toString().trim()
				hm.put(k,v1)
			}
			hm.sort()
			logi.logInfo("Individual row of hash is :: "+hm.toString())

			mDB.put("Line Item"+row,hm)
			temp = row;

		}
		temp++
		// voidedTransactions hash

		int noOfvoidedPayments = 0
		HashMap voidedpHash = new HashMap<String,String>()
		String loopCountQry = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-3,-2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,('- '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-13,-12,-3,-2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfvoidedPayments = db.executeQueryP2(String.format(loopCountQry,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in voided transactions : "+noOfvoidedPayments)
		for(int pay = 1;pay <=noOfvoidedPayments; pay++){

			voidedpHash = db.executeQueryP2(String.format(addnlLoop,accountNo,accountNo,pay))
			voidedpHash.put("UNITS", " ")
			voidedpHash.put("RATE", " ")
			voidedpHash.put("STMT_DATE", " ")
			logi.logInfo("The voided related hash is : "+voidedpHash.toString())

			//pHash.put("Line Item"+temp,"")
			if(voidFlag=='TRUE')
			{
				logi.logInfo("voidflag is true")
				mDB.put("Line Item"+temp,voidedpHash)
				temp++
			}
		}


		// Balance Transfer Hash
		int noOfBalTrans = 0
		HashMap balHash = new HashMap<String,String>()
		noOfBalTrans = db.executeQueryP2("Select count(*) from ariacore.acct_transaction where acct_no="+accountNo+ " and statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no ="+accountNo+"  ) and transaction_type IN (4,5)" ).toString().toInteger()
		logi.logInfo("Total Balance Transfers: "+noOfBalTrans)
		String btItemsLoop = "SELECT Plan_name, AMOUNT from (Select tt.description as Plan_name,TRIM(TO_CHAR(atr.amount,'99,990.99')) AS AMOUNT ,row_number() over (order by event_no asc) AS seq_no from ariacore.transaction_types tt JOIN ariacore.acct_transaction atr ON tt.transaction_type=atr.transaction_type where atr.acct_no="+accountNo+" and atr.transaction_type IN  (4,5) and atr.statement_no=(Select max(statement_no) from ariacore.acct_statement where acct_no="+accountNo+")) where  seq_no=%s"
		for(int bt=1;bt<=noOfBalTrans;bt++)
		{
			balHash = db.executeQueryP2(String.format(btItemsLoop,bt))
			balHash.put("UNITS", "")
			balHash.put("RATE", "0.00")
			balHash.put("STMT_DATE", "-")
			logi.logInfo("The payment related hash is : "+balHash.toString())
			mDB.put("Line Item"+temp,balHash)
			temp++
		}
		// Transactions hash


		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Plan_name, a.amount FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select NVL(max(statement_no),'0') from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			pHash.put("UNITS", " ")
			pHash.put("RATE", " ")
			pHash.put("STMT_DATE", " ")
			logi.logInfo("The payment related hash is : "+pHash.toString())
			mDB.put("Line Item"+temp,pHash)
			temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB;
	}


	public def uv_insertGLTaxDebit_Experian(String testcaseId)
	{
		logi.logInfo ("Calling uv_insertGLTaxDebit_Experian")
		String userid = u.getValueFromRequest("create_acct_complete.a","//userid")
		DecimalFormat df = new DecimalFormat("##.##");
		ConnectDB db = new ConnectDB()
		String parentacctNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String childacctNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")

		String Parent_Query = "SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = (select max(invoice_no) from ariacore.gl where acct_no in( "+parentacctNo+","+childacctNo+")) and SERVICE_NO IN(401,402,403,404,405)	"
		def Parent_tax = (db.executeQueryP2(Parent_Query))
		logi.logInfo "Parent Tax amount "+Parent_tax
		return Parent_tax
	}
	/////////////////////////////////// 227
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB_Experian_227(String tcid)
	{
		
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getStmtInvoiceLineItemsFromStatements_DB_Experian_227(tcid,accountNo)
	}
	
	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatements_DB_Experian_227(String tcid,String accountNo){
		
				logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatements_DB_Experian")
		
				LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
				HashMap hm = new HashMap<String,String>()
				String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
				int temp = 0
				int c = 0
				String invoiceNo = ""
				String seqCount = ""
				String recuringRateScale = ""
				String usageRateScale = ""
				double tempUsageRate = 0.0
				int recRateScale = 0
				int usgRateScale = 0
				int tempUsageRate1=0
				String orderlabelQuery="select alt_label  from ariacore.order_items  where order_no= ( select order_no  from ariacore.orders  where acct_no=%s)"
				String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
				String seqNumQry = "select count(seq_num) as COUNT from ariacore.gl_detail where invoice_no = %s and service_no not in(400,401,402,403,404,405) AND surcharge_rate_seq_no IS NULL and service_no not in (Select glt.service_no from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no=%s and ser.service_type IN ('AC') and glt.client_no=ser.client_no and glt.client_no=%s) order by seq_num"
				String usageRateQry = "select NVL(trim(to_char(usage_rate,'99,990.99')),0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
				String generalQry = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL THEN TRIM(TO_CHAR(a.comments))   WHEN a.is_order_based=1     then '%s' ELSE TRIM(TO_CHAR(a.plan_name)) || ' ' || a.comments  END ) as Plan_name, TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) AS Stmt_Date, (CASE  WHEN a.usage_rate =0 and a.service_no =0 THEN '0.00' WHEN a.usage_rate is null THEN '0.00' WHEN a.recurring=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end) WHEN a.usage_based=1  then  (case when  regexp_like(trim(to_char(a.usage_rate,'99,990.99')),'[.]') THEN RPAD(trim(to_char(a.usage_rate,'99,990.99')), %s, 0) ELSE RPAD(trim(to_char(a.usage_rate,'99,990.99'))||'.', %s, 0) end)  WHEN a.is_setup=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) when a.is_tax = 1 then '0.00' END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, nvl(aid.usage_rate,0) as usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee,ser.is_order_based,ser.is_tax,ser.is_surcharge, aid.orig_coupon_cd,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
				String generalQry1 = "SELECT (CASE WHEN a.orig_coupon_cd IS NOT NULL  THEN TRIM(TO_CHAR(REGEXP_SUBSTR(a.comments,'[^,]+')))  WHEN a.is_order_based=1  THEN '%s' WHEN a.usage_based = 1 then trim(regexp_substr(a.comments, '[^(]+', 1, 1)) ELSE REGEXP_SUBSTR(a.comments,'[^,]+') END ) AS ITEM_NAME, TRIM(TO_CHAR(a.debit,'99,990.99')) AS Amount FROM  (SELECT ser.recurring,  NVL(aid.usage_rate,0) AS usage_rate,    aid.comments,    aid.debit,    aid.plan_name,    ser.is_setup,    ser.is_min_fee,    ser.is_order_based,    ser.is_tax,   ser.usage_based, ser.is_surcharge,    aid.orig_coupon_cd,    aid.service_no  FROM ariacore.services ser  JOIN ariacore.all_invoice_details aid  ON ser.service_no   = aid.service_no  WHERE aid.invoice_no    = %s  AND aid.seq_num  = %s AND (ser.custom_to_client_no=%s  OR ser.custom_to_client_no IS NULL) )a"
					
				String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
				String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		
				ConnectDB db = new ConnectDB()
				String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
				invoiceNo = db.executeQueryP2(invoiceNoQry)
				logi.logInfo("Invoice num is :: "+invoiceNo)
				String olable= db.executeQueryP2(String.format(orderlabelQuery,accountNo))
				seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,invoiceNo,clientNo))
				logi.logInfo("Seq num is :: "+seqCount)
				
				List seq=[]
				String Seqquery ="select seq_num from ariacore.gl_detail where invoice_no = "+invoiceNo+" and service_no not in(400,401,402,403,404,405) AND surcharge_rate_seq_no IS NULL and service_no not in (Select glt.service_no from ariacore.gl_detail glt join ariacore.all_service ser on glt.service_no=ser.service_no where glt.invoice_no= "+invoiceNo+" and ser.service_type IN ('AC') and glt.client_no=ser.client_no and glt.client_no="+clientNo+") order by seq_num"
				ResultSet rs1=db.executePlaQuery(Seqquery)
				logi.logInfo "Executed result set...."
				while(rs1.next())
				{
					logi.logInfo" Inside result set of plan....."
					String Seq_no=rs1.getString(1)
					seq.add(Seq_no)
				}
				logi.logInfo "seq list : "+seq.toString()
					
				
				for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){
		
					recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
					logi.logInfo("recuringRateScale num is :: "+recuringRateScale)
		
					
					ResultSet rs = db.executePlaQuery(String.format(usageRateQry,invoiceNo,row+1));
					while(rs.next()) {
						tempUsageRate = rs.getDouble(1)
					}
					
					logi.logInfo("tempUsageRate is :: "+tempUsageRate)
		
					usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
					logi.logInfo("usageRateScale num is :: "+usageRateScale)
		
					if(tempUsageRate != null){
						logi.logInfo("Inside not null statement")
						if(tempUsageRate.toString().contains("."))
						{
							String txt=tempUsageRate.toString().split("\\.")[0]
							tempUsageRate1=txt.length()
							logi.logInfo("tempUsageRate1 is ::: "+tempUsageRate1)
							recRateScale =Integer.parseInt(recuringRateScale)+tempUsageRate1+1
							logi.logInfo("recRateScale is ::: "+recRateScale)
							usgRateScale = tempUsageRate1+Integer.parseInt(usageRateScale)+1
							logi.logInfo("usgRateScale is :: "+usgRateScale)
							
						}else
					   {
						recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
						logi.logInfo("recRateScale is :: "+recRateScale)
						usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
						logi.logInfo("usgRateScale is :: "+usgRateScale)
					  }
						
					} else{
						logi.logInfo("Inside not null else statement")
						recRateScale = Integer.parseInt(recuringRateScale)
						logi.logInfo("recRateScale is :: "+recRateScale)
						usgRateScale = Integer.parseInt(usageRateScale)
						logi.logInfo("usgRateScale is :: "+usgRateScale)
					}
					logi.logInfo "seq number : "+seq[c].toString()
					hm = db.executeQueryP2(String.format(generalQry1,olable,invoiceNo,seq[c],clientNo))
					c=c+1
					hm.each {k,v ->
						String v1= v.toString().trim()
						hm.put(k,v1)
					}
					hm.sort()
					logi.logInfo("Individual row of hash is :: "+hm.toString())
		
					mDB.put("Line Item"+row,hm)
					temp = row;
		
				}
				temp++
				
				logi.logInfo("Final hash is : "+mDB.toString())
		
				return mDB;
			}
		

	public def uv_insertGLTaxDebit_Experian_227(String testcaseId)
	{
		logi.logInfo ("Calling uv_insertGLTaxDebit_Experian")
		String userid = u.getValueFromRequest("create_acct_complete.a","//userid")
		DecimalFormat df = new DecimalFormat("##.##");
		ConnectDB db = new ConnectDB()
		String parentacctNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		String childacctNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		
		String Parent_Query = "SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) from ARIACORE.GL_DETAIL WHERE INVOICE_NO = (select max(invoice_no) from ariacore.gl where acct_no in( "+parentacctNo+","+childacctNo+")) and SERVICE_NO IN(401,402,403,404,405)	"
		def Parent_tax = (db.executeQueryP2(Parent_Query))
		logi.logInfo "Parent Tax amount "+Parent_tax
		return Parent_tax
	}
	
	public boolean uv_PRINT_NON_POS_INV_LINE_ITEMS(String testCaseId)
	{
		logi.logInfo("Calling PRINT_NON_POS_INV_LINE_ITEMS");
		boolean flg = false;
		ConnectDB db = new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client" +clientNo);
		String item_xpath = "//div[@id='content']/table[2]/tbody/tr[3]/td[@class='lineitem'][5]"
		logi.logInfo("xpath "+item_xpath)
		try{

			if(driver.findElement(By.xpath(item_xpath))){
				logi.logInfo("inside if ")
				int seqNum = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[3]/td[@class='lineitem'][5]")).size()
				logi.logInfo("num count is :: "+seqNum)
				if(seqNum == 0)	{
					flg=false
				}
				else{
					flg=true
				}
			}
		}
		catch(Exception e)
		{
			flg=false
			logi.logInfo("Unable to find element: "+e)

		}

		return flg;
	}
	public boolean uv_PRINT_NON_POS_INV_LINE_ITEMS_DB (String testCaseId){
		logi.logInfo("Calling uv_PRINT_NON_POS_INV_LINE_ITEMS_DB");
		boolean flg = false;
		ConnectDB db = new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		logi.logInfo("Client" +clientNo);
		String query = db.executeQueryP2("SELECT PARAM_VAL from ariacore.aria_client_params WHERE client_no = "+clientNo+" AND param_name = 'PRINT_NON_POS_INV_LINE_ITEMS'")
		logi.logInfo("query value" +query);
		if (query =='TRUE')	{
			flg=true
		}
		else{
			flg=false
		}
		return flg
	}

	public String uv_GlOnlyBaseDebitParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlOnlyBaseDebit_222(acct_no)
	}

	public String uv_GlOnlyBaseDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlOnlyBaseDebit_222(acct_no)
	}
	public String uv_GlOnlyBaseDebit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlOnlyBaseDebit_222 method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glOnlyBaseDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String glonlybasedbtq="SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO NOT IN(401,402,403,404,405)"
		glOnlyBaseDebit=db.executeQueryP2(glonlybasedbtq)
		return glOnlyBaseDebit
	}

	public String uv_GlOnlyTaxDebitParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlOnlyTaxDebit_222(acct_no)
	}

	public String uv_GlOnlyTaxDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlOnlyTaxDebit_222(acct_no)
	}
	public String uv_GlOnlyTaxDebit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlOnlyBaseDebit_222 method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glOnlyTaxDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String glonlyTaxdbtq="SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO IN(401,402,403,404,405)"
		glOnlyTaxDebit=db.executeQueryP2(glonlyTaxdbtq)
		return glOnlyTaxDebit
	}
	/*public LinkedHashMap uv_getCreditActivtyLoopItems_DB_222_two(String tcid)
	 {
	 LinkedHashMap creditActivtyLoopItemsDB222 = new LinkedHashMap<String , HashMap<String,String>>()
	 String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
	 String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
	 String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
	 try {
	 logi.logInfo("Calling uv_getCreditActivtyLoopItems_DB_222_two")
	 HashMap hm = new HashMap<String,String>()
	 ConnectDB db = new ConnectDB()
	 int counter=0
	 String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
	 String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
	 String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
	 logi.logInfo("voidinvoice "+voidinvoice)
	 String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
	 //CashCredits
	 String cashCreditsCountQuery="select count(*) from ariacore.acct_transaction where acct_no="+acct_no+"  and transaction_type=10"
	 int rows1=db.executeQueryP2(String.format(cashCreditsCountQuery,acct_no,invoiceNo)).toInteger()
	 String query11="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT   CASE  WHEN credit_type='C'  THEN 'Cash Credit'  END  AS SHORTCREDITACTIVITYDESCRIPTION ,  reason_cd AS CREDITACTIVITYREASONCODE,TO_CHAR(TRUNC(create_date),'dd-mm-yyyy') AS CREDITACTIVITYDATE,REPLACE(( CASE WHEN TO_CHAR(TRUNC(proration_related_start_date ,'dd-mm-yyyy')) IS NULL THEN ' ' ELSE TO_CHAR(TRUNC( proration_related_start_date,'dd-mm-yyyy')) END),'') AS CREDITACTIVITYSTARTDATE,REPLACE((CASE WHEN TO_CHAR(TRUNC(proration_related_end_date ,'dd-mm-yyyy')) IS NULL THEN ' 'ELSE TO_CHAR(TRUNC( proration_related_end_date,'dd-mm-yyyy'))    END),'')    AS CREDITACTIVITYENDDATE,reason_text AS CREDITACTIVITYREASONTEXT,    'Cash Credit (applied '|| TO_CHAR(TRUNC(create_date),'dd-mm-yyyy')|| ')' AS CREDITACTIVITYDESCRIPTION,    comments AS CREDITACTIVITYCOMMENT,('%s ') ||trim(TO_CHAR(left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE(( CASE WHEN orig_coupon_cd IS NULL THEN ' 'ELSE orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||'-'  ||trim(TO_CHAR(amount,'99,990.99'))   AS CREDITACTIVITYAMOUNT,row_number() over(order by credit_id DESC) AS seq FROM ariacore.all_credits WHERE acct_no  ='%s' AND credit_type='C')a WHERE seq='%s'"
	 for(int row=1 ; row<=rows1 ; row++){
	 counter=counter+1
	 hm = db.executeQueryP2(String.format(query11,symbol,symbol,acct_no,row))
	 logi.logInfo("Individual row of hash after adding cash credits :: "+hm.toString())
	 hm.each {k,v ->
	 String v1= v.toString().trim()
	 hm.put(k,v1)
	 }
	 hm=hm.sort()
	 creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)
	 }
	 logi.logInfo( "counter value : "+counter.toString())
	 //payments
	 String paymnetsCountQuery1= "SELECT COUNT(*)   FROM ariacore.acct_transaction ATRANS   JOIN ARIACORE.transaction_types TRANS   ON ATRANS.TRANSACTION_TYPE  = TRANS.TRANSACTION_TYPE   WHERE ATRANS.acct_no = %s   AND TRANS.transaction_type IN (2,3)   AND atrans.statement_no     =    (SELECT MAX(statement_no)     FROM ariacore.acct_statement     WHERE acct_no = %s ) "
	 int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
	 String query4="SELECT  CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM   (SELECT TRANS.DESCRIPTION as SHORTCREDITACTIVITYDESCRIPTION,  ('%s' ||' '|| '-' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99'))) AS CREDITACTIVITYAMOUNT , to_char(ATRANS.CREATE_DATE,'dd-mm-yyyy') as CREDITACTIVITYDATE ,  '%s' ||' ' || trim(to_char((ATRANS.amount-ATRANS.applied_amount),'99,990.99')) as CREDITACTIVITYUNAPPLIEDAMOUNT,  case when ATRANS.transaction_type=3 then 'Electronic Payment (applied ' else 'Check (applied ' end || to_char(ATRANS.CREATE_DATE,'dd-mm-yyyy') || ')' as CREDITACTIVITYDESCRIPTION, ' ' as CREDITACTIVITYREASONCODE, ' ' as CREDITACTIVITYREASONTEXT, ' ' as CREDITACTIVITYSTARTDATE, ' ' as CREDITACTIVITYENDDATE,' ' as CREDITACTIVITYCOUPONCODE ,' ' as CREDITACTIVITYCOMMENT, row_number() over (order by event_no ASC)      AS seq   FROM ariacore.acct_transaction ATRANS   JOIN ARIACORE.transaction_types TRANS   ON ATRANS.TRANSACTION_TYPE  = TRANS.TRANSACTION_TYPE   WHERE ATRANS.acct_no        = %s   AND TRANS.transaction_type IN (2,3)  AND atrans.statement_no     =   (SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no = %s     )   )A  where A.seq=%s"
	 for(int a=1 ; a<=paymnetscount1 ; a++){
	 counter=counter+1
	 hm = db.executeQueryP2(String.format(query4,symbol,symbol,acct_no,acct_no,a))
	 hm.each {k,v ->
	 String v1= v.toString().trim()
	 hm.put(k,v1)
	 }
	 logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
	 hm=hm.sort()
	 creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)
	 }
	 //ServiceCredits
	 String svcCreditsCountQuery="select count(*) from (select row_number() over(ORDER BY seq_num asc ) as sno  from ariacore.all_invoice_details where invoice_no=%s and orig_credit_id is not null and orig_credit_id in (select credit_id from  ariacore.credits where acct_no=%s and reason_cd=97) )"
	 int rows2=db.executeQueryP2(String.format(svcCreditsCountQuery,invoiceNo,acct_no)).toInteger()
	 String query12="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT  TO_CHAR(TRUNC(cs.create_date),'dd-mm-yyyy') AS CREDITACTIVITYDATE ,'Service Credit' as SHORTCREDITACTIVITYDESCRIPTION ,   ('%s ')  ||trim(TO_CHAR(cs.left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE((CASE WHEN ids.orig_coupon_cd IS NULL THEN ' '   ELSE ids.orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||trim(TO_CHAR(ids.debit,'99,990.99'))        AS CREDITACTIVITYAMOUNT, REPLACE((CASE WHEN TO_CHAR(cs.proration_related_start_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR( cs.proration_related_start_date,'dd-mm-yyyy')  END),'') AS CREDITACTIVITYSTARTDATE,  REPLACE((  CASE  WHEN TO_CHAR(cs.proration_related_end_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR(cs.proration_related_end_date,'dd-mm-yyyy')  END),'')    AS CREDITACTIVITYENDDATE, cs.reason_text AS CREDITACTIVITYREASONTEXT, to_char(cs.reason_cd)  AS CREDITACTIVITYREASONCODE, 'Service Credit (applied '  || TO_CHAR(TRUNC(cs.create_date),'dd-mm-yyyy') || ')'   AS CREDITACTIVITYDESCRIPTION,cs.comments AS CREDITACTIVITYCOMMENT, row_number() over(ORDER BY ids.seq_num ASC ) AS sno FROM ariacore.all_invoice_details ids join ariacore.all_credits cs on cs.client_no=ids.client_no and ids.orig_credit_id= cs.credit_id WHERE ids.invoice_no    =%s  AND ids.orig_credit_id IS NOT NULL and  cs.acct_no=%s  AND cs.reason_cd=97  )where sno=%s "
	 for(int row=1 ; row<=rows2 ; row++){
	 counter=counter+1
	 hm = db.executeQueryP2(String.format(query12,symbol,symbol,invoiceNo,acct_no,row))
	 logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
	 hm.each {k,v ->
	 String v1= v.toString().trim()
	 hm.put(k,v1)
	 }
	 hm=hm.sort()
	 creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)
	 }
	 //Coupons
	 String couponsCountquery="select count(rule_no) from ariacore.all_invoice_details  ids  join ariacore.coupon_discount_rule_map bm on bm.coupon_cd=ids.orig_coupon_cd where ids.invoice_no=%s and ids.orig_coupon_cd is not null  and ids.orig_credit_id is null "
	 int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
	 String couponquery="select CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION from (select row_number()  over (order by seq_num )as sno , comments|| ' (applied ' || to_char(start_Date,'dd-mm-yyyy')||')' as CREDITACTIVITYDESCRIPTION ,'Coupon Application' as CREDITACTIVITYREASONTEXT ,'9999' as CREDITACTIVITYREASONCODE ,comments as CREDITACTIVITYCOMMENT,('%s ') || '0.00'as CREDITACTIVITYUNAPPLIEDAMOUNT  ,to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYDATE , to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYSTARTDATE ,to_char(end_date,'dd-mm-yyyy') as CREDITACTIVITYENDDATE,orig_coupon_cd as CREDITACTIVITYCOUPONCODE ,('%s ')||trim(to_char(debit,'99,990.99')) as CREDITACTIVITYAMOUNT ,comments as SHORTCREDITACTIVITYDESCRIPTION from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  ) where sno=%s"
	 //String couponamountquery="select '%s ' || CREDITACTIVITYAMOUNT from (SELECT orig_coupon_cd, TRIM(TO_CHAR((NVL(SUM(debit),0)),'99,990.99')) as CREDITACTIVITYAMOUNT FROM ariacore.all_invoice_details WHERE invoice_no    =%s AND orig_coupon_cd IS NOT NULL group by orig_coupon_cd )"
	 for(int a=1 ; a<=couponsCount ; a++){
	 counter=counter+1
	 hm = db.executeQueryP2(String.format(couponquery,symbol,symbol,invoiceNo,a))
	 logi.logInfo("Individual row of hash after adding DRs :: "+hm.toString())
	 //String amount=db.executeQueryP2(String.format(couponquery,symbol,invoiceNo))
	 //hm.put("CREDITACTIVITYAMOUNT",amount)
	 hm=hm.sort()
	 creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)
	 }
	 logi.logInfo( "counter value : "+counter.toString())
	 //Discount bundles
	 String bundlesCountquery="select count(distinct bundle_no) from ariacore.all_invoice_details  ids join ariacore.coupon_discount_bundle_map bm on bm.coupon_cd=ids.orig_coupon_cd where ids.invoice_no=%s and ids.orig_coupon_cd is not null  and ids.orig_credit_id is null "
	 int bundlesCount=db.executeQueryP2(String.format(bundlesCountquery,invoiceNo)).toInteger()
	 String bundlequery="select distinct * from (select comments|| ' (applied ' || to_char(start_Date,'dd-mm-yyyy')||')' as CREDITACTIVITYDESCRIPTION ,'Coupon Application' as CREDITACTIVITYREASONTEXT ,'9999' as CREDITACTIVITYREASONCODE ,comments as CREDITACTIVITYCOMMENT,('%s ') || '0.00'as CREDITACTIVITYUNAPPLIEDAMOUNT  ,to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYDATE , to_char(start_date,'dd-mm-yyyy') as CREDITACTIVITYSTARTDATE ,to_char(end_date,'dd-mm-yyyy') as CREDITACTIVITYENDDATE,orig_coupon_cd as CREDITACTIVITYCOUPONCODE ,('%s ')||trim(to_char(debit,'99,990.99')) as CREDITACTIVITYAMOUNT ,comments as SHORTCREDITACTIVITYDESCRIPTION from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  )"
	 String amountquery="select '%s ' || CREDITACTIVITYAMOUNT from (SELECT orig_coupon_cd, TRIM(TO_CHAR((NVL(SUM(debit),0)),'99,990.99')) as CREDITACTIVITYAMOUNT FROM ariacore.all_invoice_details WHERE invoice_no    =%s AND orig_coupon_cd IS NOT NULL group by orig_coupon_cd )"
	 for(int a=1 ; a<=bundlesCount ; a++){
	 counter=counter+1
	 hm = db.executeQueryP2(String.format(bundlequery,symbol,symbol,invoiceNo))
	 logi.logInfo("Individual row of hash after adding bundles :: "+hm.toString())
	 String camount=db.executeQueryP2(String.format(amountquery,symbol,invoiceNo))
	 hm.put("CREDITACTIVITYAMOUNT",camount)
	 hm=hm.sort()
	 creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)
	 }
	 logi.logInfo( "counter value : "+counter.toString())
	 }
	 catch (Exception  e) {
	 logi.logInfo e.getMessage()
	 }
	 return creditActivtyLoopItemsDB222
	 }*/
	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_222_two(String tcid)

	{

		LinkedHashMap creditActivtyLoopItemsDB222 = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		try {
			logi.logInfo("Calling uv_getCreditActivtyLoopItems_DB_222_two")


			LinkedHashMap hm = new LinkedHashMap<String,String>()

			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			//payments
			String query4="SELECT  CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM   (SELECT TRANS.DESCRIPTION as SHORTCREDITACTIVITYDESCRIPTION,  ('%s' ||' '|| '-' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99'))) AS CREDITACTIVITYAMOUNT , cast(ATRANS.CREATE_DATE as date) as CREDITACTIVITYDATE ,  '%s' ||' ' || trim(to_char((ATRANS.amount-ATRANS.applied_amount),'99,990.99')) as CREDITACTIVITYUNAPPLIEDAMOUNT,  case when ATRANS.transaction_type=3 then 'Electronic Payment (applied ' else 'Check (applied ' end || to_char(ATRANS.CREATE_DATE,'dd-mm-yyyy') || ')' as CREDITACTIVITYDESCRIPTION, ' ' as CREDITACTIVITYREASONCODE, ' ' as CREDITACTIVITYREASONTEXT, ' ' as CREDITACTIVITYSTARTDATE, ' ' as CREDITACTIVITYENDDATE,' ' as CREDITACTIVITYCOUPONCODE ,' ' as CREDITACTIVITYCOMMENT, row_number() over (order by event_no ASC)      AS seq   FROM ariacore.acct_transaction ATRANS   JOIN ARIACORE.transaction_types TRANS   ON ATRANS.TRANSACTION_TYPE  = TRANS.TRANSACTION_TYPE   WHERE ATRANS.acct_no        = %s   AND TRANS.transaction_type IN (2,3)  AND atrans.statement_no     =   (SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no = %s     )   )A "
			String f1=String.format(query4,symbol,symbol,acct_no,acct_no)
			logi.logInfo(":: Individual Query for payments  :: ")
			logi.logInfo(f1)


			//CashCredits
			String query11="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT   CASE  WHEN credit_type='C'  THEN 'Cash Credit'  END  AS SHORTCREDITACTIVITYDESCRIPTION ,  to_char(reason_cd) AS CREDITACTIVITYREASONCODE,cast(CREATE_DATE as date)  AS CREDITACTIVITYDATE,REPLACE(( CASE WHEN TO_CHAR(TRUNC(proration_related_start_date ,'dd-mm-yyyy')) IS NULL THEN ' ' ELSE TO_CHAR(TRUNC( proration_related_start_date,'dd-mm-yyyy')) END),'') AS CREDITACTIVITYSTARTDATE,REPLACE((CASE WHEN TO_CHAR(TRUNC(proration_related_end_date ,'dd-mm-yyyy')) IS NULL THEN ' 'ELSE TO_CHAR(TRUNC( proration_related_end_date,'dd-mm-yyyy'))    END),'')    AS CREDITACTIVITYENDDATE,reason_text AS CREDITACTIVITYREASONTEXT,    'Cash Credit (applied '|| TO_CHAR(TRUNC(create_date),'dd-mm-yyyy')|| ')' AS CREDITACTIVITYDESCRIPTION,    comments AS CREDITACTIVITYCOMMENT,('%s ') ||trim(TO_CHAR(left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE(( CASE WHEN orig_coupon_cd IS NULL THEN ' 'ELSE orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||'-'  ||trim(TO_CHAR(amount,'99,990.99'))   AS CREDITACTIVITYAMOUNT,row_number() over(order by credit_id DESC) AS seq FROM ariacore.all_credits WHERE acct_no  ='%s' AND credit_type='C')a "
			String f2= String.format(query11,symbol,symbol,acct_no)
			logi.logInfo(":: Individual Query for cash credits  :: ")
			logi.logInfo(f2)

			//ProrationServiceCredits
			String query12="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT  cast(g.bill_date as date) AS CREDITACTIVITYDATE ,'Service Credit' as SHORTCREDITACTIVITYDESCRIPTION ,   ('%s ')  ||trim(TO_CHAR(cs.left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE((CASE WHEN ids.orig_coupon_cd IS NULL THEN ' '   ELSE ids.orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||trim(TO_CHAR(ids.debit,'99,990.99'))        AS CREDITACTIVITYAMOUNT, REPLACE((CASE WHEN TO_CHAR(cs.proration_related_start_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR( cs.proration_related_start_date,'dd-mm-yyyy')  END),'') AS CREDITACTIVITYSTARTDATE,  REPLACE((  CASE  WHEN TO_CHAR(cs.proration_related_end_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR(cs.proration_related_end_date,'dd-mm-yyyy')  END),'')    AS CREDITACTIVITYENDDATE, cs.reason_text AS CREDITACTIVITYREASONTEXT, to_char(cs.reason_cd)  AS CREDITACTIVITYREASONCODE, 'Service Credit (applied '  || TO_CHAR(TRUNC(cs.create_date),'dd-mm-yyyy') || ')'   AS CREDITACTIVITYDESCRIPTION,cs.comments AS CREDITACTIVITYCOMMENT, row_number() over(ORDER BY ids.seq_num ASC ) AS sno FROM ariacore.all_invoice_details ids join ariacore.all_credits cs on cs.client_no=ids.client_no and ids.orig_credit_id= cs.credit_id  join ariacore.gl g on g.invoice_no=ids.invoice_no WHERE ids.invoice_no    =%s  AND ids.orig_credit_id IS NOT NULL and  cs.acct_no=%s  AND cs.reason_cd=97  order by 1)"
			String f3 = String.format(query12,symbol,symbol,invoiceNo,acct_no)
			logi.logInfo(":: Individual Query forProration ServiceCredits  :: ")
			logi.logInfo(f3)

			//Discount rules
			String couponquery="select CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION from (select row_number()  over (order by ids.seq_num )as sno , ids.comments|| ' (applied ' || to_char(ids.start_Date,'dd-mm-yyyy')||')' as CREDITACTIVITYDESCRIPTION ,'Coupon Application' as CREDITACTIVITYREASONTEXT ,'9999' as CREDITACTIVITYREASONCODE ,ids.comments as CREDITACTIVITYCOMMENT,('%s ') || '0.00'as CREDITACTIVITYUNAPPLIEDAMOUNT  ,cast(g.bill_date as date) as CREDITACTIVITYDATE , to_char(ids.start_date,'dd-mm-yyyy') as CREDITACTIVITYSTARTDATE ,to_char(ids.end_date,'dd-mm-yyyy') as CREDITACTIVITYENDDATE,ids.orig_coupon_cd as CREDITACTIVITYCOUPONCODE ,('%s ')||trim(to_char(ids.debit,'99,990.99')) as CREDITACTIVITYAMOUNT ,ids.comments as SHORTCREDITACTIVITYDESCRIPTION from ariacore.all_invoice_details ids join ariacore.gl g on g.invoice_no=ids.invoice_no   join  ariacore.all_invoice_discount_details glddd on glddd.invoice_no = ids.invoice_no    AND ids.seq_num = glddd.gl_discount_seq_num  join  ariacore.client_discount_rules cdr on glddd.rule_no = cdr.rule_no AND glddd.client_no = cdr.client_no  left join ariacore.client_trans_type_alt_label alt_labels on  alt_labels.client_no= cdr.client_no where ids.invoice_no=%s and orig_coupon_cd is not null  )"
			String f4=String.format(couponquery,symbol,symbol,invoiceNo)
			logi.logInfo(":: Individual Query for discount rules  :: ")
			logi.logInfo(f4)

			//Discount bundles
			String bundlequery="select CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION from (select ids.comments|| ' (applied ' || to_char(ids.start_Date,'dd-mm-yyyy')||')' as CREDITACTIVITYDESCRIPTION ,'Coupon Application' as CREDITACTIVITYREASONTEXT ,'9999' as CREDITACTIVITYREASONCODE ,ids.comments as CREDITACTIVITYCOMMENT,('%s ') || '0.00'as CREDITACTIVITYUNAPPLIEDAMOUNT  ,cast(ids.start_date as date)  as CREDITACTIVITYDATE , to_char(ids.start_date,'dd-mm-yyyy') as CREDITACTIVITYSTARTDATE ,to_char(ids.end_date,'dd-mm-yyyy') as CREDITACTIVITYENDDATE,ids.orig_coupon_cd as CREDITACTIVITYCOUPONCODE ,('%s ')||trim(to_char(ids.debit,'99,990.99')) as CREDITACTIVITYAMOUNT ,ids.comments as SHORTCREDITACTIVITYDESCRIPTION from ariacore.all_invoice_details  ids join ariacore.coupon_discount_bundle_map bm on bm.coupon_cd=ids.orig_coupon_cd  where ids.invoice_no=%s and ids.orig_coupon_cd is not null  and ids.orig_credit_id is null )"
			String f5 = String.format(bundlequery,symbol,symbol,invoiceNo)
			logi.logInfo(":: Individual Query for discount bundles  :: ")
			logi.logInfo(f5)

			//Paymethod credits
			String query13="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT  cast(g.bill_date as date) AS CREDITACTIVITYDATE ,'Service Credit' as SHORTCREDITACTIVITYDESCRIPTION ,   ('%s ')  ||trim(TO_CHAR(cs.left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE((CASE WHEN ids.orig_coupon_cd IS NULL THEN ' '   ELSE ids.orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||trim(TO_CHAR(ids.debit,'99,990.99'))        AS CREDITACTIVITYAMOUNT, REPLACE((CASE WHEN TO_CHAR(cs.proration_related_start_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR( cs.proration_related_start_date,'dd-mm-yyyy')  END),'') AS CREDITACTIVITYSTARTDATE,  REPLACE((  CASE  WHEN TO_CHAR(cs.proration_related_end_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR(cs.proration_related_end_date,'dd-mm-yyyy')  END),'')    AS CREDITACTIVITYENDDATE, cs.reason_text AS CREDITACTIVITYREASONTEXT, to_char(cs.reason_cd)  AS CREDITACTIVITYREASONCODE, 'Service Credit (applied '  || TO_CHAR(TRUNC(cs.create_date),'dd-mm-yyyy') || ')'   AS CREDITACTIVITYDESCRIPTION,cs.comments AS CREDITACTIVITYCOMMENT, row_number() over(ORDER BY ids.seq_num ASC ) AS sno FROM ariacore.all_invoice_details ids join ariacore.all_credits cs on cs.client_no=ids.client_no and ids.orig_credit_id= cs.credit_id  join ariacore.gl g on g.invoice_no=ids.invoice_no WHERE ids.invoice_no    =%s  AND ids.orig_credit_id IS NOT NULL and  cs.acct_no=%s  AND cs.reason_cd=95  order by 1)"
			String f6 = String.format(query13,symbol,symbol,invoiceNo,acct_no)
			logi.logInfo(":: Individual Query for Paymethod credits  :: ")
			logi.logInfo(f6)
			
			//GeneralServiceCredits
			String query14="SELECT CREDITACTIVITYREASONTEXT,CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT,CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE,CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT,SHORTCREDITACTIVITYDESCRIPTION FROM  (SELECT  cast(g.bill_date as date) AS CREDITACTIVITYDATE ,'Service Credit' as SHORTCREDITACTIVITYDESCRIPTION ,   ('%s ')  ||trim(TO_CHAR(cs.left_to_apply,'99,990.99')) AS CREDITACTIVITYUNAPPLIEDAMOUNT ,REPLACE((CASE WHEN ids.orig_coupon_cd IS NULL THEN ' '   ELSE ids.orig_coupon_cd END),'') AS CREDITACTIVITYCOUPONCODE,('%s ')||trim(TO_CHAR(ids.debit,'99,990.99'))        AS CREDITACTIVITYAMOUNT, REPLACE((CASE WHEN TO_CHAR(cs.proration_related_start_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR( cs.proration_related_start_date,'dd-mm-yyyy')  END),'') AS CREDITACTIVITYSTARTDATE,  REPLACE((  CASE  WHEN TO_CHAR(cs.proration_related_end_date ,'dd-mm-yyyy') IS NULL   THEN ' '  ELSE TO_CHAR(cs.proration_related_end_date,'dd-mm-yyyy')  END),'')    AS CREDITACTIVITYENDDATE, cs.reason_text AS CREDITACTIVITYREASONTEXT, to_char(cs.reason_cd)  AS CREDITACTIVITYREASONCODE, 'Service Credit (applied '  || TO_CHAR(TRUNC(cs.create_date),'dd-mm-yyyy') || ')'   AS CREDITACTIVITYDESCRIPTION,cs.comments AS CREDITACTIVITYCOMMENT, row_number() over(ORDER BY ids.seq_num ASC ) AS sno FROM ariacore.all_invoice_details ids join ariacore.all_credits cs on cs.client_no=ids.client_no and ids.orig_credit_id= cs.credit_id  join ariacore.gl g on g.invoice_no=ids.invoice_no WHERE ids.invoice_no    =%s  AND ids.orig_credit_id IS NOT NULL and  cs.acct_no=%s  AND cs.reason_cd=1  order by 1)"
			String f7 = String.format(query14,symbol,symbol,invoiceNo,acct_no)
			logi.logInfo(":: Individual Query for General ServiceCredits  :: ")
			logi.logInfo(f7)
			
			String f="select  CREDITACTIVITYREASONTEXT, CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT, CREDITACTIVITYDESCRIPTION, CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE, CREDITACTIVITYENDDATE,CREDITACTIVITYAMOUNT, SHORTCREDITACTIVITYDESCRIPTION  from (select p.*,row_number() over (ORDER BY 1 ) lineno   from (select CREDITACTIVITYREASONTEXT, CREDITACTIVITYUNAPPLIEDAMOUNT,CREDITACTIVITYREASONCODE,to_char(CREDITACTIVITYDATE,'dd-mm-yyyy') as CREDITACTIVITYDATE,CREDITACTIVITYCOMMENT, CREDITACTIVITYDESCRIPTION,CREDITACTIVITYCOUPONCODE,CREDITACTIVITYSTARTDATE, CREDITACTIVITYENDDATE, CREDITACTIVITYAMOUNT,  SHORTCREDITACTIVITYDESCRIPTION   from ( "+f1+" UNION ALL "+f2+" UNION ALL "+f3+" UNION ALL "+f4+" UNION ALL "+f5	+" UNION ALL "+f6+"	UNION ALL "+f7+" ) order by CREDITACTIVITYDATE )p )q where q.lineno=%s"
			logi.logInfo("<==== Final Query for all credits  ==> ")
			logi.logInfo(f)

			int tcount=db.executeQueryP2 ("select count(*) from ( "+f1+" UNION ALL "+f2+" UNION ALL "+f3+" UNION ALL "+f4+" UNION ALL "+f5	+" UNION ALL "+f6+"UNION ALL "+f7+" ) order by CREDITACTIVITYDATE").toInteger()
			logi.logInfo("Total number of credits ::  "+tcount.toString())
			for(int a=1 ; a<=tcount ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(f,a))
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				hm=hm.sort()
				logi.logInfo("Final row of hash after all credits :: "+hm.toString())
				creditActivtyLoopItemsDB222.put("Line Item"+counter,hm)


			}



		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB222
	}
	public String uv_clientName_209(String tcid)
	{
		String formattedXpath="//p[contains(text(),', please contact ')]"
		String uiValue
		if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
			uiValue= driver.findElement(By.xpath(formattedXpath)).getText().split(", please contact ")[1]
			uiValue=uiValue.split(" Customer Care:")[0]
			return uiValue.trim()
		}
	}
	public String uv_ServGrp1Total_209(String tcid)
	{

		logi.logInfo " Entered into uv_ServGrp1Total_209 method "
		ConnectDB db = new ConnectDB()
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		String invoice_No = db.executeQueryP2(invoiceQry)
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		//String gldbtq="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND SOURCE_NO="+invoice_No
		String amtquery=" select TRIM(TO_CHAR((NVL(SUM(debit),0)),'99,990.99'))  from ariacore.all_invoice_details where invoice_no=%s and service_no = (select  service_no from ARIACORE.client_service_grp_map where client_no=%s and grp_num=1)"
		String val=db.executeQueryP2(String.format(amtquery, invoice_No,clientNo))
		return val
	}
	public String uv_AcctTotalOwed (String tcid)
	{

		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_insertAcctTotalOwed(acct_no)
	}
	public String uv_ChildAcctTotalOwed (String tcid)
	{

		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_insertAcctTotalOwed(acct_no)
	}
	public String uv_insertAcctTotalOwed (String acct_no)
	{
		DecimalFormat d = new DecimalFormat("##,##0.00");
		logi.logInfo " Entered into uv_AcctTotalOwed method "
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String transQuery="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND transaction_type in (4)"
		String gldbtq="SELECT CASE WHEN bal_fwd IS NULL THEN TRIM(TO_CHAR(NVL((debit-credit),0),'99,990.99')) ELSE TRIM(TO_CHAR(NVL(((bal_fwd+debit)-credit),0),'99,990.99')) End FROM ariacore.gl WHERE acct_no =%s and invoice_no = (select max(invoice_no) from ariacore.gl where acct_no=%s) "
		glDebit=db.executeQueryP2(String.format(gldbtq,acct_no,acct_no))
		String transamt=db.executeQueryP2(transQuery)
		return  d.format( glDebit.toDouble()+transamt.toDouble()).toString()
	}
	public LinkedHashMap uv_getInvoiceLineItemsFromStatements_DB_210(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_DB_210")
		String accountNo = u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_DB_210(accountNo)

	}
	public LinkedHashMap uv_getChildInvoiceLineItemsFromStatements_DB_210(String tcid)
	{
		logi.logInfo("Calling uv_getInvoiceLineItemsFromStatements_DB_210")
		String accountNo = u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		return uv_stmt_getInvoiceLineItemsFromStatements_DB_210(accountNo)

	}
	public LinkedHashMap uv_stmt_getInvoiceLineItemsFromStatements_DB_210(String accountNo){

		logi.logInfo("Calling uv_stmt_getInvoiceLineItemsFromStatements_DB_210")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		LinkedHashMap hm = new LinkedHashMap<String,String>()
		List<String> gl_seq_num = []
		List<String> exclude_seq_num = []
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0

		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		String seqNumQry = "Select count(*) from ariacore.all_invoice_details glt JOIN ariacore.services ser ON glt.service_no=ser.service_no where invoice_no= %s AND  (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) and (ser.recurring=1 or  orig_coupon_cd is not null)"
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT comments, (CASE WHEN (START_DATE IS NULL OR END_DATE IS NULL) THEN '-' ELSE  to_number(TO_CHAR(start_date,'dd'))  || '-'  || to_number(TO_CHAR(start_date,'mm'))  || '-'  ||to_number(TO_CHAR(start_date,'yyyy')) || ' - ' ||(TO_CHAR(end_date,'dd'))  || '-'  || to_number(TO_CHAR(end_date,'mm'))  || '-'  ||to_number(TO_CHAR(end_date,'yyyy')) END) AS Result, usage_units, usage_rate, debit  FROM ariacore.all_invoice_details WHERE invoice_no =%s and seq_num=%s "
		String generalQry =  "SELECT CASE when a.orig_coupon_cd is not null then a.comments|| ' (applied ' || '%s' || ')'  WHEN a.service_no=0  THEN  ' Service Credit (applied ' || '%s'   || ')' else (TRIM(TO_CHAR(a.plan_name)) || ' ' || (CASE WHEN (a.START_DATE IS NULL OR a.END_DATE IS NULL) THEN '-' ELSE (TO_CHAR(a.start_date,'dd-mm-yyyy')) ||' - '|| (TO_CHAR(a.end_date,'dd-mm-yyyy')) END) ) end  AS Plan_name,  case when  a.orig_coupon_cd is not null     then ' ' WHEN a.service_no=0     THEN ' ' else TRIM(TO_CHAR(a.comments))  end  as Service_name , TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE  WHEN ( a.recurring=1 and   a.orig_coupon_cd is null )THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.service_no=0 THEN ' '  when a.orig_coupon_cd is not null     then ' ' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  when a.orig_coupon_cd IS NOT NULL then ' '  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, aid.usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup,ser.is_order_based,aid.orig_coupon_cd,aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND   (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		//"SELECT TRIM(TO_CHAR(a.plan_name)) as Plan_name, TRIM(TO_CHAR(a.comments)) as Service_name , TRIM(TO_CHAR(a.debit,'99,990.99')) as Amount, (CASE  WHEN a.recurring=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate||'.00') WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') END) AS Rate,(CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' )  ELSE TO_CHAR(a.usage_units)  END ) AS Units FROM (SELECT ser.recurring, aid.usage_rate, aid.comments, aid.usage_units, aid.debit, ser.usage_based, aid.plan_name, ser.is_setup,ser.is_order_based, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND aid.seq_num = %s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.all_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"

		ConnectDB db = new ConnectDB()

		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
		exclude_seq_num.add(db.executeQueryP2("SELECT glt.seq_num FROM ariacore.all_invoice_details glt JOIN ariacore.services ser ON glt.service_no =ser.service_no WHERE invoice_no= "+invoiceNo +" AND (ser.custom_to_client_no="+ clientNo+" OR ser.custom_to_client_no IS NULL) and ser.is_setup=1 and glt.orig_coupon_cd is null ") )
		exclude_seq_num.add(db.executeQueryP2("SELECT glt.seq_num FROM ariacore.all_invoice_details glt JOIN ariacore.services ser ON glt.service_no =ser.service_no WHERE invoice_no= "+invoiceNo +" AND (ser.custom_to_client_no="+ clientNo+" OR ser.custom_to_client_no IS NULL) and ser.is_surcharge=1") )
		logi.logInfo("Excluded Seq num list  :: "+exclude_seq_num.toString())
		String exc_text=ConvertArrayListtoCommaDelimited(exclude_seq_num)
		logi.logInfo("exc_text is :: "+exc_text)
		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,clientNo))
		logi.logInfo("Seq num is :: "+seqCount)

		String lineitemsSeqQuery="SELECT glt.seq_num FROM ariacore.all_invoice_details glt JOIN ariacore.services ser ON glt.service_no =ser.service_no WHERE invoice_no=%s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) and glt.seq_num not in (%s) and (glt.orig_coupon_cd is  null and glt.orig_credit_id is  null) "
		String creditlineitemsSeqQuery="SELECT gs.seq_num FROM ariacore.all_invoice_details gs JOIN ariacore.services ser ON gs.service_no=ser.service_no WHERE invoice_no =%s AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) and gs.seq_num not in (%s) and (gs.orig_coupon_cd is not null or gs.orig_credit_id is not null) order by gs.orig_credit_id "
		ResultSet rs= db.executePlaQuery(String.format(lineitemsSeqQuery,invoiceNo,clientNo,exc_text))

		while(rs.next())
		{
			String num = rs.getString(1)
			gl_seq_num.add(num)
		}
		rs.close();
		ResultSet rs1= db.executePlaQuery(String.format(creditlineitemsSeqQuery,invoiceNo,clientNo,exc_text))

		while(rs1.next())
		{
			String num1 = rs1.getString(1)
			gl_seq_num.add(num1)
		}
		rs1.close();

		logi.logInfo("Seq num list is :: "+gl_seq_num.toString())
		for(int row=1 ; row<=gl_seq_num.size() ; row++){

			recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row))
			logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			logi.logInfo("usageRateScale num is :: "+usageRateScale)

			if(tempUsageRate != null){
				logi.logInfo("Inside not null statement")
				recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			} else{
				logi.logInfo("Inside not null else statement")
				recRateScale = Integer.parseInt(recuringRateScale)
				logi.logInfo("recRateScale is :: "+recRateScale)
				usgRateScale = Integer.parseInt(usageRateScale)
				logi.logInfo("usgRateScale is :: "+usgRateScale)
			}

			logi.logInfo("Formatted query"+String.format(generalQry,glbillDate,glbillDate,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))

			hm = db.executeQueryP2(String.format(generalQry,glbillDate,glbillDate,recRateScale,usgRateScale,invoiceNo,gl_seq_num.get(row-1).toString(),clientNo))
			hm.each {k,v ->
				if(k.toString().contains("PLAN"))
				{
					String v1= v.toString().trim()
					hm.put(k,v1)
				}

			}
			logi.logInfo("Individual row of hash is :: "+hm.toString())
			hm=hm.sort()
			mDB.put("Line Item"+row,hm)

		}

		logi.logInfo("Final hash is : "+mDB.toString())

		return mDB.sort();
	}
	public LinkedHashMap uv_getTrueCreditActivtyLoopItems_ST_202(String tcid)

	{
		logi.logInfo("Calling uv_getCreditActivtyLoopItems_ST_202")
		String creditLoopTable="//table[4]//td[contains(@style,'font-weight:bold')]/../td[1][contains(text(),'-')]/.."
		LinkedHashMap<String,String> creditActivtyLoopItems=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'CREDITACTIVITYDATE',
			'CREDITSHORTACTIVITYDESCRIPTION',
			'CREDITACTIVITYAMOUNT'
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(creditLoopTable));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td[string-length()>1]"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText())
					kcounter=kcounter+1

				}
				kcounter=0
				hm=hm.sort()
				creditActivtyLoopItems.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}


		return creditActivtyLoopItems

	}
	public LinkedHashMap uv_getTrueCreditActivtyLoopItems_DB_202(String tcid)
	{
		String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
		return uv_getSTmtTrueCreditActivtiyLoopItems_DB_202(acct_no)
	}
	public LinkedHashMap uv_getSTmtTrueCreditActivtiyLoopItems_DB_202(String acct_no)
	{
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		try {
			logi.logInfo("Calling uv_getSTmtCreditActivtiyLoopItems_DB_202")

			HashMap hm = new HashMap<String,String>()

			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			//payments
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT to_char(create_date,'DD-MM-YYYY') as CREDITACTIVITYDATE,a.description as CREDITSHORTACTIVITYDESCRIPTION ,  a.amount as CREDITACTIVITYAMOUNT FROM (SELECT TRANS.DESCRIPTION ,('%s'||' -'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				hm=hm.sort()
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			// refunds
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT to_char(create_date,'DD-MM-YYYY') as CREDITACTIVITYDATE,a.description as CREDITSHORTACTIVITYDESCRIPTION ,  a.amount as CREDITACTIVITYAMOUNT FROM (SELECT TRANS.DESCRIPTION ,('%s'||' ' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					hm=hm.sort()
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}
			logi.logInfo( "counter value : "+counter.toString())
		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB
	}
	public String uv_getChildTotalStmntCredits(String tcid)
	{
		logi.logInfo("Calling uv_getChildTotalStmntCredits")
		DecimalFormat d = new DecimalFormat("0.00");
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String fval=" - "+d.format(val).toString()
		return fval.trim()
	}
	public String uv_getChildOrderComments_DB_201(String tcid)
	{
		logi.logInfo("Calling uv_getChildOrderComments_DB_201")
		String acct_no=u.getValueFromResponse("create_acct_complete.a","//*/*:acct_no[1]")
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String val=db.executeQueryP2("SELECT REPLACE((   CASE    WHEN comments IS NULL    THEN ' '    ELSE comments  END),'') AS FROM_DATE FROM ARIACORE.ORDERS WHERE ACCT_NO="+acct_no)
		return val.trim()
	}
	public LinkedHashMap uv_getWTaxlAllItemsLoop_ST_205(String tcid)

	{

		logi.logInfo("Calling uv_getWTaxlAllItemsLoop_ST_205")
		String wTaxLoopTable205="//div[@id='content']/div[1]/table[2]//td[@class='lineitem']/.."
		LinkedHashMap<String,String> wTaxLoopItems205=new LinkedHashMap<String , HashMap<String,String>>()
		List<String> keys=[
			'DESCRIPTION',
			'QUANTITY',
			'NETUNITPRICE',
			'NETTOTALPRICE',
			'TAX',
			'TOTALPRICE',
		]
		int counter=0
		int kcounter=0
		try {
			List<WebElement> items = driver.findElements(By.xpath(wTaxLoopTable205));
			logi.logInfo("Items count : "+items.size().toString())
			items.each  { item ->
				logi.logInfo("Preparing Item Hash")
				counter=counter+1
				HashMap hm = new HashMap<String,String>()
				List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
				logi.logInfo("tds count : "+tds.size().toString())
				tds.each {titem ->
					logi.logInfo("td value "+titem.getText())
					hm.put(keys.get(kcounter), titem.getText().trim())
					kcounter=kcounter+1

				}
				kcounter=0
				wTaxLoopItems205.put("Line Item"+counter,hm)

			}

		} catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return wTaxLoopItems205

	}
	public LinkedHashMap uv_getWTaxlAllItemsLoop_DB_205(String tcid)

	{

		logi.logInfo("Calling uv_getWTaxlAllItemsLoop_DB_205")
		LinkedHashMap creditActivtyLoopItemsDB205 = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String ItemsCountQuery1= "select count(*) from ariacore.all_invoice_details where invoice_no=%s "
			int itemscount1=db.executeQueryP2(String.format(ItemsCountQuery1,invoiceNo)).toString().toInteger()
			//String query4="select replace((case when to_char(trunc(start_date)) is null then ' ' else to_char(trunc(start_date))  end),'')  as FROM_DATE, replace((case when to_char(trunc(end_date)) is null then ' ' else to_char(trunc(end_date))  end),'')  as TO_DATE , replace((case when to_char(base_plan_units) is null then ' ' else to_char(base_plan_units)  end),'')   as QUANTITY, (plan_name||' '||comments) as DESCRIPTION,'%s' || ' ' ||trim(to_char(NVL(usage_rate,0),'99,990.99')) as PRICE, '%s' || ' ' || '%s' as TAX,replace((case when to_char(NVL(proration_factor*100,0),'99,990.99') is null then ' ' else (to_char(NVL(proration_factor*100,0) ,'99,990.99') ) end),'')  as PERCENT_OF_PERIOD,usage_rate+%s as TOTAL_PRICE from ariacore.all_invoice_details where invoice_no=%s and seq_num=%s"

			String recuringRateScale = ""
			String usageRateScale = ""
			int tempUsageRate = 0
			int recRateScale = 0
			int usgRateScale = 0

			String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
			String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
			String usageRateQry = "select NVL(usage_rate,0) from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"

			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			for(int a=1 ; a<=itemscount1 ; a++){
				counter=counter+1

				recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
				logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

				tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,a)).toString().toDouble().toInteger()
				logi.logInfo("tempUsageRate is :: "+tempUsageRate)

				usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
				logi.logInfo("usageRateScale num is :: "+usageRateScale)

				if(tempUsageRate != null){
					logi.logInfo("Inside not null statement")
					recRateScale = tempUsageRate.toString().length()+Integer.parseInt(recuringRateScale)+1
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = tempUsageRate.toString().length()+Integer.parseInt(usageRateScale)+1
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				} else{
					logi.logInfo("Inside not null else statement")
					recRateScale = Integer.parseInt(recuringRateScale)
					logi.logInfo("recRateScale is :: "+recRateScale)
					usgRateScale = Integer.parseInt(usageRateScale)
					logi.logInfo("usgRateScale is :: "+usgRateScale)
				}
				String taxQuery="select  trim(TO_CHAR(NVL(sum(debit),0),'99,990.99')) from ariacore.gl_tax_detail where invoice_no=%s and is_excluded=0 and taxed_seq_num=%s"
				String taxValue=db.executeQueryP2(String.format(taxQuery,invoiceNo,a)).toString()
				String orderlabelQuery="select alt_label  from ariacore.order_items  where item_no= (select item_no from ariacore.all_invoice_details where invoice_no= %s and seq_num= %s) and order_no=(select order_no from ariacore.all_invoice_details where invoice_no = %s and seq_num  = %s)and client_no=%s"
				String olable= db.executeQueryP2(String.format(orderlabelQuery,invoiceNo,a,invoiceNo,a,clientNo))
				String query4="SELECT ( CASE WHEN a.is_order_based=1  THEN '%s'||' ' ||a.line_comments  when a.line_comments is not null then a.comments ||' ' ||a.line_comments ELSE a.comments  END ) AS DESCRIPTION,  REPLACE((  CASE  WHEN TO_CHAR(a.usage_units) IS NULL  THEN ' '  ELSE TO_CHAR(a.usage_units)  END),'') AS QUANTITY,  ( CASE WHEN a.usage_rate =0  AND a.service_no  =0 THEN '0.00'  WHEN a.recurring=1  THEN ( CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %s, 0)  ELSE RPAD(a.usage_rate  ||'.', %s, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %s, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1  THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0  THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00'  END) AS NETUNITPRICE,TRIM(TO_CHAR (a.debit,'99,990.99')) as NETTOTALPRICE, TRIM(TO_CHAR (a.taxdebit,'99,990.99')) AS TAX, trim(TO_CHAR((a.debit+a.taxdebit),'99,990.99')) AS TOTALPRICE FROM   (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.proration_factor, aid.orig_coupon_cd, aid.line_comments,aid.orig_credit_id , aid.base_plan_units, aid.start_date, aid.end_date,ser.usage_based,aid.plan_name, ser.is_setup,ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no , CASE WHEN ( orig_coupon_cd IS NOT NULL OR orig_credit_id     IS NOT NULL ) THEN 0.00  ELSE gtl.debit  END AS taxdebit FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no   LEFT JOIN ariacore.gl_tax_Detail gtl   ON aid.invoice_no =gtl.invoice_no AND aid.seq_num =gtl.taxed_seq_num   WHERE aid.invoice_no = %s  AND aid.seq_num = %s   AND ser.is_tax =0   AND (ser.custom_to_client_no=%s   OR ser.custom_to_client_no IS NULL)   )a"
				hm = db.executeQueryP2(String.format(query4,olable,recRateScale,recRateScale,usgRateScale,invoiceNo,a,clientNo))
				//hm = db.executeQueryP2(String.format(query4,symbol,symbol,taxValue,taxValue,invoiceNo,a))
				hm.each {k,v ->
					String v1= v.toString().trim()
					hm.put(k,v1)
				}
				logi.logInfo("Individual row of hash after adding all items :: "+hm.toString())
				creditActivtyLoopItemsDB205.put("Line Item"+counter,hm)

			}
		}

		catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB205

	}

	public LinkedHashMap uv_getBillingAdressLineItem_ST_205(String tcid)
	{
		logi.logInfo("Calling uv_getBillingAdressLineItem_ST_205")
		String addr_line_items="//table//td//strong[text()='Invoiced to:']/../..//td[3]"
		//logi.logInfo "xpath framed "+addr_line_items
		String txt1=driver.findElement(By.xpath(addr_line_items)).getAttribute("innerHTML")
		logi.logInfo"Plaintext"
		logi.logInfo txt1
		txt1=txt1.replace("\n", "").replace("\r", "")
		txt1=txt1.replace("</p>","")
		txt1= txt1.trim()
		txt1.replaceAll("","")
		logi.logInfo"****************"
		logi.logInfo txt1
		LinkedHashMap<String,String> addrDetails_exp=new LinkedHashMap<String,String>()
		List <String> addrLines=txt1.split("<br>")
		logi.logInfo "addrLines list "+addrLines.toString()
		logi.logInfo "addrLines list  size"+addrLines.size().toString()
		List <String> finaladdrLines=[]
		addrLines.each  {line ->
			logi.logInfo "line--"+line
			if(!line.trim().equals(""))
			{
				logi.logInfo("adding to new loop")
				finaladdrLines.add(line.trim().toString())
				//logi.logInfo "finaladdrLines list in if111 "+finaladdrLines.toString()

			}
			else
			{
				logi.logInfo "elseline--"+line
			}
		}

		int max=finaladdrLines.size()
		int counter=6
		String val

		int b=max-1
		while(b>=0)
		{
			counter=counter-1
			String key="ADDR_LINE"+counter.toString()
			//logi.logInfo "key "+key
			if(!finaladdrLines.get(b).trim().equals(""))
			{
				val=finaladdrLines.get(b).trim()
			}
			//logi.logInfo "val "+val
			addrDetails_exp.put(key,val)
			if(b==0)
				break
			else
				b--
		}
		logi.logInfo addrDetails_exp.toString()
		return addrDetails_exp.sort()
	}

	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_214_REV(String tcid)

	{

		logi.logInfo("Calling uv_getCreditActivtyLoopItems_DB_214_REV")
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select case when currency_symbol = 'EUR' then '�' else currency_htm_symbol end from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")


			// Voided refunds
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}

			//payments - external
			String extpaymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int extpaymnetscount1=db.executeQueryP2(String.format(extpaymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String extquery4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=extpaymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(extquery4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			// Reversals
			String elrefundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (8) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int elrefundsCount=db.executeQueryP2(String.format(elrefundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String elquery5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (8) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=elrefundsCount ; a++){

				hm = db.executeQueryP2(String.format(elquery5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}

			//cash credit
			String ccextpaymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int ccextpaymnetscount1=db.executeQueryP2(String.format(ccextpaymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String ccquery4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=ccextpaymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(ccquery4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			// Voided invoice
			String paymnetsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount=db.executeQueryP2(String.format(paymnetsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query2="SELECT a.description || ' '||%s|| ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'|| '-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount ; a++){

				hm = db.executeQueryP2(String.format(query2,voidinvoice,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided invoice :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)
				}
			}

			//ServiceCredits
			String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
			String serviceCreditsCountQuery="select count(*) from ariacore.all_credits where acct_no=%s and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s AND orig_coupon_cd is null and orig_credit_id is not null)"
			int rows=db.executeQueryP2(String.format(serviceCreditsCountQuery,acct_no,invoiceNo)).toInteger()
			String query1="select description,amount from ( select  ( (case when  credit_type='S' then 'Service Credit (applied ' end ) || '%s'  || ')' ) as DESCRIPTION ,( '%s' ||'-'||TRIM(TO_CHAR((amount),'99,990.99')) ) as AMOUNT, row_number() over (order by credit_id asc) as seq from ariacore.all_credits where acct_no=%s  and credit_id in  ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s  AND orig_coupon_cd is null and orig_credit_id is not null) ) where seq=%s"
			for(int row=1 ; row<=rows ; row++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query1,glbillDate,symbol,acct_no,invoiceNo,row))
				logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			logi.logInfo( "counter value : "+counter.toString())

			//Coupons
			String couponsCountquery="select count(*) from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and orig_credit_id is null"
			int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
			String query3="select DESCRIPTION,AMOUNT from ( select gld.comments|| ' (applied '  || to_char(gl.due_date,'DD-MM-YYYY')  || ')' as DESCRIPTION,(  '%s'||TRIM(TO_CHAR((gld.debit),'99,990.99'))) as AMOUNT,row_number() over(order by gld.seq_num) as seq  from ariacore.gl_Detail  gld join ariacore.gl on gl.invoice_no=gld.invoice_no where gld.invoice_no=%s and gld.orig_coupon_cd is not null) where seq=%s"
			//String query3="select DESCRIPTION,AMOUNT from ( select gldac.comments|| ' (applied '  || to_char(gl.due_date,'DD-MM-YYYY')  || ')' as DESCRIPTION,(  '%s'||' '||TRIM(TO_CHAR((gldac.debit),'99,990.99'))) as AMOUNT,row_number() over (order by gldac.invoice_no asc) AS seq  from ariacore.gl_detail_after_credit gldac join ariacore.gl on gl.invoice_no=gldac.invoice_no where gldac.invoice_no=%s and gldac.seq_num in (select seq_num from gl_detail where invoice_no =%s and orig_coupon_cd is not null and orig_credit_id is null)) where seq=%s"
			for(int a=1 ; a<=couponsCount ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query3,symbol,invoiceNo,a))
				logi.logInfo("Individual row of hash after adding coupons :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

			//payments - electronic
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||'-'||TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB

	}
	public LinkedHashMap uv_GetHeaderDetails_1_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails_1_216")

		List headers = [
			"PREVIOUS BALANCE",
			"TOTAL NEW CHARGES",
			"PAYMENTS/CREDITS",
			"NET BALANCE",
			"NEXT PAYMENT DUE",
			"PAYMENT METHOD"
		];
		int rowCount
		int columnCount=2
		int listcounter=0
		String header1items=""
		LinkedHashMap header1Map = new LinkedHashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr")).size()
		logi.logInfo("row  count is :: "+rowCount)

		for(int i=1 ; i<=rowCount ; i++){

			String expath = "//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[%s]/td[%s]"
			String formattedXpath = String.format(expath,i,columnCount)
			logi.logInfo("formattedXpath : "+formattedXpath)
			header1items = "NO VALUE"

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					header1items = driver.findElement(By.xpath(formattedXpath)).getText()
					logi.logInfo("header1items : "+header1items)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}

			header1Map.put(headers.get(listcounter),header1items)
			header1Map.sort()
			listcounter++
			logi.logInfo " Final header1Map HashMap "+header1Map
		}

		return header1Map.sort()
	}

	public LinkedHashMap uv_GetHeaderDetails1DB_Parent(String tcid)
	{

		logi.logInfo("Entered into uv_uv_GetHeaderDetails1DB_Parent")
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return GetHeaderDetails1DB(acct_no)
	}

	public LinkedHashMap uv_GetHeaderDetails1DB_Child(String tcid)
	{

		logi.logInfo("Entered into uv_uv_GetHeaderDetails1DB_Child")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return GetHeaderDetails1DB(acct_no)
	}

	public LinkedHashMap GetHeaderDetails1DB(String acct_no)
	{

		logi.logInfo("Entered into GetHeaderDetails1DB")
		DecimalFormat d = new DecimalFormat("0.00")
		LinkedHashMap header1DBMap = new LinkedHashMap<String,String>()
		ConnectDB db=new ConnectDB()
		String invoice_No=""
		String glbalfwd=""
		String glDebit=""
		String acctTotalOwed=""
		String paymethod=""
		String nextbilldate=""

		String ccnumber=u.getValueFromRequest("create_acct_complete","//cc_number")
		String pmtd=u.getValueFromRequest("create_acct_complete","//pay_method")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)

		//InsertGlBalFwd
		String glbalfwdqry="SELECT '%s ' ||T.BALANCE_FORWARD \"PREVIOUS BALANCE\" FROM(SELECT BALANCE_FORWARD,ROW_NUMBER() OVER(ORDER BY STATEMENT_NO DESC) SEQ  FROM ARIACORE.ACCT_STATEMENT  WHERE ACCT_NO="+acct_no+" )T WHERE T.SEQ=1"
		glbalfwd= db.executeQueryP2(String.format(glbalfwdqry,symbol))
		logi.logInfo "Glbalfwd is "+glbalfwd

		//InsertGlDebit
		//String gldbtq="SELECT'%s ' || TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) \"TOTAL NEW CHARGES\" FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND SOURCE_NO="+invoice_No+" AND AMOUNT > 0"
		String gldbtq="SELECT'%s ' || TRIM(TO_CHAR(NVL(sum(debit),0),'99,990.99'))  \"TOTAL NEW CHARGES\" from ariacore.all_invoices where invoice_No="+invoice_No
		glDebit=db.executeQueryP2(String.format(gldbtq,symbol))
		logi.logInfo "glDebit is "+glDebit

		//InsertCurrentStmtTotalCredits
		/*String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13)"
		 String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		 logi.logInfo "trans_amt is "+trans_amt
		 String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		 String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		 logi.logInfo "credit_amt is "+credit_amt
		 double val=trans_amt.toDouble()+credit_amt.toDouble()
		 logi.logInfo("total "+val.toString())
		 String paycredits=" -"+symbol+""+ (d.format(val).toString())*/
		String paycredits=uv_getTotalStmntCredits("tcid")
		logi.logInfo "paycredit is "+paycredits

		//InsertAcctTotalOwed
		//String acctTotalOwedqry="SELECT CASE WHEN bal_fwd IS NULL THEN '%s ' ||TRIM(TO_CHAR(NVL((debit-credit),0),'99,990.99')) ELSE '%s ' ||TRIM(TO_CHAR(NVL(((bal_fwd+debit)-credit),0),'99,990.99'))  End AS \"NET BALANCE\" FROM ariacore.gl WHERE acct_no ="+acct_no+" and invoice_no ="+invoice_No
		String acctTotalOwedqry="SELECT '%s ' || TRIM(TO_CHAR(NVL(SUM(debit)),0),'99,990.99')) AS \"NET BALANCE\" FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No
		acctTotalOwed=db.executeQueryP2(String.format(acctTotalOwedqry,symbol,symbol))
		logi.logInfo "acctTotalOwed is "+acctTotalOwed

		//InsertNextbilldate
		String nxtbilldateqry="SELECT TO_CHAR(NEXT_BILL_DATE,'DD-MM-YYYY')  FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no
		nextbilldate=db.executeQueryP2(nxtbilldateqry)
		logi.logInfo "nextbilldate is "+nextbilldate

		//Pay method
		if(!ccnumber.equals('NoVal'))
		{
			logi.logInfo "Credit card details provided"
			String paymtdqry="select METHOD_NAME from ariacore.payment_methods where method_ID="+pmtd
			String mtdname=db.executeQueryP2(paymtdqry)
			String mask= ccnumber.substring(12,16)
			String t=" (Visa) ************"
			String maskedcc= t+mask
			paymethod=mtdname+maskedcc
		}
		else
			paymethod='Other/None'

		header1DBMap.put("PREVIOUS BALANCE",glbalfwd)
		header1DBMap.put("TOTAL NEW CHARGES",glDebit)
		header1DBMap.put("PAYMENTS/CREDITS",paycredits)
		header1DBMap.put("NET BALANCE",acctTotalOwed)
		header1DBMap.put("NEXT PAYMENT DUE",nextbilldate)
		header1DBMap.put("PAYMENT METHOD",paymethod)

		logi.logInfo "Header1DBMap is "+header1DBMap

		return header1DBMap.sort()

	}


	public LinkedHashMap uv_GetHeaderDetails_2_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails_2_216")

		List headers = [
			"NAME",
			"VIP",
			"ACCOUNT NUMBER",
			"BILLING CONTACT",
			"INVOICE NUMBER",
			"INVOICE PERIOD"
		];
		int rowCount
		int columnCount=2
		int listcounter=0
		String header2items=""
		LinkedHashMap header2Map = new LinkedHashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[1]/tbody/tr/td[1]/table/tbody/tr")).size()
		logi.logInfo("row  count is :: "+rowCount)

		for(int i=1 ; i<=rowCount ; i++){

			String expath = "//div[@id='content']/table[1]/tbody/tr/td[1]/table/tbody/tr[%s]/td[%s]"
			String formattedXpath = String.format(expath,i,columnCount)
			logi.logInfo("formattedXpath : "+formattedXpath)
			header2items = "NO VALUE"

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					header2items = driver.findElement(By.xpath(formattedXpath)).getText()
					logi.logInfo("header2items : "+header2items)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}

			header2Map.put(headers.get(listcounter),header2items)
			header2Map.sort()
			listcounter++
			logi.logInfo " Final header2Map HashMap "+header2Map
		}

		return header2Map.sort()
	}


	public LinkedHashMap uv_GetHeaderDetails2DB_Parent(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails2DB_Parent")
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return GetHeaderDetails2DB(acct_no)
	}

	public LinkedHashMap uv_GetHeaderDetails2DB_Child(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails2DB_Child")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return GetHeaderDetails2DB(acct_no)
	}

	public LinkedHashMap GetHeaderDetails2DB(String acct_no)
	{

		logi.logInfo("Entered into GetHeaderDetails2DB")
		LinkedHashMap header2DBMap = new LinkedHashMap<String,String>()
		ConnectDB db=new ConnectDB()
		String name=""
		String billcontact=""
		String invoice_No=""
		String daterange=""

		String nameqry="SELECT TRIM(FIRST_NAME ||' '|| LAST_NAME) FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no
		name=db.executeQueryP2(nameqry)
		logi.logInfo "Name is "+name

		String billctatqry="SELECT CASE WHEN FIRST_NAME IS NULL THEN LAST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||CITY||', '||STATE||' '||ZIP WHEN LAST_NAME IS NULL THEN FIRST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||CITY||', '||STATE||' '||ZIP WHEN ADDRESS1 IS NULL THEN FIRST_NAME||' '||LAST_NAME||' '||ADDRESS2||' '||CITY||', '||STATE||' '||ZIP WHEN ADDRESS2 IS NULL THEN FIRST_NAME||' '||LAST_NAME||' '||ADDRESS1||' '||CITY||', '||STATE||' '||ZIP WHEN CITY IS NULL THEN FIRST_NAME||' '||LAST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||STATE||' '||ZIP WHEN STATE IS NULL THEN FIRST_NAME||' '||LAST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||CITY||', '||ZIP WHEN ZIP IS NULL THEN FIRST_NAME||' '||LAST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||CITY||', '||STATE ELSE FIRST_NAME||' '||LAST_NAME||' '||ADDRESS1||' '||ADDRESS2||' '||CITY||', '||STATE||' '||ZIP END FROM ARIACORE.CURRENT_BILLING_INFO WHERE ACCT_NO="+acct_no
		billcontact=db.executeQueryP2(billctatqry)
		logi.logInfo "billcontact is "+billcontact

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)
		logi.logInfo "billcontact is "+invoice_No

		String daterangeqry="SELECT TO_CHAR(FROM_DATE,'DD-MM-YYYY')||' - '||TO_CHAR(TO_DATE,'DD-MM-YYYY') FROM ARIACORE.GL WHERE ACCT_NO="+acct_no
		daterange=db.executeQueryP2(daterangeqry)
		logi.logInfo "billcontact is "+daterange

		header2DBMap.put("NAME",name)
		header2DBMap.put("BILLING CONTACT",billcontact)
		header2DBMap.put("INVOICE NUMBER",invoice_No)
		header2DBMap.put("ACCOUNT NUMBER",acct_no)
		header2DBMap.put("INVOICE PERIOD",daterange)
		header2DBMap.put("VIP","")

		return header2DBMap.sort()

	}

	public LinkedHashMap uv_GetHeaderDetails_2_DB_216(String tcid)
	{
		LinkedHashMap header2Map = new LinkedHashMap<String,String>()
		header2Map.put("NAME","TEST")
		header2Map.put("VIP","TEST")
		header2Map.put("INVOICE NUMBER","TEST")
		header2Map.put("INVOICE PERIOD","TEST")
		header2Map.put("BILLING CONTACT","TEST")

		return header2Map.sort()
	}

	public LinkedHashMap uv_GetServiceLineItems_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetServiceLineItems_216")

		List servicelineitems = [
			"SERVICES",
			"QUANTITY",
			"UNIT PRICE",
			"EXTENDED"
		];
		int rowCount,columnCount
		int counter=1
		String seritems=""
		LinkedHashMap serviceitemMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap serviceitemSubMap = new HashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=4 and td[@class='billingdata']]")).size()
		columnCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=4 and td[@class='billingdata']][1]/td")).size()
		logi.logInfo("row  count is :: "+rowCount)
		logi.logInfo("column count is :: "+columnCount)

		for(int i=1 ; i<=rowCount ; i++){
			serviceitemSubMap = new HashMap<String,String>()
			for(int k=1 ; k<=columnCount ; k++){

				String expath = "//div[@id='content']/table[2]/tbody/tr[count(td)=4 and td[@class='billingdata']][%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				seritems = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						seritems = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+seritems)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				serviceitemSubMap.put(servicelineitems.get(k-1),seritems)
				serviceitemSubMap.sort()
				logi.logInfo " Internal HashMap "+serviceitemSubMap

			}

			serviceitemMap.put("Line Item"+counter, serviceitemSubMap)
			logi.logInfo("Final hash at the time of sequence no "+counter+" is : "+serviceitemMap)
			counter++
		}
		return serviceitemMap.sort()
	}

	public LinkedHashMap uv_GetServiceLineItemsDB_Parent_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetServiceLineItemsDB_Parent_216")
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return getServiceLineItems_DB_216(acct_no)
	}

	public LinkedHashMap uv_GetServiceLineItemsDB_Child_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetServiceLineItemsDB_Child_216")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return getServiceLineItems_DB_216(acct_no)
	}


	public LinkedHashMap getServiceLineItems_DB_216(String acct_no)

	{
		logi.logInfo("Entered into getServiceLineItems_DB_216")
		LinkedHashMap serviceLineItemDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap serLineItemInternal = new HashMap<String,String>()
		int counter=0
		String invoice_No=""
		String masterPlan=""
		String suppPlan=""
		ConnectDB db = new ConnectDB()

		try {
			String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
			invoice_No = db.executeQueryP2(invoiceQry)

			masterPlan=u.getValueFromRequest("create_acct_complete","//master_plan_no")
			suppPlan=u.getValueFromRequest("create_acct_complete","//supp_plans")

			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			int seqnum=db.executeQueryP2("SELECT COUNT(*) FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND (SERVICE_NO IN(SELECT SERVICE_NO FROM ARIACORE.ALL_SERVICE WHERE IS_RECURRING=1 OR IS_USAGE=1) OR ORIG_CLIENT_SKU IS NOT NULL)").toInteger()

			for(int rcount=1;rcount <= seqnum;rcount++)
			{
				logi.logInfo "rcount value is"+rcount
				String seritemqry="SELECT * FROM (SELECT PLAN_NAME ||' '||COMMENTS SERVICES,USAGE_UNITS QUANTITY,'%S '|| TRIM((TO_CHAR(USAGE_RATE,'99,990.9999'))) \"UNIT PRICE\",'%S '|| TRIM((TO_CHAR(DEBIT,'99,990.99'))) EXTENDED, ROW_NUMBER()OVER(ORDER BY 1 ) SEQ_NO FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO IN(SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE CLIENT_NO ="+clientNo+" AND PLAN_NO IN(%S) AND SERVICE_NO IN(SELECT SERVICE_NO FROM ARIACORE.ALL_SERVICE  WHERE IS_RECURRING=1 OR IS_USAGE=1))) WHERE SEQ_NO="+rcount
				serLineItemInternal=db.executeQueryP2(String.format(seritemqry,symbol,symbol,masterPlan))
				serLineItemInternal.remove("SEQ_NO")
				serLineItemInternal.sort()
				logi.logInfo "Summary internal Hashmap "+serLineItemInternal
				serviceLineItemDB.put("Line Item"+rcount,serLineItemInternal)
				logi.logInfo("Final Service item hashmap is "+serviceLineItemDB)
				counter=rcount
			}
			counter++
		}

		catch (Exception  e) {
			logi.logInfo "MethodException"
			logi.logInfo e.getMessage()
		}

		return serviceLineItemDB.sort()

	}


	public LinkedHashMap uv_GetTaxLineItems_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetTaxLineItems_216")

		List taxlineitems = [
			"TAXES",
			"AMOUNT"
		];
		int rowCount,columnCount
		int counter=1
		String taxItems=""
		LinkedHashMap taxitemMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap taxitemSubMap = new HashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Taxes')]]")).size()
		columnCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Taxes')]][1]/td")).size()
		logi.logInfo("row  count is :: "+rowCount)
		logi.logInfo("column count is :: "+columnCount)

		for(int i=2 ; i<=rowCount ; i++){
			taxitemSubMap = new HashMap<String,String>()
			for(int k=1 ; k<=columnCount ; k++){

				String expath = "//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Taxes')]][%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				taxItems = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						taxItems = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+taxItems)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				taxitemSubMap.put(taxlineitems.get(k-1),taxItems)
				taxitemSubMap.sort()
				logi.logInfo " taxitem submap "+taxitemSubMap

			}

			taxitemMap.put("Line Item"+counter, taxitemSubMap)
			logi.logInfo("Final hash at the time of sequence no "+counter+" is : "+taxitemMap)
			counter++
		}
		return taxitemMap.sort()
	}


	public LinkedHashMap uv_GetTaxLineItemsParent_DB_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetTaxLineItemsParent_DB_216")
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return GetTaxLineItems_DB_216(acct_no)
	}

	public LinkedHashMap uv_GetTaxLineItemsChild_DB_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetTaxLineItemsChild_DB_216")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return GetTaxLineItems_DB_216(acct_no)
	}

	public LinkedHashMap GetTaxLineItems_DB_216(String acct_no)
	{

		List taxes = [
			" ",
			"Federal Taxes",
			"State Taxes",
			"County Taxes",
			"City Taxes"
		]

		logi.logInfo("Entered into uv_GetTaxLineItems_DB_216")

		DecimalFormat d = new DecimalFormat("#.00")
		String invoice_No=""
		List services=[]
		String service=""
		List groupNo=[]
		double grouptotal=0.00
		String fnltotal=""
		double qrytotal
		int groupCount
		List groupservices=[]
		List keys=["TAXES", "AMOUNT"];
		String grpser=""
		String fnltotawithsym=""
		LinkedHashMap taxLineMap=new LinkedHashMap<String,String>()
		LinkedHashMap taxLine=new LinkedHashMap<String,String>()

		ConnectDB db=new ConnectDB()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String serqry="SELECT SERVICE_NO FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO > 0 AND SERVICE_NO NOT IN(401,402,403,404)"
		services=Arrays.asList(db.executeQueryReturnValuesAsArray(serqry))
		service=ConvertArrayListtoCommaDelimited(services)
		logi.logInfo "Comma separated service no" + service

		String grpqry="SELECT DISTINCT GRP_NUM  FROM CLIENT_SERVICE_GRP_MAP WHERE CLIENT_NO = "+clientNo+"  AND SERVICE_NO IN("+service+")"
		groupNo= Arrays.asList(db.executeQueryReturnValuesAsArray(grpqry))
		logi.logInfo "List of group numbers" + groupNo

		groupCount= groupNo.size()
		int cntr =0

		for(int ep=1;ep<=4;ep++)
		{
			taxLineMap = new LinkedHashMap<String,String>()
			logi.logInfo "Entered into for -  existing for the services "+ep
			if(groupNo.contains(ep.toString()))
			{
				logi.logInfo("Entered into group total calculation")

				String grpserqry="SELECT SERVICE_NO FROM CLIENT_SERVICE_GRP_MAP WHERE CLIENT_NO = "+clientNo+"  AND GRP_NUM="+groupNo.get(cntr)
				groupservices= Arrays.asList(db.executeQueryReturnValuesAsArray(grpserqry))
				grpser=ConvertArrayListtoCommaDelimited(groupservices)

				String finalqry="SELECT SUM(DEBIT) FROM ARIACORE.ALL_INVOICE_DETAILS WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO IN("+grpser+")"
				qrytotal=db.executeQueryP2(finalqry).toString().toDouble()
				logi.logInfo "qrytotal" + qrytotal
				logi.logInfo "grouptotal" + grouptotal


				fnltotal=d.format(grouptotal+qrytotal).toString()
				logi.logInfo "fnltotal calculated is " +fnltotal
				fnltotawithsym=symbol+' '+fnltotal
				logi.logInfo "Final group total with symbol "+fnltotawithsym

				taxLineMap.put(keys.get(0),taxes.get(ep))
				taxLineMap.put(keys.get(1),fnltotawithsym)

				logi.logInfo "Hashmap calculated for existing groups "+taxLineMap
				cntr++

				taxLine.put("Line Item"+ep,taxLineMap)
			}
			else
			{
				logi.logInfo "Constructing result Since group no does not exist"

				logi.logInfo "k1 "+keys.get(0)
				logi.logInfo "K2 "+taxes.get(ep)

				taxLineMap.put(keys.get(0).toString(),taxes.get(ep))
				taxLineMap.put(keys.get(1),symbol+" 0.00")

				taxLine.put("Line Item"+ep,taxLineMap)
			}

		}
		logi.logInfo "Final Tax item map after calculation & construction :" +taxLine.toString()

		return taxLine.sort()
	}
	public LinkedHashMap uv_GetCreditLineItems_216(String tcid)
	{

		logi.logInfo("Entered into uv_GetCreditLineItems_216")

		List taxlineitems = [
			"DESCRIPTION",
			"AMOUNT"
		];
		int rowCount,columnCount
		int counter=1
		String creditItems=""
		LinkedHashMap credititemMap = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap credititemSubMap = new HashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Credits')]]/following-sibling::tr[td[contains(text(),' ')]]")).size()
		columnCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Credits')]]/td")).size()
		logi.logInfo("row  count is :: "+rowCount)
		logi.logInfo("column count is :: "+columnCount)

		for(int i=1 ; i<=rowCount ; i++){
			credititemSubMap = new HashMap<String,String>()

			for(int k=1 ; k<=columnCount ; k++){

				String expath = "//*[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Credits')]]/following-sibling::tr[td[contains(text(),' ')]][%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				creditItems = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						creditItems = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+creditItems)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				credititemSubMap.put(taxlineitems.get(k-1),creditItems)
				credititemSubMap.sort()
				logi.logInfo " credit item submap "+credititemSubMap

			}

			credititemMap.put("Line Item"+counter, credititemSubMap)
			logi.logInfo("Final hash at the time of sequence no "+counter+" is : "+credititemMap)
			counter++
		}
		return credititemMap.sort()
	}

	public String uv_GlBaseDebitParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlBaseDebit_222(acct_no)
	}

	public String uv_GlBaseDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlBaseDebit_222(acct_no)
	}
	public String uv_GlBaseDebit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlBaseDebit_222 method "
		DecimalFormat d = new DecimalFormat("#.00");
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glBaseDebit=""
		String balXferAmt=""
		String finalGlBaseDebit=""
		String finalGlBaseDebit2=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String glbasedbtq="SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO NOT IN(401,402,403,404,405)"
		glBaseDebit=db.executeQueryP2(glbasedbtq)

		String balXferAmtqry="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.BALANCE_TRANSFER WHERE TO_ACCT_NO="+acct_no+" AND CLIENT_NO="+clientNo
		balXferAmt=db.executeQueryP2(balXferAmtqry)

		finalGlBaseDebit=(glBaseDebit.toDouble()+ balXferAmt.toDouble()).toString()
		logi.logInfo " parsed double "
		finalGlBaseDebit2=d.format(finalGlBaseDebit.toDouble()).toString()
		logi.logInfo " formatted double " + finalGlBaseDebit2

		return finalGlBaseDebit2
	}


	public String uv_GlTaxDebitParent_222(String tcid)
	{
		logi.logInfo " Entered into uv_GlTaxDebitParent_222 method "
		ReadData rdObj =new ReadData()
		DecimalFormat d = new DecimalFormat("##.00")
		String services =""
		String resp_level_cd=""
		String parentAcctno=""
		String childAcctno=""
		String parentTaxAmt=""
		String childTaxAmt=""
		String totTaxAmt=""
		String localTcId = tcid.split("-")[1]

		services= rdObj.getTestDataMap("StatementsUI",localTcId).get("Service_Order")
		logi.logInfo "Service order  " +services

		resp_level_cd=rdObj.getTestDataMap("create_acct_complete.a",localTcId).get("resp_level_cd")
		logi.logInfo "Resp_level_cd from input  " +resp_level_cd

		if(services.contains("create_acct_complete.a") && (resp_level_cd.equals("2") || resp_level_cd.equals("3") ||resp_level_cd.equals("4")))
		{
			parentAcctno=u.getValueFromResponse("create_acct_complete","//acct_no")
			parentTaxAmt=GlTaxDebit_222(parentAcctno)
			logi.logInfo "Tax amt for parent  " +parentTaxAmt

			childAcctno=u.getValueFromResponse("create_acct_complete.a","//acct_no")
			childTaxAmt=GlTaxDebit_222(childAcctno)
			logi.logInfo "Tax amt for child  " +childTaxAmt

			totTaxAmt=d.format(parentTaxAmt.toDouble()+childTaxAmt.toDouble()).toString()
			logi.logInfo "Total Tax amount  " +totTaxAmt
			return totTaxAmt
		}
		else
		{
			String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
			return GlTaxDebit_222(acct_no)
		}
	}

	public String uv_GlTaxDebitChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return GlTaxDebit_222(acct_no)
	}
	public String GlTaxDebit_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlTaxDebit_222 method "
		DecimalFormat d = new DecimalFormat("#0.00")
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glTaxDebit=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String glTaxdbtq="SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO  IN(401,402,403,404,405)"
		glTaxdbtq=db.executeQueryP2(glTaxdbtq)

		return d.format(glTaxdbtq.toDouble())
	}



	public String uv_glTotalCreditsParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return glTotalCredits_222(acct_no)
	}

	public String uv_glTotalCreditsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return glTotalCredits_222(acct_no)
	}

	public String glTotalCredits_222(String acct_no)
	{
		logi.logInfo " Entered into glTotalCredits_222 method "
		DecimalFormat d = new DecimalFormat("#0.00");
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glTotalCredits=""
		String electPayment=""
		String extPayment=""
		String allCredits=""


		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String startDateQry="select to_char(usage_from_date,'dd-MON-yy hh12:mi:ss') as Startdate from ariacore.gl where  acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoice_No
		String startDate=db.executeQueryP2(startDateQry)

		String endDateQry="select to_char(bill_date,'dd-MON-yy hh12:mi:ss') as Enddate from ariacore.gl where  acct_no="+acct_no+" and client_no="+clientNo+" and invoice_no="+invoice_No
		String endDate=db.executeQueryP2(endDateQry)

		String allCreditsQry="Select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.credits where acct_no="+acct_no+"  and create_date between '"+startDate+"' and '"+endDate+"'"
		allCredits=db.executeQueryP2(allCreditsQry)

		String electPaymentQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.payments where acct_no="+acct_no+" and payment_date between '"+startDate+"' and '"+endDate+"'"
		electPayment=db.executeQueryP2(electPaymentQry)

		String extPaymentQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.external_payments where acct_no="+acct_no+" and entered_date between '"+startDate+"' and '"+endDate+"'"
		extPayment=db.executeQueryP2(extPaymentQry)

		glTotalCredits=d.format(allCredits.toDouble()+electPayment.toDouble()+extPayment.toDouble())
		return glTotalCredits

	}


	public String uv_ttlCashAndServiceCrdtsParent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return ttlCashAndServiceCrdts_222(acct_no)
	}

	public String uv_ttlCashAndServiceCrdtsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return ttlCashAndServiceCrdts_222(acct_no)
	}
	public String ttlCashAndServiceCrdts_222(String acct_no)
	{
		logi.logInfo " Entered into ttlCashAndServiceCrdts_222 method "
		DecimalFormat d = new DecimalFormat("#0.00");
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String creditAmt=""
		String discountAmt=""
		String cashCredit=""
		String ttlCashAndSerCrdts=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String creditQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.credit_application where invoice_no="+invoice_No
		creditAmt=db.executeQueryP2(creditQry)

		String discountCountQry="SELECT count(*)  FROM ariacore.gl_dtl_discount_dtl  where  invoice_no="+invoice_No+" AND  client_no="+clientNo
		String discountCount=db.executeQueryP2(discountCountQry)

		if(discountCount > 0)
		{
			String discountAmtQry="SELECT TRIM(TO_CHAR(NVL(SUM(ACTUAL_DISCOUNT_AMT),0),'99,990.99')) FROM ariacore.gl_dtl_discount_dtl aidd,ariacore.client_discount_rules cdrs WHERE aidd.invoice_no = "+invoice_No+" AND aidd.client_no = "+clientNo+" AND cdrs.client_no =aidd.client_no  AND cdrs.inline_offset_ind <> 'I'"
			discountAmt=db.executeQueryP2(discountAmtQry)
		}
		else
			discountAmt =0.00

		String cashCreditQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.all_credits where credit_type='C' and acct_no="+acct_no
		cashCredit=db.executeQueryP2(cashCreditQry)


		ttlCashAndSerCrdts='-'+d.format(creditAmt.toDouble()+discountAmt.toDouble()+cashCredit.toDouble())
		return ttlCashAndSerCrdts
	}

	public boolean viewCreditNoteFromRecentComm()
	{
		boolean flg = false
		logi.logInfo("Entered into viewCreditNoteFromRecentComm")
		try
		{

			if(driver.findElement(creditNote).isDisplayed())
			{
				logi.logInfo("Credit note sent")
				driver.findElement(lnkCreditNote).click()
				logi.logInfo("Clicked on Recent credit note")
				if(driver.findElement(buttonResendMessage).isDisplayed())
				{
					flg = true;
				}

			}
			else
				logi.logInfo("Credit note sent")

		}catch (Exception e)
		{
			logi.logInfo("Credit note clicked and view is not opened " +e.printStackTrace());
			flg = true;

		}
		driver.findElement(barresize).click()
		Thread.sleep(1000)
		driver.findElement(barresize).click()
		return flg;

	}

	public LinkedHashMap uv_GetCreditNoteLineItems_voided(String tcid)
	{
		logi.logInfo("Entered into uv_GetCreditNoteLineItems_voided")
		LinkedHashMap creditnotevoideditems = new LinkedHashMap<String,String>()

		driver.switchTo().frame(0)
		String elements=driver.findElement(By.tagName("body")).getText()
		logi.logInfo " Credit note elements " + elements
		List splitted=elements.split("Crediting Transaction:")[1].split("Credited Transaction:")[0].split("\\n")
		splitted.each{ items->
			logi.logInfo "Values splitted using \n" +items
			if(items.length() >0)
			{
				String k=items.split(":")[0]
				String v=items.split(":")[1]
				creditnotevoideditems.put(k.trim(),v.trim())
			}
		}

		logi.logInfo "Credit note voided hash " + creditnotevoideditems
		//driver.switchTo().defaultContent()
		driver.switchTo().defaultContent()
		return creditnotevoideditems.sort()
	}


	public def uv_getCreditNoteLineItems_voided_DB_Parent(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return getCreditNoteLineItems_voided_DB(acct_no)
	}

	public def uv_getCreditNoteLineItems_voided_DB_Child(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return getCreditNoteLineItems_voided_DB(acct_no)
	}

	public LinkedHashMap getCreditNoteLineItems_voided_DB(String acct_no)
	{
		logi.logInfo("Entered into getCreditNoteLineItems_voided_DB")
		LinkedHashMap creditnotevoideditemsDb = new LinkedHashMap<String,String>()

		DecimalFormat d = new DecimalFormat("#0.00")
		ConnectDB db = new ConnectDB()
		String creditAmt=""
		String creditCode=""
		String creditDate=""
		String stmtNo=""
		String cashCreditCode=""

		String reason_cd=u.getValueFromRequest("void_transaction","//reason_code")
		String creditAmtQry="Select TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) from ariacore.gl where acct_no="+acct_no+" and voiding_event_no is not null"
		creditAmt=db.executeQueryP2(creditAmtQry)

		String creditCodeQry="select label from ariacore.void_reasons where reason_no="+reason_cd
		creditCode=db.executeQueryP2(creditCodeQry)

		String creditDateQry="select to_char(create_date,'DD-MM-YYYY') from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type=-1"
		creditDate=db.executeQueryP2(creditDateQry)
		String stmntQry="SELECT ACCT_STATEMENT_SEQ_NO FROM ariacore.acct_statement where statement_no IN(SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no="+acct_no+")"
		stmtNo=db.executeQueryP2(stmntQry)

		creditnotevoideditemsDb.put("CREDIT AMOUNT",creditAmt)
		creditnotevoideditemsDb.put("CREDIT DATE",creditDate)
		creditnotevoideditemsDb.put("CREDIT REASON",creditCode)
		creditnotevoideditemsDb.put("CREDIT DESC","Voided Invoice")
		creditnotevoideditemsDb.put("CREDIT UNAPPLIED AMOUNT",' ')
		creditnotevoideditemsDb.put("STATEMENT NO",stmtNo)

		creditnotevoideditemsDb.each { k,v ->
			String v1 = v.toString().trim()
			creditnotevoideditemsDb.put(k,v1)
		}

		logi.logInfo " Final DB hash returned from DB is " + creditnotevoideditemsDb

		return creditnotevoideditemsDb.sort()


	}

	public def uv_getCreditNoteLineItems_CashCredit_DB_Parent(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return getCreditNoteLineItems_CashCredit_DB(acct_no)
	}

	public def uv_getCreditNoteLineItems_CashCredit_DB_Child(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return getCreditNoteLineItems_CashCredit_DB(acct_no)
	}

	public LinkedHashMap getCreditNoteLineItems_CashCredit_DB(String acct_no)
	{
		logi.logInfo("Entered into getCreditNoteLineItems_CashCredit_DB")
		LinkedHashMap creditnoteCashCredititemsDb = new LinkedHashMap<String,String>()

		DecimalFormat d = new DecimalFormat("#0.00")
		ConnectDB db = new ConnectDB()
		String creditAmt=""
		String creditCode=""
		String creditDate=""
		String stmtNo=""
		String cashCreditCode=""
		String unAppAmt=""

		String reason_cd=u.getValueFromRequest("apply_cash_credit","//credit_reason_code")
		String creditAmtQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) from ariacore.all_credits where acct_no="+acct_no+" and reason_cd="+reason_cd
		creditAmt=db.executeQueryP2(creditAmtQry)

		String creditCodeQry="select Text from ariacore.credit_reasons where reason_cd="+reason_cd
		creditCode=db.executeQueryP2(creditCodeQry)

		String creditDateQry="select to_char(create_date,'DD-MM-YYYY') from ariacore.all_credits where acct_no="+acct_no+" and reason_cd="+reason_cd
		creditDate=db.executeQueryP2(creditDateQry)
		String stmntQry="SELECT ACCT_STATEMENT_SEQ_NO FROM ariacore.acct_statement where statement_no IN(SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no="+acct_no+")"
		stmtNo=db.executeQueryP2(stmntQry)

		String unAppAmtQry="select TRIM(TO_CHAR(NVL(SUM(AMOUNT-APPLIED_AMOUNT),0),'99,990.99'))  from ariacore.acct_transaction where  acct_no="+acct_no+"  and transaction_type=10"
		unAppAmt=db.executeQueryP2(unAppAmtQry)


		creditnoteCashCredititemsDb.put("CREDIT AMOUNT",creditAmt)
		creditnoteCashCredititemsDb.put("CREDIT DATE",creditDate)
		creditnoteCashCredititemsDb.put("CREDIT REASON",creditCode)
		creditnoteCashCredititemsDb.put("CREDIT DESC","Cash Credit")
		creditnoteCashCredititemsDb.put("CREDIT UNAPPLIED AMOUNT",unAppAmt)
		creditnoteCashCredititemsDb.put("STATEMENT NO",stmtNo)

		creditnoteCashCredititemsDb.each { k,v ->
			String v1 = v.toString().trim()
			creditnoteCashCredititemsDb.put(k,v1)
		}

		logi.logInfo " Final DB hash returned from DB is " + creditnoteCashCredititemsDb

		return creditnoteCashCredititemsDb.sort()


	}


	public def uv_getCreditNoteLineItems_Refunds_DB_Parent(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return getCreditNoteLineItems_Refunds_DB(acct_no)
	}

	public def uv_getCreditNoteLineItems_Refunds_DB_Child(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return getCreditNoteLineItems_Refunds_DB(acct_no)
	}

	public LinkedHashMap getCreditNoteLineItems_Refunds_DB(String acct_no)
	{
		logi.logInfo("Entered into getCreditNoteLineItems_Refunds_DB")
		LinkedHashMap creditnoteRefundsitemsDb = new LinkedHashMap<String,String>()

		DecimalFormat d = new DecimalFormat("#0.00")
		ConnectDB db = new ConnectDB()
		String creditAmt=""
		String creditCode=""
		String creditDate=""
		String stmtNo=""


		String reason_cd=u.getValueFromRequest("issue_refund_to_acct.a","//reason_code")
		String creditAmtQry="select TRIM(TO_CHAR(NVL(SUM(applied_amount),0),'99,990.99')) from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type in(13,12)"
		creditAmt=db.executeQueryP2(creditAmtQry)

		String creditCodeQry="select label from ariacore.refund_reason_codes where reason_cd="+reason_cd
		creditCode=db.executeQueryP2(creditCodeQry)

		String creditDateQry="select to_char(create_date,'DD-MM-YYYY')  from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type in(13,12)"
		creditDate=db.executeQueryP2(creditDateQry)

		String stmntQry="SELECT ACCT_STATEMENT_SEQ_NO FROM ariacore.acct_statement where statement_no IN(SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no="+acct_no+")"
		stmtNo=db.executeQueryP2(stmntQry)


		creditnoteRefundsitemsDb.put("CREDIT AMOUNT",creditAmt)
		creditnoteRefundsitemsDb.put("CREDIT DATE",creditDate)
		creditnoteRefundsitemsDb.put("CREDIT REASON",creditCode)
		creditnoteRefundsitemsDb.put("CREDIT DESC","Refund Related Reversal")
		creditnoteRefundsitemsDb.put("CREDIT UNAPPLIED AMOUNT",' ')
		creditnoteRefundsitemsDb.put("STATEMENT NO",stmtNo)

		creditnoteRefundsitemsDb.each { k,v ->
			String v1 = v.toString().trim()
			creditnoteRefundsitemsDb.put(k,v1)
		}

		logi.logInfo " Final DB hash returned from DB is " + creditnoteRefundsitemsDb

		return creditnoteRefundsitemsDb.sort()


	}

	public def uv_getCreditNoteLineItems_voidedbycollection_DB_Parent(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return getCreditNoteLineItems_voidedbycollection_DB(acct_no)
	}

	public def uv_getCreditNoteLineItems_voidedbycollection_DB_Child(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return getCreditNoteLineItems_voidedbycollection_DB(acct_no)
	}

	public LinkedHashMap getCreditNoteLineItems_voidedbycollection_DB(String acct_no)
	{
		logi.logInfo("Entered into getCreditNoteLineItems_voidedbycollection_DB")
		LinkedHashMap creditnotevoidedbycollectionitemsDb = new LinkedHashMap<String,String>()

		DecimalFormat d = new DecimalFormat("#0.00")
		ConnectDB db = new ConnectDB()
		String creditAmt=""
		String creditDate=""
		String stmtNo=""
		String cashCreditCode=""

		String creditAmtQry="Select TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) from ariacore.gl where acct_no="+acct_no+" and voiding_event_no is not null"
		creditAmt=db.executeQueryP2(creditAmtQry)

		String creditDateQry="select to_char(create_date,'DD-MM-YYYY') from ariacore.acct_transaction where acct_no="+acct_no+" and transaction_type=-1"
		creditDate=db.executeQueryP2(creditDateQry)
		String stmntQry="SELECT ACCT_STATEMENT_SEQ_NO FROM ariacore.acct_statement where statement_no IN(SELECT MAX(statement_no) FROM ariacore.acct_statement WHERE acct_no="+acct_no+")"
		stmtNo=db.executeQueryP2(stmntQry)

		creditnotevoidedbycollectionitemsDb.put("CREDIT AMOUNT",creditAmt)
		creditnotevoidedbycollectionitemsDb.put("CREDIT DATE",creditDate)
		creditnotevoidedbycollectionitemsDb.put("CREDIT REASON","collection failed")
		creditnotevoidedbycollectionitemsDb.put("CREDIT DESC","Voided Invoice")
		creditnotevoidedbycollectionitemsDb.put("CREDIT UNAPPLIED AMOUNT",' ')
		creditnotevoidedbycollectionitemsDb.put("STATEMENT NO",stmtNo)

		creditnotevoidedbycollectionitemsDb.each { k,v ->
			String v1 = v.toString().trim()
			creditnotevoidedbycollectionitemsDb.put(k,v1)
		}

		logi.logInfo " Final DB hash returned from DB is " + creditnotevoidedbycollectionitemsDb

		return creditnotevoidedbycollectionitemsDb.sort()


	}	
	/*
	public LinkedHashMap uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII(String tcid){
		logi.logInfo("Calling uv_getStmtInvoiceLineItemsFromStatementsDB_AdobeII")

		LinkedHashMap mDB = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap hm = new HashMap<String,String>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		//String accountNo = u.getValueFromResponse("create_acct_complete","///*:acct_no[1]")

		int temp = 0
		String invoiceNo = ""
		String seqCount = ""
		String recuringRateScale = ""
		String usageRateScale = ""
		String tempUsageRate = ""
		int recRateScale = 0
		int usgRateScale = 0


		String invoiceNoQry = "select MAX(invoice_no) from ariacore.gl where acct_no = "+accountNo
		//String seqNumQry = "select count(seq_Num) as COUNT from ariacore.gl_detail where invoice_no = %s and usage_type is null and length(service_no)>4 and service_no not in (select service_no from ariacore.all_service where is_surcharge=1 and client_no=%s) and orig_coupon_cd is null and orig_credit_id is null"
		String seqNumQry = "select count(SEQ_NUM) as totalCount FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no           = aid.service_no  WHERE aid.invoice_no        = %s  AND ser.is_tax              = 0   AND aid.orig_credit_id     IS NULL   AND ser.is_setup            = 0   AND aid.usage_type         IS NULL   AND (ser.custom_to_client_no=%s   OR ser.custom_to_client_no IS NULL) "
		String usageRateQry = "select usage_rate from ariacore.all_invoice_details WHERE invoice_no=%s and seq_num =%s"
		//String generalQry = "SELECT case when a.orig_coupon_cd is not null then a.comments || ' (applied '|| '%s' || ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Licenses, (CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0  and  (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String generalQry = "SELECT TRIM(TO_CHAR(a.plan_name)) AS Description, (CASE WHEN a.usage_rate =0 AND a.service_no  =0 THEN '0.00' WHEN a.recurring=1 THEN (CASE WHEN regexp_like(trim(a.usage_rate), '[.]') THEN RPAD(a.usage_rate, %d, 0) ELSE RPAD(a.usage_rate ||'.', %d, 0) END) WHEN a.usage_based=1 THEN RPAD(a.usage_rate ||'.', %d, 0) WHEN a.is_setup=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_min_fee=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.is_surcharge=1 THEN TRIM(TO_CHAR (a.usage_rate,'99,990.99')) WHEN a.service_no=0 THEN '0.00' WHEN a.is_order_based=1 THEN TO_CHAR(a.usage_rate ||'.00') WHEN a.is_tax = 1 THEN '0.00' END) AS Unit_Price, (CASE WHEN a.usage_units IS NULL THEN NVL( TO_CHAR( a.usage_units), ' ' ) ELSE TO_CHAR(a.usage_units) END ) AS Licenses, TRIM(TO_CHAR(a.debit,'99,990.99')) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s  AND aid.seq_num = %s  and ser.is_tax = 0 and aid.orig_credit_id is null  and ser.is_setup = 0 AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a"
		String recuringQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'RECURRING_RATE_PER_UNIT_SCALE'"
		String usageQry = "select param_val from ariacore.aria_client_params where client_no = %s and param_name = 'USAGE_RATE_PER_UNIT_SCALE'"
		String seqNumQry2 = "SELECT aid.seq_num FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no WHERE aid.invoice_no = %s AND ser.is_tax = 0  AND ser.is_min_fee = 0 and ser.is_setup =0 and aid.orig_credit_id is null AND aid.usage_type IS NULL AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) order by seq_num"

		ConnectDB db = new ConnectDB()
		String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")
		invoiceNo = db.executeQueryP2(invoiceNoQry)
		logi.logInfo("Invoice num is :: "+invoiceNo)
		String glbillDate=db.executeQueryP2("select to_char(bill_date,'dd-mm-yyyy') from ariacore.gl where invoice_no="+invoiceNo)
		seqCount = db.executeQueryP2(String.format(seqNumQry,invoiceNo,clientNo))
		logi.logInfo("Seq num is :: "+seqCount)

		List seqNum = []
		seqNum = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(seqNumQry2, invoiceNo, clientNo)))

		for(int row=1 ; row<=Integer.parseInt(seqCount) ; row++){

			   recuringRateScale = db.executeQueryP2(String.format(recuringQry,clientNo))
			   logi.logInfo("recuringRateScale num is :: "+recuringRateScale)

			   tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,seqNum.get(row-1)))
			   logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			   usageRateScale = db.executeQueryP2(String.format(usageQry,clientNo))
			   logi.logInfo("usageRateScale num is :: "+usageRateScale)

			   if(tempUsageRate != null){
					  logi.logInfo("Inside not null statement")
					  recRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(recuringRateScale)+1
					  logi.logInfo("recRateScale is :: "+recRateScale)
					  usgRateScale = tempUsageRate.length().toInteger()+Integer.parseInt(usageRateScale)+1
					  logi.logInfo("usgRateScale is :: "+usgRateScale)
			   } else{
					  logi.logInfo("Inside not null else statement")
					  recRateScale = Integer.parseInt(recuringRateScale)
					  logi.logInfo("recRateScale is :: "+recRateScale)
					  usgRateScale = Integer.parseInt(usageRateScale)
					  logi.logInfo("usgRateScale is :: "+usgRateScale)
			   }

			   logi.logInfo("Formatted query"+String.format(generalQry,recRateScale,recRateScale,Integer.parseInt(usageRateScale),invoiceNo,seqNum.get(row-1),clientNo))
			   hm = new HashMap<String,String>()
	   hm = db.executeQueryP2(String.format(generalQry,recRateScale,recRateScale,usgRateScale,invoiceNo,seqNum.get(row-1),clientNo))

			   if(hm.get("Licenses").equals(" "))
			   {
					  logi.logInfo("Units has space")
					  hm.put("Licenses","")
			   }
			   if(hm.containsKey("Description"))
			   {
					  String pname = hm.get("Description").toString().trim()
					  hm.put("Description",pname)
			   }
			   hm.sort()
			   logi.logInfo("Individual row of hash is :: "+hm.toString())

			   mDB.put("Line Item"+row,hm)
			   temp = row;
		}
		temp++
		logi.logInfo("mDB mDB mDB mDB :: "+mDB.toString())

		//Service Credits
		String countquery="select count(*) from (select row_number() over(ORDER BY orig_credit_id asc ) as sno  from ariacore.all_invoice_details where invoice_no=%s and orig_credit_id is not null and orig_credit_id in (select credit_id from  ariacore.credits where acct_no=%s) )"
		 int creditrows= db.executeQueryP2(String.format(countquery,invoiceNo,accountNo)).toInteger()
		logi.logInfo("creditrows " +creditrows)
		String generalcreditquery="SELECT case when a.orig_coupon_cd is not null and  a.orig_credit_id is NULL then a.comments || ' (applied '|| '%s' || ')'  when a.orig_credit_id  is not null  THEN 'Service Credit' || ' (applied ' || '%s'|| ')' else  TRIM(TO_CHAR(a.plan_name)) end AS Description,(CASE WHEN a.param_val = ',.' THEN TRIM(TO_CHAR(a.debit,'99G999D99MI', 'NLS_NUMERIC_CHARACTERS = '',. '' ')) ELSE TRIM(TO_CHAR(a.debit,'99,990.99')) END) AS Net_Price FROM (SELECT ser.recurring, NVL(aid.usage_rate,0) AS usage_rate, aid.comments, aid.usage_units, aid.debit, aid.start_date, aid.end_date, aid.orig_coupon_cd,aid.orig_credit_id,ser.usage_based, aid.plan_name, ser.is_setup, ser.is_min_fee, ser.is_order_based, ser.is_tax, ser.is_surcharge, aid.service_no, acp.param_val ,row_number() over (order by aid.debit) as sno FROM ariacore.services ser JOIN ariacore.all_invoice_details aid ON ser.service_no = aid.service_no JOIN ariacore.aria_client_params acp ON aid.client_no = acp.client_no WHERE aid.invoice_no = %s  AND  (aid.orig_coupon_cd is not null or aid.orig_credit_id is not null)  and ser.is_tax = 0  and (ser.is_surcharge is null or ser.is_surcharge =0) and ser.is_min_fee = 0  AND acp.client_no = %s and acp.param_name = 'OVERRIDE_CURRENCY_FORMAT_MASK'AND  aid.usage_type is null AND (ser.custom_to_client_no=%s OR ser.custom_to_client_no IS NULL) )a where sno=%s"
		HashMap hms = new HashMap<String,String>()
		for(int row=1 ; row<=creditrows ; row++){
			   hms = db.executeQueryP2(String.format(generalcreditquery,glbillDate,glbillDate,invoiceNo,clientNo,clientNo,row))

			   hms.each {k,v ->
					  String v1= v.toString().trim()
					  hms.put(k,v1)
			   }
			   hms.put("LICENSES",' ')
			   hms.put("UNIT_PRICE",' ')
			   hms.sort()
			   logi.logInfo("Individual row of hash is :: "+hms.toString())

			   mDB.put("Line Item"+temp,hms)
			   temp++



		}
		// Usage items
		HashMap hmu = new HashMap<String,String>()
		int usgRateScale1 = 0

		usageRateQry = "select a.usage_rate from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null)a where a.custom_seq_num=%s"
		String generalUsageQry =  "select TRIM(TO_CHAR(a.plan_name)) as DESCRIPTION, (case when regexp_like(trim(a.usage_rate), '[.]') then RPAD(a.usage_rate, %d, 0)   else  RPAD(a.usage_rate  ||'.', %d, 0)  end)  AS UNIT_PRICE,  replace((case when a.base_plan_units is null then ' ' else to_char(a.base_plan_units) end),'') as LICENSES, TRIM(TO_CHAR(a.debit,'99,990.99')) as NET_PRICE from (select ser.service_no, aid.plan_name, aid.usage_rate, aid.debit, ser.usage_based, aid.base_plan_units, row_number() over (order by seq_num ASC) AS custom_seq_num FROM ariacore.all_invoice_details aid JOIN ariacore.services ser on ser.service_no = aid.service_no where aid.invoice_no = %s and ser.custom_to_client_no=%s and ser.is_min_fee = 0 and ser.usage_based=1 and aid.ORIG_COUPON_CD is null )a where a.custom_seq_num = %s"
		String totalCountQry = "select count(*) from ( select usage_rate , row_number() over (order by seq_num ASC) AS custom_seq_num from ariacore.all_invoice_details WHERE invoice_no=%s and usage_type is not null and orig_coupon_cd is null)a"

		//int totalCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[td[string-length()=0]]/td[1]")).size()
		int totalCount = db.executeQueryP2(String.format(totalCountQry,invoiceNo)).toString().toInteger()
		logi.logInfo("Total no of rows available is : "+totalCount)

		for(int row=1 ; row<=totalCount ; row++){

			   tempUsageRate = db.executeQueryP2(String.format(usageRateQry,invoiceNo,row)).toString().toDouble().toInteger()
			   logi.logInfo("tempUsageRate is :: "+tempUsageRate)

			   int tempUsageRateI = tempUsageRate.toString().length()
			   logi.logInfo("tempUsageRate1 is :: "+tempUsageRateI)

			   if(tempUsageRateI != null){
					  logi.logInfo("Inside not null statement")
					  usgRateScale = tempUsageRateI + Integer.parseInt(usageRateScale)+1
					  logi.logInfo("usgRateScale is :: "+usgRateScale)
			   } else{
					  logi.logInfo("Inside not null else statement")
					  usgRateScale = Integer.parseInt(usageRateScale)
					  logi.logInfo("usgRateScale is :: "+usgRateScale)
			   }

			   logi.logInfo("Formatted query"+String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))

			   hmu = db.executeQueryP2(String.format(generalUsageQry,usgRateScale,usgRateScale,invoiceNo,clientNo,row))
			   hmu.each {k,v ->
					  String v1= v.toString().trim()
					  hmu.put(k,v1)
			   }

			   hmu.sort()
			   logi.logInfo("Individual row of hash is :: "+hmu.toString())

			   mDB.put("Line Item"+temp,hmu)
			   temp++

		}

		// Payments

		int noOfPayments = 0
		HashMap pHash = new HashMap<String,String>()
		String loopCountQry1 = "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
		String addnlLoop1 = "SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as Description, a.Net_Price FROM (SELECT TRANS.DESCRIPTION ,('- '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS Net_Price , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3,13,10) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"

		noOfPayments = db.executeQueryP2(String.format(loopCountQry1,accountNo,accountNo)).toString().toInteger()
		logi.logInfo("The total no of rows in paymnent "+noOfPayments)
		for(int pay = 1;pay <=noOfPayments; pay++){

			   pHash = db.executeQueryP2(String.format(addnlLoop1,accountNo,accountNo,pay))
			   pHash.put("LICENSES", " ")
			   pHash.put("UNIT_PRICE", " ")
			   logi.logInfo("The payment related hash is : "+pHash.toString())

			   //                                            pHash.each {k,v ->
			   //                                                            String v1= v.toString().trim()
			   //                                                            pHash.put(k,v1)
			   //                                            }

			   mDB.put("Line Item"+temp,pHash)
			   temp++

		}
		logi.logInfo("Final hash is : "+mDB.toString())

		// Tax Items
		String percent = "0.00"
		HashMap hmtax = new HashMap<String,String>()
		String lineQry = "select distinct(seq_num) from ariacore.gl_tax_detail where invoice_no = %s order by seq_num"
		String taxRateQry = "select distinct(tax_rate) from ariacore.gl_tax_detail where invoice_no = %s and seq_num = %s"
		//String taxItemsQry = "select s.comments AS Description ,sum(gtl.debit ) AS Net_Price, ' ' AS Unit_price, ' '  AS Licenses from ariacore.all_invoice_tax_details gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"
		String taxItemsQry = "select s.comments || ' %s' AS Description ,TRIM(TO_CHAR((sum(gtl.debit )),'99,990.99')) AS Net_Price from ariacore.gl_tax_detail gtl join ariacore.gl_detail g on g.invoice_no=gtl.invoice_no join ariacore.all_service alls on alls.service_no=g.service_no join ariacore.services s on alls.service_no=s.service_no where  gtl.invoice_no=%s and alls.service_type in ('TX') and gtl.client_no = %s group by s.comments"

		def totalLineItems = []

		totalLineItems = Arrays.asList(db.executeQueryReturnValuesAsArray(String.format(lineQry,invoiceNo)))
		logi.logInfo("Total Line items are : "+totalLineItems)
		for(int lines = 0;lines<totalLineItems.size();lines++){
			   logi.logInfo("Inside for loop ")

			   percent = db.executeQueryP2(String.format(taxRateQry,invoiceNo,totalLineItems.get(lines)))
			   logi.logInfo("The Tax percentage is : "+percent.toString())
			   percent = "0"+percent+"%"
			   logi.logInfo("The Tax percentage after concat : "+percent.toString())

			   hmtax = db.executeQueryP2(String.format(taxItemsQry,percent,invoiceNo,clientNo))
			   hmtax.put("LICENSES", " ")
			   hmtax.put("UNIT_PRICE", " ")
			   logi.logInfo("The Tax related hash is : "+pHash.toString())

			   mDB.put("Line Item"+temp,hmtax)
			   temp++
			   logi.logInfo("Individual Hash tax are : "+hmtax.toString())
		}

		logi.logInfo("Final hash is : "+mDB.toString())
		return mDB;
  }

	def uv_getFormattedStmtID(String tcid){
		
		logi.logInfo("Calling method uv_getFormattedStmtID")
		LinkedHashMap lHM = new LinkedHashMap<String,String>()
		
		String accountNo = u.getValueFromResponse("create_acct_complete","//**:acct_no[1]")
		logi.logInfo("Account Number is "+accountNo)
		
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		ConnectDB db = new ConnectDB()
		String seqNum = ""
		int paramVal = 0
		String formattedStatementId = ""
		
		String generalQry = "SELECT ACT.COUNTRY||'-'||LPAD(ACT.MAX_SEQ_NO,%s,0) FROM ARIACORE.ACCT ACT WHERE ACT.ACCT_NO = %s"
		String valQry = "SELECT PARAM_VAL  FROM ARIACORE.ARIA_CLIENT_PARAMS WHERE PARAM_NAME = 'ACCT_LVL_STMT_ID_LEN' AND CLIENT_NO = "+clientNo
		String seqNumQry = "select MAX_SEQ_NO from ariacore.acct where  acct_no ="+accountNo
		
		paramVal = db.executeQueryP2(valQry).toInteger()
		logi.logInfo("Param val is :: "+paramVal)
		
		seqNum = db.executeQueryP2(seqNumQry)
		logi.logInfo("SeqNum val is :: "+seqNum)
		logi.logInfo("Length of SeqNum val is :: "+seqNum.length())
		
//           int x = (paramVal - seqNum.length())
//           logi.logInfo("Value of x is : "+x)
		
		formattedStatementId = db.executeQueryP2(String.format(generalQry,paramVal,accountNo)).toString()
		logi.logInfo("Formatted formattedStatementId val is :: "+formattedStatementId)
		
		return formattedStatementId
  }*/
	public LinkedHashMap uv_GetCreditLineItems_217(String tcid)
	{

		logi.logInfo("Entered into uv_GetCreditLineItems_217")

		List taxlineitems = [
			"DESCRIPTION",
			"AMOUNT"
		];
		int rowCount,columnCount
		int counter=1
		String creditItems=""
		LinkedHashMap credititem217Map = new LinkedHashMap<String , HashMap<String,String>>()
		HashMap credititem217SubMap = new HashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Cash Credits')]]/following-sibling::tr[td[contains(text(),' ') and not(contains(text(),'Subtotal'))]]")).size()
		columnCount = driver.findElements(By.xpath("//div[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Cash Credits')]]/td")).size()
		logi.logInfo("row  count is :: "+rowCount)
		logi.logInfo("column count is :: "+columnCount)

		for(int i=1 ; i<=rowCount ; i++){
			credititem217SubMap = new HashMap<String,String>()

			for(int k=1 ; k<=columnCount ; k++){

				String expath = "//*[@id='content']/table[2]/tbody/tr[count(td)=2 and td[contains(text(),'Payments/Cash Credits')]]/following-sibling::tr[td[contains(text(),' ')]][%s]/td[%s]"
				String formattedXpath = String.format(expath,i,k)
				logi.logInfo("formattedXpath : "+formattedXpath)
				creditItems = "NO VALUE"

				try {
					logi.logInfo("inside try : ")
					if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
						creditItems = driver.findElement(By.xpath(formattedXpath)).getText()
						logi.logInfo("uiValue : "+creditItems)
					}
				} catch (Exception e) {
					logi.logInfo ("Element not found Exception")
					e.printStackTrace()
				}

				credititem217SubMap.put(taxlineitems.get(k-1),creditItems)
				credititem217SubMap.sort()
				logi.logInfo " credit item submap "+credititem217SubMap

			}

			credititem217Map.put("Line Item"+counter, credititem217SubMap)
			logi.logInfo("Final hash at the time of sequence no "+counter+" is : "+credititem217Map)
			counter++
		}
		return credititem217Map.sort()
	}

	public LinkedHashMap uv_getCreditActivtyLoopItems_DB_217(String tcid)

	{

		logi.logInfo("Calling uv_getCreditActivtyLoopItems_DB_217")
		LinkedHashMap creditActivtyLoopItemsDB = new LinkedHashMap<String , HashMap<String,String>>()
		try {

			HashMap hm = new HashMap<String,String>()
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
			ConnectDB db = new ConnectDB()
			int counter=0
			String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
			String voidedinvoicequery="select source_no from ariacore.acct_trans_void_history_view where acct_no=%s and transaction_type=-1"
			String voidinvoice=db.executeQueryP2(String.format(voidedinvoicequery,acct_no))
			logi.logInfo("voidinvoice "+voidinvoice)
			String voidFlag=db.executeQueryP2("select param_val from ariacore.all_client_params where client_no="+clientNo+"  and param_name ='INCLUDE_VOIDED_TRANSACTIONS_ON_STATEMENTS' ")

			// Voided invoice
			String paymnetsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,TRIM(TO_CHAR((ATRANS.amount),'99,990.99'))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount=db.executeQueryP2(String.format(paymnetsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query2="SELECT a.description || ' '||%s|| ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' ' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-1) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount ; a++){

				hm = db.executeQueryP2(String.format(query2,voidinvoice,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided invoice :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)
				}
			}
			// Voided refunds
			String refundsCountQuery= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,TRIM(TO_CHAR((ATRANS.amount),'99,990.99'))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (13,-12) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int refundsCount=db.executeQueryP2(String.format(refundsCountQuery,acct_no,acct_no)).toString().toInteger()
			String query5="SELECT a.description ||' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' '|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (-12,13) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=refundsCount ; a++){

				hm = db.executeQueryP2(String.format(query5,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding voided refunds :: "+hm.toString())
				if(voidFlag=='TRUE')
				{
					counter=counter+1
					creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

				}
			}
			//ServiceCredits
			String serviceCreditsCountQuery="select count(*) from ariacore.all_credits where acct_no=%s and credit_type='S' and credit_id in ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s and service_no =0)"
			int rows=db.executeQueryP2(String.format(serviceCreditsCountQuery,acct_no,invoiceNo)).toInteger()
			String query1="select description,amount from ( select  ( (case when  credit_type='S' then 'Service Credit (applied ' end ) || to_char(create_date,'DD-MM-YYYY') ) || ')' as DESCRIPTION ,( '%s' ||' ' || TRIM(TO_CHAR((amount),'99,990.99')) ) as AMOUNT, row_number() over (order by credit_id asc) as seq from ariacore.all_credits where acct_no=%s  and credit_id in  ( select orig_credit_id from ariacore.gl_detail where invoice_no=%s and service_no =0) ) where seq=%s"
			for(int row=1 ; row<=rows ; row++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query1,symbol,acct_no,invoiceNo,row))
				logi.logInfo("Individual row of hash after adding service credits :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			logi.logInfo( "counter value : "+counter.toString())

			//Coupons
			String couponsCountquery="select count(*) from ariacore.all_invoice_details where invoice_no=%s and orig_coupon_cd is not null  and orig_credit_id is null"
			int couponsCount=db.executeQueryP2(String.format(couponsCountquery,invoiceNo)).toInteger()
			String query3="select DESCRIPTION,AMOUNT from ( select gld.comments|| ' (applied '  || to_char(gl.due_date,'DD-MM-YYYY')  || ')' as DESCRIPTION,(  '%s'||' '||TRIM(TO_CHAR((gld.debit),'99,990.99'))) as AMOUNT,row_number() over(order by gld.seq_num) as seq  from ariacore.gl_Detail  gld join ariacore.gl on gl.invoice_no=gld.invoice_no where gld.invoice_no=%s and gld.orig_coupon_cd is not null) where seq=%s"
			for(int a=1 ; a<=couponsCount ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query3,symbol,invoiceNo,a))
				logi.logInfo("Individual row of hash after adding coupons :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}
			//payments
			String paymnetsCountQuery1= "SELECT count(*) FROM (SELECT TRANS.DESCRIPTION ,('-'|| TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A "
			int paymnetscount1=db.executeQueryP2(String.format(paymnetsCountQuery1,acct_no,acct_no)).toString().toInteger()
			String query4="SELECT a.description || ' (applied '  || to_char(create_date,'DD-MM-YYYY')  || ')' as description, a.amount FROM (SELECT TRANS.DESCRIPTION ,('%s'||' ' || TRIM(TO_CHAR((ATRANS.amount),'99,990.99')))   AS AMOUNT , ATRANS.STATEMENT_NO AS STNO,trunc(ATRANS.CREATE_DATE) as create_date, row_number() over (order by event_no ASC) AS seq FROM ariacore.acct_transaction ATRANS JOIN ARIACORE.transaction_types TRANS ON ATRANS.TRANSACTION_TYPE = TRANS.TRANSACTION_TYPE WHERE ATRANS.acct_no  = %s AND TRANS.transaction_type IN (2,3) and atrans.statement_no = (select max(statement_no) from ariacore.acct_statement where acct_no = %s  ) )A WHERE A.seq = %s"
			for(int a=1 ; a<=paymnetscount1 ; a++){
				counter=counter+1
				hm = db.executeQueryP2(String.format(query4,symbol,acct_no,acct_no,a))
				logi.logInfo("Individual row of hash after adding all payments :: "+hm.toString())
				creditActivtyLoopItemsDB.put("Line Item"+counter,hm)

			}

		}

		catch (Exception  e) {
			logi.logInfo e.getMessage()
		}

		return creditActivtyLoopItemsDB

	}

	public LinkedHashMap uv_GetHeaderDetails1217DB_Parent(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails1217DB_Parent")
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return GetHeaderDetails1217DB(acct_no)
	}

	public LinkedHashMap uv_GetHeaderDetails1217DB_Child(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails1217DB_Child")
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return GetHeaderDetails1217DB(acct_no)
	}

	public LinkedHashMap GetHeaderDetails1217DB(String acct_no)
	{

		logi.logInfo("Entered into GetHeaderDetails1217DB")
		DecimalFormat d = new DecimalFormat("0.00")
		LinkedHashMap header1217DBMap = new LinkedHashMap<String,String>()
		ConnectDB db=new ConnectDB()
		String invoice_No=""
		String glbalfwd=""
		String stmtTotPymt=""
		String glDebit=""
		String acctTotalOwed=""
		String paymethod=""
		String nextbilldate=""

		String ccnumber=u.getValueFromRequest("create_acct_complete","//cc_number")
		String pmtd=u.getValueFromRequest("create_acct_complete","//pay_method")
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")
		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)

		//InsertGlBalFwd
		String glbalfwdqry="SELECT '%s ' || TRIM(TO_CHAR(NVL(SUM(T.BALANCE_FORWARD),0),'99,990.99')) \"PREVIOUS BALANCE\" FROM(SELECT BALANCE_FORWARD,ROW_NUMBER() OVER(ORDER BY STATEMENT_NO DESC) SEQ  FROM ARIACORE.ACCT_STATEMENT  WHERE ACCT_NO="+acct_no+" )T WHERE T.SEQ=1"
		glbalfwd= db.executeQueryP2(String.format(glbalfwdqry,symbol))
		logi.logInfo "Glbalfwd is "+glbalfwd

		//InsertCurrentStmtTotalPymts
		String stmtTotalPymtqry="SELECT  '-'||'%s '|| TRIM(TO_CHAR(NVL(SUM(e.pymt_total),0),'99,990.99')) \"LAST PAYMENT RECEIVED\" FROM(select pymt_total,row_number()over(order by asm.statement_no desc) as seq from ariacore.acct_stmt_sum	asm Join ariacore.acct_statement ast on asm.statement_no=ast.statement_no where ast.acct_no="+acct_no+" )e where e.seq=1"
		stmtTotPymt= db.executeQueryP2(String.format(stmtTotalPymtqry,symbol))
		logi.logInfo "Statment total payment is "+stmtTotPymt

		//InsertCurrentStmtTotalCredits
		String stmtno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (2,3,10,13)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String paycredits="-"+symbol+' '+d.format(val).toString()
		logi.logInfo "paycredit is "+paycredits


		//InsertGlDebit
		//String gldbtq="SELECT'%s ' || TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) \"TOTAL NEW CHARGES\" FROM ARIACORE.ACCT_TRANSACTION WHERE ACCT_NO="+acct_no+" AND SOURCE_NO="+invoice_No+" AND AMOUNT > 0"
		String gldbtq="SELECT'%s ' || TRIM(TO_CHAR(NVL(sum(debit),0),'99,990.99'))  \"TOTAL NEW CHARGES\" from ariacore.all_invoices where invoice_No="+invoice_No
		glDebit=db.executeQueryP2(String.format(gldbtq,symbol))
		logi.logInfo "glDebit is "+glDebit

		//InsertAcctTotalOwed
		// String acctTotalOwedqry="SELECT CASE WHEN bal_fwd IS NULL THEN '%s ' ||TRIM(TO_CHAR(NVL((debit-credit),0),'99,990.99')) ELSE '%s ' ||TRIM(TO_CHAR(NVL(((bal_fwd+debit)-credit),0),'99,990.99'))  End AS \"NET BALANCE\" FROM ariacore.gl WHERE acct_no ="+acct_no+" and invoice_no ="+invoice_No
		String acctTotalOwedqry="SELECT '%s ' || TRIM(TO_CHAR(NVL(SUM(debit),0),'99,990.99')) AS \"NET BALANCE\" FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No
		acctTotalOwed=db.executeQueryP2(String.format(acctTotalOwedqry,symbol,symbol))
		logi.logInfo "acctTotalOwed is "+acctTotalOwed

		//InsertNextbilldate
		String nxtbilldateqry="SELECT TO_CHAR(NEXT_BILL_DATE,'DD-MM-YYYY')  FROM ARIACORE.ACCT WHERE ACCT_NO="+acct_no
		nextbilldate=db.executeQueryP2(nxtbilldateqry)
		logi.logInfo "nextbilldate is "+nextbilldate


		header1217DBMap.put("PREVIOUS BALANCE",glbalfwd)
		header1217DBMap.put("LAST PAYMENT RECEIVED",stmtTotPymt)
		header1217DBMap.put("CREDITS & ADJUSTMENTS",paycredits)
		header1217DBMap.put("TOTAL NEW CHARGES",glDebit)
		header1217DBMap.put("NET BALANCE",acctTotalOwed)
		header1217DBMap.put("NEXT BILLING DATE",nextbilldate)

		logi.logInfo "header1217DBMap is "+header1217DBMap

		return header1217DBMap.sort()

	}

	public LinkedHashMap uv_GetHeaderDetails_1_217(String tcid)
	{

		logi.logInfo("Entered into uv_GetHeaderDetails_1_217")

		List headers = [
			"PREVIOUS BALANCE",
			"LAST PAYMENT RECEIVED",
			"CREDITS & ADJUSTMENTS",
			"TOTAL NEW CHARGES",
			"NET BALANCE",
			"NEXT BILLING DATE"
		];
		int rowCount
		int columnCount=2
		int listcounter=0
		String header1items=""
		LinkedHashMap header1Map = new LinkedHashMap<String,String>()

		rowCount = driver.findElements(By.xpath("//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr")).size()
		logi.logInfo("row  count is :: "+rowCount)

		for(int i=1 ; i<=rowCount ; i++){

			String expath = "//div[@id='content']/table[1]/tbody/tr/td[2]/table/tbody/tr[%s]/td[%s]"
			String formattedXpath = String.format(expath,i,columnCount)
			logi.logInfo("formattedXpath : "+formattedXpath)
			header1items = "NO VALUE"

			try {
				logi.logInfo("inside try : ")
				if(driver.findElement(By.xpath(formattedXpath)).isDisplayed()){
					header1items = driver.findElement(By.xpath(formattedXpath)).getText()
					logi.logInfo("header1items : "+header1items)
				}
			} catch (Exception e) {
				logi.logInfo ("Element not found Exception")
				e.printStackTrace()
			}

			header1Map.put(headers.get(listcounter),header1items)
			header1Map.sort()
			listcounter++
			logi.logInfo " Final header1Map HashMap "+header1Map
		}

		return header1Map.sort()
	}

	public String uv_GlBaseDebit217Parent_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_GlBaseDebit217_222(acct_no)
	}

	public String uv_GlBaseDebit217Child_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GlBaseDebit217_222(acct_no)
	}
	public String uv_GlBaseDebit217_222(String acct_no)
	{

		logi.logInfo " Entered into uv_GlBaseDebit217_222 method "
		DecimalFormat d = new DecimalFormat("#.00");
		ConnectDB db = new ConnectDB()
		String invoice_No = ""
		String glBaseDebit=""
		String balXferAmt=""
		String finalGlBaseDebit=""
		String finalGlBaseDebit2=""

		String invoiceQry = "select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no
		invoice_No = db.executeQueryP2(invoiceQry)

		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')

		String glbasedbtq="SELECT TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_No+" AND SERVICE_NO NOT IN(401,402,403,404,405)"
		glBaseDebit=db.executeQueryP2(glbasedbtq)

		String balXferAmtqry="SELECT TRIM(TO_CHAR(NVL(SUM(AMOUNT),0),'99,990.99')) FROM ARIACORE.BALANCE_TRANSFER WHERE TO_ACCT_NO="+acct_no+" AND CLIENT_NO="+clientNo
		balXferAmt=db.executeQueryP2(balXferAmtqry)

		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")

		finalGlBaseDebit=(glBaseDebit.toDouble()+ balXferAmt.toDouble()).toString()
		logi.logInfo " parsed double "
		finalGlBaseDebit2=d.format(finalGlBaseDebit.toDouble()).toString()
		logi.logInfo " formatted double " + finalGlBaseDebit2
		String fnlGlBaseDebit='Subtotal: '+symbol+' '+finalGlBaseDebit2
		logi.logInfo " formatted double " + fnlGlBaseDebit

		return fnlGlBaseDebit
	}


	public String uv_AcctTotNewPymts_217(String tcid)
	{
		logi.logInfo " Entered into uv_AcctTotNewPymts_217 method "
		ConnectDB db=new ConnectDB()

		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")

		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")

		logi.logInfo " Currency symbol retrieved "
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String acctttlqry="SELECT '%s ' || TRIM(TO_CHAR(NVL(new_payments,0),'99,990.99'))  from ariacore.acct_statement where acct_no = "+acct_no+" and client_no = "+clientNo+" order by statement_no desc"
		String acctttlnewpymt=db.executeQueryP2(String.format(acctttlqry,symbol))
		String fnlnewpymt='Subtotal: '+acctttlnewpymt
		return fnlnewpymt
	}


	public String uv_GlTaxDebit_217(String tcid)
	{
		logi.logInfo " Entered into uv_GlTaxDebit_217 method "
		ConnectDB db=new ConnectDB()
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")

		String symbol=db.executeQueryP2("select currency_htm_symbol from ariacore.currency where currency_cd =(select currency_cd from ariacore.acct where acct_no="+acct_no+")")

		String gltaxqry="SELECT 'Subtotal: '||'%s ' || TRIM(TO_CHAR(NVL(SUM(DEBIT),0),'99,990.99')) FROM ARIACORE.GL_DETAIL WHERE SERVICE_NO  IN(401,402,403,404,405) AND INVOICE_NO IN(select MAX(invoice_no) from ariacore.gl where acct_no ="+acct_no+")"
		String gldbt=db.executeQueryP2(String.format(gltaxqry,symbol))

		return gldbt
	}

	public boolean previewNextStatement()
	{
		boolean nxtStmtFlg = false
		logi.logInfo("Entered into  previewNextStatement")
		try
		{
			driver.findElement(linkRecentStatements).click()
			logi.logInfo(" Clicked Recent Statements")
			waitForTheElement(tabStatements)
			String acct_no=u.getValueFromResponse("create_acct_complete","//*/*:acct_no[1]")
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')

			if(driver.findElement(previewNxtStmt).isDisplayed())
			{
				driver.findElement(previewNxtStmt).click();
				Thread.sleep(2000)
				driver.switchTo().frame(0)
				logi.logInfo(" inside frame for preview statement")
				nxtStmtFlg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			nxtStmtFlg = true;
		}
		return nxtStmtFlg;
	}

	public LinkedHashMap uv_getXmlChargeLineitems_102(String tcid)
	{
		logi.logInfo("calling uv_getXmlChargeLineitems_102")
		String t=uv_getXmlText_102()
		logi.logInfo(" Text inside frame ")
		logi.logInfo(t)
		def rootNode = new XmlParser().parseText(t)
		List charges=rootNode.invoices.invoice.invoice_line_item_charges.invoice_line_item
		logi.logInfo "Total charges : "+charges.size().toString()
		LinkedHashMap chargeitems= new LinkedHashMap<String , LinkedHashMap<String,String>>()
		int counter=0
		charges.each {it->
			counter=counter+1
			LinkedHashMap details=new LinkedHashMap<String,String>()
			it.each {item->
				details.put(item.name(), item.text())
			}
			logi.logInfo "details hash "+details.toString()
			logi.logInfo "---------------------------------------"
			chargeitems.put("LineItem"+counter.toString(),details)
		}
		logi.logInfo  "Final hash "+chargeitems.toString()
		return chargeitems
	}
	public String uv_getXmlText_102()
	{
		logi.logInfo("calling uv_getXmlText_102")
		String txt= driver.pageSource
		txt=txt.split("Accounts</div>")[1].split("</div></body></html>")[0]
		//logi.logInfo(" Text inside frame ")
		//logi.logInfo(txt)
		return txt
	}

	public String uv_GLTotalRecurAfterCredits_222(String tcid)
	{
		logi.logInfo " Entered into uv_GLTotalRecurAfterCredits_222 method "
		ReadData rdObj =new ReadData()
		DecimalFormat d = new DecimalFormat("##.00")
		String serviceOrder =""
		String resp_level_cd=""
		String parentAcctno=""
		String childAcctno=""
		String totalRecAftCdtParent=""
		String totalRecAftCdtChild=""
		String totalRecAftCdt=""
		String localTcId = tcid.split("-")[1]

		serviceOrder= rdObj.getTestDataMap("StatementsUI",localTcId).get("Service_Order")
		logi.logInfo "Service order  " +serviceOrder

		resp_level_cd=rdObj.getTestDataMap("create_acct_complete.a",localTcId).get("resp_level_cd")
		logi.logInfo "Resp_level_cd from input  " +resp_level_cd

		if(serviceOrder.contains("create_acct_complete.a") && (resp_level_cd.equals("2") || resp_level_cd.equals("3") ||resp_level_cd.equals("4")))
		{
			parentAcctno=u.getValueFromResponse("create_acct_complete","//acct_no")
			totalRecAftCdtParent=uv_GLTotalRecAfterCredit(parentAcctno)
			logi.logInfo "Total Recurring amt after Credit for parent  " +totalRecAftCdtParent

			childAcctno=u.getValueFromResponse("create_acct_complete.a","//acct_no")
			totalRecAftCdtChild=uv_GLTotalRecAfterCredit(childAcctno)
			logi.logInfo "Total Recurring amt after Credit for child  " +totalRecAftCdtChild

			totalRecAftCdt=d.format(totalRecAftCdtParent.toDouble()+totalRecAftCdtChild.toDouble()).toString()
			logi.logInfo "TotalRecurring amount  " +totalRecAftCdt
			return totalRecAftCdt
		}
		else{

			String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
			return uv_GLTotalRecAfterCredit(acct_no)
		}

	}
	public String uv_GLTotalRecurAfterCreditsChild_222(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_GLTotalRecAfterCredit(acct_no)
	}
	
	public String uv_GLTotalDueParent(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete","//acct_no")
		return uv_getGLTotalDue(acct_no)
	}
	public String uv_GLTotalDueChild(String tcid)
	{
		String acct_no = u.getValueFromResponse("create_acct_complete.a","//acct_no")
		return uv_getGLTotalDue(acct_no)
	}
	
	
	public String uv_getGLTotalDue(String acct_no)
	{
		logi.logInfo("Calling uv_getGLTotalDue")
		DecimalFormat d = new DecimalFormat("0.00");
		String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceNo = u.getInvoiceNoVT(clientNo, acct_no).toString();
		ConnectDB db = new ConnectDB()
		String stno=db.executeQueryP2("select max(statement_no) from ariacore.acct_statement where acct_no="+acct_no)
		String transQuery="select TRIM(TO_CHAR((NVL(SUM(amount),0)),'99,990.99')) from ariacore.acct_transaction where acct_no=%s and statement_no=%s and transaction_type in (10)"
		String trans_amt=db.executeQueryP2(String.format(transQuery,acct_no,stno))
		String creditsQuery="SELECT TRIM(TO_CHAR((NVL(SUM(debit)*-1,0)),'99,990.99')) FROM ariacore.gl_detail  WHERE invoice_no=(SELECT MAX(invoice_no) FROM ariacore.gl WHERE acct_no=%s ) and (orig_coupon_cd is not null or orig_credit_id is not null )"
		String credit_amt=db.executeQueryP2(String.format(creditsQuery,acct_no))
		double val=trans_amt.toDouble()+credit_amt.toDouble()
		logi.logInfo("total "+val.toString())
		String fval=d.format(val).toString()
		return fval.trim()
	}
	/**UI verification on billing address for 25972597 and 110793 templates 
	 * @param tcid
	 * @return
	 */
	public LinkedHashMap uv_stmtbillingcontact25972597_m(String tcid)
	{
		   logi.logInfo("Calling uv_stmt_billingcontact25972597")
		   LinkedHashMap<String,String> contact_detail=new LinkedHashMap<String,String>()
		   List<WebElement> items = driver.findElements(By.xpath("//table[@class='bill']//tr//td[ string-length()>1 and not (contains(text() ,'GST ')) and not (contains(text(),'QST')) and not (contains(text(),'CA'))]"))
		   int counter=0
		   items.each  { item ->
				  logi.logInfo("Preparing Item Hash")
				  if (item.getText().length()>0)
				  {
					 counter=counter+1
					 logi.logInfo(item.getText())
					 contact_detail.put("Billing_Address_Line"+counter, item.getText())
				  }
		   }
		   
		   return contact_detail
	}
	
	public boolean gotoInvoiceFromLeftNavigation(String str)
	{
		boolean flg = false
		logi.logInfo("Calling gotoInvoiceFromLeftNavigation")
		if(str.contains("#"))
		str=str.replace("#",",")
		logi.logInfo("str value "+str)
		try
		{
			logi.logInfo("title in gotoInvoiceFromLeftNavigation =>"+ driver.title)
			logi.logInfo "leftnavigationinvoice exits "+driver.findElement(leftnavigationinvoice).isDisplayed()
			driver.findElement(leftnavigationinvoice).click();
			logi.logInfo(" Clicked Statement&Invoice from left nvaifation")
			waitForTheElement(tabInvoice)
			driver.findElement(tabInvoice).click();
			logi.logInfo(" Clicked invoice tab")
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			String plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
			ConnectDB db = new ConnectDB()
			String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where master_plan_instance_no = " + plan_instance+" and client_no="+clientNo
			String invoiceNo = db.executeQueryP2(query)			
			
			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
			{
				driver.findElement(linkInvoiceno).click();
			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			}
			else
			{
				logi.logInfo(" Unable to find invoce "+invoiceNo)
				flg=false;
			}
			
			
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = true;

		}


		return flg;



	}
	public LinkedHashMap uv_getBillingGroupReplacementStrings_821821(String tcid)
	
		{
	
			try {
				logi.logInfo("Calling uv_getBillingGroupReplacementStrings_821821")
				logi.logInfo("driver inside method "+driver)
				LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
				List keyValues = [
				                  "INSERTSTMTBILLINGGROUPNO",
				                  "INSERTSTMTBILLINGGROUPNAME",
				                  "INSERTSTMTBILLINGGROUPCDID",
				                  "INSERTSTMTBILLINGGROUPDESC",
				                  ];
				String billingGroup_line_items="//table[2]//td"
						String txt=driver.findElement(By.xpath(billingGroup_line_items)).getAttribute("innerHTML")
						List items=txt.split("<br><br>")
						int i=0;
						items.each  {line ->
			
			if(line.contains("Billing Group"))
			{
				
				if(line.contains(":"))
				{
				
				String temp=line.split(":")[1].trim()				
				bg_detail.put(keyValues[i],temp)
				
				}
				i++
			}
			
		}
							
						
						return bg_detail.sort()
			} catch (Exception e) {
			logi.logInfo("Method Exception found : "+e.getMessage())
			}
		}

		public LinkedHashMap uv_getBillingGroupReplacementStrings_821821_DB (String tcid )
		{
			logi.logInfo("Calling uv_stmt_getBillingAdressLineItem_DB")
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
			String query="SELECT billing_group_no  insertStmtBillingGroupNo ,profile_name insertStmtBillingGroupName,profile_cdid insertStmtBillingGroupCDID ,profile_description insertStmtBillingGroupDesc from  ariacore.acct_billing_group where acct_no="+acct_no
			LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
			ConnectDB db = new ConnectDB()
			bg_detail=db.executeQueryP2(query)
			return bg_detail.sort()
		}
		public LinkedHashMap uv_getStatementContactReplacementStrings_821821_DB (String tcid )
		{
			logi.logInfo("Calling uv_getStatementContactReplacementStrings_821821_DB")
			String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
			String query="SELECT first_name insertStmtFirstName  ,MIDDLE_INITIAL insertStmtMiddleInit ,LAST_NAME insertStmtLastName ,COMPANY_NAME insertStmtCompanyName ,ADDRESS1 insertStmtAddress1 ,ADDRESS2 insertStmtAddress2 ,ADDRESS3 insertStmtAddress3 ,CITY insertStmtCity ,LOCALITY insertStmtLocality ,STATE insertStmtState ,COUNTRY insertStmtCountry ,ZIP insertStmtZip ,PHONE_NPA insertStmtPhoneNPA ,PHONE_NXX insertStmtPhoneNXX ,PHONE_SUFFIX insertStmtPhoneSuffix ,PHONE_EXTENSION insertStmtPhoneExt ,INTL_PHONE insertStmtFullPhone ,CELL_PHONE_NPA insertStmtCellPhoneNPA ,CELL_PHONE_NXX insertStmtCellPhoneNXX ,CELL_PHONE_SUFFIX insertStmtCellPhoneSuffix ,CELL_PHONE insertStmtFullCellPhone ,EMAIL insertStmtEmail   from ariacore.acct_statement a join ariacore.ACCT_BILLING_GROUP  bg  on  bg.acct_no=a.acct_no  and bg.BILLING_GROUP_NO=a.BILLING_GROUP_NO join ariacore.acct_address addr on addr.acct_no=a.acct_no and addr.ADDRESS_SEQ=bg.STATEMENT_ADDRESS_SEQ where a.acct_no="+acct_no
			LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
			ConnectDB db = new ConnectDB()
			bg_detail=db.executeQueryP2(query)
			return bg_detail.sort()
		}
		public LinkedHashMap uv_getStatementContactReplacementStrings_821821(String tcid)
		
			{
				
		
				try {
					logi.logInfo("Calling uv_getStatementContactReplacementStrings_821821")
					logi.logInfo("driver inside method "+driver)
					LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
					List keyValues = ["INSERTSTMTFIRSTNAME" , "INSERTSTMTMIDDLEINIT" , "INSERTSTMTLASTNAME" , "INSERTSTMTCOMPANYNAME" , "INSERTSTMTADDRESS1" , " INSERTSTMTADDRESS2" , " INSERTSTMTADDRESS3" , " INSERTSTMTCITY" , " INSERTSTMTLOCALITY" , " INSERTSTMTSTATE" , " INSERTSTMTCOUNTRY" , " INSERTSTMTZIP" , " INSERTSTMTPHONENPA" , " INSERTSTMTPHONENXX" , " INSERTSTMTPHONESUFFIX" , " INSERTSTMTPHONEEXT" , " INSERTSTMTFULLPHONE" , " INSERTSTMTCELLPHONENPA" , " INSERTSTMTCELLPHONENXX" , " INSERTSTMTCELLPHONESUFFIX" , " INSERTSTMTFULLCELLPHONE" , " INSERTSTMTEMAIL"
									  ];
					String billingGroup_line_items="//table[2]//td"
							String txt=driver.findElement(By.xpath(billingGroup_line_items)).getAttribute("innerHTML")
							logi.logInfo("FOUND")
							List items=txt.split("<br><br>")
							int i=0;
							items.each  {line ->
				
				if(line.contains("Stmt"))
				{
					
					if(line.contains(":"))
					{
					
					String temp=line.split(":")[1].trim()
					bg_detail.put(keyValues[i].trim(),temp)
					
					}
					i++
				}
				
			}
								
							
							return bg_detail.sort()
				} catch (Exception e) {
				logi.logInfo("Method Exception found : "+e.getMessage())
				}
			}
			public LinkedHashMap uv_getbeginLoopAcctAllTxns_789789(String tcid)
			
				{
			
					try {
						logi.logInfo("Calling uv_getbeginLoopAcctAllTxns_789789")
						logi.logInfo("driver inside method "+driver)
						LinkedHashMap<String,LinkedHashMap<String,String>> detail=new LinkedHashMap<String,LinkedHashMap<String,String>>()
						List keyValues = [
										  "INSERTACCTTXNNO",
										  "INSERTACCTTXNTYPEDESC",
										  "INSERTACCTTXNDT",
										  "INSERTACCTTXNAMT",
										  ];
									  int counter=0
									  int kcounter=0
						String trXpath="//table[@class='loop']//td[@class='lineitem']/.."
								 List<WebElement> items=driver.findElements(By.xpath(trXpath))
								 items.each  { item ->
									 logi.logInfo("Preparing Item Hash")
									 counter=counter+1
									 LinkedHashMap internalHM = new LinkedHashMap<String,String>()
									 List<WebElement> tds = item.findElements(By.xpath(".//td"))
									 logi.logInfo("tds count : "+tds.size().toString())
									 tds.each {titem ->
										 logi.logInfo("td value "+titem.getText())
										 internalHM.put(keyValues.get(kcounter), titem.getText())
										 kcounter=kcounter+1
						 
									 }
									 kcounter=0
									 internalHM=internalHM.sort()
									 detail.put("Line Item"+counter,internalHM)
					
				}
									
								
								return detail.sort()
					} catch (Exception e) {
					logi.logInfo("Method Exception found : "+e.getMessage())
					}
				}
				public LinkedHashMap uv_getbeginLoopAcctAllTxns_789789_DB (String tcid)
				{
					logi.logInfo("Calling uv_getbeginLoopAcctAllTxns_789789_DB")
					ConnectDB db = new ConnectDB()
					LinkedHashMap<String,LinkedHashMap<String,String>> detail=new LinkedHashMap<String,LinkedHashMap<String,String>>()
					String acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m("1",null)
					String invNo=im.md_get_invoice_no_m("1,1m",null);
					String countquery="  select  count(*) from (  select row_number() over ( order by atrans.event_no ) as sno ,atrans.event_no INSERTACCTTXNNO ,tr.description INSERTACCTTXNTYPEDESC,  TO_CHAR(atrans.create_date,'MM/dd/yyyy') INSERTACCTTXNDT ,atrans.amount INSERTACCTTXNAMT from ariacore.acct_transaction  atrans  join ariacore.transaction_types tr on  tr.transaction_type=atrans.transaction_type  where atrans.statement_no=(  select statement_no from ariacore.acct_statement where  acct_no=%s and invoice_no= %s) ) "
					int cnt=db.executeQueryP2(String.format(countquery,acct_no,invNo)).toInteger()
					logi.logInfo("cnt "+cnt);
					String query="  select INSERTACCTTXNNO ,INSERTACCTTXNTYPEDESC,INSERTACCTTXNDT,INSERTACCTTXNAMT from (  select row_number() over ( order by atrans.event_no ) as sno ,atrans.event_no INSERTACCTTXNNO ,tr.description INSERTACCTTXNTYPEDESC,  TO_CHAR(atrans.create_date,'FMMM/dd/yyyy') INSERTACCTTXNDT ,atrans.amount INSERTACCTTXNAMT from ariacore.acct_transaction  atrans  join ariacore.transaction_types tr on  tr.transaction_type=atrans.transaction_type   and tr.transaction_type not in (-4,4,5,-5,21)  where atrans.statement_no=(  select statement_no from ariacore.acct_statement where  acct_no=%s and invoice_no= %s) ) where sno=%s" 
					LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
					
					for(int i=1;i<=cnt;i++)
					{
					bg_detail=db.executeQueryP2(String.format(query,acct_no,invNo,i))
					detail.put("Line Item"+i,bg_detail)
					
					}
					return detail.sort()
				}
				
				public String uv_isBalanceXferExistsOnLoop_m(String str)
				{
					boolean res=false
					try {
						logi.logInfo("Calling uv_isBalanceXferExistsOnLoop_m")
						ConnectDB db = new ConnectDB()
						String acct_no=null
						logi.logInfo("param"+str)
						if(str.contains("&"))
						str=str.replace("&",",")
						
						logi.logInfo("str value "+str)
						if(str.contains("#"))				
						acct_no=im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
						else
						acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
						logi.logInfo("Fetched acct_no"+acct_no)
						List<String> BalanceXferTxns=["4","5","-4","-5"]			
						String query="select transaction_type from ariacore.acct_transaction   where  acct_no=%s and event_no in (%s)"
										
						String trXpath="//table[@class='loop']//td[@class='lineitem']/../td[1]"
						List<WebElement> items=driver.findElements(By.xpath(trXpath))
						items.each  { item ->
							logi.logInfo ("txnNo "+item.getText());
							String txnInTemplate=db.executeQueryP2(String.format(query,acct_no,item.getText()));
							if(BalanceXferTxns.contains(txnInTemplate))
							res=true
						
						}
						
						return res.toString().toUpperCase()
					}
					
				catch (Exception e) {
				logi.logInfo("Method Exception found : "+e.getMessage())
				}
				
					
				}
				public boolean parentchildaccountSearch(String str)
				{
					  logi.logInfo " Reloading the page... "
					  driver.navigate().refresh()
					  boolean success = false;
					  try {
							waitForTheElement(tabAccounts)
							//Store the current window handle
							winHandleMain = driver.getWindowHandle();
							logi.logInfo( "TESTCASEID ON SEARCH "+Constant.TESTCASEID)
							if(driver.findElement(tabAccounts).isDisplayed())
							{
								  String isAcctTabSelected = driver.findElement(tabAcctOnClick).getAttribute("aria-selected")
								  logi.logInfo("isAcctTabSelected "+isAcctTabSelected)
								  if(isAcctTabSelected.equalsIgnoreCase("false"))
								  {
										logi.logInfo(" Going to select accounts tab")
										driver.findElement(tabAccounts).click();
										logi.logInfo("Selected accounts tab")
								  }
								  else
								  {
										logi.logInfo("Accounts tab already selected")
								  }
								  if(driver.findElement(tabAcctOnClick).getAttribute("aria-selected").equalsIgnoreCase("true"))
								  {
										logi.logInfo("Accounts tab expanded")
										logi.logInfo "acct search "+driver.findElement(tabAcctSrch).isDisplayed()
										//waitForTheElement(tabAcctSrch)
										String isSrchExpanded = driver.findElement(tabSrchOnClk).getAttribute("class")
										logi.logInfo "isSrchExpanded "+isSrchExpanded
										if(isSrchExpanded.trim().equals("") )
										{
											  logi.logInfo("Going to click search option")
											  driver.findElement(tabAcctSrch).click();
											  logi.logInfo("Clicked search option")
										}
										/*if(driver.findElement(txtSrch).isDisplayed())
										{
										logi.logInfo("search text box is aviailable")
										boolean flg_search=search()
										if(flg_search)
										success=true
										}*/
										else if(driver.findElement(tabSrchOnClk).getAttribute("class").equalsIgnoreCase("active_subnav"))
										{
											  logi.logInfo "Already search option enabled"
										}
										waitForTheElement(tabAdhocSrch)
										driver.findElement(tabAdhocSrch).click();
										Thread.sleep(6000)
										String acct_no= im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
										boolean flg_search=search(acct_no)
										if(flg_search)
											  success=true
								  }
								  else
								  {
										logi.logInfo("acct search else part")
								  }
							}
							return success
					  } catch (Exception e) {
							e.printStackTrace()
							captureShot("Errro in accountSearch_"+Constant.TESTCASEID)
					  }
				}
				

	public boolean pagotoInvoiceFromLeftNavigation(String str)
	{
		boolean flg = false

		logi.logInfo("Calling pagotoInvoiceFromLeftNavigation")
		logi.logInfo("title of the driver in goto invoice navigation 5566  =>"+ driver.title)
		if(str.contains("&"))
			str=str.replaceAll("&",",")
		//Thread.sleep(1000)
		logi.logInfo("str value "+str)

		try
		{
			logi.logInfo("title in gotoInvoiceFromLeftNavigation 1212=>"+ driver.title)
			logi.logInfo "leftnavigationinvoice exits "+driver.findElement(linkViewAllInvoices).isDisplayed()

			driver.findElement(linkViewAllInvoices).click();
			logi.logInfo(" Clicked linkViewAllInvoices from account overview")

			waitForTheElement(tabInvoice)
			driver.findElement(tabInvoice).click();
			logi.logInfo(" Clicked invoice tab")
			String plan_instance
			String clientNo=Constant.mycontext.expand('${Properties#Client_No}')
			if(str.contains("#")){
				plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
			}else{
				plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
			}
			ConnectDB db = new ConnectDB()
			String query = "select max(invoice_no) as invoice_no from ariacore.gl_detail where master_plan_instance_no = " + plan_instance + " and client_no = "+clientNo
			String invoiceNo = db.executeQueryP2(query)

			if(driver.findElement(linkInvoiceno).getText()==invoiceNo)
				driver.findElement(linkInvoiceno).click();
			waitForTheElement(linkviewPrintStatement)
			logi.logInfo(" Clicked invoce "+invoiceNo)
			if(driver.findElement(linkviewPrintStatement).isDisplayed())
			{
				flg = true;
			}

		}catch (Exception e)
		{
			logi.logInfo(e.printStackTrace());
			flg = false;

		}


		return flg

	}


				public int uv_DB_template_email_m(String str){
					/***pa&1#a&24553***/
					String s1,acct_no;
					logi.logInfo "inside db template versions...."+str
					 ConnectDB db = new ConnectDB()
					 logi.logInfo "inside db template versions.2.0.."
					String s = str.split("&")[0]
					logi.logInfo "string split "+s
					if(s.contains("pa")){
						 s1 = str.split("&")[1]
						 logi.logInfo "string split "+s1
						 acct_no = im.md_GET_ACCT_OR_INSTANCE_NO2_m(s1,null)
					}else{
					s1 = str.split("&")[1]
					logi.logInfo "string split in else  "+s1
					 acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s1,null)
					}
					String template_no = str.split("&")[2]
					logi.logInfo "string split on template "+template_no
					String s3 = "SELECT COUNT(*) AS COUNTS FROM ARIACORE.EMAIL_HIST WHERE ACCT_NO = "+acct_no+" AND TEMPLATE_NO = "+template_no
					String query = db.executeQueryP2(s3)
					int reval = Integer.parseInt(query)
					logi.logInfo "return value "+reval
					return reval
				}
				
				
				/** UI invoice line items for MPIAllTxnsLoop 5793
				 * @param s
				 * @return values from UI with array 'keys'
				 */
				public  LinkedHashMap uv_mpialltrnx_5793(String s)
				   {
					 logi.logInfo("Calling uv_mpialltrnx_5793"+s)
				LinkedHashMap<String,String> alltrnx=new LinkedHashMap<String,String>()
				int counter=0
				int kcounter=0
				List<String> keys=[
					"MPINo",
					"ClientMPIId",
					"MPITxnNo",
					"MPITxnTypeDesc",
					"MPITxnDt",
					"ItemServiceNo",
					"ItemPlanNo",
					"ItemPlanName",
					"ItemSimpleLabel",
					"ItemStartDate",
					"ItemEndDate",
					"ItemPeriodDays",
					"ItemBaseUnits",
					"SPINo",
					"ClientSPIId",
					"ItemComments",
					"ItemPlanAndComments",
					"ItemLineComments",
					"ItemUnits",
					"ItemRate",
					"ItemTierLimits",
					"ItemSurchargeNo",
					"ItemTaxTotal",
					"ItemTierBasePlanUnits",
					"ItemTaxType",
					"ItemTaxRate",
					"ItemTaxTInd",
					"ItemGrossTotal",
					"ItemPercentProrationFactor",
					"ItemProrationFactor",
					"ItemOrigChargeInvoiceNo",
					"ItemOrigChargeTxnNo",
					"RateScheduleDescription",
					"ClientRateScheduleId",
					"ItemCouponCode",
					"ItemCouponDesc",
					"ItemCreditReasonCode",
					"ItemCreditReasonText",
					"ItemNetPrice",
					"ItemMonthlyAmount",
					"TotalBillingAmount",
					"MPITxnAmt",
					"PONoInstance"

				]
					List<WebElement> items = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'MPIAllTxnsLoop')]//../following-sibling::tr"));
					logi.logInfo("uv_mpialltrnx_5793 Items count : "+items.size().toString())
					items.each  { item ->
						logi.logInfo("Preparing Item Hash uv_mpialltrnx_5793")
						counter=counter+1
						LinkedHashMap hm = new LinkedHashMap<String,String>()
						List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
						String sss = tds.size().toString()
						logi.logInfo("uv_mpialltrnx_5793 tds count : "+sss)
						if(tds.size()<=0){logi.logInfo "no values entered here "+tds.size().toString();
							counter = counter-1
						}else{
						int tdm = Integer.parseInt(sss)
						logi.logInfo("converted to integer : "+tdm)
						for(int i=0;i<tdm;i++){
							logi.logInfo "inside the loop "+keys.get(i)
							if(tds.get(i).getText().toString().equals("")){
								logi.logInfo "got an empty node at : "+i
							}else{
							logi.logInfo "adding values at "+keys.get(i)+" with "+tds.get(i).getText()
							hm.put(keys.get(i).toUpperCase(), tds.get(i).getText())
							}
							logi.logInfo("HashMap Values"+hm.toString())
							//kcounter=kcounter+1
						}
						
						
						/*
						tds.each {titem ->
							
							logi.logInfo("uv_mpialltrnx_5793 td value "+titem.getText())
							
								logi.logInfo "BOSCO NO"
								hm.put(keys.get(kcounter), titem.getText())
								kcounter=kcounter+1
							
						}*/
						//kcounter=0
						hm=hm.sort()
						alltrnx.put("Line Item"+counter,hm)
						}
					}
					logi.logInfo("MPIAllTxnsLoop "+alltrnx.toString())
					return alltrnx.sort()
				   }
				
				   public  LinkedHashMap uv_statement_box_5763_m(String s)
				   {
					  
					   List<WebElement> items = driver.findElements(By.xpath("//table[@class='box']//tr"));
						logi.logInfo("box Items count : "+items.size())
						LinkedHashMap hm = new LinkedHashMap<String,String>()
					for (int i=1; i<= items.size();i++){
						String item1 = driver.findElement(By.xpath("//table[@class='box']//tr["+i+"]/th")).getText().toString();
						logi.logInfo("item1 : "+item1)
						String item2 = driver.findElement(By.xpath("//table[@class='box']//tr["+i+"]/td")).getText().toString();
						logi.logInfo("item2 : "+item2)
						hm.put(item1.trim().toUpperCase(), item2.trim())
					}
					logi.logInfo "hash map hm: "+hm
					return hm.sort()
					
						}
				   
				   public LinkedHashMap uv_statement_box_DB_5763_m(String str)
				   {
					   LinkedHashMap hm = new LinkedHashMap<String,String>()
					    List<String> keys=[
						   "Account Number",
						   "Statement Date",
						   "Statement Number",
						   "Invoice Number",
						   "Billing Period",
						   "Balance Due",
						   "Due Date",
						   "Acct New End bal1",
						   "Starting bal1",
						   "Acct New End bal2"]
					   logi.logInfo "inside the DB_Box 5793 1 : "+str
					   ConnectDB db = new ConnectDB()
					   String statementxpath = "//*[contains(text(),'Statement Number')]/../td"
					  //statement no from UI
					    String item1 = driver.findElement(By.xpath(statementxpath)).getText().toString();
					   logi.logInfo "statement no : "+item1
					   String acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null);
					   String query1 = "SELECT A.ACCT_NO,TO_CHAR(AI.BILL_DATE,'fmMM/fmDD/YYYY') AS STATEMENT_DATE,AST.STATEMENT_NO, AI.INVOICE_NO,"+
										" TO_CHAR(GL.FROM_DATE,'fmMM/fmDD/YYYY')||' - '||TO_CHAR(GL.TO_DATE,'fmMM/fmDD/YYYY') AS BILLING_PERIOD,"+
										" TO_CHAR(AST.BALANCE,'fm99990.90') AS BALANCE_DUE,TO_CHAR(AI.DUE_DATE,'fmMM/fmDD/YYYY') AS BALANCEDUEDATE,TO_CHAR((AST.NEW_CHARGES-AST.NEW_PAYMENTS),'fm99990.90') AS ACCT_NEW_END_BAL_1,"+
										" TO_CHAR(AST.BALANCE_FORWARD,'fm99990.90') AS STARTING_BAL_1,TO_CHAR(AST.BALANCE,'fm99990.90') AS ACCT_NEW_END_BAL_2"+
										" FROM ARIACORE.ACCT A"+
										" JOIN ARIACORE.ACCT_STATEMENT AST"+
										" ON A.ACCT_NO = AST.ACCT_NO"+
										" AND A.CLIENT_NO     = AST.CLIENT_NO"+
										" JOIN ARIACORE.ALL_INVOICES AI"+ 
										" ON A.ACCT_NO = AI.ACCT_NO"+
										" AND AI.INVOICE_NO = AST.INVOICE_NO"+
										" AND A.CLIENT_NO = AI.CLIENT_NO"+
										" JOIN ARIACORE.GL GL ON GL.INVOICE_NO = AI.INVOICE_NO"+
										" AND GL.CLIENT_NO = A.CLIENT_NO"+
										" WHERE A.ACCT_NO     = "+acct_no+
										" AND AST.STATEMENT_NO = "+item1
					   ResultSet resultSet = db.executePlaQuery(query1)
					   ResultSetMetaData md = resultSet.getMetaData()
					   int columns = md.getColumnCount();
					   int loops=1
					   while (resultSet.next())
					   {
					   
							   for(int u=1; u<=columns; ++u)
							 {
								   		logi.logInfo "u values "+u+" u-1 value "+u-1
										 hm.put(keys.get(u-1).toUpperCase(),resultSet.getObject(u));
							 }
							 
							
					   }
					   //tentative
						hm.put("STARTING BAL2","4.00")				
						return hm.sort()
				   }
				
				   public LinkedHashMap uv_billingpayment_addr_5793(String tcid)
				   {
						  logi.logInfo("Calling uv_billingpayment_addr_5793")
						  LinkedHashMap hm = new LinkedHashMap<String,String>()
						  String billingaddr = "//*[contains(text(),'Remit Payment To:')]/.."
						  logi.logInfo("value of text varname" + billingaddr)
						  //List<WebElement> items = driver.findElements(By.xpath("//table[@class='inv']//*[contains(text(),'Customer Account')][1]/.."))
						  String items=driver.findElement(By.xpath(billingaddr)).getText().toString().replaceAll("\\n", " ").trim()
						  logi.logInfo("items from driver "+items)
						 String item1 = items.split("Bill To:")[0]
						 logi.logInfo "item1 : "+item1
						 String addr1 = item1.split("Remit Payment To:")[1]
						 logi.logInfo "addr1 : "+addr1.trim()
						 String item2 = items.split("Bill To:")[1]
						 logi.logInfo "item2 : "+item2.trim()
						  hm.put("Remit Payment To",addr1.trim())
						  hm.put("Bill To",item2.trim())
						  return hm
				   }
				   public LinkedHashMap uv_billingpayment_addrDB_5793(String str)
				   {
					   logi.logInfo "inside uv_billingpayment_addrDB_5793 ..... 9876767 "
					   LinkedHashMap hm = new LinkedHashMap<String,String>()
					   ConnectDB db = new ConnectDB()
					   def client_no=Constant.mycontext.expand('${Properties#Client_No}')
					   String plan_instance_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null);
					   logi.logInfo "got plan instance no "+plan_instance_no
					   String query1 = "SELECT CLIENT_NAME||' 600 Reed Rd. Broomall, PA 19008 '||CSR_PHONE AS REMIT_TO FROM ARIACORE.CLIENT WHERE CLIENT_NO = "+client_no
					   String remit = db.executeQueryP2(query1).toString().trim()
					   logi.logInfo "got remit "+remit
					   hm.put("Remit Payment To",remit)
					   String query2 = "SELECT AA.ADDRESS1||' '||AA.CITY||', '||AA.STATE||' '||AA.ZIP AS BILL_TO"+
										" FROM ARIACORE.PLAN_INSTANCE_MASTER PIM"+
										" JOIN ARIACORE.ACCT_BILLING_GROUP ABG"+
										" ON ABG.BILLING_GROUP_NO    = PIM.BILLING_GROUP_NO"+
										" AND ABG.CLIENT_NO          = PIM.CLIENT_NO"+
										" JOIN ARIACORE.ACCT_ADDRESS AA"+
										" ON AA.ADDRESS_SEQ = ABG.STATEMENT_ADDRESS_SEQ"+
										" AND AA.CLIENT_NO = ABG.CLIENT_NO"+
										" WHERE PIM.PLAN_INSTANCE_NO ="+plan_instance_no
					   String bill = db.executeQueryP2(query2).toString().trim()
					   logi.logInfo "got bill "+bill
					   hm.put("Bill To",bill)
					   return hm
				   }
				   
				   
				   public  LinkedHashMap uv_getBillingAddressDB_m(String s)
				   {
						  logi.logInfo("md_getUsageHistory_m")
						  ConnectDB db = new ConnectDB()
						  int j=0
						  LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
						  LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
						  def client_no=Constant.mycontext.expand('${Properties#Client_No}')
						  def acct_no = im.md_GET_ACCT_OR_INSTANCE_NO_m(s,null)
						  logi.logInfo("acct_no" +acct_no )
						  String query ="SELECT ad.COMPANY_NAME,'ATTN: '||ad.FIRST_NAME||ad.LAST_NAME as test2,ad.ADDRESS1||' '||ad.ADDRESS2 as test1,ad.CITY||', '||ad.STATE||', '||ad.ZIP as test3,'Emailed to: '||ad.email as test4 FROM ariacore.acct_address ad JOIN ariacore.billing_info bi ON ad.acct_no     =bi.acct_no AND ad.ADDRESS_SEQ=bi.BILL_ADDRESS_SEQ WHERE ad.acct_no =" + acct_no
						  ResultSet rs4=db.executePlaQuery(query)
						  ResultSetMetaData md4 = rs4.getMetaData();
						  int columns4 = md4.getColumnCount();
						  while (rs4.next())
						  {
								 for(int l=1; l<=columns4; l++)
								 {
									   if((rs4.getObject(l))!=null)
									   {
											  
													 row.put("Billing_Address_Line"+l,(rs4.getObject(l)));
											  
									   
									   }
								 }
						  }
						  return row
				   }
				   
				   public LinkedHashMap uv_mpitaxsummartrxn_245163_DB_m (String str )
				   {
	 
	   
						  logi.logInfo("Calling uv_mpitaxsummartrxn_245163_DB_m")
						  LinkedHashMap hashmp = new LinkedHashMap()
						  if(str.contains("&"))
						  str=str.replaceAll("&",",")
						  //Thread.sleep(1000)
						  logi.logInfo("str value "+str)
						  ConnectDB db = new ConnectDB()
						  String acct_no=null
						InputMethods im = new InputMethods()
						
				        if(str.contains("#"))
											  acct_no=im.md_GET_ACCT_OR_INSTANCE_NO2_m(str,null)
											  else
											  acct_no=im.md_GET_ACCT_OR_INSTANCE_NO_m(str,null)
											  logi.logInfo("Fetched acct_no"+acct_no)
						  String invoice_no = db.executeQueryP2("SELECT INVOICE_NO FROM ARIACORE.ACCT_STATEMENT WHERE ACCT_NO="+acct_no).toString();
						  logi.logInfo("Thrown Invoice No" + invoice_no)
						  String query="SELECT TO_CHAR(attd.create_date,'dd-mm-yy HH:MM:SS am') AS TaxSummaryMPITxnDt,  TO_CHAR(gld.Debit, '9999.99') AS TaxSummaryBaseAmount, to_char(a.TaxSummaryRate, '9999.99') AS TaxSummaryRate,case when aitd.TAX_METHOD ='1'then 'State Taxes'end as TaxSummaryTypeDesc,abbm.MASTER_PLAN_INSTANCE_NO AS TaxSummaryMPINo , abbm.MASTER_PLAN_INSTANCE_NO as TaxSummaryClientMPIId,attd.TAX_EVENT_NO as TaxSummaryMPITxnNo , attd.AMOUNT as TaxSummaryAmount FROM (SELECT CASE WHEN NVL((SUM(TAXED_SEQ_NUM)),0)=0 THEN ' ' ELSE TO_CHAR(NVL((SUM(TAXED_SEQ_NUM)),0))  END  AS TaxSummaryRate FROM ARIACORE.gl_tax_detail gtd WHERE invoice_no = "+invoice_no+")a, ariacore.ACCT_TRAN_TAX_DETAIL attd JOIN ariacore.gl_detail gld ON attd.client_no = gld.client_no JOIN ARIACORE.ACCT_BILLABILITY_BY_BILLDATE_M abbm  ON abbm.acct_no  = attd.ACCT_NO JOIN ariacore.ALL_INVOICE_TAX_DETAILS aitd ON aitd.invoice_no  = gld.invoice_no JOIN ariacore.gl_tax_detail gtd ON gtd.invoice_no = gld.invoice_no WHERE attd.ACCT_NO ="+acct_no+" and gld.invoice_no ="+invoice_no+" AND gld.seq_num  IN   (SELECT taxed_seq_num FROM ariacore.gl_tax_detail WHERE invoice_no ="+invoice_no+")"
						  LinkedHashMap<String,String> bg_detail=new LinkedHashMap<String,String>()
	 
						  bg_detail=db.executeQueryP2(query)
						  bg_detail.sort()
						  hashmp.put("Line Item1",bg_detail)
						  return hashmp
				   }
	 
				   /** UI invoice line items for MPITaxSummaryTxns 245163
					* @param s
					* @return values from UI with array 'keys'
					*/
				 public  LinkedHashMap uv_mpitaxsummartrxn_245163_m(String s)
					  {
						  logi.logInfo("Calling uv_mpitaxsummartrxn_245163_m"+s)
				 LinkedHashMap<String,String> alltrnx=new LinkedHashMap<String,String>()
				 int counter=0
				 int kcounter=0
				 List<String> keys=[
					   "TaxSummaryMPINo",
					   "TaxSummaryClientMPIId",
					   "TaxSummaryMPITxnNo",
					   "TaxSummaryMPITxnDt",
					   "TaxSummaryTypeInd",
					   "TaxSummaryTypeDesc",
					   "TaxSummaryRate",
					   "TaxSummaryBaseAmount",
					   "TaxSummaryAmount"

				 ]
					   List<WebElement> items = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'MPITaxSummaryTxns')]//../following-sibling::tr"));
					   logi.logInfo("uv_mpitaxsummartrxn_245163_m Items count : "+items.size().toString())
					   items.each  { item ->
							 logi.logInfo("Preparing Item Hash uv_mpitaxsummartrxn_245163_m")
							 counter=counter+1
							 LinkedHashMap hm = new LinkedHashMap<String,String>()
							 List<WebElement> tds = item.findElements(By.xpath(".//following-sibling::td"))
							 String sss = tds.size().toString()
							 logi.logInfo("uv_mpitaxsummartrxn_245163_m tds count : "+sss)
							 if(tds.size()<=0){logi.logInfo "no values entered here "+tds.size().toString();
								   counter = counter-1
							 }else{
							 int tdm = Integer.parseInt(sss)
							 logi.logInfo("uv_mpitaxsummartrxn_245163_m converted to integer : "+tdm)
							 for(int i=0;i<tdm;i++){
								   logi.logInfo "uv_mpitaxsummartrxn_245163_m inside the loop "+keys.get(i)
								   if(tds.get(i).getText().toString().equals("")){
										 logi.logInfo "uv_mpitaxsummartrxn_245163_m got an empty node at : "+i
								   }else{
								   logi.logInfo "uv_mpitaxsummartrxn_245163_m adding values at "+keys.get(i)+" with "+tds.get(i).getText()
								   hm.put(keys.get(i).toUpperCase(), tds.get(i).getText())
								   }
								   //kcounter=kcounter+1
							 }
							 
							 
							 /*
							 tds.each {titem ->
								   
								   logi.logInfo("uv_mpialltrnx_5793 td value "+titem.getText())
								   
										 logi.logInfo "BOSCO NO"
										 hm.put(keys.get(kcounter), titem.getText())
										 kcounter=kcounter+1
								   
							 }*/
							 //kcounter=0
							 hm=hm.sort()
							 alltrnx.put("Line Item"+counter,hm)
							 }
					   }
					   logi.logInfo("MPIAllTxnsLoop "+alltrnx.toString())
					   return alltrnx.sort()
					  }

 
}