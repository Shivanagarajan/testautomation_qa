package library.Constants

/**
* Class holding all the xPath constants of all the API's
*/
class ExPathAdminToolsApi
{


	   //API - get_service_details
	   public static final GET_SERVICE_ERRORCODE1 = "//wddxPacket[1]/data[1]/struct[1]/var[1]/number[1]"
	   public static final GET_SERVICE_ERRORMSG1 = "//wddxPacket[1]/data[1]/struct[1]/var[2]/string[1]"
	   public static final GET_SERVICE_SERVICE_NO1 ="//wddxPacket[1]/data[1]/struct[1]/var[3]/string[1]"

	   //API - get_plans
	   public static final GET_PLANS_ERRORCODE1 = "//wddxPacket[1]/data[1]/struct[1]/var[1]/number[1]"
	   public static final GET_PLANS_ERRORMSG1 = "//wddxPacket[1]/data[1]/struct[1]/var[2]/string[1]"
	   public static final GET_PLANS_PLAN_NO1 = "//wddxPacket[1]/data[1]/struct[1]/var[3]/array[1]/struct[1]/var[1]/number[1]"

	   //API - get_plan_service_details
	   public static final GET_PLANS_SERVICE_DETAILS_ERRORCODE1 = "//wddxPacket[1]/data[1]/struct[1]/var[1]/number[1]"
	   public static final GET_FVPLANS_SERVICE_DETAILS_ERRORMSG1 = "//wddxPacket[1]/data[1]/struct[1]/var[2]/string[1]"
	   public static final GET_PLANS_SERVICE_DETAILS_PLAN_NO1 = "//wddxPacket[1]/data[1]/struct[1]/var[3]/string[1]"
	   public static final GET_PLANS_SERVICE_DETAILS_SERVICE_NO1 = "//wddxPacket[1]/data[1]/struct[1]/var[5]/string[1]"

	   //API - get_plan_details
	   public static final GET_PLANS_DETAILS_ERRORCODE1 = "//wddxPacket[1]/data[1]/struct[1]/var[1]/number[1]"
	   public static final GET_PLANS_DETAILS_ERRORMSG1 = "//wddxPacket[1]/data[1]/struct[1]/var[2]/string[1]"
	   public static final GET_PLANS_DETAILS_PLAN_NO1 = "//wddxPacket[1]/data[1]/struct[1]/var[3]/string[1]"
	   public static final GET_PLANS_DETAILS_PLAN_NAME1 = "//wddxPacket[1]/data[1]/struct[1]/var[6]/string[1]"

	   //API - get_usage_types
	   public static final GET_USAGE_TYPES_ERRORCODE1 = "//wddxPacket[1]/data[1]/struct[1]/var[1]/number[1]"
	   public static final GET_USAGE_TYPES_ERRORMSG1 = "//wddxPacket[1]/data[1]/struct[1]/var[2]/string[1]"
	   public static final GET_USAGE_TYPES_USAGETYPENO1 = "//wddxPacket[1]/data[1]/struct[1]/var[3]/array[1]/struct[1]/var[1]/number[1]"

	   //API - create_new_plan
	   public static final CREATE_PLAN_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_PLAN_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_PLAN_PLAN_NO1 = "//*/*:plan_no[1]"

	   //API - create_inventory_item
	   public static final CREATE_INVENTORY_ITEM_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_INVENTORY_ITEM_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_INVENTORY_ITEM_ITEM_NO1 = "//*/*:item_no[1]"
	   public static final CREATE_INVENTORY_ITEM_NULL_ITEM = "//*/*:item_no/@null"

	   //API - update_inventory_item
	   public static final UPDATE_INVENTORY_ITEM_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_INVENTORY_ITEM_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_INVENTORY_ITEM_ITEM_NO1 = "//*/*:item_no[1]"

	   //API - create_coupon

	   public static final CREATE_COUPON_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_COUPON_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_COUPON_COUPON_CODE1 = "//*/*:coupon_cd[1]"

	   //API - update_coupon

	   public static final UPDATE_COUPON_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_COUPON_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_COUPON_COUPON_CODE1 = "//*/*:coupon_cd[1]"

	   //API - get_coupon_details
	   public static final GET_COUPON_DETAILS_CREDIT_TEMPLATE_NAME = "//*/*/*/*:credit_template_name"
	   public static final GET_COUPON_DETAILS_CREDIT_TEMPLATE_ID = "//*/*/*/*:credit_template_id"
	   public static final GET_COUPON_DETAILS_ELIGIBLE_PLAN_NO = "//*/*/*/*:eligible_plan_no"
	   public static final GET_COUPON_DETAILS_ELIGIBLE_SERVICE_NO = "//*/*/*/*:eligible_service_no"
	   public static final GET_COUPON_DETAILS_DISCOUNT_AMOUNT = "//*/*/*/*:discount_amt"
	   public static final GET_COUPON_DETAILS_PERCENTAGE_PLAN = "//*/*/*/*:percentage_plan_no"
	   public static final GET_COUPON_DETAILS_PERCENTAGE_SERVICE = "//*/*/*/*:percentage_service_no"
	   public static final GET_COUPON_DETAILS_ALTERNATE_SERVICE = "//*/*/*/*:alt_service_no"
	   public static final GET_COUPON_DETAILS_DISCOUNT_TYPE = "//*/*/*/*:discount_type"
	   public static final GET_COUPON_DETAILS_CREDIT_INTERVAL = "//*/*/*/*:time_between_credits"
	   public static final GET_COUPON_DETAILS_NO_OF_CREDITS = "/*/*/*/*:no_of_credits"
	   
	   public static final GET_COUPON_DETAILS_COUPON_CD = "//*/*:coupon_cd"
	   public static final GET_COUPON_DETAILS_COUPON_DESC = "//*/*:coupon_desc"
	   public static final GET_COUPON_DETAILS_COUPON_MSG = "//*/*:coupon_msg"
	   public static final GET_COUPON_DETAILS_COUPON_START_DATE = "//*/*:start_date"
	   public static final GET_COUPON_DETAILS_COUPON_END_DATE = "//*/*:end_date"
	   public static final GET_COUPON_DETAILS_COUPON_NO_OF_USES = "//*/*:no_of_uses"
	   public static final GET_COUPON_DETAILS_COUPON_STATUS = "//*/*:status_ind"
	   
	   public static final GET_COUPON_TEMP_ELIGIBLE_SERVICE_TYPE = "//*/*:credit_template/*/*:eligible_service_types/*:e"

	   //API - get_recurring_credit_template_details
	   public static final GET_RECUR_TEMP_DETAILS_CREDIT_TEMPLATE_NAME = "//*/*:credit_template_name"
	   public static final GET_RECUR_TEMP_CLIENT_CREDIT_TEMPLATE_ID = "//*/*:client_credit_template_id"
	   public static final GET_RECUR_TEMP_CREDIT_TEMPLATE_ID = "//*/*:credit_template_id"
	   public static final GET_RECUR_TEMP_ELIGIBLE_PLAN_NO = "//*/*:eligible_plan_no"
	   public static final GET_RECUR_TEMP_ELIGIBLE_SERVICE_NO=  "//*/*:eligible_service_no"
	   public static final GET_RECUR_TEMP_DISCOUNT_AMOUNT = "//*/*:discount_amt"
	   public static final GET_RECUR_TEMP_ALTERNATE_SERVICE = "//*/*:alt_service_no"
	   public static final GET_RECUR_TEMP_DISCOUNT_TYPE = "//*/*:discount_type"
	   public static final GET_RECUR_TEMP_CREDIT_INTERVAL = "//*/*:time_between_credits"
	   public static final GET_RECUR_TEMP_NO_OF_CREDITS = "//*/*:no_of_credits"
	   public static final GET_RECUR_TEMP_COUPON_CD = "//*/*/*/*:coupon_cd"
	   public static final GET_RECUR_TEMP_COUPON_DESC = "//*/*/*/*:coupon_desc"
	   public static final GET_RECUR_TEMP_COUPON_MSG = "//*/*/*/*:coupon_msg"
	   public static final GET_RECUR_TEMP_MAX_USES = "//*/*/*/*:no_of_uses"
	   public static final GET_RECUR_TEMP_ELIGIBLE_SERVICE_TYPE = "//*/*:eligible_service_types/*:e"
	   public static final GET_RECUR_TEMPLATE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final GET_RECUR_TEMPLATE_ERRORMSG1 = "//*/*:error_msg[1]"

	   //API - create_credit_template
	   public static final CREATE_CREDIT_TEMPLATE_NUMBER = "//*/*:credit_template_no"
	   public static final CREATE_CREDIT_TEMPLATE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_CREDIT_TEMPLATE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_CREDIT_TEMPLATE_NO1 ="//*/*:credit_template_no[1]"
	   
	   //API - get_recurring_credit_templates
	   public static final GET_RECURRING_CREDIT_TEMPLATES_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final GET_RECURRING_CREDIT_TEMPLATES_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - create_service
	   public static final CREATE_SERVICE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_SERVICE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_SERVICE_SERVICE_NO1 ="//*/*:service_no[1]"
	   
	   //API - create_promotion
	   public static final CREATE_PROMOTION_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_PROMOTIONERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_PROMOTION_CD ="//*/*:promo_cd[1]"

	   //API - create_promo_plan_set
	   public static final CREATE_PROMO_PLAN_SET_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_PROMO_PLAN_SET_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_PROMO_PLAN_SET_NO1 ="//*/*:promo_plan_set_no[1]" 
	   
	   //API - create_discount_rule
	   public static final CREATE_RULE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_RULE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_RULE_NO1 ="//*/*:rule_no[1]"
	   
	   //API - create_discount_bundle
	   public static final CREATE_BUNDLE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_BUNDLE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_BUNDLE_NO1 ="//*/*:bundle_no[1]"
	   
	   //API - coa
	   public static final ADD_COA_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final ADD_COA_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final ADD_COA_NO1 ="//*/*:coa[1]/*:coa_id[1]"
	   
	   //API - create_supp_field
	   public static final ACCT_FIELD_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final ACCT_FIELD_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final ACCT_FIELD_NO1 ="//*/*:field_name[1]"
	   
	   //API - delete_plans
	   public static final DELETE_PLANS_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final DELETE_PLANS_ERRORMSG = "//*/*:error_msg[1]"
	   public static final DELETE_PLANS_PLAN_NO = "//*/*:plans[1]/*:e[1]/*:plan_no[1]"
	   
	   //API - delete_coupons
	   public static final DELETE_COUPONS_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final DELETE_COUPONS_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - delete_rules
	   public static final DELETE_RULE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final DELETE_RULE_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - delete_bundles
	   public static final DELETE_BUNDLE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final DELETE_BUNDLE_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - update_credit_template
	   public static final UPDATE_CREDIT_TEMPLATE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_CREDIT_TEMPLATE_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - update_promo_plan_set
	   public static final UPDATE_PROMO_PLAN_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_PROMO_PLAN_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_PROMO_PLAN_SET_NO1 = "//*/*:promo_plan_set_no[1]"
	   
	   //API - edit_coa
	   public static final EDIT_COA_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final EDIT_COA_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final EDIT_COA_ID1 ="//*/*:coa[1]/*:coa_id[1]"
	   public static final EDIT_COA_CODE1 ="//*/*:coa[1]/*:coa_code[1]"
	   public static final EDIT_COA_DESC1 ="//*/*:coa[1]/*:coa_description[1]"
	   
	   //API - update_promotion	   
	   public static final UPDATE_PROMO_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_PROMO_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_PROMO_CODE1 = "//*/*:promo_cd[1]"
	   
	   //API - update_service
	   public static final UPDATE_SERVICE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_SERVICE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_SERVICE_NO1 = "//*/*:service_no[1]"
	   
	   //API - update_supp_field
	   public static final UPDATE_SUPP_FIELD_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_SUPP_FIELD_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_SUPP_FIELD_NAME1 = "//*/*:field_name[1]"
	   
	   //API - create_plan_group
	   public static final CREATE_PLAN_GRP_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_PLAN_GRP_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_PLAN_GRP_GROUPNO1 = "//*/*:group_no[1]"
	   
	   //API - update_plan_group
	   public static final UPDATE_PLAN_GRP_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_PLAN_GRP_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_PLAN_GRP_GROUPNO1 = "//*/*:group_no[1]"
	   
	   //API - delete_plan_group
	   public static final DELETE_PLAN_GRP_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final DELETE_PLAN_GRP_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final DELETE_PLAN_GRP_GROUPNO = "//*/*:plan_groups[1]/*:e[1]/*:group_no[1]"
	   
	   //API - edit_plan
	   public static final EDIT_PLAN_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final EDIT_PLAN_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final EDIT_PLAN_NO1 = "//*/*:plan_no[1]"
	   
	   
	   //API - set_company_profile and get_company_profile
	   public static final COMPANY_PROFILE_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final COMPANY_PROFILE_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final COMPANY_PROFILE_ADDRESS1 = "//*/*:client[1]/*:address1[1]"
	   public static final COMPANY_PROFILE_ADDRESS2 = "//*/*:client[1]/*:address2[1]"
	   public static final COMPANY_PROFILE_BILLING_ADDRESS1 = "//*/*:client[1]/*:billing_address1[1]"
	   public static final COMPANY_PROFILE_BILLING_ADDRESS2 = "//*/*:client[1]/*:billing_address2[1]"
	   public static final COMPANY_PROFILE_BILLING_CITY = "//*/*:client[1]/*:billing_city[1]"
	   public static final COMPANY_PROFILE_BILLING_CONTACT = "//*/*:client[1]/*:billing_contact[1]"
	   public static final COMPANY_PROFILE_BILLING_COUNTRY = "//*/*:client[1]/*:billing_country[1]"
	   public static final COMPANY_PROFILE_BILLING_EMAIL = "//*/*:client[1]/*:billing_email[1]"
	   public static final COMPANY_PROFILE_BILLING_LOCALITY = "//*/*:client[1]/*:billing_locality[1]"
	   public static final COMPANY_PROFILE_BILLING_PHONE = "//*/*:client[1]/*:billing_phone[1]"
	   public static final COMPANY_PROFILE_BILLING_STATE = "//*/*:client[1]/*:billing_state[1]"
	   public static final COMPANY_PROFILE_BILLING_ZIP = "//*/*:client[1]/*:billing_zip[1]"
	   public static final COMPANY_PROFILE_CITY = "//*/*:client[1]/*:city[1]"
	   public static final COMPANY_PROFILE_CLIENT_NAME = "//*/*:client[1]/*:client_name[1]"
	   public static final COMPANY_PROFILE_CONTACT = "//*/*:client[1]/*:contact[1]"
	   public static final COMPANY_PROFILE_CONTACT_ADDRESS1 = "//*/*:client[1]/*:contact_address1[1]"
	   public static final COMPANY_PROFILE_CONTACT_ADDRESS2 = "//*/*:client[1]/*:contact_address2[1]"
	   public static final COMPANY_PROFILE_CONTACT_CITY = "//*/*:client[1]/*:contact_city[1]"
	   public static final COMPANY_PROFILE_CONTACT_COUNTRY = "//*/*:client[1]/*:contact_country[1]"
	   public static final COMPANY_PROFILE_CONTACT_EMAIL = "//*/*:client[1]/*:contact_email[1]"
	   public static final COMPANY_PROFILE_CONTACT_LOCALITY = "//*/*:client[1]/*:contact_locality[1]"
	   public static final COMPANY_PROFILE_CONTACT_PHONE = "//*/*:client[1]/*:contact_phone[1]"
	   public static final COMPANY_PROFILE_CONTACT_STATE = "//*/*:client[1]/*:contact_state[1]"
	   public static final COMPANY_PROFILE_CONTACT_ZIP = "//*/*:client[1]/*:contact_zip[1]"
	   public static final COMPANY_PROFILE_COUNTRY = "//*/*:client[1]/*:country[1]"
	   public static final COMPANY_PROFILE_DOMAIN = "//*/*:client[1]/*:domain[1]"
	   public static final COMPANY_PROFILE_LOCALITY = "//*/*:client[1]/*:locality[1]"
	   public static final COMPANY_PROFILE_PHONE = "//*/*:client[1]/*:phone[1]"
	   public static final COMPANY_PROFILE_POSTAL_CODE = "//*/*:client[1]/*:postal_code[1]"
	   public static final COMPANY_PROFILE_STATE_PROV = "//*/*:client[1]/*:state_prov[1]"
	   
	   //API - get_service_details
	   public static final GET_SERVICE_DETAILS_ERROR_CODE1 = "//*/*:error_code[1]"
	   public static final GET_SERVICE_DETAILS_ERROR_MSG1 = "//*/*:error_msg[1]"
	   public static final GET_SERVICE_DETAILS_CLIENT_ID = "//*/*:client_service_id[1]"
	   public static final GET_SERVICE_DETAILS_SERVICE_NO = "//*/*:service_no[1]"
	   
	   
	   //API - get_plan_groups
	   public static final GET_PLAN_GROUPS_ERROR_CODE1 = "//*/*:error_code[1]"
	   public static final GET_PLAN_GROUP_ERROR_MSG1 = "//*/*:error_msg[1]"
	   
	   //API - get_plan_group_details
	   public static final GET_PLAN_GROUP_DETAILS_ERROR_CODE1 = "//*/*:error_code[1]"
	   public static final GET_PLAN_GROUP_DETAILS_ERROR_MSG1 = "//*/*:error_msg[1]"
	   public static final GET_PLAN_GROUP_DETAILS_GROUP_NO1 = "//*/*:group_no[1]"
	   
	   //API - create_usage_type
	   public static final CREATE_USAGE_TYPE_ERROR_CODE1 = "//*/*:error_code[1]"
	   public static final CREATE_USAGE_TYPE_ERROR_MSG1 = "//*/*:error_msg[1]"
	   public static final CREATE_USAGE_TYPE_NO1 = "//*/*:usage_type_no[1]"
	   
	   
	   //API - create_supp_obj_field
	   public static final PROD_FIELD_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final PROD_FIELD_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final PROD_FIELD_NO1 ="//*/*:field_no[1]"
	   
	   //API - update_supp_obj_field
	   public static final UPDATE_PROD_FIELD_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final UPDATE_PROD_FIELD_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final UPDATE_PROD_FIELD_NO1 ="//*/*:field_no[1]"
	   
	   //API - create_acct_complete
	   public static final CREATE_ACCT_COMPLETE_ACCT_NO = "//*/*:acct_no[1]"
	   public static final CREATE_ACCT_COMPLETE_ERROR_CODE = "//*/*:error_code[1]"
	   public static final CREATE_ACCT_COMPLETE_ERROR_MSG = "//*/*:error_msg[1]"
	   public static final CREATE_ACCT_COMPLETE_COUPON = "//*/*:e[6]/*:invoice_line_amount"
	   
	   //API - create_new_plan_m API
	   public static final CREATE_PLAN_M_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final CREATE_PLAN_M_ERRORMSG1 = "//*/*:error_msg[1]"
	   
	   //API - edit_plan_m API
	   public static final EDIT_PLAN_M_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final EDIT_PLAN_M_ERRORMSG1 = "//*/*:error_msg[1]"
	   

       //API - get_supp_obj_fields_m
	   public static final GET_SUPP_OBJ_FIELDS_ERRORCODE1 = "//*/*:error_code[1]"
	   public static final GET_SUPP_OBJ_FIELDS_ERRORMSG1 = "//*/*:error_msg[1]"
	   public static final GET_SUPP_OBJ_FIELDS_DATATYPE ="//*/*:datatype[1]"
	   public static final GET_SUPP_OBJ_FIELDS_INPUTTYPE ="//*/*:form_input_type[1]"
	   public static final GET_SUPP_OBJ_FIELDS_MAXSEL ="//*/*:max_no_sel[1]"
	   public static final GET_SUPP_OBJ_FIELDS_MINSEL ="//*/*:min_no_sel[1]"

  
}
