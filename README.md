# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Here is where we tested the Jenkins and BitBucket Integration

### What are the commands used in the Jenkins and thier syntax ###

Proceed with below steps in Execute Windows Batch Command

* Runner\bin\testrunner.bat -s<Suite Name> -I <Project File Name>.xml
* java -jar Status_Mailer.jar "<Suite Name>" "ended" "<Project Name>" "<Machine name>"
* java -jar Cleaner.jar



### Best Practices ###

* Please check that MailIds.xls file has all the receipent mail IDs
* The commands mentioned above are to be in seperate # Build # step
* For Scheduling keyword Tips refer http://stackoverflow.com/questions/12472645/how-to-schedule-jobs-in-jenkins

### Please note ###

* Before configuring please make sure to have sufficent space on disk
* Try to Remove the Reports periodically from this repository to reduce clone time